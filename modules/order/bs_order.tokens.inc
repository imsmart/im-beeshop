<?php

/**
 * @file
 * Builds placeholder replacement tokens for node-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;
use Drupal\bs_order\Entity\OrderInterface;

/**
 * Implements hook_token_info().
 */
function bs_order_token_info() {
  $type = [
    'name' => t("BeeShop order"),
    'description' => t("Tokens related to BeeShop orders."),
    'needs-data' => 'bs_order',
    'module' => 'token',
  ];

  $bs_order['full_number'] = [
    'name' => t("Full number"),
    'description' => t("The order full number."),
  ];

  $bs_order['order_total'] = [
    'name' => t("Total amount"),
    'description' => t("The order total amount."),
    'type' => 'bs_price',
  ];

  $bs_order['mail'] = [
    'name' => t("Order contact email"),
    'description' => t("The order contact email."),
    'type' => 'email',
  ];

  $bs_order['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the node's edit page."),
  ];

  $bs_order['urls'] = [
    'name' => t("Url by link rel"),
    'description' => t("URLs"),
    'dynamic' => TRUE,
  ];

  // Chained tokens for nodes.
  $bs_order['created'] = [
    'name' => t("Date created"),
    'type' => 'date',
  ];

    return [
      'types' => ['bs_order' => $type],
      'tokens' => ['bs_order' => $bs_order],
    ];
}

/**
 * Implements hook_tokens().
 */
function bs_order_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }
  $replacements = [];

  if ($type == 'bs_order' && !empty($data['bs_order'])) {
    /** @var OrderInterface $bs_order */
    $bs_order = $data['bs_order'];

    if ($link_rels = \Drupal::token()->findWithPrefix($tokens, 'urls')) {
      foreach ($link_rels as $rel => $original) {
        if ($bs_order->hasLinkTemplate($rel)) {
          $replacements[$original] = $bs_order->toUrl($rel)->setOption('absolute', TRUE)->toString();
        }
      }
    }

    foreach ($tokens as $name => $original) {

      switch ($name) {
        case 'full_number':
          $replacements[$original] = $bs_order->getFullNumber();
          break;

        case 'order_total_formated':
          $replacements[$original] = preg_replace('#\.0+$#', '', $bs_order->total_amount->first()->value);
          break;

        case 'order_total':
          $replacements[$original] = preg_replace('#\.0+$#', '', $bs_order->total_amount->first()->toPrice()->__toString());
          break;

        case 'short_url':
          $order_full_url = $bs_order->toUrl('brief-info', ['absolute' => 1])->toString();
          $short_url = '--error--';
          try {
            $short_url = helper_module2_get_short_url($order_full_url);
          }
          catch (\Exception $e) {
            watchdog_exception('order_short_url', $e);
          }
          $replacements[$original] = $short_url;
          break;

        case 'urls':
          break;

        case 'mail':
          $replacements[$original] = $bs_order->getEmail();
          break;

        case 'order_items_table':

          $element = [
          '#prefix' => '<div class="order-items">',
          '#suffix' => '</div>',
          '#type' => 'view',
          '#name' => 'order_items',
          '#display_id' => 'block_order_items_table',
          '#arguments' => [$bs_order->id()],
          '#embed' => TRUE,
          ];

          $replacements[$original] = \Drupal::service('renderer')->render($element);
          break;

        case 'order_items_for_mail':
          $element = [
          '#prefix' => '<div class="order-items">',
          '#suffix' => '</div>',
          '#type' => 'view',
          '#name' => 'order_items',
          '#display_id' => 'block_order_items_mail',
          '#arguments' => [$bs_order->id()],
          '#embed' => TRUE,
          ];

          $replacements[$original] = \Drupal::service('renderer')->render($element);
          break;
  // Simple key values on the node.
  //         case 'nid':
  //           $replacements[$original] = $node->id();
  //           break;

  //         case 'vid':
  //           $replacements[$original] = $node->getRevisionId();
  //           break;

  //         case 'type':
  //           $replacements[$original] = $node->getType();
  //           break;

  //         case 'type-name':
  //           $type_name = node_get_type_label($node);
  //           $replacements[$original] = $type_name;
  //           break;

  //         case 'title':
  //           $replacements[$original] = $node->getTitle();
  //           break;

  //         case 'body':
  //         case 'summary':
  //           $translation = \Drupal::entityManager()->getTranslationFromContext($node, $langcode, ['operation' => 'node_tokens']);
  //           if ($translation->hasField('body') && ($items = $translation->get('body')) && !$items->isEmpty()) {
  //             $item = $items[0];
  //             // If the summary was requested and is not empty, use it.
  //             if ($name == 'summary' && !empty($item->summary)) {
  //               $output = $item->summary_processed;
  //             }
  //             // Attempt to provide a suitable version of the 'body' field.
  //             else {
  //               $output = $item->processed;
  //               // A summary was requested.
  //               if ($name == 'summary') {
  //                 // Generate an optionally trimmed summary of the body field.

  //                 // Get the 'trim_length' size used for the 'teaser' mode, if
  //                 // present, or use the default trim_length size.
  //                 $display_options = entity_get_display('node', $node->getType(), 'teaser')->getComponent('body');
  //                 if (isset($display_options['settings']['trim_length'])) {
  //                   $length = $display_options['settings']['trim_length'];
  //                 }
  //                 else {
  //                   $settings = \Drupal::service('plugin.manager.field.formatter')->getDefaultSettings('text_summary_or_trimmed');
  //                   $length = $settings['trim_length'];
  //                 }

  //                 $output = text_summary($output, $item->format, $length);
  //               }
  //             }
  //             // "processed" returns a \Drupal\Component\Render\MarkupInterface
  //             // via check_markup().
  //             $replacements[$original] = $output;
  //           }
  //           break;

  //         case 'langcode':
  //           $replacements[$original] = $node->language()->getId();
  //           break;

  //         case 'url':
  //           $replacements[$original] = $node->url('canonical', $url_options);
  //           break;

    case 'edit-url':
      $replacements[$original] = $bs_order->url('edit-form', $url_options);
      break;

      // Default values for the chained tokens handled below.
      //         case 'author':
      //           $account = $node->getOwner() ? $node->getOwner() : User::load(0);
      //           $bubbleable_metadata->addCacheableDependency($account);
      //           $replacements[$original] = $account->label();
      //           break;

      case 'created':
        $date_format = DateFormat::load('medium');
        $bubbleable_metadata->addCacheableDependency($date_format);
        $replacements[$original] = format_date($bs_order->getCreatedTime(), 'medium', '', NULL, $langcode);
        break;

        //         case 'changed':
        //           $date_format = DateFormat::load('medium');
        //           $bubbleable_metadata->addCacheableDependency($date_format);
        //           $replacements[$original] = format_date($node->getChangedTime(), 'medium', '', NULL, $langcode);
        //           break;
  }
  }

  //     if ($author_tokens = $token_service->findWithPrefix($tokens, 'author')) {
  //       $replacements += $token_service->generate('user', $author_tokens, ['user' => $node->getOwner()], $options, $bubbleable_metadata);
  //     }

  //     if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
  //       $replacements += $token_service->generate('date', $created_tokens, ['date' => $node->getCreatedTime()], $options, $bubbleable_metadata);
  //     }

  //     if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
  //       $replacements += $token_service->generate('date', $changed_tokens, ['date' => $node->getChangedTime()], $options, $bubbleable_metadata);
  //     }
  }

  return $replacements;
}


