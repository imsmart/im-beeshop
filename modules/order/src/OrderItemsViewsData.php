<?php
namespace Drupal\bs_order;

use Drupal\views\EntityViewsData;

class OrderItemsViewsData extends EntityViewsData {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bs_order_items']['edit_quantity']['field'] = [
      'title' => t('Quantity edit field'),
      'help' => t('Adds a text field for editing the quantity.'),
      'id' => 'bs_order_item_edit_quantity',
    ];

    $data['bs_order_items']['edit_price']['field'] = [
      'title' => t('Order item price edit field'),
      'help' => t('Adds a text field for editing the order item price.'),
      'id' => 'bs_order_item_price_edit',
    ];

    $data['bs_order_items']['table']['base']['query_id'] = 'bs_order_items_views_query';

    $data['bs_order_items']['bs_order_items_bulk_form'] = [
      'title' => $this->t('Order items operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple order items.'),
      'field' => [
        'id' => 'bs_order_items_bulk_form',
      ],
    ];

    return $data;
  }
}

