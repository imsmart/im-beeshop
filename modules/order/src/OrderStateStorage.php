<?php
namespace Drupal\bs_order;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\beeshop\HierarchicalEntityStorageTrait;
use Drupal\Component\Utility\Html;

class OrderStateStorage extends ConfigEntityStorage implements OrderStateStorageInterface {

  use HierarchicalEntityStorageTrait;

  public function getStatesOptionsList($exclude_ids = []) {
    $options = [];

    $excluded = [];

    if ($states = $this->loadTree()) {
      foreach ($states as $state) {
        if (in_array($state->id(), $exclude_ids) ||
          ($state->getParent() && (in_array($state->getParent(), $exclude_ids) || in_array($state->getParent(), $excluded)))){

             $excluded[] = $state->id();
             continue;
        }
        $options[$state->id()] = str_repeat('-', $state->depth) . ' ' . Html::escape($state->label());
      }
    }

    return $options;
  }

}

