<?php
namespace Drupal\bs_order\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Entity Reference Selection with distinguishing field.
 *
 * @EntityReferenceSelection(
 *   id = "default:bs_order_state",
 *   label = @Translation("Order state"),
 *   group = "default",
 * )
 */
class OrderStatesSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {

    if ($match || $limit) {
      return parent::getReferenceableEntities($match, $match_operator, $limit);
    }

    $options = [];

    if ($states = $this->entityManager->getStorage('bs_order_state')->loadTree()) {
      foreach ($states as $state) {
        $options['bs_order_state'][$state->id()] = str_repeat('-', $state->depth) . ' ' . Html::escape($state->label());
      }
    }

    return $options;
  }

}

