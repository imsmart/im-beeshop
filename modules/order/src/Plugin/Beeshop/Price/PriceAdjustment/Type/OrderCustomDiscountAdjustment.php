<?php
namespace Drupal\bs_order\Plugin\Beeshop\Price\PriceAdjustment\Type;

use Drupal\bs_price\PriceAdjustmentTypeInterface;
use Drupal\bs_price\PriceAdjustmentTypeBase;

/**
 *
 * @PriceAdjustmentType(
 *   id="bs_order_custom_discount",
 *   label = @Translation("Custom discount"),
 *   weight = 20,
 *   can_be_included = false,
 * )
 */
class OrderCustomDiscountAdjustment extends PriceAdjustmentTypeBase implements PriceAdjustmentTypeInterface {

}

