<?php

namespace Drupal\bs_order\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Action\Plugin\Action\EntityActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_cart\CartInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\bs_order\Entity\OrderItemInterface;
use Drupal\beeshop\IsApplicableToEntity;
use Drupal\Core\Entity\EntityInterface;

/**
 * Delete order item.
 *
 * @Action(
 *   id = "bs_order_item_add_to_cart_action",
 *   label = @Translation("Add order item to cart"),
 *   type = "bs_order_item"
 * )
 */
class AddToCart extends EntityActionBase implements IsApplicableToEntity {

  /**
   *
   * @var CartInterface
   */
  protected $cart;

  /**
   * Constructs a new DeleteAction object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CartInterface $current_user_cart) {
    $this->cart = $current_user_cart;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user_cart')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {

    $items = &drupal_static('Action:AddToCart:items', []);

    /**
     * @var OrderItemInterface $object
     */
    if ($product = $object->getProduct()) {
      if ($product->canBePurchased()) {
        $items[] = $this->cart->addProduct($product);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }


  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityInterface $entity) {
    /**
     * @var OrderItemInterface $entity
     */

    if ($product = $entity->getProduct()) {
      return $product->isAvailable();
    }

    return FALSE;

  }

}
