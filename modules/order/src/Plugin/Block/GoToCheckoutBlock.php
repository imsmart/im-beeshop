<?php
namespace Drupal\bs_order\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "bs_ordergo_to_checkout_block",
 *   admin_label = @Translation("Go to checkout block"),
 * )
 */
class GoToCheckoutBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $cart_provider = \Drupal::service('bs_cart.cart_provider');
    $cart = $cart_provider->getCart();

    $cart_provider = \Drupal::service('bs_cart.cart_provider');
    $cart = $cart_provider->getCart();

    if ($cart->isEmpty()) {
      return;
    }

    $build = [];

    if ($cart_adjustments = $cart->getDataValue('adjustments')) {
      if (!empty($cart_adjustments['pre_order_products_discount'])) {
        $build['cart_has_discounted_products_info'] = [
          '#markup' => '<div class="cart-has-discounts-info">
  <div class="total-to-pay-notice">В вашем заказе есть товары по предзаказу. На них предоставляется скидка -10%</div>
</div>',
          '#weight' => 10,
        ];
      }
    }

    $cart_total = [
      '#prefix' => '<div class="cart-total">',
      '#suffix' => '</div>',
      '#theme' => 'bs_price_item',
      '#price_item' => $cart->getCartTotalAmount(),
    ];

    $build['to_pay_info'] = [
      '#markup' => '
<div class="top-pay-info">
  <div class="info-label">' . $this->t('Total to pay') . '</div>
  <div class="to-pay-amount">' . render($cart_total) . '</div>
  <div class="total-to-pay-notice">' . $this->t('Without shipping') . '!</div>
</div>
',
      '#weight' => 20
    ];

    $form = \Drupal::formBuilder()->getForm('\Drupal\bs_order\Form\ToCheckoutForm');

    $build['form'] = $form;

    $build['form']['#weight'] = 30;

    $current_shop = \Drupal::service('bs_shop.default_shop_resolver')->resolve();

    $payment_logos = $current_shop->id() == 1 ?
                    '/themes/anker/img/payments-logos.svg' :
                    '/themes/anker/img/PayPal_2014_logo.png';

    $build['footer'] = [
      '#markup' => '
<div class="to-checkout-block-footer">
  <div class="payment-label">' . $this->t('You can pay with'). ':</div>
<div class=""><img src="' . $payment_logos . '"></div>
</div>
',
      '#weight' => 1000,
    ];

    $hooks = ['bs_order_goto_checkout_block'];

    \Drupal::moduleHandler()->alter($hooks, $build, $cart);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}

