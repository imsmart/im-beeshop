<?php

namespace Drupal\bs_order\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\bs_order\Entity\OrderItemInterface;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Render\Element;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "bs_order_items_view_based",
 *   label = @Translation("Order items (view based)"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class OrderItemsWidget extends WidgetBase {

  protected $items;

  protected $new_items;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $class = get_class($this);
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    /**
     * @var \Drupal\bs_order\Entity\OrderInterface $order
     */
    $order = $items->getEntity();

    if (!isset($field_state['last_new_order_item_id'])) {
      $field_state['last_new_order_item_id'] = 0;
    }

    if (!empty($field_state['new_items'])) {
      //$this->new_items = $field_state['new_items'];
      $items_entities = $items->referencedEntities();

      foreach ($field_state['new_items'] as $new_item_entity) {
        $items_entities[] = $new_item_entity;
      }

      $field_state['new_items'] = NULL;

      $items->setValue($items_entities);
    }

    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $this->items = $items;

    $wrapper_id = 'bs-order-items-widget-wrapper';
    $element['#type'] = 'fieldset';
    $element['#prefix'] = '<div id="' . $wrapper_id . '">';
    $element['#suffix'] = '</div>';

    $element['items'] = [
//       '#prefix' => '<div class="order-items">',
//       '#suffix' => '</div>',
      '#type' => 'view',
      '#name' => 'order_items',
      '#display_id' => 'order_items_widget',
      '#arguments' => [
        $items->getEntity()->id()
      ],
      '#embed' => TRUE,
      '#items' => $items,
      '#new_items' => $this->new_items,
    ];

    foreach ($items->referencedEntities() as $item_entity) {
      $element['items'][$item_entity->id()]['quantity'] = [
        '#type' => 'number',
        '#title' => $this->t('Quantity'),
        '#title_display' => 'invisible',
        '#default_value' => $item_entity->getQuantity(),
        '#item_id' => $item_entity->id(),
        '#size' => 4,
        '#min' => 0,
        '#step' => 1,
        '#required' => TRUE,
        '#attributes' => [
          'class' => [
            'item-quantity'
          ]
        ],
//         '#ajax' => [
//           'callback' => [get_class($this), 'itemQuantityChangedCallbackAjax'],
//           'wrapper' => $wrapper_id,
//           'method' => 'replaceWith',
//           'event' =>'change',
//         ]
      ];


      $element['items'][$item_entity->id()]['price'] = [
        '#type' => 'bs_price',
        '#title_display' => 'invisible',
        '#default_value' => $item_entity->get('price')->first()->getValue(),
        '#available_currencies' => [$order->getOrderCurrencyCode()],
        '#item_id' => $item_entity->id(),
        '#required' => TRUE,
        '#attributes' => [
          'class' => [
            'item-price'
          ]
        ],
        //         '#ajax' => [
          //           'callback' => $class . '::ajaxCartUpdateItemQuantity',
          //           'event' =>'change',
          //           'wrapper' => !empty($form['#wrapper_id']) ?  $form['#wrapper_id'] : 'bs-cart-edit-form-wrapper',
          //         ],
      ];
    }

    $element['add_item_to_order'] = [
      '#prefix' => '<div id="add-new-order-item-container">',
      '#suffix' => '</div>',
    ];

    if (!empty($field_state['show_add_new_item_form'])) {

      $field_state['show_add_new_item_form'] = FALSE;
      static::setWidgetState($parents, $field_name, $form_state, $field_state);

      $element['add_item_to_order']['new_item_data'] = [
        'new_item_id' => [
          '#title' => t('Product title/sku'),
          '#type' => 'entity_autocomplete',
          '#target_type' => 'bs_product',
          '#selection_settings' => [
            'only_available' => TRUE,
          ],
          '#selection_handler' => 'real_items:bs_product',
          '#description' => t('Define title or SKU of product that you want to add to the order'),
        ],
        'add_new' => [
          '#type' => 'submit',
          '#value' => t('Add'),
          '#op' => 'add_item_to_order',
          '#ajax' => [
            'callback' => [get_class($this), 'addItemFormCallbackAjax'],
            'wrapper' => $wrapper_id,
            'method' => 'replaceWith',
          ],
          '#submit' => [[get_class($this), 'addItemToOrderSubmit']],
        ],
        'cancel' => [
            '#type' => 'submit',
            '#value' => t('Cancel'),
            '#op' => 'cancel_add_item_to_order',
            '#ajax' => [
              'callback' => [get_class($this), 'addItemFormCallbackAjax'],
              'wrapper' => 'add-new-order-item-container',
              'method' => 'replace',
              ],
            '#submit' => [[get_class($this), 'cancelAddNewItemSubmit']],
          ]
      ];

      if ($items->count()) {
        foreach ($items->referencedEntities() as $item_entity) {
          /**
           * @var OrderItemInterface $item_entity
           */
          $element['add_item_to_order']['new_item_data']['new_item_id']['#selection_settings']['exclude_pids'][] = $item_entity->getPid();
        }
      }
    }
    else {
      $element['add_item_to_order']['add_new_item_button'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add new item to order'),
        '#op' => 'show_add_item_form',
        '#ajax' => [
          'callback' => [get_class($this), 'addNewItemFormAjax'],
          'wrapper' => 'add-new-order-item-container',
          'method' => 'replace',
        ],
        '#submit' => [[get_class($this), 'addNewItemSubmit']],
      ];
    }

    $element['#pre_render'] = [
      [$class, 'preRender'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    foreach ($this->items->referencedEntities() as $item_entity) {
      if (!empty($values['items'][$item_entity->id()])) {
        if (isset($values['items'][$item_entity->id()]['quantity'])) {
          $item_entity->setQuantity($values['items'][$item_entity->id()]['quantity']);
        }

        if (isset($values['items'][$item_entity->id()]['price'])) {
          $item_price = $item_entity->get('price')->first()->toPrice();
          $item_price->setPriceValue($values['items'][$item_entity->id()]['price']['value']);
          $item_entity->set('price', $item_price);
        }

      }
    }

    $items = $this->items->referencedEntities();

    return $items;
  }

  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {

    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    $this->items = $items;
    // Let the widget massage the submitted values.
    $values = $this->massageFormValues($values, $form, $form_state);

    // Assign the values and remove the empty ones.
    $items->setValue($values);

  }

//   /**
//    * {@inheritdoc}
//    */
//   public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {

//   }

  public static function preRender($element) {

//     dpm($element);

    $search = [];
    $replace = [];

    foreach (Element::children($element['items']) as $item_id) {

      if (!empty($element['items'][$item_id]['quantity'])) {
        $search[] = "<!--order-item-quantity-edit-$item_id-->";
        $replace[] = \Drupal::service('renderer')->render($element['items'][$item_id]['quantity']);
      }

      if (!empty($element['items'][$item_id]['price'])) {
        $search[] = "<!--order-item-price-edit-$item_id-->";
        $replace[] = \Drupal::service('renderer')->render($element['items'][$item_id]['price']);
      }

    }

    $items_output = str_replace($search, $replace, \Drupal::service('renderer')->render($element['items']));

    $element['items'] = [
      '#markup' => ViewsRenderPipelineMarkup::create($items_output),
    ];

    return $element;
  }


  /**
   * {@inheritDoc}
   */
  public static function isApplicable(\Drupal\Core\Field\FieldDefinitionInterface $field_definition) {
    $field_definition_settings = $field_definition->getSettings();

    if (!empty($field_definition_settings['target_type']) && $field_definition_settings['target_type'] == 'bs_order_item') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Submission handler for the "Add another item" button.
   */
  public static function addNewItemSubmit(array $form, FormStateInterface $form_state) {

    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['show_add_new_item_form'] = TRUE;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

  public static function cancelAddNewItemSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));

    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['show_add_new_item_form'] = FALSE;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

  public static function addItemToOrderSubmit(array $form, FormStateInterface $form_state) {

    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));

    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    if (!isset($field_state['last_new_order_item_id'])) {
      $field_state['last_new_order_item_id'] = 0;
    }

    if ($new_item_pid = $form_state->getValue(['order_items', 'add_item_to_order', 'new_item_data', 'new_item_id'])) {

      if ($product = Product::load($new_item_pid)) {

        $quantity = 1;
        $field_state['last_new_order_item_id']--;

        $order_item_values = [
          'order_item_id' => $field_state['last_new_order_item_id'],
          'pid' => $new_item_pid,
          'quantity' => $quantity,
        ];

        $order_items_storage = \Drupal::entityTypeManager()->getStorage('bs_order_item');
        $order_item = $order_items_storage->create($order_item_values);
        $order_item->enforceIsNew();

        if ($product_price = $product->getActualPrice()) {

          $price = $product_price->toPrice();

          $order_item->set('price', $price);

          $totl_price = $price->multiply($quantity);
          $order_item->set('total_price', $totl_price);

          $field_state['new_items'][] = $order_item;

        }
      }

    }


    $field_state['show_add_new_item_form'] = FALSE;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();

  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addNewItemFormAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    return $element;
  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addItemFormCallbackAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));

    return $element;
  }

  public function itemQuantityChangedCallbackAjax(array $form, FormStateInterface $form_state) {
    $triggeredElement = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($triggeredElement['#array_parents'], 0, -3));

    return $element;
  }

}

