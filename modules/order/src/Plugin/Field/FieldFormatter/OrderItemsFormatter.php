<?php

namespace Drupal\bs_order\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "order_items",
 *   label = @Translation("Order items"),
 *   description = @Translation("Display order items."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class OrderItemsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'display_id' => 'order_items',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['display_id'] = [
      '#type' => 'textfield',
      '#title' => 'Display ID',
      '#default_value' => $this->getSetting('display_id'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('display_id')) {
      $summary[] = t('Display "@display_id"', ['@display_id' => $this->getSetting('display_id')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [
      '#type' => 'view',
      '#name' => 'order_items',
      '#display_id' => $this->getSetting('display_id') ?: 'order_items',
      '#arguments' => [
        $items->getEntity()->id()
      ],
      '#embed' => TRUE,
    ];


    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() == 'bs_order';
  }

}

