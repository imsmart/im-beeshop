<?php

namespace Drupal\bs_order\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element for editing the cart item quantity.
 *
 * @ViewsField("bs_order_adjustments_total")
 */
class OrderAdjustmentsTotal extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition/*, CartManagerInterface $cart_manager*/) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition//,
//       $container->get('commerce_cart.cart_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $order = $this->getEntity($row);
    if ($adjustments_total_price = $order->getOrderAdjustmentsTotalPrice()) {
      return $adjustments_total_price;
    }
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($value = $this->getValue($values)) {
      $price_array = [
        '#theme' => 'bs_price_item',
        '#price_item' => $value,
      ];
      $rendered = $this->getRenderer()->render($price_array);
      return $rendered;
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }



}
