<?php
namespace Drupal\bs_order\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\beeshop\IsApplicableToEntity;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\beeshop\Ajax\BeeShopEventCommand;
use Drupal\bs_cart\CartInterface;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("bs_order_items_bulk_form")
 */
class OrderItemsBulkForm extends BulkForm {

  public static function ajaxSubmit(array $form, FormStateInterface $form_state, Request $request) {

    /**
     * @var CartInterface $cart
     */
    $cart = \Drupal::service('current_user_cart');
    $event = $cart->getCartEvent();
    $response = $event->getResponse();

    $items = &drupal_static('Action:AddToCart:items', []);

    $response->addCommand(new BeeShopEventCommand('BeeShop:productsAddedToCartInBulk', [
      'message' => t('В <a href="/shop/cart">корзину</a> добавлено товаров: @count', ['@count' => count($items)])
    ]));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $form['hide_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide header'),
      '#default_value' => $this->options['hide_header'],
    ];

    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Selected actions'),
      '#options' => $this->getBulkOptions(FALSE),
      '#default_value' => $this->options['action'],
    ];

    $form['submit_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button text'),
      '#default_value' => $this->options['submit_button_text'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_header'] = ['default' => TRUE];
    $options['action'] = ['default' => ''];
    $options['submit_button_text'] = [
      'default' => 'Submit for selected items',
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No order items selected.');
  }

//   /**
//    * {@inheritDoc}
//    */
//   protected function getBulkOptions($filtered = TRUE) {

//   }

  public function viewsForm(&$form, FormStateInterface $form_state) {
    parent::viewsForm($form, $form_state);

    $class = get_class($this);

    $action_plugin = ($this->options['hide_header'] && !empty($this->actions[$this->options['action']])) ?
                     $this->actions[$this->options['action']]->getPlugin() : FALSE;

    if (!empty($form[$this->options['id']])) {
      foreach ($this->view->result as $row_index => $row) {

        $entity = $this->getEntityTranslation($this->getEntity($row), $row);

        $form[$this->options['id']][$row_index]['#title_display'] = 'after';
        $form[$this->options['id']][$row_index]['#title'] = ' ';


        if ($action_plugin && $action_plugin instanceof IsApplicableToEntity) {
          if (!$action_plugin->isApplicable($entity)) {
            $form[$this->options['id']][$row_index]['#disabled'] = TRUE;
            $form[$this->options['id']][$row_index]['#attributes']['readonly'] = TRUE;
          }
        }
      }
    }

    if ($this->options['hide_header'] && isset($form['header'])) {
      unset($form['header']);
      $form['action'] = [
        '#type' => 'value',
        '#value' => $this->options['action'],
      ];
    }

    if ($this->options['submit_button_text']) {
      $form['actions']['submit']['#value'] = $this->t($this->options['submit_button_text']);
    }

    $form['actions']['submit']['#ajax'] = [
      'callback' => $class . '::ajaxSubmit',
    ];

  }

  public function viewsFormSubmit(&$form, FormStateInterface $form_state) {
    //dpm($form_state->getValues());
    parent::viewsFormSubmit($form, $form_state);
    $this->messenger()->deleteAll();
  }

}

