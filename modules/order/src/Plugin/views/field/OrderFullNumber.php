<?php
namespace Drupal\bs_order\Plugin\views\field;

use Drupal\bs_order\Entity\Order;
use Drupal\views\Plugin\views\field\EntityLabel;
use Drupal\views\ResultRow;

/**
 * Field handler to display entity label optionally linked to entity page.
 *
 * @ViewsField("bs_order_full_number")
 */
class OrderFullNumber extends EntityLabel {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return $values->_entity instanceof Order ? $values->_entity->getFullNumber() : '-';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This function exists to override parent query function.
    // Do nothing.
  }

}

