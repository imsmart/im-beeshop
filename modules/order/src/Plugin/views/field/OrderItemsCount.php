<?php
namespace Drupal\bs_order\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\bs_order\Entity\OrderInterface;

/**
 * Defines a form element for editing the cart item quantity.
 *
 * @ViewsField("bs_order_items_count")
 */
class OrderItemsCount extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    /**
     * @var OrderInterface $order
     */
    $order = $this->getEntity($row);
    return $order->get('order_items')->count();
  }
}

