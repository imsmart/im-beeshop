<?php

namespace Drupal\bs_order\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element for editing the order item price.
 *
 * @ViewsField("bs_order_item_price_edit")
 */
class EditOrderItemPrice extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition/*, CartManagerInterface $cart_manager*/) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

//     $this->cartManager = $cart_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition//,
//       $container->get('commerce_cart.cart_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $order_item = $this->getEntity($row);
    return '<!--order-item-price-edit-' . $order_item->id() . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
//   public function viewsForm(array &$form, FormStateInterface $form_state) {
//     // Make sure we do not accidentally cache this form.
//     $form['#cache']['max-age'] = 0;
//     // The view is empty, abort.
//     if (empty($this->view->result)) {
//       unset($form['actions']);
//       return;
//     }

//     $form[$this->options['id']]['#tree'] = TRUE;
//     foreach ($this->view->result as $row_index => $row) {
//       $order_item = $this->getEntity($row);

//       $form[$this->options['id']][$row_index] = [
//         '#type' => 'number',
//         '#title' => $this->t('Quantity'),
//         '#title_display' => 'invisible',
//         '#default_value' => $order_item->get('price')->value,
//         '#size' => 10,
//         '#length' => 10,
//         '#min' => 0,
//         '#step' => 1,
//       ];
//     }
//   }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $quantities = $form_state->getValue($this->options['id'], []);
    foreach ($quantities as $row_index => $quantity) {
      /** @var \Drupal\bs_cart\Entity\CartItem $cart_item */
      $cart_item = $this->getEntity($this->view->result[$row_index]);
      if ($cart_item->getQuantity() != $quantity) {
        $cart_item->setQuantity($quantity);
        $cart_item->save();
        $cart = $cart_item->getCart();
        //         $cart->update();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
