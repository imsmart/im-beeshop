<?php
namespace Drupal\bs_order\Plugin\views\style;

use Drupal\views\Plugin\views\style\Table;

/**
 * Style plugin to render each item as a row in a table.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "bs_order_items_table",
 *   title = @Translation("BeeShop order items table"),
 *   help = @Translation("Displays order items in a table."),
 *   theme = "views_view_bs_order_items_table",
 *   display_types = {"normal"},
 *   base = "bs_order_items",
 * )
 */
class OrderItemsTable extends Table {
}

