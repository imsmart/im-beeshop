<?php
namespace Drupal\bs_order\Plugin\views\query;

use Drupal\views\Plugin\views\query\Sql;
use Drupal\Core\Form\FormStateInterface;

/**
 * Views query plugin for an SQL query for bs_order_item entities.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "bs_order_items_views_query",
 *   title = @Translation("Beeshop order items SQL Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class BsOrderItemsViewsQuery extends Sql {



  /**
   * Add settings for the ui.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['contextual_filters_or'] = array(
      '#title' => t('Contextual filters OR'),
      '#description' => t('Contextual filters applied to OR logic.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['contextual_filters_or']),
    );
  }

  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $op = $this->options['contextual_filters_or'] ? 'OR' : 'AND';
      $this->setWhereGroup($op, $group);
    }

    $this->where[$group]['conditions'][] = array(
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    );

  }

  public function addWhereExpression($group, $snippet, $args = array()) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $op = $this->options['contextual_filters_or'] ? 'OR' : 'AND';
      $this->setWhereGroup($op, $group);
    }

    $this->where[$group]['conditions'][] = array(
      'field' => $snippet,
      'value' => $args,
      'operator' => 'formula',
    );
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['contextual_filters_or'] = array(
      'default' => FALSE,
    );

    return $options;
  }

}

