<?php

namespace Drupal\bs_order\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;

/**
 * Provides a 'Order total by products' condition.
 *
 * @Condition(
 *   id = "bs_order_total_by_products",
 *   label = @Translation("Order total by products"),
 *   context = {
 *     "bs_order" = @ContextDefinition("entity:bs_order", label = @Translation("Order"))
 *   }
 * )
 */
class OrderTotalByProducts extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {

    return TRUE;

    if (empty($this->configuration['bundles']) && !$this->isNegated()) {
      return TRUE;
    }
    $node = $this->getContextValue('node');
    return !empty($this->configuration['bundles'][$node->getType()]);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

    return '****';

//     if (count($this->configuration['bundles']) > 1) {
//       $bundles = $this->configuration['bundles'];
//       $last = array_pop($bundles);
//       $bundles = implode(', ', $bundles);
//       return $this->t('The node bundle is @bundles or @last', ['@bundles' => $bundles, '@last' => $last]);
//     }
//     $bundle = reset($this->configuration['bundles']);
//     return $this->t('The node bundle is @bundle', ['@bundle' => $bundle]);
  }

}

