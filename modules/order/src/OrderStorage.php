<?php
namespace Drupal\bs_order;

use Drupal\bs_order\Entity\OrderItem;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\bs_price\Price;
use Drupal\bs_order\Entity\OrderState;
use Drupal\bs_cart\CartInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\BaseFieldDefinition;


class OrderStorage extends SqlContentEntityStorage implements OrderStorageInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ContentEntityStorageBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   */
  public function __construct(EntityTypeInterface $entity_type, Connection $database, EntityManagerInterface $entity_manager, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $database, $entity_manager, $cache, $language_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity.manager'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function createByCart(CartInterface $cart = NULL) {
    $cart = $cart ? $cart : $this->getCart();

    $cart_violations = $cart->validateAndFix();

    if ($cart_violations && $cart_violations->count()) {
      return FALSE;
    }

    $new_order_values = [
      'state' => 'new', // @TODO detect defaul state for new orders automaticaly
      'order_items' => [],
      'cart' => $cart,
    ];

    $order_items = [];

    $items_currency_code = '';

    foreach ($cart->cart_items->referencedEntities() as $cart_item_entity) {

      if ($cart_item_entity->get('unavailable')->value) {
        continue;
      }

      $pid = $cart_item_entity->getPid();

      $price = $cart_item_entity->get('price')->first();
      $price_value = $price->value;
      $currency_code = $price->currency_code;
      $items_currency_code = $currency_code;
      $quantity = $cart_item_entity->getQuantity();

      $order_item_values = [
        'pid' => $pid,
        'quantity' => $quantity,
      ];

      $order_item = $this->entityTypeManager->getStorage('bs_order_item')->create($order_item_values);

      $order_item->set('price', [
        'value' => $price_value,
        'currency_code' => $currency_code,
      ]);

      $order_item->set('total_price', [
        'value' => $price_value * $quantity,
        'currency_code' => $currency_code,
      ]);

      $new_order_values['order_items'][] = $order_item;

    }

    $new_order_values['total_by_products_amount'] = [
      'value' => 0,
      'currency_code' => $items_currency_code,
    ];

    $new_order_values['total_amount'] = [
      'value' => 0,
      'currency_code' => $items_currency_code,
    ];

    $order = $this->create($new_order_values);

    foreach ($cart->getFieldDefinitions() as $cart_field_definition) {

      $copy_to_order = FALSE;
      $order_field_name = $cart_field_definition->getName();

      if ($cart_field_definition instanceof FieldConfigInterface) {
        $copy_to_order = $cart_field_definition->getThirdPartySetting('bs_cart', 'copy_to_order', FALSE);
      }
      elseif ($cart_field_definition instanceof BaseFieldDefinition) {
        $copy_to_order = $cart_field_definition->getSetting('copyToOrder');
        $order_field_name = $cart_field_definition->getSetting('bs_order_target_field_name') ? $cart_field_definition->getSetting('bs_order_target_field_name') : $cart_field_definition->getName();
      }

      if ($copy_to_order) {
        if ($order->hasField($order_field_name)) {
          $order->set($order_field_name, $cart->get($cart_field_definition->getName())->getValue());
        }
      }
    }

    return $order;
  }

  public function loadMultiple(array $ids = NULL) {
    $entities = parent::loadMultiple($ids);

//     $states_ids = [];

//     foreach ($entities as $entity) {
//       if (!$entity->get('state')->isEmpty()) {
//         if (!in_array($entity->getState(), $states_ids)) {
//           $states_ids[] = $entity->getState();
//         }
//       }
//     }

//     $states = OrderState::loadMultiple($states_ids);

//     foreach ($entities as $entity) {
//       if (!$entity->get('state')->isEmpty()) {
//         $order_states = $entity->get('state')->referencedEntities();
//         $order_state = reset($order_states);

//         if ($order_state->get('actuality_time') && time() > strtotime($order_state->get('actuality_time'), $entity->changed->value)) {

//           if ($order_state->get('nex_state_on_actuality_exprired')) {
//             $entity->set('state', $order_state->get('nex_state_on_actuality_exprired'));
//             if ($entity->save()) {
//               $t_args = ['%order_number' => $entity->label()];
//               $context = array_merge($t_args, ['link' => $entity->toLink($entity->t('View'), 'edit-form')->toString()]);
//               $this->logger()->notice('Order "%order_number" was resaved with new state after loading', $context);
//             }
//           }

//         }
//       }
//     }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOrderItems($order_id) {
    $query = $this->database->select('bs_order_items', 'ois');
    $query->condition('ois.order_id', $order_id);
    $query->fields('ois', ['order_item_id']);
    $items_ids = $query->execute()->fetchCol(0);

    return OrderItem::loadMultiple($items_ids);
  }

  /**
   *
   * @return \Drupal\bs_cart\CartInterface
   */
  protected function getCart() {
    return \Drupal::service('current_user_cart');
  }

  /**
   * {@inheritdoc}
   */
  protected function getFromStorage(array $ids = NULL) {
    $entities = parent::getFromStorage($ids);

    foreach ($entities as $oid => $order_entity) {
      if ($order_items = $this->loadOrderItems($oid)) {
        $order_entity->set('order_items', $order_items);
      }
    }

    return $entities;
  }

  /**
   * @return \Psr\Log\LoggerInterface
   */
  protected function logger() {
    return \Drupal::logger('bs_order');
  }

//   protected function loadCartItems($cart_id) {
//     $query = $this->database->select('bs_users_carts_items', 'ci');
//     $query->condition('ci.cart_id', $cart_id);
//     $query->fields('ci', ['cart_item_id']);

//     $cart_items_ids = $query->execute()->fetchCol(0);

//     $cart_items = CartItem::loadMultiple($cart_items_ids);
//     return $cart_items;
//   }

}

