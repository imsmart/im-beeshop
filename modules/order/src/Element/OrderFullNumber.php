<?php

namespace Drupal\bs_order\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Psr\Container\ContainerInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\bs_order\OrderStorageInterface;

/**
 * Provides a price form element.
 *
 * Usage example:
 * @code
 * $form['amount'] = [
 *   '#type' => 'bs_price',
 *   '#title' => $this->t('Amount'),
 *   '#default_value' => ['value' => '99.99', 'currency_code' => 'USD'],
 *   '#allow_negative' => FALSE,
 *   '#size' => 60,
 *   '#maxlength' => 128,
 *   '#required' => TRUE,
 *   '#available_currencies' => ['USD', 'EUR',],
 * ];
 * @endcode
 *
 * @FormElement("bs_order_full_number")
 */
class OrderFullNumber extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      // List of currencies codes. If empty, all currencies will be available.
//       '#size' => 10,
//       '#maxlength' => 128,
      '#default_value' => NULL,
//       '#allow_negative' => FALSE,
      '#process' => [
        [$class, 'processElement'],
//         [$class, 'processGroup'],
      ],
      '#attached' => [
        'library' => ['bs_order/element.orderFullNumber'],
      ],
      '#pre_render' => [
//         [$class, 'preRenderGroup'],
      ],
      '#input' => TRUE,
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Builds the bs_price form element.
   *
   * @param array $element
   *   The initial bs_price form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built bs_price form element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #default_value is not an instance of
   *   \Drupal\bs_price\Price.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {

    $default_value = !empty($element['#default_value']) ? self::splitOrderFullNumber($element['#default_value']) : NULL;

    $element['number_parts'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'order-number-parts',
        ]
      ]
    ];

    $element['number_parts']['part_1'] = [
      '#type' => 'number',
      '#default_value' => $default_value ? $default_value['part1'] : NULL,
      '#required' => TRUE,
      '#size' => 6,
      '#maxlength' => 6,
      '#attributes' => [
        'class' => ['part-1']
      ]
    ];

    $element['number_parts']['part_2'] = [
      '#type' => 'textfield',
      '#default_value' => $default_value ? $default_value['part2'] : NULL,
      '#required' => TRUE,
      '#size' => 8,
      '#maxlength' => 8,
      '#attributes' => [
        'class' => ['part-2']
      ]
    ];

    $element['#element_validate'] = [[get_called_class(), 'validateOrderFullNumber']];
    $element['#tree'] = TRUE;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {

    if ($input === FALSE || empty($input['number_parts']['part_1']) || empty($input['number_parts']['part_2'])) {
      $element += ['#default_value' => []];
      return $element['#default_value'];
    }

    $value = implode('-', [$input['number_parts']['part_1'], $input['number_parts']['part_2']]);
    return $value;
  }

  public static function validateOrderFullNumber(&$element, FormStateInterface $form_state, &$complete_form) {

    $part1 = $element['number_parts']['part_1']['#value'];
    $part2 = $element['number_parts']['part_2']['#value'];

    if (strlen($part1) == 0 || strlen($part2) == 0) {
      $form_state->setError($element, t('Order number is reqired.'));
    }
    else {
      $order_storage = self::getOrderStorage();
      $orders_count = $order_storage->getQuery()
        ->condition('order_id', $part1)
        ->condition('order_number', $part2)
        ->count()
        ->execute();

       if (!$orders_count) {
         $form_state->setError($element, t('Order with specified number does not exist.'));
       }
    }

    $form_state->setValueForElement($element, implode('-', [$part1, $part2]));

    return $element;
  }

  /**
   * @return \Drupal\bs_order\OrderStorageInterface
   */
  protected static function getOrderStorage() {
    return \Drupal::entityTypeManager()->getStorage('bs_order');
  }

  protected static function splitOrderFullNumber($order_number) {
    if ($order_number && preg_match('#^(?<part1>\d{1,})-(?<part2>\w{1,})$#', $order_number, $match)) {
      return ['part1' => $match['part1'], 'part2' => $match['part2']];
    }

    return FALSE;
  }

}
