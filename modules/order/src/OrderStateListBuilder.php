<?php
namespace Drupal\bs_order;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\beeshop\HierarchicalDraggableListBuilderTrait;
use Drupal\beeshop\HierarchicalDraggableListBuilder;

class OrderStateListBuilder extends HierarchicalDraggableListBuilder {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  protected $entitiesKey = 'bs_order_states';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_order_state_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    $destination = $this->getDestinationArray();

    foreach ($operations as $key => $operation) {
      if (empty($operation['query'])) {
        $operations[$key]['query'] = $destination;
      }
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    return $form;
//     //////////
//     $entities = $this->storage->loadTree();
//     $delta = count($entities);

//     $form['entities'] = [
//       '#type' => 'table',
//       '#header' => [
//         $this->t('Name'),
//         $this->t('Weight'),
//         $this->t('Description'),
//         $this->t('Operations')],
//       '#empty' => $this->t('No order states available. <a href=":link">Add new</a>.', [':link' => Url::fromRoute('entity.bs_order_state.add_form', [])]),
//       '#attributes' => [
//         'id' => 'entities-hiearch-table',
//       ],
//     ];

//     $form['entities']['#tabledrag'][] = [
//       'action' => 'order',
//       'relationship' => 'sibling',
//       'group' => 'weight',
//     ];
//     $form['entities']['#tabledrag'][] = [
//       'action' => 'match',
//       'relationship' => 'parent',
//       'group' => 'parent',
//       'subgroup' => 'parent',
//       'source' => 'id',
//       'hidden' => FALSE,
//     ];
//     $form['entities']['#tabledrag'][] = [
//       'action' => 'depth',
//       'relationship' => 'group',
//       'group' => 'depth',
//       'hidden' => FALSE,
//     ];

//     foreach ($entities as $key => $entity) {
//       /** @var $term \Drupal\Core\Entity\EntityInterface */
//       $form['entities'][$key]['#entity'] = $entity;
//       $indentation = [];
//       if (isset($entity->depth) && $entity->depth > 0) {
//         $indentation = [
//           '#theme' => 'indentation',
//           '#size' => $entity->depth,
//         ];
//       }
//       $form['entities'][$key]['label']['data']['label'] = [
//         '#type' => 'markup',
//         '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
//         '#markup' => $entity->label,
//       ];
//       $form['entities'][$key]['label']['id'] = [
//         '#type' => 'hidden',
//         '#value' => $entity->id(),
//         '#attributes' => [
//           'class' => ['id'],
//         ],
//       ];
//       $form['entities'][$key]['label']['parent'] = [
//         '#type' => 'hidden',
//         // Yes, default_value on a hidden. It needs to be changeable by the
//         // javascript.
//         '#default_value' => $entity->parents[0],
//         '#attributes' => [
//           'class' => ['parent'],
//         ],
//       ];
//       $form['entities'][$key]['label']['depth'] = [
//         '#type' => 'hidden',
//         // Same as above, the depth is modified by javascript, so it's a
//         // default_value.
//         '#default_value' => isset($entity->depth) ? $entity->depth : 0,
//         '#attributes' => [
//           'class' => ['depth'],
//         ],
//       ];

//       $form['entities'][$key]['weight'] = [
//         '#type' => 'weight',
//         '#delta' => $delta,
//         '#title' => $this->t('Weight for added term'),
//         '#title_display' => 'invisible',
//         '#default_value' => $entity->getWeight(),
//         '#attributes' => [
//           'class' => ['weight'],
//         ],
//       ];

//       $form['entities'][$key]['description'] = [
//         '#type' => 'markup',
//         '#markup' => $entity->getDescription(),
//         '#attributes' => [
//           'class' => ['description'],
//         ],
//       ];

//       $form['entities'][$key]['operations'] = [
//         '#type' => 'operations',
//         '#links' => $this->getOperations($entity),
//       ];

//       $form['entities'][$key]['#attributes']['class'][] = 'draggable';
//     }

//     if ($delta > 1) {

//       $form['actions'] = ['#type' => 'actions'];
//       $form['actions']['submit'] = [
//         '#type' => 'submit',
//         '#button_type' => 'primary',
//         '#value' => $this->t('Save'),
//       ];
//     }

//     return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   *
   * @param string|null $theme
   *   (optional) The theme to display the blocks for. If NULL, the current
   *   theme will be used.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The block list as a renderable array.
   */
  public function render() {

    return parent::render();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['emails'] = $this->t('Customer/manager<br/> notification');
    $header['actuality_time'] = t('Expired after');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['name']['data']['label_markup']['#markup'] = $entity->label();

    $row['emails']['data']['#markup'] = $this->t('None');

    if ($entity->send_email_to_customer && $entity->send_email_to_manager) {
      $row['emails']['data']['#markup'] = $this->t('Both');
    }
    elseif (!$entity->send_email_to_customer && !$entity->send_email_to_manager) {
      $row['emails']['data']['#markup'] = $this->t('None');
    }
    elseif ($entity->send_email_to_customer) {
      $row['emails']['data']['#markup'] = $this->t('To customer');
    }
    elseif ($entity->send_email_to_customer) {
      $row['emails']['data']['#markup'] = $this->t('To manager');
    }

    if ($actuality_limit = $entity->get('actuality_time')) {
      $row['actuality_time']['data']['#markup'] = $actuality_limit . ' => ' . $entity->get('nex_state_on_actuality_exprired');
    }
    else {
      $row['actuality_time']['data']['#markup'] = '';
    }


    if ($description = $entity->getDescription()) {
//       $row['label']['data']['description'] = [
//         '#markup' => $description,
//         '#prefix' => '<div class="description">',
//         '#suffix' => '</div>'
//       ];
    }


    //$row['description']['data'] = ['#markup' => $translated];
    return $row + parent::buildRow($entity);
  }


//   /**
//    * {@inheritdoc}
//    */
//   public function submitForm(array &$form, FormStateInterface $form_state) {

//     uasort($form_state->getValue('entities'), ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

//     $changed_entities = [];
//     $level_weights = [];
//     $weight = 0;
//     foreach ($form_state->getValue('entities') as $id => $values) {
//       if (isset($form['entities'][$id]['#entity'])) {
//         $entity = $form['entities'][$id]['#entity'];

//         if ($values['label']['parent'] == '' && $entity->getWeight() !== $weight) {
//           $entity->setWeight($weight);
//           $changed_entities[$entity->id()] = $entity;
//         }
//         // Terms not at the root level can safely start from 0 because they're all on this page.
//         elseif ($values['label']['parent'] != '') {
//           $level_weights[$values['label']['parent']] = isset($level_weights[$values['label']['parent']]) ? $level_weights[$values['label']['parent']] + 1 : 0;
//           if ($level_weights[$values['label']['parent']] !== $entity->getWeight()) {
//             $entity->setWeight($level_weights[$values['label']['parent']]);
//             $changed_entities[$entity->id()] = $entity;
//           }
//         }
//         // Update any changed parents.
//         if ($values['label']['parent'] != $entity->parents[0]) {
//           $entity->parent = $values['label']['parent'];
//           $changed_entities[$entity->id()] = $entity;
//         }
//         $weight++;
//       }
//     }
//     // Save all updated terms.
//     foreach ($changed_entities as $entity) {
//       $entity->save();
//     }
//     drupal_set_message($this->t('The configuration options have been saved.'));

//   }

}

