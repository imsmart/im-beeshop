<?php
namespace Drupal\bs_order\Controller;

use Drupal\bs_order\Entity\Order;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;


class OrderController extends EntityController {

  use StringTranslationTrait;

  /**
   * @var OrderInterface
   */
  protected $storedOrder = NULL;

  public static function saveOrderIdToPS(OrderInterface $order) {
    /** @var PrivateTempStoreFactory $private_tempstore */
    $private_tempstore = \Drupal::service('tempstore.private');
    $bs_order_storage = $private_tempstore->get('bs_order');
    $bs_order_storage->set('oid', $order->id());
    return true;
  }

  public static function getOrderIdFromPS() {
    /** @var PrivateTempStoreFactory $private_tempstore */
    $private_tempstore = \Drupal::service('tempstore.private');
    $bs_order_storage = $private_tempstore->get('bs_order');
    return $bs_order_storage->get('oid');
  }

  /**
   * @return \Drupal\bs_order\Entity\Order|NULL
   */
  public function getStoredOrder() {

    if ($this->storedOrder !== NULL) {
      return $this->storedOrder;
    }

    if (($oid = OrderController::getOrderIdFromPS()) && ($order = Order::load($oid))) {
      $this->storedOrder = $order;
    }
    else {
      $this->storedOrder = FALSE;
    }

    return $this->storedOrder;
  }

  public function newOrderOverviewAccess(AccountInterface $account) {

    $order = $this->getStoredOrder();

    return AccessResult::allowedIf($order);
  }

  public function newOrderOverviewTitle() {
    $order = $this->getStoredOrder();
    return $this->t('Your order @order_full_number', ['@order_full_number' => $order ? $order->getFullNumber() : '-']);
  }

  public function orderOverviewTitle($bs_order_full_number) {
    return $this->t('Your order @order_full_number', ['@order_full_number' => $bs_order_full_number]);
  }

  public function getCheckoutPageTitle() {
    $title = 'Checkout page';

    \Drupal::moduleHandler()->alter('checkout_page_title', $title);

    $title = $this->t($title);

    return $title;
  }

  public function newOrderOverview() {

    $order = $this->getStoredOrder();

    if (!$order) {
      return [];
    }

    $page = $this->entityTypeManager
    ->getViewBuilder($order->getEntityTypeId())
    ->view($order, 'new_order');

    $page['#entity_type'] = $order->getEntityTypeId();
    $page['#' . $page['#entity_type']] = $order;

    return $page;

//     if (empty($_SESSION['new_order_id'])) {
//       //$this->redirect($route_name)
//       return $build;
//     }

//     $order = Order::load($_SESSION['new_order_id']);

//     if (!$order) {
//       drupal_set_message('Error while loading order for view: order not found', 'error');
//       return $build;
//     }

//     $order_data = $order->get('data')->first();

//     $order_full_number = $order->getFullNumber();
//     //$order->set('order_number', $order_full_number);

//     $build = $this->prepareOrderForView($order, 'new_order');

//     foreach ($build['#pre_render'] as $key => $pre_render) {
//       if ($pre_render[1] == 'buildTitle') {
//         unset($build['#pre_render'][$key]);
//       }
//     }

//     $build['order_number'] = [
//       '#markup' => '<div class="field field--order-namber field--label-inline">
//               <div class="field__item">' . $order_full_number . '</div>
//           </div>',
//     ];

//     $build['customer_name'] = [
//       '#markup' => '<div class="field field--name-customer-name field--label-inline">
//     <div class="field__label">'. $this->t('Whom') . '</div>
//               <div class="field__item">' . $order_data->name . '</div>
//           </div>',
//     ];

//     if ($customer_tel = $order_data->tel) {
//       $build['customer_tel'] = [
//         '#markup' => '<div class="field field--name-customer-tel field--label-inline">
//     <div class="field__label">'. $this->t('Phone') . '</div>
//               <div class="field__item">' . $customer_tel. '</div>
//           </div>',
//       ];
//     }

//     if ($customer_city= $order_data->city) {
//       $build['customer_city'] = [
//         '#markup' => '<div class="field field--name-customer-city field--label-inline">
//     <div class="field__label">'. $this->t('City') . '</div>
//               <div class="field__item">' . $customer_city. '</div>
//           </div>',
//       ];
//     }

//     if ($customer_zip = $order_data->zip) {
//       $build['customer_zip'] = [
//         '#markup' => '<div class="field field--name-customer-zip field--label-inline">
//     <div class="field__label">'. $this->t('ZIP') . '</div>
//               <div class="field__item">' . $customer_zip. '</div>
//           </div>',
//       ];
//     }

//     if ($customer_street = $order_data->street) {
//       $build['customer_street'] = [
//         '#markup' => '<div class="field field--name-customer-street field--label-inline">
//     <div class="field__label">'. $this->t('Street Address') . '</div>
//               <div class="field__item">' . $customer_street. '</div>
//           </div>',
//       ];
//     }

//     if ($customer_house= $order_data->house) {
//       $build['customer_house'] = [
//         '#markup' => '<div class="field field--name-customer-house field--label-inline">
//     <div class="field__label">Дом,Корпус</div>
//               <div class="field__item">' . $customer_house. '</div>
//           </div>',
//       ];
//     }

//     if ($customer_app = $order_data->app) {
//       $build['customer_app'] = [
//         '#markup' => '<div class="field field--name-customer-app field--label-inline">
//     <div class="field__label">Квартира / Офис</div>
//               <div class="field__item">' . $customer_app . '</div>
//           </div>',
//       ];
//     }

//     // Письмо с подтверждением на почте
//     // Если Вы не получите уведомление о заказ на email в течение 5 минут, пожалуйста, проверьте папку «Спам». А если письма нет — напишите нам на <a href="mailto:info@anker-company.com">info@anker-company.com</a>
//     $build['mail'] = [
//       '#markup' => '<div class="field field--name-customer-mail field--label-above">
//     <div class="field__label">' . $this->t('Order confirmation on your mail') . ':</div>
//               <div class="field__item">' . $order->mail->value. '</div>
//               <div class="field-description">' . $this->t('If you do not receive an email notification within 5 minutes, please check the Spam folder. And if there is no letter - write to us at @mail_link', ['@mail_link' => new FormattableMarkup('<a href="mailto:info@anker-company.com">info@anker-company.com</a>', [])]) . '</div>
//           </div>',
//     ];

//     //\Drupal::moduleHandler()->invokeAll('bs_order_view_alter', [&$build, $order]);

    return $build;
  }


  public function orderOverviewPage($bs_order, $view_mode = 'full') {
    $order_data = $bs_order->get('data')->first();

    $order_full_number = $bs_order->getFullNumber();
    //$order->set('order_number', $order_full_number);

    $build = $this->prepareOrderForView($bs_order, $view_mode);

    foreach ($build['#pre_render'] as $key => $pre_render) {
      if ($pre_render[1] == 'buildTitle') {
        unset($build['#pre_render'][$key]);
      }
    }

    $build['order_number'] = [
      '#markup' => '<div class="field field--order-namber field--label-inline">
        <div class="field__label">' . $this->t('Your order') . '</div>
              <div class="field__item">' . $order_full_number . '</div>
          </div>',
    ];

    $build['customer_name'] = [
      '#markup' => '<div class="field field--name-customer-name field--label-inline">
    <div class="field__label">' . $this->t('Whom') . '</div>
              <div class="field__item">' . $order_data->name . '</div>
          </div>',
    ];

    if ($customer_tel = $order_data->tel) {
      $build['customer_tel'] = [
        '#markup' => '<div class="field field--name-customer-tel field--label-inline">
    <div class="field__label">' . $this->t('Phone') . '</div>
              <div class="field__item">' . $customer_tel. '</div>
          </div>',
      ];
    }

    if ($customer_city= $order_data->city) {
      $build['customer_city'] = [
        '#markup' => '<div class="field field--name-customer-city field--label-inline">
    <div class="field__label">' . $this->t('City') . '</div>
              <div class="field__item">' . $customer_city. '</div>
          </div>',
      ];
    }

    if ($customer_zip = $order_data->zip) {
      $build['customer_zip'] = [
        '#markup' => '<div class="field field--name-customer-zip field--label-inline">
    <div class="field__label">' . $this->t('ZIP') . '</div>
              <div class="field__item">' . $customer_zip. '</div>
          </div>',
      ];
    }

    if ($customer_street = $order_data->street) {
      $build['customer_street'] = [
        '#markup' => '<div class="field field--name-customer-street field--label-inline">
    <div class="field__label">'. $this->t('Street Address') . '</div>
              <div class="field__item">' . $customer_street. '</div>
          </div>',
      ];
    }

    if ($customer_house= $order_data->house) {
      $build['customer_house'] = [
        '#markup' => '<div class="field field--name-customer-house field--label-inline">
    <div class="field__label">Дом,Корпус</div>
              <div class="field__item">' . $customer_house. '</div>
          </div>',
      ];
    }

    if ($customer_app = $order_data->app) {
      $build['customer_app'] = [
        '#markup' => '<div class="field field--name-customer-app field--label-inline">
    <div class="field__label">Квартира / Офис</div>
              <div class="field__item">' . $customer_app . '</div>
          </div>',
      ];
    }

    $build['mail'] = [
      '#markup' => '<div class="field field--name-customer-mail field--label-above">
    <div class="field__label">E-mail:</div>
              <div class="field__item">' . $bs_order->mail->value. '</div>
          </div>',
    ];

    \Drupal::moduleHandler()->invokeAll('bs_order_content_alter', [&$build, $bs_order]);

    return $build;

  }


  public function briefOrderOverview($bs_order_full_number) {

    $build = [
      '#markup' => '',
    ];

    $order_parts = explode('-', $bs_order_full_number);

    if (count($order_parts) != 2) {
      drupal_set_message(t('Sorry. But requested order not found.'), 'error');
      return $build;
    }

    $orders_storage = \Drupal::entityTypeManager()->getStorage('bs_order');

    $orders = $orders_storage->loadByProperties([
      'order_id' => $order_parts[0],
      'order_number' => $order_parts[1]
    ]);

    if (!$orders) {
      drupal_set_message(t('Sorry. But requested order not found.'), 'error');
      return $build;
    }

    $order = reset($orders);

    if (!$order) {
      return [];
    }

    $page = $this->entityTypeManager
    ->getViewBuilder($order->getEntityTypeId())
    ->view($order, 'new_order');

    $page['#entity_type'] = $order->getEntityTypeId();
    $page['#' . $page['#entity_type']] = $order;

    return $page;
  }


  public function repeatOrder($bs_order) {
    return [];
  }


  public function title(RouteMatchInterface $route_match, EntityInterface $_entity = NULL) {
    return $this->t('Your order @order_full_number', ['@order_full_number' => $_entity->getFullNumber()]);
  }

  public function editTitle(RouteMatchInterface $route_match, EntityInterface $_entity = NULL) {

    if ($entity = $this->doGetEntity($route_match, $_entity)) {
      return $this->t('Edit order %label', ['%label' => $entity->getFullNumber()]);
    }

    $this->t('Edit order');
  }


  public function userOrdersOverviewPage(UserInterface $user) {

    $connection = Database::getConnection();

    $oids_select = $connection->select('bs_order', 'o')
    ->fields('o', ['order_id']);

    $or_cond = new Condition('OR');
    $or_cond->condition('o.uid', $user->id())
      ->condition('o.mail', $user->getEmail());
      $oids_select->condition($or_cond);

    $oids = $oids_select->execute()
      ->fetchCol();

    $build['customer_orders'] = [
      '#prefix' => '<div class="customer_orders">',
      '#suffix' => '</div>',
      '#type' => 'view',
      '#name' => 'bs_orders',
      '#display_id' => 'block_customer_orders',
      '#arguments' => [implode('+', $oids)],
      '#embed' => TRUE,
    ];

//     $build['#cache'] = [
//       'max-age' => 0,
//     ];

    return $build;
  }

  public function userOrdersOverviewCheckAccess(UserInterface $user) {

    $current_user = \Drupal::currentUser();

    $result = AccessResult::allowedIfHasPermission($current_user, 'administer users');

    if ($result instanceof AccessResultAllowed) return $result;

    if ($current_user->id() == $user->id()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  private function prepareOrderForView($order, $view_mode = 'full') {
    $build = parent::view($order, $view_mode);

    $build['ordered_items'] = [
      '#prefix' => '<div class="order-items">',
      '#suffix' => '</div>',
      '#type' => 'view',
      '#name' => 'order_items',
      '#display_id' => 'block_order_items',
      '#arguments' => [$order->id()],
      '#embed' => TRUE,
    ];

    $build['#pre_render'][] = 'field_group_entity_view_pre_render';

    return $build;
  }

}

