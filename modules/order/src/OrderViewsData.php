<?php
namespace Drupal\bs_order;

use Drupal\views\EntityViewsData;

class OrderViewsData extends EntityViewsData {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

//     $data['bs_order']['order_adjustments']['field'] = [
//       'title' => t('Order adjustments total'),
//       'help' => t('Display Order adjustments total summ'),
//       'id' => 'bs_order_adjustments_total',
//     ];

    $data['bs_order']['order_full_number'] = [
      'title' => $this->t('Order full number'),
      'field' => [
        'id' => 'bs_order_full_number',
        'default_formatter' => 'string',
        // 'field_name' => 'order_full_number',
      ],
    ];

    $data['views']['bs_order_items_count'] = [
      'title' => t('Order items count'),
      'help' => t('Displays the order items count'),
      'real field' => 'order_id',
      'field' => [
        'id' => 'bs_order_items_count',
        'default_formatter' => 'number_integer',
      ],
    ];

    $data['views']['bs_order_total'] = [
      'title' => t('Order total'),
      'help' => t('Displays the order total amount info and adjustments.'),
      'area' => [
        'id' => 'bs_order_total',
      ],
    ];

    return $data;
  }
}

