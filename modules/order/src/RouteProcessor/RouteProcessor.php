<?php

namespace Drupal\bs_order\RouteProcessor;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;
use Drupal\bs_order\Entity\Order;

/**
 * Provides a route processor to replace <current>.
 */
class RouteProcessor implements OutboundRouteProcessorInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new RouteProcessorCurrent.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($route_name, Route $route, array &$parameters, BubbleableMetadata $bubbleable_metadata = NULL) {

    if (!empty($parameters['bs_order'])) {
      $route_options = $route->getOptions();
      if ($route_options['parameters']['bs_order']['type'] == 'bs_order_by_full_number') {
        if ($order = Order::load($parameters['bs_order'])) {
          $parameters['bs_order'] = $order->getFullNumber();
        }
      }
    }

  }

}
