<?php
namespace Drupal\bs_order\Entity;

use Drupal\bs_order\Event\OrderEvent;
use Drupal\bs_order\Event\OrderEvents;
use Drupal\bs_price\Price;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_product\Entity\Product;
use Drupal\bs_price\Plugin\Field\FieldType\PriceItem;
use Drupal\bs_price\EntityWithPriceAdjustmentTrait;

/**
 * Defines the order entity class.
 *
 * @ContentEntityType(
 *   id = "bs_order",
 *   label = @Translation("Customer order"),
 *   label_collection = @Translation("Orders"),
 *   label_singular = @Translation("order"),
 *   label_plural = @Translation("orders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order",
 *     plural = "@count orders",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_order\OrderStorage",
 *     "access" = "Drupal\bs_order\OrderAccessControlHandler",
 *     "view_builder" = "Drupal\bs_order\OrderViewBuilder",
 *     "permission_provider" = "Drupal\bs_order\OrderPermissionProvider",
 *     "views_data" = "Drupal\bs_order\OrderViewsData",
 *     "list_builder" = "Drupal\bs_order\OrderListBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_order\Form\OrderForm",
 *       "add" = "Drupal\bs_order\Form\OrderForm",
 *       "edit" = "Drupal\bs_order\Form\OrderForm",
 *       "delete" = "Drupal\bs_order\Form\OrderDeleteForm",
 *       "checkout" = "Drupal\bs_order\Form\CheckoutForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_order\Routing\OrderRouteProvider",
 *     },
 *   },
 *   base_table = "bs_order",
 *   admin_permission = "administer bs_order",
 *   entity_keys = {
 *     "id" = "order_id",
 *     "label" = "order_number",
 *     "uuid" = "uuid",
 *     "adjustment" = "adjustments",
 *   },
 *   links = {
 *     "canonical" = "/orders/{bs_order}",
 *     "repeat" = "/orders/{bs_order}/repeat",
 *     "brief-info" = "/order/view-order-brief/{bs_order_full_number}",
 *     "edit-form" = "/admin/beeshop/order/orders/list/{bs_order}/edit",
 *     "delete-form" = "/admin/beeshop/order/orders/list/{bs_order}/delete",
 *     "checkout-form" = "/checkout",
 *     "collection" = "/admin/beeshop/order/orders"
 *   },
 *   field_ui_base_route = "entity.bs_order.admin_form",
 *   common_reference_target = TRUE,
 *   constraints_by_groups = {}
 * )
 */
class Order extends ContentEntityBase implements OrderInterface {

  use EntityChangedTrait;
  use EntityWithPriceAdjustmentTrait;

  protected $saveOp;

  protected $changed_fields;

  protected $orderCurrencyCode;

  protected $inSaveProcess = FALSE;

  protected $onGetfield = [];

  public function &__get($name) {
    $result = parent::__get($name);

    $this->checkAndUpdateFieldOnGet($name, $result);

    return $result;
  }


  public function get($field_name) {
    $result = parent::get($field_name);

    $this->checkAndUpdateFieldOnGet($field_name, $result);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['order_number'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Order number'))
    ->setDescription(t('The order number displayed to the customer.'))
    ->setRequired(TRUE)
    ->setDefaultValue('')
    ->setSetting('max_length', 255)
    ->setDisplayConfigurable('view', TRUE)
    ->setReadOnly(TRUE);

    $fields['order_full_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order full number'))
      ->setDescription(t('The order full number displayed to the customer.'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shop_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shop'))
    ->setDescription(t('The shop to which the order belongs.'))
    ->setCardinality(1)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_shop')
    ->setSetting('handler', 'default')
    ->setTranslatable(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Customer'))
    ->setDescription(t('The customer.'))
    ->setSetting('target_type', 'user')
    ->setSetting('handler', 'default')
    ->setDefaultValueCallback('Drupal\bs_order\Entity\Order::getCurrentUserId')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'author',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['user_langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Customer language'))
    ->setDescription(t('The user language on which the order was placed.'));

    $fields['mail'] = BaseFieldDefinition::create('email')
    ->setLabel(t('Contact email'))
    ->setDescription(t('The email address associated with the order.'))
    ->setDefaultValue('')
    ->setSetting('max_length', 255)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'weight' => 0,
    ])
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['order_items'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Order items'))
    ->setDescription(t('The order items.'))
    ->setSetting('target_type', 'bs_order_item')
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setSetting('handler', 'default')
    ->setSetting('onChange', [
      '::updateTotalByProductsAmount',
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setCustomStorage(TRUE);

    $fields['total_by_products_amount'] = BaseFieldDefinition::create('bs_price')
      ->setLabel(t('Total amount by products'))
      ->setDescription(t('The total price of the order.'))
      ->setReadOnly(TRUE)
      ->setSetting('presave_callback', [
        'entity::totalByProductsAmountPresave',
      ])
      ->setSetting('onGet', [
        '::updateTotalByProductsAmount',
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        //'type' => 'commerce_order_total_summary',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total_amount'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Total amount'))
    ->setDescription(t('The total price of the order.'))
    ->setReadOnly(TRUE)
    ->setSetting('presave_callback', [
      'entity::totalAmountPresave',
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      //'type' => 'commerce_order_total_summary',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', FALSE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order status'))
      ->setDescription(t('The order status.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bs_order_state')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'sort' => [
          'field' => 'weight',
          'order' => 'ASC',
        ],
      ])
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['adjustments'] = BaseFieldDefinition::create('bs_price_adjustment')
      ->setLabel(t('Adjustments'))
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'bs_price_adjustment_default',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setSetting('source_field', 'total_by_products_amount')
      ->setSetting('target_field', 'total_amount');

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Order placement date'))
    ->setDescription(t('The time when the order was placed.'))
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'timestamp',
      'weight' => 0,
    ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
    ->setLabel(t('Changed'))
    ->setDescription(t('The time when the order was last edited.'))
    ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }


  public function getAdjustments() {

  }

//   public function updateOrderTotals() {
//     $tootal_by_order = new Price(0, $this->getOrderCurrencyCode());

// //     $total_by_order_products = 0;

//     //$total_by_products = new Price(0, 'RUR');



//     $order_data = $this->get('data')->first();

//     if ($shipping_price = $order_data->shipping_price) {
//       //$shipping_price = new Price($shipping_price_value, $tootal_by_order->getCurrencyCode());
//       //$tootal_by_order += $shipping_price;
//       $tootal_by_order->add($shipping_price);
//     }

//     if ($order_adjustments = $this->getData('adjustments')) {
//       foreach ($order_adjustments as $order_adjustment) {
//         if (!isset($order_adjustment['price'])) continue;
//         $tootal_by_order->add($order_adjustment['price']);
//       }
//     }

//     $this->set('total_amount', [
//       'value' => $tootal_by_order->getPriceValue(),
//       'currency_code' => $tootal_by_order->getCurrencyCode(),
//     ]);

// //     $this->set('total_by_products_amount', [
// //       'value' => $total_by_products->getPriceValue(),
// //       'currency_code' => $total_by_products->getCurrencyCode(),
// //     ]);
//   }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  public function getCustomerId() {
    return $this->get('uid')->target_id;
  }

  public function getEmail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getItems() {
    $items_entities = [];

    foreach ($this->get('order_items') as $delta => $order_item_reference) {
      if ($order_item_reference->entity) $items_entities[] = $order_item_reference->entity;
    }

    return $items_entities;//$this->get('order_items')->referencedEntities();
  }

  public function isStateChanged() {

    if (empty($this->values['original'])) return TRUE;

    return $this->getState() != $this->values['original']->getState();
  }

  public function getFullNumber() {

    if ($this->isNew()) {
      return FALSE;
    }

    if (!$this->get('order_full_number')->isEmpty()) {
      return $this->get('order_full_number')->value;
    }

    $this->set('order_full_number', $this->id() . '-' . $this->get('order_number')->value);

    return $this->get('order_full_number')->value;
  }

  public function getOrderTotalByItemsPrice() {
    $items = $this->getItems();

    $total_by_items_price = NULL;

    foreach ($items as $order_item) {
      if ($item_price = $order_item->get('total_price')->first()) {
        if (!$total_by_items_price) {
          $total_by_items_price = $item_price->toPrice();
        }
        else {
          $total_by_items_price->add($item_price->toPrice());
        }
      }
    }

    return $total_by_items_price;
  }

  /**
   * {@inheritdoc}
   */
  public function getProductsListForDL() {

    $products = [];

    /**
     * @var \Drupal\bs_product\Normalizer\ProductDataLayerJsonNormalizer $product_normalizer
     */
    $product_normalizer = \Drupal::service('serializer.normalizer.bs_product.datalayer_json');

    foreach ($this->getItems() as $order_item) {
      if ($product = $order_item->getProduct()) {
        $norm_product = $product_normalizer->normalize($product);
        $norm_product['quantity'] = intval($order_item->getQuantity());
        $norm_product['price'] = $order_item->getPrice()->value;
        $products[] = $norm_product;
      }
    }

    return $products;
  }

  public function getState() {
    if ($states = $this->get('state')->referencedEntities()) {
      return reset($states);
    }
    return NULL;
  }

  public function getUserLangcode() {
    return $this->get('user_langcode')->value;
  }

  public function getShopId() {
    return $this->get('shop_id') ?
           $this->get('shop_id')->target_id :
           null;
  }

  public function getShippingPrice() {
    $order_data = $this->get('data')->first();

    if (empty($order_data->shipping_price)) {
      return NULL;
    }

    return $order_data->shipping_price instanceof Price ?
           $order_data->shipping_price :
           new Price($order_data->shipping_price, 'RUR');
  }

  public function getShop() {
    if ($shops = $this->get('shop_id')->referencedEntities()) {
      return reset($shops);
    }

    return NULL;
  }

  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderAdjustmentsTotalPrice() {

    $adjustments_total = new Price(0, $this->getOrderCurrencyCode());

    if ($order_adjustments = $this->getData('adjustments')) {
      foreach ($order_adjustments as $order_adjustment) {
        if (!isset($order_adjustment['price'])) continue;
        $adjustments_total->add($order_adjustment['price']);
      }
    }

    return $adjustments_total;
  }

  public function getOrderCurrencyCode() {

    if ($this->orderCurrencyCode)
      return $this->orderCurrencyCode;

    foreach ($this->getItems() as $order_item) {
      if ($item_price = $order_item->getPrice()) {
        $this->orderCurrencyCode = $item_price->getCurrencyCode();
        return $this->orderCurrencyCode;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderTotalAmountPrice() {

    $this->updateTotalAmount();

    if ($total_amount = $this->get('total_amount')->first()) {
      $total_amount_price = $total_amount->toPrice();
    }

    return $total_amount_price;
  }

  public function getData($data_key) {
    if ($data = $this->get('data')->first()) {
      return $data->{$data_key};
    }
    return NULL;
  }

  public function setData($key, $value) {
    if ($data = $this->get('data')->first()) {
      $data->set($key, $value);
      return $this;
    }
    else {
      $data = [
        $key => $value,
      ];
    }

    $this->set('data', $data);
    return $this;
  }

  public function totalAmountPresave(PriceItem $item) {
//     $total_by_products = $this->get('total_by_products_amount')->first();
//     $total_by_products->presave();

//     $item->setValue($total_by_products->toPrice());
    $this->updateTotalAmount();
  }

  public function updateTotalAmount() {
    $total_by_products = $this->get('total_by_products_amount')->first();
    $total_by_products->presave();

    $this->set('total_amount', $total_by_products->toPrice());

    $this->get('adjustments')->preSave();

  }

  public function totalByProductsAmountPresave(PriceItem $item) {

    $total_by_products = new Price(0, $this->getOrderCurrencyCode());
    foreach ($this->getItems() as $order_item) {
      if ($order_item_total = $order_item->getItemTotalPrice()) {

        $total_by_products->add($order_item_total);
      }
    }

    $item->setValue($total_by_products);
  }

  public function updateTotalByProductsAmount() {

    $total_by_products = FALSE;
    foreach ($this->getItems() as $order_item) {

      if (!$total_by_products) {
        $total_by_products = $order_item->getItemTotalPrice();
      }
      else {
        if ($order_item_total = $order_item->getItemTotalPrice()) {

          $total_by_products->add($order_item_total);
        }
      }

    }

    $this->set('total_by_products_amount', $total_by_products);

  }

  public function addItem($order_item) {
    $this->order_items->appendItem($order_item);
    $this->onChange('order_items');
  }

  public function deleteItem(OrderItemInterface $order_item_to_delete) {

    foreach ($this->getItems() as $index => $order_item) {
      if ($order_item_to_delete == $order_item) {
        $order_item->delete();
        $this->order_items->removeItem($index);
      }
    }
  }

  public function addPoductById($pid, $quantity = 1) {

    if ($product = Product::load($pid)) {
      $order_item_values = [
        'pid' => $pid,
        'quantity' => $quantity,
      ];
      $order_items_storage = \Drupal::entityTypeManager()->getStorage('bs_order_item');
      $order_item = $order_items_storage->create($order_item_values);

      if ($product_price = $product->getActualPrice()) {

        $price = $product_price->toPrice();

        $order_item->set('price', [
          'value' => $price->getPriceValue(),
          'currency_code' => 'RUR',
        ]);

        $order_item->set('total_price', [
          'value' => $price->getPriceValue() * $quantity,
          'currency_code' => 'RUR',
        ]);

        $this->addItem($order_item);

        return $order_item;
      }


    }

  }

  public function preSave(EntityStorageInterface $storage) {

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = new OrderEvent($this);
    $dispatcher->dispatch(OrderEvents::ORDER_PRESAVE, $event);

    parent::preSave($storage);

    if (!$this->user_langcode->value) {
      $languageManager = \Drupal::languageManager();
      $current_lang = $languageManager->getCurrentLanguage()->getId();
      $this->set('user_langcode', $current_lang);
    }

    if ($this->isNew()) {
      $number = substr(preg_replace('#[^0-9]#', '', sha1(time() . rand(0, 1000))), 0, 8);
      $this->set('order_number', $number);

      $languageManager = \Drupal::languageManager();
      $current_lang = $languageManager->getCurrentLanguage()->getId();
      $this->set('user_langcode', $current_lang);
    }
  }



  public function save() {
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = $this->id() . '-' . parent::label();

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($name) {
    parent::onChange($name);
    $this->changed_fields[$name] = $this->get($name)->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);
    foreach ($entities as $entity) {
      $entity->set('order_full_number', $entity->id() . '-' . $entity->get('order_number')->value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = new OrderEvent($this);
    $dispatcher->dispatch(OrderEvents::ORDER_CREATE, $event);

  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    $order_items = [];
    /** @var \Drupal\bs_order\Entity\OrderInterface $entity */
    foreach ($entities as $entity) {
      foreach ($entity->getItems() as $order_item) {
        $order_items[$order_item->id()] = $order_item;
      }
    }

    $order_item_storage = \Drupal::service('entity_type.manager')->getStorage('bs_order_item');
    $order_item_storage->delete($order_items);


    foreach ($entities as $entity) {

      $dispatcher = \Drupal::service('event_dispatcher');
      $event = new OrderEvent($entity);
      $dispatcher->dispatch(OrderEvents::ORDER_DELETE, $event);

    }

  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    foreach ($this->getItems() as $order_item) {
      if ($order_item->getOrderId() != $this->id()) {
        $order_item->set('order_id', $this->id());
      }

      if ($order_item->getQuantity() > 0.0) {
        $order_item->save();
      }
      else {
        $order_item->delete();
      }

    }

    parent::postSave($storage, $update);

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = new OrderEvent($this);

    if ($update) {
      $dispatcher->dispatch(OrderEvents::ORDER_UPDATE, $event);

      if ($this->isStateChanged()) {
        $dispatcher->dispatch(OrderEvents::ORDER_STATE_CHANGED, $event);
      }
    }
    else {
      $dispatcher->dispatch(OrderEvents::ORDER_INSERT, $event);
    }

    if ($this->cart) {
      $this->cart->clear();
    }
  }

  public static function splitFullOrderNumber($order_full_number) {
    if (preg_match('#^(?<id>\d{1,})-(?<number>\w{1,})$#', $order_full_number, $match)) {
      return ['order_id' => $match['id'], 'order_number' => $match['number']];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    $link_templates = $this->linkTemplates();

    if (isset($link_templates[$rel]) && preg_match('#\{bs_order_full_number\}#ui', $link_templates[$rel])) {
      $uri_route_parameters['bs_order_full_number'] = $this->getFullNumber();
    }

    return $uri_route_parameters;
  }


  protected function checkAndUpdateFieldOnGet($field_name, $value) {
    if (!isset($this->onGetfield[$field_name]) && isset($this->fieldDefinitions[$field_name])) {

      $this->onGetfield[$field_name] = $field_name;

      if ($on_get_callbacks = $this->fieldDefinitions[$field_name]->getSetting('onGet')) {
        foreach ($on_get_callbacks as $callback) {
          $callback = $this->prepareCallback($callback);
          if (is_callable($callback)) {
            $callback($value);
          }
        }
      }

      unset($this->onGetfield[$field_name]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCallback($callback) {
    if (is_string($callback) && substr($callback, 0, 2) == '::') {
      $callback = [$this, substr($callback, 2)];
    }
    return $callback;
  }
}

