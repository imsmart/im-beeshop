<?php
namespace Drupal\bs_order\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_price\Plugin\Field\FieldType\PriceItem;

interface OrderItemInterface extends ContentEntityInterface {

  public function getPid();


  /**
   * @return \Drupal\bs_product\Entity\ProductInterface|NULL
   */
  public function getProduct();

  /**
   * @return PriceItem|NULL
   */
  public function getPrice();

  public function getQuantity();

  /**
   * @param integer|float $new_quantity_value
   *
   * @return OrderItemInterface
   */
  public function setQuantity($new_quantity_value);

}

