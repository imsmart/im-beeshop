<?php

namespace Drupal\bs_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\bs_order\OrderStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\filter\Plugin\FilterInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines the currency entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_order_state",
 *   label = @Translation("Order state"),
 *   label_singular = @Translation("order state"),
 *   label_plural = @Translation("order states"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order state",
 *     plural = "@count order states",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_order\OrderStateStorage",
 *     "form" = {
 *       "add" = "Drupal\bs_order\Form\OrderStateForm",
 *       "edit" = "Drupal\bs_order\Form\OrderStateForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_order\OrderStateListBuilder",
 *   },
 *   admin_permission = "administer beeshop order states",
 *   config_prefix = "state",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *     "parent" = "parent"
 *   },
 *   inheritable_options = {
 *
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "description",
 *     "uuid",
 *     "sid",
 *     "weight",
 *     "parent",
 *     "send_email_to_customer",
 *     "send_email_to_manager",
 *     "edit_permitions",
 *     "actuality_time",
 *     "nex_state_on_actuality_exprired",
 *     "mails",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/order/order_states/add",
 *     "edit-form" = "/admin/beeshop/order/order_states/{bs_order_state}",
 *     "delete-form" = "/admin/beeshop/order/order_states/{bs_order_state}/delete",
 *     "collection" = "/admin/beeshop/order/order_states",
 *     "shop-collection" = "/admin/beeshop/config/shops/{bs_shop}/order_states/overview"
 *   }
 * )
 *
 */
class OrderState extends ConfigEntityBase implements OrderStateInterface {

  /**
   * A brief description of this store type.
   *
   * @var string
   */
  protected $description;

  /**
   * A price type weight.
   *
   * @var int
   */
  protected $weight;

  protected $parentStateEntity;

//   public function label() {
//     $cloned = clone $this;
//     $langcode = \Drupal::currentUser()->getPreferredLangcode();
//     dpm($langcode);
//     $translation = $this->entityRepository()->getTranslationFromContext($cloned, $langcode);
//     dpm($translation);

//     $translation = \Drupal::entityManager()->getTranslationFromContext($cloned, $langcode);
//     dpm($translation);

//     return parent::label();
//   }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  public function getCustomerMessageSubject() {
    return $this->customer_message_subject;
  }

  public function getCustomerMessageTemplate() {
    return !empty($this->customer_message_template['value']) ? $this->customer_message_template['value'] : '';
  }

  public function getCustomerMessageTemplateFormat() {
    return !empty($this->customer_message_template['format']) ? $this->customer_message_template['format'] : '';
  }


  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailParams($recipient_type = 'customer', $token_data = NULL, $token_replace_options = []) {
    $mails = $this->get('mails');

    if (!empty($mails[$recipient_type]) && $token_data !== NULL) {
      foreach ($mails[$recipient_type] as $key => $value) {
        if ($value && $key != 'body') {
          $mails[$recipient_type][$key] = $this->token()->replace($value, $token_data, $token_replace_options);
        }

        if ($key == 'body' && !empty($value['value'])) {
          $mails[$recipient_type][$key] = Markup::create($this->token()->replace($value['value'], $token_data, $token_replace_options));

//           $format_id = !empty($value['format']) ?
//                        $value['format'] :
//                        static::configFactory()->get('filter.settings')->get('fallback_format');
//           /** @var \Drupal\filter\Entity\FilterFormat $format **/
//           $format = FilterFormat::load($format_id);

//           if (!$format || !$format->status()) {
//             $message = !$format ? 'Missing text format: %format.' : 'Disabled text format: %format.';
//             static::logger('filter')->alert($message, ['%format' => $format_id]);
//             $mails[$recipient_type][$key]['value'] = '';
//           }
//           else {

//             $filter_types_to_skip = [];

//             $filter_must_be_applied = function(FilterInterface $filter) use ($filter_types_to_skip) {
//               $enabled = $filter->status === TRUE;
//               $type = $filter->getType();
//               // Prevent FilterInterface::TYPE_HTML_RESTRICTOR from being skipped.
//               $filter_type_must_be_applied = $type == FilterInterface::TYPE_HTML_RESTRICTOR || !in_array($type, $filter_types_to_skip);
//               return $enabled && $filter_type_must_be_applied;
//             };

//             // Convert all Windows and Mac newlines to a single newline, so filters only
//             // need to deal with one possibility.
//             $text = str_replace(["\r\n", "\r"], "\n", $text);

//             // Get a complete list of filters, ordered properly.
//             /** @var \Drupal\filter\Plugin\FilterInterface[] $filters **/
//             $filters = $format->filters();

//             // Give filters a chance to escape HTML-like data such as code or formulas.
//             foreach ($filters as $filter) {
//               if ($filter_must_be_applied($filter)) {
//                 $text = $filter->prepare($text, $langcode);
//               }
//             }

//             // Perform filtering.
//             $metadata = BubbleableMetadata::createFromRenderArray($element);
//             foreach ($filters as $filter) {
//               if ($filter_must_be_applied($filter)) {
//                 $result = $filter->process($text, $langcode);
//                 $metadata = $metadata->merge($result);
//                 $text = $result->getProcessedText();
//               }
//             }

//             // Filtering and sanitizing have been done in
//             // \Drupal\filter\Plugin\FilterInterface. $text is not guaranteed to be
//             // safe, but it has been passed through the filter system and checked with
//             // a text format, so it must be printed as is. (See the note about security
//             // in the method documentation above.)
//               $element['#markup'] = FilteredMarkup::create($text);
//           }
        }
      }
    }

    return !empty($mails[$recipient_type]) ? $mails[$recipient_type] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    $allways_allowed = [
      'view', 'view label'
    ];

    if (in_array($operation, $allways_allowed)) {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

  /**
   *
   * @return \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected function entityRepository() {
    return \Drupal::service('entity.repository');
  }

  /**
   * Gets the token service.
   *
   * @return \Drupal\Core\Utility\Token
   */
  protected function token() {
    return \Drupal::token();
  }
}