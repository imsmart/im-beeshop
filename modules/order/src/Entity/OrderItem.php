<?php
namespace Drupal\bs_order\Entity;

use Drupal\bs_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_price\Price;

/**
 * Defines the cart item entity class.
 *
 * @ContentEntityType(
 *   id = "bs_order_item",
 *   label = @Translation("Order item"),
 *   admin_permission = "access cart",
 *   handlers = {
 *     "views_data" = "Drupal\bs_order\OrderItemsViewsData",
 *   },
 *   base_table = "bs_order_items",
 *   entity_keys = {
 *     "id" = "order_item_id",
 *     "uuid" = "uuid",
 *   },
 *   common_reference_target = TRUE
 * )
 */
class OrderItem extends ContentEntityBase implements OrderItemInterface {

  /**
   * @var Drupal\bs_order\Entity\Order
   */
  protected $orderEntity;

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The order backreference, populated by Order::postSave().
    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Referenced order'))
    ->setDescription(t('The order id.'))
    ->setSetting('target_type', 'bs_order')
    ->setReadOnly(TRUE);

    $fields['pid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Product'))
    ->setDescription(t('The reference to product.'))
    ->setSetting('target_type', 'bs_product')
    ->setSetting('handler', 'base:bs_product')
    ->setRequired(TRUE);

    $fields['quantity'] = BaseFieldDefinition::create('decimal')
    ->setLabel(t('Quantity'))
    ->setDescription(t('The number of units.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE)
    ->setDefaultValue(1)
    ->setDisplayOptions('form', [
      'type' => 'number',
      'weight' => 1,
    ]);

    $fields['price'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Order item price'))
    ->setDescription(t('The price of a single unit.'))
    ->setReadOnly(TRUE);

    $fields['total_price'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Total price'))
    ->setDescription(t('The total price of the item in cart.'))
    ->setReadOnly(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

  public function getItemTotalPrice() {
    $item_price = $this->getPrice() ? $this->getPrice()->toPrice() : NULL;
    if (!$item_price) {
      return NULL;
    }
    $item_quantity = $this->getQuantity();
    return $item_price->multiply($item_quantity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  public function getOrderId() {
    return $this->get('order_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPid() {
    return $this->get('pid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice() {
      return $this->price ? $this->price->first() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalPrice() {
    return !$this->total_price->isEmpty() ? $this->total_price->first() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getProduct() {
    return $this->get('pid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity() {
    return (float) $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity($new_quantity_value) {
    if (!is_numeric($new_quantity_value)) {
      throw new \Exception('Order item quantity must be numeric value');
    }
    $this->set('quantity', $new_quantity_value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    if ($this->id() < 0) {
      $this->set('order_item_id', NULL);
      $this->enforceIsNew();
    }

    parent::preSave($storage);
    $this->total_price->value = $this->getItemTotalPrice() ?
                                $this->getItemTotalPrice()->getPriceValue() :
                                0;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderEntity($order) {
    $this->orderEntity = $order;
  }

}

