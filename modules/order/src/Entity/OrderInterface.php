<?php
namespace Drupal\bs_order\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\bs_order\OrderStateInterface;

interface OrderInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   *
   * @return \Drupal\bs_order\OrderStateInterface or NULL
   */
  public function getState();

  public function getFullNumber();

  public function getEmail();

  /**
   * @return \Drupal\bs_order\Entity\OrderItemInterface[]
   */
  public function getItems();

  /**
   *
   * @param string $key
   * @param mixed $value
   */
  public function setData($key, $value);

}

