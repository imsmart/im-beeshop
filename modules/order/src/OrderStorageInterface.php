<?php
namespace Drupal\bs_order;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\bs_cart\CartInterface;

interface OrderStorageInterface extends ContentEntityStorageInterface {

  /**
   * Constructs a new order entity object by cart, without permanently saving it.
   *
   * @param CartInterface $cart
   *   Cart
   *
   * @return \Drupal\bs_order\Entity\OrderInterface
   *   A new order entity object.
   */
  public function createByCart(CartInterface $cart = NULL);

}

