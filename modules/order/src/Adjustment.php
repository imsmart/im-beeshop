<?php
namespace Drupal\bs_order;

class Adjustment {

  /**
   * The adjustment label.
   *
   * @var string
   */
  protected $label;

  /**
   * The adjustment amount.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $amount;

  /**
   * The adjustment type.
   *
   * @var string
   */
  protected $type;

  /**
   * The source entity type of the adjustment.
   *
   * The source entity type, if known. For example, a promotion or promotion coupon entity for
   * a discount adjustment.
   *
   * @var string
   */
  protected $sourceEntityType;

  /**
   * The source identifier of the adjustment.
   *
   * Points to the source object, if known. For example, a promotion or promotion coupon entity for
   * a discount adjustment.
   *
   * @var string
   */
  protected $sourceId;


}

