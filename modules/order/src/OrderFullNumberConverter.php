<?php
namespace Drupal\bs_order;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Routing\Route;
use Drupal\bs_order\Entity\Order;

class OrderFullNumberConverter implements ParamConverterInterface {

  /**
   * The Entity type manager
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrderFullNumberConverter.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {

    if ($order_number = Order::splitFullOrderNumber($value)) {
      $order_storage = $this->entityTypeManager->getStorage('bs_order');

      if ($orders = $order_storage->loadByProperties($order_number)) {
        return reset($orders);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {

    return $name == 'bs_order' && !empty($definition['type']) && $definition['type'] == 'bs_order_by_full_number';;
  }
}

