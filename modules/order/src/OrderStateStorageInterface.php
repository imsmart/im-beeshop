<?php
namespace Drupal\bs_order;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

interface OrderStateStorageInterface extends ConfigEntityStorageInterface {
}

