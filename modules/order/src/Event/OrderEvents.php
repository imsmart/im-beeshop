<?php

namespace Drupal\bs_order\Event;

final class OrderEvents {

  /**
   * Name of the event fired after creating a new order.
   *
   * Fired before the order is saved.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_CREATE = 'bs_order.create';

  /**
   * Name of the event fired before saving an order.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_PRESAVE = 'bs_order.presave';

  /**
   * Name of the event fired after saving a new order.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_INSERT = 'bs_order.insert';

  /**
   * Name of the event fired after saving an existing order.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_UPDATE = 'bs_order.update';

  /**
   * Name of the event fired after changing order state.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_STATE_CHANGED = 'bs_order.state_changed';

  /**
   * Name of the event fired before deleting an order.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_PREDELETE = 'bs_order.predelete';

  /**
   * Name of the event fired after deleting an order.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const ORDER_DELETE = 'bs_order.delete';

  /**
   * Name of the event fired after loading an order item.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_LOAD = 'bs_order.bs_order_item.load';

  /**
   * Name of the event fired after creating a new order item.
   *
   * Fired before the order item is saved.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_CREATE = 'bs_order.bs_order_item.create';

  /**
   * Name of the event fired before saving an order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_PRESAVE = 'bs_order.bs_order_item.presave';

  /**
   * Name of the event fired after saving a new order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_INSERT = 'bs_order.bs_order_item.insert';

  /**
   * Name of the event fired after saving an existing order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_UPDATE = 'bs_order.bs_order_item.update';

  /**
   * Name of the event fired before deleting an order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_PREDELETE = 'bs_order.bs_order_item.predelete';

  /**
   * Name of the event fired after deleting an order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_order\Event\OrderItemEvent
   */
  const ORDER_ITEM_DELETE = 'bs_order.bs_order_item.delete';

}
