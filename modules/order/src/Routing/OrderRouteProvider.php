<?php

namespace Drupal\bs_order\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for the Order entity.
 */
class OrderRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($checkout_route = $this->getCheckoutFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.checkout_form", $checkout_route);
    }

    if ($repeat_route = $this->getRepeatOrderRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.repeat", $repeat_route);
    }

    return $collection;
  }

  protected function getRepeatOrderRoute($entity_type) {
    if ($entity_type->hasLinkTemplate('repeat')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('repeat'));
      // Use the edit form handler, if available, otherwise default.

      $route
        ->setDefaults([
          '_controller' => "\Drupal\bs_order\Controller\OrderController::repeatOrder",
        ])
        ->setRequirement('_entity_access', "bs_order.view")
//         ->setRequirement('_permission', 'access content')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

          // Entity types with serial IDs can specify this in their route
          // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCanonicalRoute($entity_type);
    // Replace the 'full' view mode with the 'admin' view mode.
//     $route->setDefault('_entity_view', 'bs_order.admin');
    $route = parent::getCanonicalRoute($entity_type);

    $route->setDefault('_title_callback', '\Drupal\bs_order\Controller\OrderController::title');


//     $parameters = [
//       'bs_order' => [
//         'type' => 'bs_order_by_full_number',
//       ]
//     ];

// //     $route->setRequirement('bs_order_by_full_number', '\w+');

//     $route->setOption('parameters', $parameters);

//     $route->setDefaults([
//       '_controller' => 'Drupal\bs_order\Controller\OrderController::orderOverviewPage',
//       '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
//     ]);
    return $route ? $route : NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getEditFormRoute($entity_type)) {
      $route->setDefault('_title_callback', '\Drupal\bs_order\Controller\OrderController::editTitle');
      return $route;
    }
  }

  protected function getCheckoutFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('checkout-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('checkout-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('checkout')) {
        $operation = 'checkout';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\bs_order\Controller\OrderController::getCheckoutPageTitle',
        ])
        ->setRequirement('_custom_access', "bs_order_checkAccessToCheckout")
        ->setRequirement('_permission', 'access content')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

          // Entity types with serial IDs can specify this in their route
          // requirements, improving the matching process.
          if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
            $route->setRequirement($entity_type_id, '\d+');
          }
          return $route;
    }
  }

}
