<?php

/**
* @file
* Contains \Drupal\bs_order\EventSubscriber.
*/

namespace Drupal\bs_order\EventSubscriber;

use Drupal\bs_order\Event\OrderEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\bs_order\OrderStateInterface;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\bs_order\Event\OrderEvents;
use Drupal\beeshop\BeeshopRuntimeLanguageSwitcherTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\beeshop\Session\BeeshopDataLayerSessionBag;


/**
 * Class OrderEventsSubscriber.
 *
 * @package Drupal\bs_order
 */
class OrderEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;
  use BeeshopRuntimeLanguageSwitcherTrait;

  protected $configFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var Request
   */
  protected $request;

  /**
   *
   * @var Session
   */
  protected $session;

  /**
   * Constructs a new OrderReceiptSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager, MailManagerInterface $mail_manager,
    Renderer $renderer, RequestStack $request_stack, Session $session) {
      $this->configFactory = $config_factory;
      $this->entityTypeManager = $entity_type_manager;
      $this->languageManager = $language_manager;
      $this->mailManager = $mail_manager;
      $this->renderer = $renderer;
      $this->request = $request_stack->getCurrentRequest();
      $this->session = $session;
      $this->moduleHandler = \Drupal::moduleHandler();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderEvents::ORDER_INSERT] = ['onOrderInsert', -100];
    $events[OrderEvents::ORDER_STATE_CHANGED] = ['onOrderStateChanged', -100];

    return $events;
  }

  public function onOrderStateChanged(OrderEvent $event) {
    $order = $event->getOrder();

    $langcode = $order->getUserLangcode();
    $this->switchLanguageTo($langcode);

    if ($order_state = $order->getState()) {
      $this->sendNotificationsByOrderState($order_state, $order);
    }

    $this->restoreSwitchedLanguage();
  }

  public function onOrderInsert(OrderEvent $event) {

    /**
     * @var OrderInterface $order
     */
    $order = $event->getOrder();

    if ($order_state = $order->getState()) {
      $this->sendNotificationsByOrderState($order_state, $order);
    }

    /**
     * @var BeeshopDataLayerSessionBag $data_layer_bag
     */
    $data_layer_bag = $this->session->getBag(BeeshopDataLayerSessionBag::BAG_NAME);

    $info = [
      'event' => 'ee-event',
      'ee-event-category' => 'Enhanced Ecommerce',
      'ee-event-action' => 'purchase',
      'ee-event-label' => 'purchase',
      'ee-event-value' => $order->get('total_by_products_amount')->value,
      'ee-event-ni' => 'False',
      'ecommerce' => [
        'purchase' => [
          'actionField' => [
            'id' => $order->getFullNumber(),
            'revenue' => $order->get('total_by_products_amount')->value,
          ],
          'products' => [

          ],
        ]
      ],
    ];

    foreach ($order->getItems() as $order_item) {
      if ($product = $order_item->getProduct()) {
        $product_info = [
          'name' => $product->label(),
          'id' => $product->id(),
          'quantity' => intval($order_item->getQuantity()),
          'price' => $order_item->getPrice()->value,
        ];

        $info['ecommerce']['purchase']['products'][] = $product_info;
      }
    }

    try {
      $this->moduleHandler->alter('bs_order_datalayer_info', $info, $order);
    }
    catch (\Exception $e) {
      watchdog_exception('onOrderInser:bs_order_datalayer_info_alter', $e);
    }

    $data_layer_bag->add($info);

  }

  protected function sendNotificationsByOrderState(OrderStateInterface $order_state, OrderInterface $order) {

    $langcode = $order->getUserLangcode();

    $token_data = [
      'bs_order' => $order,
    ];

    $token_replace_options = ['langcode' => $langcode, 'clear' => TRUE];

    if ($order_state->get('send_email_to_manager')) {
      $order_admin_email =  bs_order_get_order_admin_email();
      if ($mail_params = $order_state->getEmailParams('manager', $token_data, $token_replace_options)) {
        $mail_params['#order'] = $order;
        $order_admin_email = !empty($mail_params['recipients']) ? $mail_params['recipients'] : $order_admin_email;

        $this->getModuleHandler()->alter('bs_order_admin_notification_recipients', $order_admin_email, $order);

        $this->mailManager->mail('beeshop', 'bs_order.satate_changed.admin', $order_admin_email, $langcode, $mail_params);
      }
    }

    if ($order_state->get('send_email_to_customer')) {

      if ($mail_params = $order_state->getEmailParams('customer', $token_data, $token_replace_options)) {
        $this->mailManager->mail('beeshop', 'bs_order.satate_changed.customer', $order->mail->value, $langcode, $mail_params);
      }
    }

  }

  /**
   * Gets the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected function getModuleHandler() {
    if (!isset($this->moduleHandler)) {
      $this->moduleHandler = \Drupal::moduleHandler();
    }
    return $this->moduleHandler;
  }
}

