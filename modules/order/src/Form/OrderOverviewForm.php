<?php

namespace Drupal\bs_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class OrderOverviewForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'order_overview_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {

    $request = \Drupal::request();

    $order_number_default = $request->getSession()->get('placed_order_number', '') ?: $this->getOrderNumberFromRequest();


    $form['order_number'] = [
      '#type' => 'bs_order_full_number',
      '#title' => $this->t('Order number'),
      '#default_value' => $order_number_default,
    ];

    $form['actions'] = [
      '#type' => 'actions'
    ];

    $form['actions']['view_order'] = [
      '#type' => 'submit',
      '#value' => $this->t('View order'),
    ];

    $form['order_info'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'order-info-container',
        ]
      ]
    ];

    $order_number = $form_state->getValue('order_number', '') ?: $order_number_default;

    if ($request->getSession()->get('placed_order_number', NULL)) {
      $request->getSession()->set('placed_order_number', NULL);
    }

    if ($order_number) {
      $orders_storage = \Drupal::entityTypeManager()->getStorage('bs_order');
      $order_number_parts = explode('-', $order_number, 2);

      if ($orders = $orders_storage->loadByProperties([
        'order_id' => $order_number_parts[0],
        'order_number' => $order_number_parts[1]
      ])) {
        $order = reset($orders);

        $form['order_info']['order'] = \Drupal::entityTypeManager()
          ->getViewBuilder('bs_order')
          ->view($order, 'brief_order_information'/*, $langcode*/);
        $form['#order'] = $order;
      }

    }

    $form['#theme'] = 'bs_order_order_overview_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!$form_state->getErrors('order_number')) {
      $form_state->setValue('order_number', $form['order_number']['#value']);
    }
    else {
      $form_state->setValue('order_number', '');

      if (!empty($form['order_info']['order'])) {
        unset($form['order_info']['order']);
      }
    }

    $form_state->setRebuild();

//     dpm($form);
//     dpm($form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritDoc}
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    return new static();
  }

  protected function getOrderNumberFromRequest() {
    $config = $this->configFactory()->get('beeshop.order.settings');

    $keys = [];

    if ($keys_text = $config->get('order_overview_form_order_number_keys')) {
      $keys = explode(PHP_EOL, $keys_text);

      foreach ($keys as $key) {
        if ($val = \Drupal::request()->get($key, NULL)) {
          return $val;
        }
      }
    }

    return '';
  }

}

