<?php

namespace Drupal\bs_order\Form;

use Drupal\bs_order\Event\OrderEvent;
use Drupal\bs_order\Event\OrderEvents;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Form\ViewsForm;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_price\PriceAdjustmentTypePluginManager;


/**
 * Form controller for the commerce_order entity edit forms.
 */
class OrderForm extends ContentEntityForm {

  /**
   * The conig factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\bs_order\Entity\OrderInterface
   */
  protected $entity;

  /**
   * Constructs a new OrderForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, DateFormatterInterface $date_formatter, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_manager, $entity_type_bundle_info, $time);

    $this->dateFormatter = $date_formatter;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    if ($this->getOperation() == 'checkout') {
      $form['#cache'] = ['max-age' => 0];
    }

    $form['advanced'] = [
      '#type' => 'container',
      '#weight' => 99,
      '#attributes' => [
        'class' => [
          'entity-meta',
        ]
      ]
    ];

    $form['meta'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      //'#access' => $this->currentUser->hasPermission('administer beeshop products'),
    ];

    $form['meta']['order_full_number'] = [
      '#type' => 'item',
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->entity->get('order_full_number')->value,
      '#access' => !$this->entity->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];

    $form['meta']['created'] = [
      '#type' => 'item',
      '#title' => $this->t('Created date'),

      '#markup' => !$this->entity->isNew() ? format_date($this->entity->getCreatedTime(), 'bs_date_time_output') : $this->t('Not saved yet'),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__created',
          'container-inline'
        ]
      ],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$this->entity->isNew() ? format_date($this->entity->getChangedTime(), 'bs_date_time_output') : $this->t('Not saved yet'),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__last-saved',
          'container-inline'
        ]
      ],
    ];

    $order_state =  $this->entity->getState();

    $form['meta']['state'] = [
      '#type' => 'item',
      '#title' => $this->t('Current state'),
      '#markup' => $order_state ? $order_state->label() : '-',
      '#wrapper_attributes' => [
        'class' => [
          'container-inline'
        ]
      ],
    ];


//     $order = $this->getEntity();

//     $order_shop = $order->getShop();
//     if (!$order_shop) {
//       $order_shop = \Drupal::entityTypeManager()->getStorage('bs_shop')->loadDefault();
//     }

//     $config = $this->configFactory->get('beeshop.order.settings');

//     $dates_format = $config->get('orders_admin_dates_format') ?
//                     $config->get('orders_admin_dates_format') :
//                     'short';

//     $last_saved = $this->dateFormatter->format($order->getChangedTime(), $dates_format);
//     $placed = $this->dateFormatter->format($order->getCreatedTime(), $dates_format);

//     $order_data = $order->get('data')->first();

//     $triggered_element = $form_state->getTriggeringElement();

//     $form['#prefix'] = '<div id="order-edit-form-container">';
//     $form['#suffix'] = '</div>';

//     $form['customer_name'] = [
//       '#type' => 'textfield',
//       '#title' => t('Recipient name'),
//       '#default_value' => $order_data->name,
//     ];

//     $form['customer_tel'] = [
//       '#type' => 'tel',
//       '#title' => t('Recipient phone number'),
//       '#default_value' => $order_data->tel,
//     ];

//     $form['customer_region'] = array(
//       '#type' => 'textfield',
//       '#title' => t('Region'),
//       '#default_value' => $order_data->region,
//     );

//     $form['customer_city'] = array(
//       '#type' => 'textfield',
//       '#title' => t('City'),
//       '#default_value' => $order_data->city,
//     );

//     $form['customer_zip'] = array(
//       '#type' => 'textfield',
//       '#title' => t('Postal code'),
//       '#default_value' => $order_data->zip,
//     );

//     $form['customer_street'] = array(
//       '#type' => 'textfield',
//       '#title' => t('Street'),
//       '#default_value' => $order_data->street,
//     );

//     $form['customer_house'] = array(
//       '#type' => 'textfield',
//       '#title' => t('House'),
//       '#default_value' => $order_data->house,
//     );

//     $form['customer_app'] = array(
//       '#type' => 'textfield',
//       '#title' => t('App/office'),
//       '#default_value' => $order_data->app,
//     );

//     $shipping_price = $order_data->shipping_price instanceof Price ?
//                       $order_data->shipping_price :
//                       (is_numeric($order_data->shipping_price) ?
//                          new Price($order_data->shipping_price, 'RUR') :
//                          null);

// //     $form['shipping_price'] = array(
// //       '#type' => 'textfield',
// //       '#title' => t('Shipping price'),
// //       '#default_value' => $order_data->shipping_price instanceof Price ?
// //                           $order_data->shipping_price->getPriceValue() :
// //                           $order_data->shipping_price,
// //     );

//     $form['shipping_price'] = [
//       '#type' => 'bs_price',
//       '#default_value' => [
//         'value' => $shipping_price ? $shipping_price->getPriceValue() : '',
//         'currency_code' => $shipping_price ?
//                            $shipping_price->getCurrencyCode() : $order_shop->getShopCurrenciesCodes(),
//       ],
//       '#available_currencies' => $order_shop->getShopCurrenciesCodes(),
//       '#title' => $this->t('Shipping price'),
//     ];

//     if (!empty($form['order_items'])) {
//       $order_items_view_arguments = [$order->id()];
//       $added_items_ids_view_argument = [];

//       foreach ($order->getItems() as $delta => $order_item_entity) {
//         if (!$order_item_entity->getOrder()) {
//           $added_items_ids_view_argument[] = $order_item_entity->id();
//         }
//       }

//       if ($added_items_ids_view_argument) {
//         if ($triggered_element && !empty($triggered_element['#op']) &&
//             $triggered_element['#op'] == 'add_item_to_order') {
//               drupal_set_message($this->t('Order contains newly added product items. Save order for final refer this items to the order.'));
//         }
//         $order_items_view_arguments[] = implode('+', $added_items_ids_view_argument);
//       }

//       $order_items = [
//         '#prefix' => '<div class="order-items">',
//         '#suffix' => '</div>',
//         '#type' => 'view',
//         '#name' => 'order_items',
//         '#display_id' => 'block_order_items_form',
//         '#arguments' => $order_items_view_arguments,
//         '#embed' => TRUE,
//       ];

//       $form['order_items'] = [
//           '#type' => 'fieldset',
//           '#title' => $this->t('Ordered items'),
//         ];

//         $form['order_items']['items_list'] = [
//           '#markup' => \Drupal::service('renderer')->renderPlain($order_items),
//           '#tree' => TRUE,
//         ];

//         foreach ($order->getItems() as $order_item) {
//           $quantity = $order_item->getQuantity();
//           $form['order_items']['items_list'][$order_item->id()]['quantity'] = [
//             '#type' => 'number',
//             '#title' => $this->t('Quantity'),
//             '#title_display' => 'invisible',
//             '#default_value' => $quantity,
//             '#size' => 4,
//             '#min' => 0,
//             '#step' => 1,
//           ];
//         }
//     }

//     =====

//     $form['ordered_items']['add_item_to_order'] = [
//       '#prefix' => '<div id="add-new-order-item-container">',
//       '#suffix' => '</div>',
//     ];

//     if ($triggered_element && !empty($triggered_element['#op']) &&
//       $triggered_element['#op'] != 'add_item_to_order' && $triggered_element['#op'] != 'cancel_add_item_to_order') {
//       $form['ordered_items']['add_item_to_order']['new_item_data'] = [
//         'new_item_id' => [
//           '#title' => $this->t('Product title/sku'),
//           '#type' => 'entity_autocomplete',
//           '#target_type' => 'bs_product',
//           '#selection_settings' => [
//             'only_available' => TRUE,
//           ],
//           '#selection_handler' => 'real_items:bs_product',
//           '#description' => $this->t('Define title or SKU of product that you want to add to the order'),
//         ],
//         'add_new' => [
//           '#type' => 'submit',
//           '#value' => $this->t('Add'),
//           '#op' => 'add_item_to_order',
//           '#ajax' => [
//             'callback' => 'Drupal\bs_order\Form\OrderForm::addOrderItem',
//             'wrapper' => 'order-edit-form-container',
//             'method' => 'replace',
//           ],
//           '#submit' => ['bs_order_edit_form_add_new_item_to_order'],
//         ],
//         'cancel' => [
//           '#type' => 'submit',
//           '#value' => $this->t('Cancel'),
//           '#op' => 'cancel_add_item_to_order',
//           '#ajax' => [
//             'callback' => 'Drupal\bs_order\Form\OrderForm::addNewOrderItemCallback',
//             'wrapper' => 'add-new-order-item-container',
//             'method' => 'replace',
//           ],
//           '#submit' => ['bs_order_edit_form_open_add_new_item_form'],
//         ]
//       ];
//     }
//     else {
//       $form['ordered_items']['add_item_to_order']['add_new_item_button'] = [
//         '#type' => 'submit',
//         '#value' => $this->t('Add new item to order'),
//         '#op' => 'show_add_item_form',
//         '#ajax' => [
//           'callback' => 'Drupal\bs_order\Form\OrderForm::addNewOrderItemCallback',
//           'wrapper' => 'add-new-order-item-container',
//           'method' => 'replace',
//         ],
//         '#submit' => ['bs_order_edit_form_open_add_new_item_form'],
//       ];
//     }


// //     $form['order_number']['widget']['0']['value']['#attributes']['readonly'] = TRUE;
// //     $form['order_number']['widget']['0']['value']['#field_prefix'] = $order->id() . '-';
// //     $form['state']['widget'][0]['value'] = [
// //       '#type' => 'select',
// //       '#title' => t('Order state'),
// //       '#options' => [
// //         'new' => t('New'),
// //         'payed' => t('Payed'),
// //         'payed_online_payanyway' => t('Payded online (Payanyway)'),
// //         'shipped' => t('Shipped'),
// //       ]
// //     ];

//     $form['advanced'] = [
//       '#type' => 'container',
//       '#attributes' => ['class' => ['entity-meta']],
//       '#weight' => -100,
//     ];

//     $form['meta'] = [
//       '#attributes' => ['class' => ['entity-meta__header']],
//       '#type' => 'container',
//       '#group' => 'advanced',
//       '#weight' => -100,
//       'number' => [
//         '#type' => 'html_tag',
//         '#tag' => 'h3',
//         '#value' => $this->t('Order #%label', ['%label' => $order->getFullNumber()]),
//         '#attributes' => [
//           'class' => 'entity-meta__title',
//         ],
//         // Hide the rendered state if there's a widget for it.
//         '#access' => empty($form['store_id']),
//       ],
//       'placed' => $this->fieldAsReadOnly($this->t('Placed'), $placed),
//       'last_changed' => $this->fieldAsReadOnly($this->t('Last saved'), $last_saved),
//       //'cur_state' => $tins->t($order->)
// //       'send_notifications' => [
// //         '#type' => 'submit',
// //         '#value' => $this->t('Send notifications'),
// //         '#submit' => ['::sendNotifications'],
// //         //'#dropbutton' => 'save',
// //       ],
//     ];

    $form['#theme'] = 'bs_order_order_edit_form';
    $form['#attached']['library'][] = 'bs_order/orderEditForm';

//     dpm($form);

    return $form;
  }

  /**
   * Builds a read-only form element for a field.
   *
   * @param string $label
   *   The element label.
   * @param string $value
   *   The element value.
   *
   * @return array
   *   The form element.
   */
  protected function fieldAsReadOnly($label, $value) {
    return [
      '#type' => 'item',
      '#wrapper_attributes' => [
        'class' => [Html::cleanCssIdentifier(strtolower($label)), 'container-inline'],
      ],
      '#markup' => '<h4 class="label inline">' . $label . '</h4> ' . $value,
    ];
  }


  public function submitCheckoutForm(array &$form, FormStateInterface $form_state) {

//     $current_shop = \Drupal::service('bs_shop.default_shop_resolver')->resolve();

//     $this->entity->set('shop_id', $current_shop->id());

//     $cart = $this->cartProvider->getCart();
//     $order_storage = \Drupal::entityTypeManager()->getStorage('bs_order');
//     $order_items_storage = \Drupal::entityTypeManager()->getStorage('bs_order_item');

//     foreach ($cart->cart_items as $cart_item) {
//       $cart_item_entity = $cart_item->entity;

//       $pid = $cart_item_entity->getPid();

//       $price = $cart_item_entity->get('price')->first();
//       $price_value = $price->value;
//       $currency_code = $price->currency_code;
//       $quantity = $cart_item_entity->getQuantity();

//       $order_item_values = [
//         'pid' => $pid,
//         'quantity' => $quantity,
//       ];

//       $order_item = $order_items_storage->create($order_item_values);

//       $order_item->set('price', [
//         'value' => $price_value,
//         'currency_code' => $currency_code,
//       ]);

//       $order_item->set('total_price', [
//         'value' => $price_value * $quantity,
//         'currency_code' => $currency_code,
//       ]);

//       $this->entity->addItem($order_item);
//     }

//     $this->cart = $cart;

//     $form_state->setRedirect('beeshop.order.new_order_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  public function disableRedirects(array $form, FormStateInterface $form_state) {
    $form_state->disableRedirect();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $order = $this->getEntity();
    $insert = $order->isNew();
    $order->save();

    $t_args = ['%title' => $order->toLink($order->label(), 'edit-form')->toString()];

    if ($insert) {
      //$this->logger('bs_order')->notice('New order %title placed.', $context);
      $this->messenger()->addStatus($this->t('New order %title has been created.', $t_args));
    }
    else {
      //$this->logger('content')->notice('@type: updated %title.', $context);
      $this->messenger()->addStatus($this->t('Order "%title" has been updated.', $t_args));
    }

    $form_state->setRedirect('entity.bs_order.collection');

//     $values = $form_state->getValues();

//     if (!empty($values['items_list'])) {
//       foreach ($this->entity->getItems() as $key => $order_item) {
//         $order_item_id = $order_item->id();
//         if ($values['items_list'][$order_item_id]['quantity'] > 0) {
//           $order_item->setQuantity($values['items_list'][$order_item_id]['quantity']);
//           $order_item->save();
//         }
//         else {
//           $order->deleteItem($order_item);
//         }

//       }
//     }

// //     $fields_to_data = [
// //       "customer_name" => 'name',
// //       "customer_tel" => 'tel',
// //       "customer_city" => 'city',
// //       "customer_zip" => 'zip',
// //       "customer_street" => 'street',
// //       "customer_house" => 'house',
// //       "customer_app" => 'app',
// //       "shipping_price" => 'shipping_price',
// //       'customer_ww_region' => 'ww_region',
// //       'customer_country' => 'country',
// //     ];

// //     $order_data = $order->get('data')->first();

// //

// //     if (isset($values['adjustments']['custom'])) {

// //       if ($values['adjustments']['custom']) {
// //         $order_custom_adjustment = [
// //           'price' => new Price($values['adjustments']['custom'], $order->getOrderCurrencyCode()),
// //           'description' => 'Произвольная скидка/надбавка',
// //         ];
// //         $order_adjustments = $this->entity->getData('adjustments');
// //         $order_adjustments['custom'] = $order_custom_adjustment;
// //       }
// //       elseif (isset($order_adjustments['custom'])) {
// //         unset($order_adjustments['custom']);
// //       }

// //       $this->entity->setData('adjustments', $order_adjustments);

// //     }

// //     foreach ($fields_to_data as $add_field_name => $data_field_name) {
// //       if (isset($values[$add_field_name])) {
// //         if ($add_field_name == 'shipping_price') {
// //           $shipping_price = $values[$add_field_name]['value'] !== '' ?
// //                             new Price($values[$add_field_name]['value'], $values[$add_field_name]['currency_code']) :
// //                             NULL;
// //           $order_data->set($data_field_name, $shipping_price);
// //         }
// //         else {
// //           $order_data->set($data_field_name, $values[$add_field_name]);
// //         }

// //       }
// //     }

//     $this->entity->save();

//     if ($this->getOperation() == 'checkout') {

//       if ($this->cart) {
//         $this->cart->clear();
//       }

//       $dispatcher = \Drupal::service('event_dispatcher');
//       $event = new OrderEvent($order);
//       $dispatcher->dispatch(OrderEvents::ORDER_INSERT, $event);
//     }

//     //drupal_set_message($this->t('The order %label has been successfully saved.', ['%label' => $this->entity->label()]));
//     //$form_state->setRedirect('entity.commerce_order.collection');
  }

  public function sendNotifications(array &$form, FormStateInterface $form_state) {

    $order = $this->entity;
    $dispatcher = \Drupal::service('event_dispatcher');
    $event = new OrderEvent($order);
    $dispatcher->dispatch(OrderEvents::ORDER_UPDATE, $event);

    if ($order->getState() == 'new') {
      $dispatcher->dispatch(OrderEvents::ORDER_INSERT, $event);
    }
    else {
      if ($order->isStateChanged()) {
        $dispatcher->dispatch(OrderEvents::ORDER_STATE_CHANGED, $event);
      }
    }


  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    $actions = parent::actions($form, $form_state);

    if ($this->getOperation() == 'checkout') {

      $actions['submit']['#submit'] = [
        '::submitForm',
        '::submitCheckoutForm',
        '::save',
      ];

    }

    if ($this->getOperation() == 'edit') {
      $actions['send_notifications'] = [
        '#type' => 'submit',
        '#value' => $this->t('Send notifications'),
        '#submit' => ['::sendNotifications'],
        //'#dropbutton' => 'save',
      ];

      $actions['submit']['#dropbutton'] = 'save';
      $actions['submit_and_reload_form'] = $actions['submit'];
      $actions['submit_and_reload_form']['#dropbutton'] = 'save';
      $actions['submit_and_reload_form']['#submit'][] = '::disableRedirects';
      $actions['submit_and_reload_form']['#value'] = $this->t('Save and reload form');

    }

    return $actions;
  }

}
