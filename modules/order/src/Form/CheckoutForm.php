<?php
namespace Drupal\bs_order\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_cart\CartInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\bs_order\Controller\OrderController;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\RemoveCommand;

class CheckoutForm extends ContentEntityForm {

  /**
   * @var \Drupal\bs_cart\CartInterface
   */
  protected $cart;

  /**
   * Constructs a new OrderForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, CartInterface $cart) {
    parent::__construct($entity_manager, $entity_type_bundle_info, $time);
    $this->cart = $cart;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user_cart')
      );
  }

  /**
   *
   * @return \Drupal\bs_cart\CartInterface
   */
  public function getCart() {
    return $this->cart;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $entity = $this->entityTypeManager->getStorage('bs_order')->createByCart($this->cart);

    return $entity;
  }

  /**
   * Returns an array of supported actions for the current entity form.
   *
   * @todo Consider introducing a 'preview' action here, since it is used by
   *   many entity types.
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    $actions['back_to_cart'] = [
      '#type' => 'link',
      '#title' => t('Back to cart'),
      '#url' => Url::fromRoute('entity.bs_cart.edit_form'),
      '#attributes' => [
        'class' => ['back-to-cart-link'],
      ],
    ];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Place order'),
      '#submit' => ['::submitForm', '::save'],
    ];

    $form_display = $this->getFormDisplay($form_state);
    $confirm_settings = $form_display->getThirdPartySetting('bs_order', 'confirmation');

    if (!empty($confirm_settings['ajax_submit'])) {
      $actions['submit']['#ajax'] = [
          'callback' => '::submitFormAjax',
      ];
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $form_display = $this->getFormDisplay($form_state);

    $order = $this->getEntity();
    $confirm_settings = $form_display->getThirdPartySetting('bs_order', 'confirmation');

    if (!empty($confirm_settings['confirmation_type'])) {
      switch ($confirm_settings['confirmation_type']) {
        case 'new_order_page':
          OrderController::saveOrderIdToPS($order);
          $form_state->setRedirect('beeshop.order.new_order_overview');
          break;
        case 'url':
          $url = Url::fromUserInput($confirm_settings['confirmation_url']);
          $form_state->setRedirectUrl($url);
          break;
        default:
          $form_state->setRedirect('beeshop.order.order_overview_form');
          break;
      }
    }
    else {
      $form_state->setRedirect('beeshop.order.order_overview_form');
    }

    $request = \Drupal::request();
    $request->getSession()->set('placed_order_number', $this->entity->getFullNumber());
  }

  public function submitFormAjax(array &$form, FormStateInterface $form_state) {

    $form_state->disableRedirect(FALSE);

    $response = new AjaxResponse();

    if ($url = $form_state->getRedirect()) {

      $response->addCommand(new RemoveCommand('form.bs-order-checkout-form .form-actions input.button--primary'));

      /**
       * @var Url $url
       */

      $response->addCommand(new RedirectCommand($url->toString()));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    OrderController::saveOrderIdToPS($this->entity);

    return $result;
  }

}

