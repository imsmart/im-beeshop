<?php
namespace Drupal\bs_order\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\TranslatableInterface;

class OrderStateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $order_state = $this->entity;
    $user = static::currentUser();
    $default_format = NULL;

    // Get a list of formats that the current user has access to.
    if ($formats = filter_formats($user)) {
      reset($formats);
      $default_format = key($formats);
    }

//     $shop_id = $type->sid;

//     if (!$type->id) {
//       $current_shop = $this->getEntityFromRouteMatch(\Drupal::routeMatch(), 'bs_shop');
//       if (!$current_shop || !$current_shop instanceof ShopInterface) {
//         drupal_set_message('Invalid shop for new price type', 'error');
//         return;
//       }
//       $shop_id = $current_shop->id();
//     }

    $form['sid'] = [
      '#type' => 'value',
      '#value' => 0,
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $order_state->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $order_state->id(),
      '#machine_name' => [
          'exists' => '\Drupal\bs_order\Entity\OrderState::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      //'#disabled' => !$type->isNew(),
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => !$order_state->isNew() ? $order_state->enabled : TRUE,
    ];

//     $form['can_be_payded'] = [
//       '#type' => 'checkbox',
//       '#title' => t('Can be payded'),
//       '#default_value' => $type->can_be_payded,
//     ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $order_state->getDescription(),
    ];


    $form['email'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Emails'),
    ];

    $form['send_emails_to'] = [
      '#type' => 'details',
      '#title' => $this->t('Send email notifications'),
      '#open' => TRUE,
      //       '#description' => $this->t('Edit the welcome email messages sent to new member accounts created by an administrator.') . ' ' . $email_token_help,
      '#group' => 'email',
      '#weight' => -10,
    ];

    $form['send_emails_to']['send_email_to_customer'] = [
      '#type' => 'checkbox',
      '#title' => t('To customer'),
      '#default_value' => !$order_state->isNew() ? $order_state->send_email_to_customer : TRUE,
    ];

    $form['send_emails_to']['send_email_to_manager'] = [
      '#type' => 'checkbox',
      '#title' => t('To manager'),
      '#default_value' => !$order_state->isNew() ? $order_state->send_email_to_manager : TRUE,
    ];

    $state_mails = $order_state->get('mails');

    $form['mails'] = [
      '#tree' => TRUE,
      '#weight' => 10
    ];

    $form['mails']['customer'] = [
      '#type' => 'details',
      '#title' => $this->t('Email message settings for customer'),
//       '#description' => $this->t('Edit the welcome email messages sent to new member accounts created by an administrator.') . ' ' . $email_token_help,
     '#group' => 'email',
      '#states' => [
        // Hide the additional settings when this email is disabled.
        'disabled' => [
          'input[name="send_email_to_customer"]' => ['checked' => FALSE],
        ],
      ],
      '#tree' => TRUE
    ];

    $form['mails']['customer']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => !empty($state_mails['customer']['subject']) ?
                          $state_mails['customer']['subject'] :
                          '',
    ];

    $form['mails']['customer']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Customer message template for this state'),
      '#default_value' => !empty($state_mails['customer']['body']['value']) ? $state_mails['customer']['body']['value'] : '',
      '#format' => !empty($state_mails['customer']['body']['format']) ? $state_mails['customer']['body']['format'] : $default_format,
      '#description' => 'Email message template that will be used for sending to customer when order state has been changed to this state',
    ];

    $form['mails']['manager'] = [
      '#type' => 'details',
      '#title' => $this->t('Email message settings for orders manager'),
      //       '#description' => $this->t('Edit the welcome email messages sent to new member accounts created by an administrator.') . ' ' . $email_token_help,
      '#group' => 'email',
      '#states' => [
        // Hide the additional settings when this email is disabled.
        'disabled' => [
          'input[name="send_email_to_manager"]' => ['checked' => FALSE],
        ],
      ],
      '#tree' => TRUE
    ];

    $form['mails']['manager']['recipients'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient(s)'),
      '#default_value' => !empty($state_mails['manager']['recipients']) ?
                          $state_mails['manager']['recipients'] :
                          '',
    ];

    $form['mails']['manager']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => !empty($state_mails['manager']['subject']) ?
                          $state_mails['manager']['subject'] :
                          '',
      '#maxlength' => 1024,
    ];

    $form['mails']['manager']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Customer message template for this state'),
      '#default_value' => !empty($state_mails['manager']['body']['value']) ? $state_mails['manager']['body']['value'] : '',
      '#format' => !empty($state_mails['manager']['body']['format']) ? $state_mails['manager']['body']['format'] : $default_format,
      '#description' => 'Email message template that will be used for sending to customer when order state has been changed to this state',
    ];

    $form['actuality_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State actuality time limit'),
      '#default_value' => $order_state->get('actuality_time'),
      '#description' => $this->t('An offset from the order last edit date such as "+1 day" or "-2 hours -30 minutes"'),
    ];

    $states_storage = $this->entityTypeManager->getStorage('bs_order_state');
    $options = $states_storage->getStatesOptionsList([$order_state->id()]);

    $form['nex_state_on_actuality_exprired'] = [
      '#type' => 'select',
      '#title' => $this->t('State, that will be setted to order after this state will expired'),
      '#options' => $options,
      '#empty_option' => $this->t('Select state'),
      '#default_value' => $order_state->get('nex_state_on_actuality_exprired'),
    ];

    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => t('Language'),
      '#languages' => LanguageInterface::STATE_ALL,
      '#default_value' => $this->entity->language()->getId(),
    ];

    if (empty($form['nex_state_on_actuality_exprired']['#options'])) {
      if (!empty($form['actuality_time'])) {
        $form['actuality_time']['#access'] = FALSE;
      }
      $form['nex_state_on_actuality_exprired']['#access'] = FALSE;
    }

    $form['third_party_settings'] = [
      '#tree' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $state = $this->entity;

    $save_status = $state->save();

    $t_args = ['%name' => $state->label()];

    if ($save_status == SAVED_UPDATED) {
      drupal_set_message(t('The order state %name has been updated.', $t_args));
    }
    elseif ($save_status == SAVED_NEW) {
      drupal_set_message(t('The order state %name has been added.', $t_args));
      $context = array_merge($t_args, ['link' => $state->toLink($this->t('View'), 'collection')->toString()]);
      $this->logger('bs_order')->notice('Added new order state "%name".', $context);
    }

    $form_state->setRedirectUrl($state->toUrl('collection'));
  }
}

