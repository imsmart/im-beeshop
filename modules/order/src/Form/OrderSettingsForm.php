<?php
namespace Drupal\bs_order\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class OrderSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'beeshop_order_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'beeshop.order.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('beeshop.order.settings');

    $form['orders_admin_dates_format'] = [
      '#type' => 'select',
      '#title' => 'Dates format on order edit form',
      '#options' => beeshop_get_dates_formats_for_options(),
      '#default_value' => $config->get('orders_admin_dates_format'),
    ];

    $form['orders_admin_mail'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email address for admin notificatios'),
      '#default_value' => $config->get('orders_admin_mail'),
    );

    $form['show_checkout_button_on_cart_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show "Checkout" button on cart page'),
      '#default_value' => $config->get('show_checkout_button_on_cart_page'),
    ];

    $form['order_overview_form_order_number_keys'] = [
      '#type' => 'textarea',
      '#title' => 'Order number query params for set default value for Order number field',
      '#default_value' => $config->get('order_overview_form_order_number_keys'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('beeshop.order.settings')
      ->set('orders_admin_dates_format', $form_state->getValue('orders_admin_dates_format'))
      ->set('orders_admin_mail', $form_state->getValue('orders_admin_mail'))
      ->set('show_checkout_button_on_cart_page', $form_state->getValue('show_checkout_button_on_cart_page'))
      ->set('order_overview_form_order_number_keys', $form_state->getValue('order_overview_form_order_number_keys'))
      ->save(TRUE);


    parent::submitForm($form, $form_state);
  }

}

