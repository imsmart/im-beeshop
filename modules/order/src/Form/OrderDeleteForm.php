<?php
namespace Drupal\bs_order\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class OrderDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return Url::fromUri('internal:/admin/beeshop/order/orders/list');
  }

}

