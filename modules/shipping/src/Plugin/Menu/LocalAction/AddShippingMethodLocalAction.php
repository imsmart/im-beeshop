<?php

namespace Drupal\bs_shipping\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\Serialization\Json;

class AddShippingMethodLocalAction extends LocalActionDefault {

  /**
   * {@inheritdoc}
   */
  public function getOptions(RouteMatchInterface $route_match) {
    return [
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return 'bs_shipping.shipping_methods.admin_library';
  }

}

