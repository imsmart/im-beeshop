<?php
namespace Drupal\bs_shipping\Plugin\Beeshop\Price\PriceAdjustment\Type;

use Drupal\bs_price\PriceAdjustmentTypeInterface;
use Drupal\bs_price\PriceAdjustmentTypeBase;

/**
 *
 * @PriceAdjustmentType(
 *   id="bs_shipping_order_price_adjustment",
 *   label = @Translation("Shipping"),
 *   default_item_label =  @Translation("Shipping"),
 *   weight = 20,
 *   can_be_included = false,
 *   only_one_in_list = true,
 * )
 */
class ShippingOrderPriceAdjustment extends PriceAdjustmentTypeBase implements PriceAdjustmentTypeInterface {

}

