<?php

namespace Drupal\bs_shipping\Plugin\Field\FieldWidget;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of 'commerce_shipping_profile'.
 *
 * @FieldWidget(
 *   id = "bs_shipping_shipment_on_checkout",
 *   label = @Translation("Shipments (on checkout form)"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CheckoutShipmentWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CheckoutShipmentWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();
    return $entity_type == 'bs_order' && $field_name == 'shipments';
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $id_prefix = implode('-', array_merge($parents, [$field_name]));
    $wrapper_id = Html::getUniqueId($id_prefix . '-wrapper');
    $elements = [];
    $elements['#prefix'] = '<div id="' . $wrapper_id . '">';
    $elements['#suffix'] = '</div>';

    $shiping_method = $items->getEntity()->get('shipping_method')->first()->entity;
    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    if (empty($field_state['shipments'])) {
      $shipment = $this->entityTypeManager->getStorage('bs_shipment')->create([
        'type' => $shiping_method->get('plugin')->plugin_configuration['shipment_type'],
        'shipping_method' => $shiping_method,
      ]);
      $field_state['shipments'][0] = $shipment;
    }

    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $elements[] = [
      '#type' => 'efw_entity_form',
      '#default_value' => $field_state['shipments'][0],
      '#form_mode' => 'on_checkout_form',
      '#parents_array' => static::getWidgetStateParents($parents, $field_name),
      '#wrapper_id' => $wrapper_id,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $parents = array_merge($form['#parents'], [$field_name, 'widget']);
    $element = NestedArray::getValue($form, $parents);

    $entities = [];

    foreach (Element::children($element) as $delta) {
      $entity_form_element = $element[$delta];
      $entities[] = $entity_form_element['#entity'];
    }

    $items->setValue($entities);
    $items->filterEmptyItems();

  }

}

