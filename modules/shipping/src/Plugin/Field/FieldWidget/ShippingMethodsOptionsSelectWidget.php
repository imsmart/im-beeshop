<?php

namespace Drupal\bs_shipping\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager;
use Drupal\Core\Render\Element;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountInterface;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "bs_shipping_methods_options_select",
 *   label = @Translation("Shipping methods select list"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class ShippingMethodsOptionsSelectWidget extends OptionsWidgetBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => 'radios',
      'handler' => 'default',
      'handler_settings' => [
        'view' => [
          'view_name' => '',
          'display_name' => '',
          'arguments' => '',
        ],
      ],
    ] + parent::defaultSettings();
  }

  /**
   * Ajax callback for the handler settings form.
   *
   * @see static::fieldSettingsForm()
   */
  public static function settingsAjax($form, FormStateInterface $form_state) {
    return NestedArray::getValue($form, $form_state->getTriggeringElement()['#ajax']['element']);
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      // Limit the settable options for the current user account.


      $options = $this->getSettableOptions();

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      $module_handler = \Drupal::moduleHandler();
      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
      ];
      $module_handler->alter('options_list', $options, $context);

      array_walk_recursive($options, [$this, 'sanitizeLabel']);

      // Options might be nested ("optgroups"). If the widget does not support
      // nested options, flatten the list.
      if (!$this->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }

      $this->options = $options;
    }
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions() {
    $handler = $this->getSelectionHandler($this->getSetting('handler'), $this->getSettings()['handler_settings']);
    $options = $handler->getReferenceableEntities();

    // Rebuild the array by changing the bundle key into the bundle label.
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('bs_shipping_method');

    $return = [];
    foreach ($options as $bundle => $entity_ids) {
      // The label does not need sanitizing since it is used as an optgroup
      // which is only supported by select elements and auto-escaped.
      $bundle_label = (string) $bundles[$bundle]['label'];
      $return[$bundle_label] = $entity_ids;
    }

    return count($return) == 1 ? reset($return) : $return;
  }

  /**
   * Render API callback: Processes the field settings form and allows access to
   * the form state.
   *
   * @see static::fieldSettingsForm()
   */
  public static function settingsAjaxProcess($form, FormStateInterface $form_state) {
    static::settingsAjaxProcessElement($form, $form);
    return $form;
  }

  /**
   * Adds entity_reference specific properties to AJAX form elements from the
   * field settings form.
   *
   * @see static::fieldSettingsAjaxProcess()
   */
  public static function settingsAjaxProcessElement(&$element, $main_form) {
    if (!empty($element['#ajax'])) {
      $element['#ajax'] = [
        'callback' => [get_called_class(), 'settingsAjax'],
        'wrapper' => $main_form['#id'],
        'element' => $main_form['#array_parents'],
      ];
    }

    foreach (Element::children($element) as $key) {
      static::settingsAjaxProcessElement($element[$key], $main_form);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form = [
      '#type' => 'container',
      '#process' => [[get_class($this), 'settingsAjaxProcess']],
//       '#element_validate' => [[get_class($this), 'settingsFormValidate']],
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#options' => $this->getTypeOptions(),
      '#description' => t('Select type.'),
    ];

    /**
     * @var SelectionPluginManager $entity_selection_pm
     */
    $entity_selection_pm = \Drupal::service('plugin.manager.entity_reference_selection');

//     dpm($entity_selection_pm->getDefinitions());

    $selection_plugins = \Drupal::service('plugin.manager.entity_reference_selection')->getSelectionGroups('bs_shipping_method');

    $handlers_options = [];
    foreach (array_keys($selection_plugins) as $selection_group_id) {
      // We only display base plugins (e.g. 'default', 'views', ...) and not
      // entity type specific plugins (e.g. 'default:node', 'default:user',
      // ...).
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':bs_shipping_method', $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':bs_shipping_method';
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }

    $form['handler'] = [
      '#type' => 'details',
      '#title' => t('Handler settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[get_class($this), 'formProcessMergeParent']],
    ];

    $form['handler']['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Handler'),
      '#options' => $handlers_options,
      '#ajax' => TRUE,
      '#limit_validation_errors' => [],
      '#default_value' => $this->getSetting('handler'),
    ];

    $form['handler']['handler_submit'] = [
      '#type' => 'submit',
      '#value' => t('Change handler'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#submit' => [[get_class($this), 'settingsAjaxSubmit']],
    ];

    $form['handler']['handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $handler_settings = $this->getSettings()['handler_settings'];
    $handler = $this->getSelectionHandler($this->getSetting('handler'), $handler_settings);

    $form['handler']['handler_settings'] += $handler->buildConfigurationForm([], $form_state);

    return $form;
  }

  /**
   * Submit handler for the non-JS case.
   *
   * @see static::fieldSettingsForm()
   */
  public static function settingsAjaxSubmit($form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Render API callback: Moves entity_reference specific Form API elements
   * (i.e. 'handler_settings') up a level for easier processing by the
   * validation and submission handlers.
   *
   * @see _entity_reference_field_settings_process()
   */
  public static function formProcessMergeParent($element) {
    $parents = $element['#parents'];
    array_pop($parents);
    $element['#parents'] = $parents;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $options = $this->getOptions($items->getEntity());
    $options_keys = !empty($options) ? array_keys($options) : [0];

    $element += [
      '#type' => $this->getSetting('type'),
      '#options' => $options,
      '#default_value' => $items->target_id ?: ($options ? reset($options_keys) : NULL),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // By default, widgets are available for all fields.
    $field_definition_settings = $field_definition->getSettings();

    if (!empty($field_definition_settings['target_type']) && $field_definition_settings['target_type'] == 'bs_shipping_method') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getTypeOptions() {
    return [
      'radios' => t('Radios'),
      'select' => t('Select list'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectionHandler($handler_name, $hendler_settings = []) {

    $options = $hendler_settings;

    $options += [
      'target_type' => 'bs_shipping_method',
      'handler' => $handler_name,
//       'entity' => $entity,
    ];
    return \Drupal::service('plugin.manager.entity_reference_selection')->getInstance($options);
  }

//   /**
//    * {@inheritdoc}
//    */
//   protected function sanitizeLabel(&$label) {
//     // Select form inputs allow unencoded HTML entities, but no HTML tags.
//     $label = Html::decodeEntities(strip_tags($label));
//   }

//   /**
//    * {@inheritdoc}
//    */
//   protected function supportsGroups() {
//     return TRUE;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   protected function getEmptyLabel() {
//     if ($this->multiple) {
//       // Multiple select: add a 'none' option for non-required fields.
//       if (!$this->required) {
//         return t('- None -');
//       }
//     }
//     else {
//       // Single select: add a 'none' option for non-required fields,
//       // and a 'select a value' option for required fields that do not come
//       // with a value selected.
//       if (!$this->required) {
//         return t('- None -');
//       }
//       if (!$this->has_value) {
//         return t('- Select a value -');
//       }
//     }
//   }



}
