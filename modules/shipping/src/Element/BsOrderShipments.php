<?php

namespace Drupal\bs_shipping\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_shipping\Entity\ShipmentInterface;

/**
 *
 * @author andrey
 *
 * @RenderElement("bs_shipping_order_shipment")
 */
class BsOrderShipments extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      // The shipment entity operated on. Required.
      '#default_value' => NULL,
      '#process' => [
//         [$class, 'attachElementSubmit'],
        [$class, 'processForm'],
      ],
      '#element_validate' => [
//         [$class, 'validateElementSubmit'],
        [$class, 'validateForm'],
      ],
      '#commerce_element_submit' => [
        [$class, 'submitForm'],
      ],
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Builds the element form.
   *
   * @param array $element
   *   The form element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #default_value is empty or not an entity, or when
   *   #available_countries is not an array of country codes.
   *
   * @return array
   *   The processed form element.
   */
  public static function processForm(array $element, FormStateInterface $form_state, array &$complete_form) {
    if (empty($element['#default_value']) && empty($element['#bundle'])) {
      throw new \InvalidArgumentException('The bs_shipping_order_shipment element requires the #default_value property.');
    }
    elseif (isset($element['#default_value']) && !($element['#default_value'] instanceof ShipmentInterface)) {
      throw new \InvalidArgumentException('The bs_shipping_order_shipment #default_value property must be a shipment entity.');
    }
    elseif (isset($element['#bundle']) && empty($element['#default_value'])) {
      $element['#default_value'] = \Drupal::entityTypeManager()->getStorage('bs_shipment')->create([
        'type' => $element['#bundle'],
      ]);
    }

    $element['#shipment'] = $element['#default_value'];
    $form_display = EntityFormDisplay::collectRenderDisplay($element['#shipment'], 'on_checkout_form');
    $form_display->buildForm($element['#shipment'], $element, $form_state);
//     if (!empty($element['address']['widget'][0])) {
//       $widget_element = &$element['address']['widget'][0];
//       // Remove the details wrapper from the address widget.
//       $widget_element['#type'] = 'container';
//       // Provide a default country.
//       if (!empty($element['#default_country']) && empty($widget_element['address']['#default_value']['country_code'])) {
//         $widget_element['address']['#default_value']['country_code'] = $element['#default_country'];
//       }
//       // Limit the available countries.
//       if (!empty($element['#available_countries'])) {
//         $widget_element['address']['#available_countries'] = $element['#available_countries'];
//       }
//     }

    return $element;
  }

  /**
   * Validates the element form.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Exception
   *   Thrown if button-level #validate handlers are detected on the parent
   *   form, as a protection against buggy behavior.
   */
  public static function validateForm(array &$element, FormStateInterface $form_state) {
    $form_display = EntityFormDisplay::collectRenderDisplay($element['#shipment'], 'on_checkout_form');
    $form_display->extractFormValues($element['#shipment'], $element, $form_state);
//     $form_display->validateFormValues($element['#profile'], $element, $form_state);
  }

  /**
   * Submits the element form.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitForm(array &$element, FormStateInterface $form_state) {
    $form_display = EntityFormDisplay::collectRenderDisplay($element['#shipment'], 'on_checkout_form');
    $form_display->extractFormValues($element['#shipment'], $element, $form_state);
    dpm($element['#shipment']);
    //$element['#profile']->save();
  }

}

