<?php

namespace Drupal\bs_shipping;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\beeshop\EntityDraggableListBuilder;

class ShippingMethodListBulder extends EntityDraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['method'] = $this->t('Method');
    $header['category'] = $this->t('Category');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    //$definition = $entity->getPlugin()->getPluginDefinition();

    $row['method']['data']['label_markup']['#markup'] = $entity->isEnabled() ?
$entity->label() :
$this->t('@label (<span class="marker">disabled</span>)', ['@label' => $entity->label()]);

    $row['category']['data']['#markup'] = '';//$definition['category'];

    $row += parent::buildRow($entity);

    $row['#attributes']['class'][] = $entity->isEnabled() ? 'method-enabled' : 'method-disabled';

    return $row;
  }

  /**
  * {@inheritDoc}
  */
  public function getFormId() {
    return 'shipping_method_list_form';
  }

}

