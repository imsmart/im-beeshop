<?php

namespace Drupal\bs_shipping;

use Drupal\Core\Plugin\Context\ContextAwarePluginManagerInterface;
use Drupal\Component\Plugin\CategorizingPluginManagerInterface;

/**
 * Provides an interface for the discovery and instantiation of shipping methods plugins.
 */
interface ShippingMethodManagerInterface extends ContextAwarePluginManagerInterface, CategorizingPluginManagerInterface {
}

