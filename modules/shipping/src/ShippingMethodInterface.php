<?php
namespace Drupal\bs_shipping;

use Drupal\Core\Entity\ContentEntityInterface;

interface ShippingMethodInterface extends ContentEntityInterface {

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\bs_shipping\ShippingMethodPluginInterface
   *   The plugin instance for this shipping method.
   */
  public function getPlugin();

}

