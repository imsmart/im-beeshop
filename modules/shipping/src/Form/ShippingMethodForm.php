<?php
namespace Drupal\bs_shipping\Form;

use Drupal\Core\Entity\ContentEntityForm;

class ShippingMethodForm extends ContentEntityForm {

//   /**
//    * The shipping method entity.
//    *
//    * @var \Drupal\bs_shipping\ShippingMethodInterface
//    */
//   protected $entity;

//   /**
//    * The plugin form manager.
//    *
//    * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
//    */
//   protected $pluginFormFactory;

//   /**
//    * The shipping method storage.
//    *
//    * @var \Drupal\Core\Entity\EntityStorageInterface
//    */
//   protected $storage;

//   /**
//    * Constructs a ShippingMethodForm object.
//    *
//    * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
//    *   The entity type manager.
//    * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_manager
//    *   The plugin form manager.
//    */
//   public function __construct(EntityTypeManagerInterface $entity_type_manager, PluginFormFactoryInterface $plugin_form_manager) {
//     $this->storage = $entity_type_manager->getStorage('bs_shipping_method');
//     $this->pluginFormFactory = $plugin_form_manager;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public static function create(ContainerInterface $container) {
//     return new static(
//       $container->get('entity_type.manager'),
//       $container->get('plugin_form.factory')
//       );
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function form(array $form, FormStateInterface $form_state) {

//     $entity = $this->entity;

//     $form['#tree'] = TRUE;
//     $form['settings'] = [];
//     $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
//     $form['settings'] = $this->getPluginForm($entity->getPlugin())->buildConfigurationForm($form['settings'], $subform_state);
// //     $form['visibility'] = $this->buildVisibilityInterface([], $form_state);

//     // If creating a new block, calculate a safe default machine name.
//     $form['id'] = [
//       '#type' => 'machine_name',
//       '#maxlength' => 64,
//       '#description' => $this->t('A unique name for this block instance. Must be alpha-numeric and underscore separated.'),
//       '#default_value' => !$entity->isNew() ? $entity->id() : $this->getUniqueMachineName($entity),
//       '#machine_name' => [
//         'exists' => '\Drupal\block\Entity\Block::load',
//         'replace_pattern' => '[^a-z0-9_.]+',
//         'source' => ['settings', 'label'],
//       ],
//       '#required' => TRUE,
//       '#disabled' => !$entity->isNew(),
//     ];

//     return $form;
//   }

//   /**
//    * Retrieves the plugin form for a given shipping method and operation.
//    *
//    * @param \Drupal\Core\Block\BlockPluginInterface $block
//    *   The block plugin.
//    *
//    * @return \Drupal\Core\Plugin\PluginFormInterface
//    *   The plugin form for the block.
//    */
//   protected function getPluginForm(ShippingMethodPluginInterface $shipping_method) {
//     if ($shipping_method instanceof PluginWithFormsInterface) {
//       return $this->pluginFormFactory->createInstance($shipping_method, 'configure');
//     }
//     return $shipping_method;
//   }

//   /**
//    * Generates a unique machine name for a block.
//    *
//    * @param \Drupal\block\BlockInterface $block
//    *   The block entity.
//    *
//    * @return string
//    *   Returns the unique name.
//    */
//   public function getUniqueMachineName(ShippingMethodInterface $shipping_method) {
//     $suggestion = $shipping_method->getPlugin()->getMachineNameSuggestion();

//     // Get all the blocks which starts with the suggested machine name.
//     $query = $this->storage->getQuery();
//     $query->condition('id', $suggestion, 'CONTAINS');
//     $block_ids = $query->execute();

//     $block_ids = array_map(function ($block_id) {
//       $parts = explode('.', $block_id);
//       return end($parts);
//     }, $block_ids);

//       // Iterate through potential IDs until we get a new one. E.g.
//       // 'plugin', 'plugin_2', 'plugin_3', etc.
//       $count = 1;
//       $machine_default = $suggestion;
//       while (in_array($machine_default, $block_ids)) {
//         $machine_default = $suggestion . '_' . ++$count;
//       }
//       return $machine_default;
//   }
}

