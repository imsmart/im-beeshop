<?php
namespace Drupal\bs_shipping\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ShippingConfigBase extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $shipping_config = $this->config('bs_shipping.settings');
    $form['show_shipping_methods_on_cart_page'] = [
      '#type' => 'checkbox',
      '#title' => t('Show shipping methods on cart page'),
      '#default_value' => $shipping_config->get('show_shipping_methods_on_cart_page'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_shipping_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bs_shipping.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bs_shipping.settings')
    ->set('show_shipping_methods_on_cart_page', $form_state->getValue('show_shipping_methods_on_cart_page'))
    ->save();

    parent::submitForm($form, $form_state);
  }

}

