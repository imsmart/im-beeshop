<?php

namespace Drupal\bs_shipping\RouteProcessor;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;

/**
 * Provides a route processor to replace <current>.
 */
class RouteProcessor implements OutboundRouteProcessorInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new RouteProcessorCurrent.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($route_name, Route $route, array &$parameters, BubbleableMetadata $bubbleable_metadata = NULL) {

    if ($route_name == 'entity.bs_shipment.add_form') {
      if ($bs_order = $this->routeMatch->getRawParameter('bs_order')) {
        $parameters['bs_order'] = $bs_order;
      }
    }
  }

}
