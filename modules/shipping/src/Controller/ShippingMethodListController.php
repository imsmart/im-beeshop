<?php
namespace Drupal\bs_shipping\Controller;

use Drupal\Core\Entity\Controller\EntityListController;

class ShippingMethodListController extends EntityListController {

  /**
   * Shows the block administration page.
   *
   * @param string|null $theme
   *   Theme key of block list.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function listing($bs_shop) {
//     $theme = $theme ?: $this->config('system.theme')->get('default');
//     if (!$this->themeHandler->hasUi($theme)) {
//       throw new NotFoundHttpException();
//     }

    return $this->entityTypeManager()->getListBuilder('bs_shipping_method')->render($bs_shop);
  }

}

