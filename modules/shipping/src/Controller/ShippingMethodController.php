<?php

namespace Drupal\bs_shipping\Controller;

use Drupal\Component\Utility\Html;
use Drupal\block\BlockInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\bs_shipping\ShippingMethodInterface;

/**
 * Controller routines for admin shipping method routes.
 */
class ShippingMethodController extends ControllerBase {

//   /**
//    * The theme handler.
//    *
//    * @var \Drupal\Core\Extension\ThemeHandlerInterface
//    */
//   protected $themeHandler;

//   /**
//    * Constructs a new BlockController instance.
//    *
//    * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
//    *   The theme handler.
//    */
//   public function __construct(ThemeHandlerInterface $theme_handler) {
//     $this->themeHandler = $theme_handler;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public static function create(ContainerInterface $container) {
//     return new static(
//       $container->get('theme_handler')
//     );
//   }

  /**
   * Calls a method on a block and reloads the listing page.
   *
   * @param \Drupal\block\BlockInterface $block
   *   The block being acted upon.
   * @param string $op
   *   The operation to perform, e.g., 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the listing page.
   */
  public function performOperation(ShippingMethodInterface $bs_shipping_method, $op) {
    $bs_shipping_method->$op()->save();
    drupal_set_message($this->t('The shipping method settings have been updated.'));
    return $this->redirect('block.admin_display');
  }

}
