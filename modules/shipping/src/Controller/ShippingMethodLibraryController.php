<?php
namespace Drupal\bs_shipping\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bs_shipping\ShippingMethodManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Menu\LocalActionManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

class ShippingMethodLibraryController extends ControllerBase {

  /**
   * The shipping method manager.
   *
   * @var \Drupal\bs_shipping\ShippingMethodManagerInterface
   */
  protected $shippingMethodManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The local action manager.
   *
   * @var \Drupal\Core\Menu\LocalActionManagerInterface
   */
  protected $localActionManager;

  /**
   * Constructs a BlockLibraryController object.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Plugin\Context\LazyContextRepository $context_repository
   *   The context repository.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Menu\LocalActionManagerInterface $local_action_manager
   *   The local action manager.
   */
  public function __construct(ShippingMethodManagerInterface $block_manager, RouteMatchInterface $route_match, LocalActionManagerInterface $local_action_manager) {
    $this->shippingMethodManager = $block_manager;
    $this->routeMatch = $route_match;
    $this->localActionManager = $local_action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.bs_shipping_method'),
      $container->get('current_route_match'),
      $container->get('plugin.manager.menu.local_action')
    );
  }

  public function listMethods(Request $request, $bs_shop) {

    // Since modals do not render any other part of the page, we need to render
    // them manually as part of this listing.
    if ($request->query->get(MainContentViewSubscriber::WRAPPER_FORMAT) === 'drupal_modal') {
      $build['local_actions'] = $this->buildLocalActions();
    }

    $definitions = $this->shippingMethodManager->getDefinitions();
    // Order by category, and then by admin label.
    $definitions = $this->shippingMethodManager->getSortedDefinitions($definitions);

    $headers = [
      ['data' => $this->t('Shiping method')],
      ['data' => $this->t('Category')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];

    foreach ($definitions as $plugin_id => $plugin_definition) {
      $row = [];
      $row['title']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="block-filter-text-source">{{ label }}</div>',
        '#context' => [
          'label' => $plugin_definition['admin_label'],
        ],
      ];
      $row['category']['data'] = $plugin_definition['category'];
      $links['add'] = [
        'title' => $this->t('Add'),
        'url' => Url::fromRoute('bs_shipping.shipping_method.admin_add', ['plugin_id' => $plugin_id, 'bs_shop' => $bs_shop]),
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 700,
          ]),
        ],
      ];
      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
      $rows[] = $row;
    }

    $build['methods'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No shipping methods available.'),
      '#attributes' => [
        'class' => ['block-add-table'],
      ],
    ];

    return $build;
  }

  /**
   * Builds the local actions for this listing.
   *
   * @return array
   *   An array of local actions for this listing.
   */
  protected function buildLocalActions() {
    $build = $this->localActionManager->getActionsForRoute($this->routeMatch->getRouteName());
    // Without this workaround, the action links will be rendered as <li> with
    // no wrapping <ul> element.
    if (!empty($build)) {
      $build['#prefix'] = '<ul class="action-links">';
      $build['#suffix'] = '</ul>';
    }
    return $build;
  }
}

