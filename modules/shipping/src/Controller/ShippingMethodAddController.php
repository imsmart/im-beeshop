<?php

namespace Drupal\bs_shipping\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for building the block instance add form.
 */
class ShippingMethodAddController extends ControllerBase {

  /**
   * Build the block instance add form.
   *
   * @param string $plugin_id
   *   The plugin ID for the block instance.
   * @param string $theme
   *   The name of the theme for the block instance.
   *
   * @return array
   *   The block instance edit form.
   */
  public function shippingMethodAddConfigureForm($plugin_id, $bs_shop) {
    // Create a block entity.
    $entity = $this->entityTypeManager()->getStorage('bs_shipping_method')->create(['plugin' => $plugin_id, 'shop' => $bs_shop]);

    return $this->entityFormBuilder()->getForm($entity);
  }

}
