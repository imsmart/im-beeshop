<?php
namespace Drupal\bs_shipping;

use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 *
 * @ingroup beeshop_shipping_api
 */
abstract class ShippingMethodBase extends PluginBase implements ShippingMethodPluginInterface, PluginWithFormsInterface {

  use PluginWithFormsTrait;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * {@inheritdoc}
   *
   * Creates a generic configuration form for all shipping methods types. Individual
   * shipping methods plugins can add elements to this form by overriding
   * ShippingMethodBase::blockForm(). Most shipping methods plugins should not override this
   * method unless they need to alter the generic form elements.
   *
   * @see \Drupal\bs_shipping\ShippingMethodBase::methodForm()
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['shipment_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Shipment type'),
      '#options' => $this->getShipmentTypesOptions(),
      '#default_value' => $this->shipmentTypeId(),
      '#required' => TRUE,
    ];

    // Add context mapping UI form elements.
//     $contexts = $form_state->getTemporaryValue('gathered_contexts') ?: [];
//     $form['context_mapping'] = $this->addContextAssignmentElement($this, $contexts);
    // Add plugin-specific settings for this block type.
    $form += $this->methodForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineNameSuggestion() {
    $definition = $this->getPluginDefinition();
    $admin_label = $definition['admin_label'];

    // @todo This is basically the same as what is done in
    //   \Drupal\system\MachineNameController::transliterate(), so it might make
    //   sense to provide a common service for the two.
    $transliterated = $this->transliteration()->transliterate($admin_label, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = Unicode::strtolower($transliterated);

    $transliterated = preg_replace('@[^a-z0-9_.]+@', '', $transliterated);

    return $transliterated;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }

    $definition = $this->getPluginDefinition();
    // Cast the admin label to a string since it is an object.
    // @see \Drupal\Core\StringTranslation\TranslatableMarkup
    return (string) $definition['admin_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function frontendLabel() {
    if (!empty($this->configuration['frontend_label'])) {
      return $this->configuration['frontend_label'];
    }

    return '';
  }

  public function shipmentTypeId() {
    if (!empty($this->configuration['shipment_type'])) {
      return $this->configuration['shipment_type'];
    }

    return '';
  }

  /**
   * Returns generic default configuration for block plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseConfigurationDefaults() {
    return [
      'id' => $this->getPluginId(),
      'label' => '',
      'provider' => $this->pluginDefinition['provider'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function methodForm($form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep(
      $this->baseConfigurationDefaults(),
      $this->defaultConfiguration(),
      $configuration
      );
  }

  /**
   * {@inheritdoc}
   *
   * Most block plugins should not override this method. To add submission
   * handling for a specific block type, override BlockBase::blockSubmit().
   *
   * @see \Drupal\Core\Block\BlockBase::blockSubmit()
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Process the block's submission handling if no errors occurred only.
    if (!$form_state->getErrors()) {
      $this->configuration['label'] = $form_state->getValue('label');
//       $this->configuration['label_display'] = $form_state->getValue('label_display');
      $this->configuration['provider'] = $form_state->getValue('provider');
      $this->shippingMethodSubmit($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function shippingMethodSubmit($form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function shippingMethodValidate($form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   *
   * Most block plugins should not override this method. To add validation
   * for a specific block type, override BlockBase::blockValidate().
   *
   * @see \Drupal\Core\Block\BlockBase::blockValidate()
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Remove the admin_label form item element value so it will not persist.
    $form_state->unsetValue('admin_label');

    $this->shippingMethodValidate($form, $form_state);
  }

  /**
   * Wraps the transliteration service.
   *
   * @return \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected function transliteration() {
    if (!$this->transliteration) {
      $this->transliteration = \Drupal::transliteration();
    }
    return $this->transliteration;
  }

  /**
   * Sets the transliteration service.
   *
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliteration service.
   */
  public function setTransliteration(TransliterationInterface $transliteration) {
    $this->transliteration = $transliteration;
  }

  public function getShippingPrice(OrderInterface $order) {
    return NULL;
  }

  public function calculateDependencies() {
    return [
      'module' => [
        'bs_shipping'
      ]
    ];
  }

  /**
   * Gets the current user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The current user.
   */
  protected function currentUser() {
    return \Drupal::currentUser();
  }

  protected function getShipmentTypesOptions() {

    $options = [];

    $entity_type_bundle_info = \Drupal::service('entity_type.bundle.info');
    $shipment_bundles = $entity_type_bundle_info->getBundleInfo('bs_shipment');

    foreach ($shipment_bundles as $id => $bundle_info) {
      $options[$id] = $bundle_info['label'];
    }

    return $options;
  }
}

