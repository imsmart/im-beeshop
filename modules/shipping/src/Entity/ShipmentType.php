<?php

namespace Drupal\bs_shipping\Entity;

use Drupal\beeshop\Entity\BeeShopBundleEntityBase;

/**
 * Defines the shipment type entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_shipment_type",
 *   label = @Translation("Shipment type"),
 *   label_singular = @Translation("shipment type"),
 *   label_plural = @Translation("shipment types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shipment type",
 *     plural = "@count shipment types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_shipping\ShipmentTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_shipping\Form\ShipmentTypeForm",
 *       "add" = "Drupal\bs_shipping\Form\ShipmentTypeForm",
 *       "edit" = "Drupal\bs_shipping\Form\ShipmentTypeForm",
 *       "delete" = "Drupal\bs_shipping\Form\ShipmentTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer beeshop shipment_type",
 *   config_prefix = "bs_shipment_type",
 *   bundle_of = "bs_shipment",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/shipping/shipment-types/add",
 *     "edit-form" = "/admin/beeshop/config/shipping/shipment-types/{bs_shipment_type}/edit",
 *     "delete-form" = "/admin/beeshop/config/shipping/shipment-types/{bs_shipment_type}/delete",
 *     "collection" = "/admin/beeshop/config/shipping/shipment-types",
 *   }
 * )
 */
class ShipmentType extends BeeShopBundleEntityBase implements ShipmentTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
