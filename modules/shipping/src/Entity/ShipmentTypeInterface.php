<?php

namespace Drupal\bs_shipping\Entity;

use Drupal\beeshop\Entity\BeeShopBundleEntityBaseInterface;

/**
 * Defines the interface for shipment types.
 */
interface ShipmentTypeInterface extends BeeShopBundleEntityBaseInterface {

  public function getDescription();

  public function setDescription($description);

}
