<?php

namespace Drupal\bs_shipping\Entity;

use Drupal\bs_shipping\ShippingMethodInterface;
use Drupal\bs_shipping\ShippingMethodPluginCollection;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines a Shipping method entity class.
 *
 * @ContentEntityType(
 *   id = "bs_shipping_method",
 *   label = @Translation("Shipping method"),
 *   label_singular = @Translation("shipping method"),
 *   label_plural = @Translation("shipping methods"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shipping method",
 *     plural = "@count shipping methods",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_shipping\ShippingMethodListBulder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\bs_shipping\Form\ShippingMethodForm",
 *       "add" = "Drupal\bs_shipping\Form\ShippingMethodForm",
 *       "edit" = "Drupal\bs_shipping\Form\ShippingMethodForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_shipping\Routing\ShippingMethodRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   base_table = "bs_shipping_method",
 *   data_table = "bs_shipping_method_field_data",
 *   admin_permission = "administer bs_shipping_method",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "sm_id",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/shipping/shipping_methods/add",
 *     "edit-form" = "/admin/beeshop/config/shipping/shipping_methods/{bs_shipping_method}",
 *     "delete-form" = "/admin/beeshop/config/shipping/shipping_methods/{bs_shipping_method}/delete",
 *     "enable" = "/admin/beeshop/config/shops/shipping_methods/{bs_shipping_method}/enable",
 *     "disable" = "/admin/beeshop/config/shops/shipping_methods/{bs_shipping_method}/disable",
 *     "collection" =  "/admin/beeshop/config/shipping/shipping_methods",
 *     "collection-by-shop" = "/t-t-t",
 *   },
 *   field_ui_base_route = "entity.bs_shipping_method.collection",
 * )
 */
class ShippingMethod extends ContentEntityBase implements ShippingMethodInterface {

  /**
   * The ID of the shipping method.
   *
   * @var string
   */
  protected $id;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\bs_shipping\ShippingMethodPluginCollection
   */
  protected $pluginCollection;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {

    if ($this->get('plugin')->isEmpty()) {
      return NULL;
    }

    return $this->get('plugin')->first()->getPluginInstance();
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function label() {
//     $settings = $this->get('settings');
//     if ($settings['label']) {
//       return $settings['label'];
//     }
//     else {
//       $definition = $this->getPlugin()->getPluginDefinition();
//       return $definition['admin_label'];
//     }
//   }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['shops'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shops'))
    ->setDescription(t('The stores for which the shipping method is valid.'))
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_shop')
    ->setSetting('handler', 'default')
    ->setDisplayConfigurable('form', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The shipping method name.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['plugin'] = BaseFieldDefinition::create('plugin_field_item')
    ->setSetting('plugin_manager_name', 'plugin.manager.bs_shipping_method')
    ->setLabel(t('Plugin'))
    ->setCardinality(1)
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE);
//     ->setDisplayOptions('form', [
//       'type' => 'commerce_plugin_radios',
//       'weight' => 1,
//     ]);

    $fields['shipment_type'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shipment type'))
    ->setDescription(t('Shipment type that will be used for this method.'))
    ->setCardinality(1)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_shipment_type')
    ->setSetting('handler', 'default')
    ->setDisplayConfigurable('form', TRUE);


    $fields['weight'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Weight'))
    ->setDescription(t('The weight of this shipping method in relation to others.'))
    ->setDefaultValue(0)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'integer',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'hidden',
    ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Enabled'))
    ->setDescription(t('Whether the shipping method is enabled.'))
    ->setDefaultValue(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => TRUE,
      ],
      'weight' => 20,
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * Helper callback for uasort() to sort configuration entities by weight and label.
   */
  public static function sort(EntityInterface $a, EntityInterface $b) {
    $a_weight = isset($a->weight->value) ? $a->weight->value : 0;
    $b_weight = isset($b->weight->value) ? $b->weight->value : 0;
    if ($a_weight == $b_weight) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($operation == 'view label') {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

}

