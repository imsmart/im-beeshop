<?php

namespace Drupal\bs_shipping\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_order\Entity\OrderInterface;
// use Drupal\profile\Entity\ProfileInterface;

/**
 * Defines the shipment entity class.
 *
 *"storage" = "Drupal\commerce_shipping\ShipmentStorage",
 *
 *
 * @ContentEntityType(
 *   id = "bs_shipment",
 *   label = @Translation("Shipment"),
 *   label_singular = @Translation("shipment"),
 *   label_plural = @Translation("shipments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shipment",
 *     plural = "@count shipments",
 *   ),
 *   bundle_label = @Translation("Shipment type"),
 *   handlers = {
 *     "access" = "Drupal\bs_shipping\ShipmentAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\bs_shipping\ShipmentsListBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_shipping\Form\ShipmentForm",
 *       "add" = "Drupal\bs_shipping\Form\ShipmentForm",
 *       "edit" = "Drupal\bs_shipping\Form\ShipmentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_shipping\Routing\ShipmentRouteProvider",
 *     },
 *   },
 *   base_table = "bs_shipment",
 *   admin_permission = "administer bs_shipment",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "sid",
 *     "bundle" = "type",
 *     "label" = "sid",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-page" = "/admin/beeshop/order/orders/list/{bs_order}/shipments/add",
 *     "add-form" = "/admin/beeshop/order/orders/list/{bs_order}/shipments/add/{bs_shipment_type}",
 *     "edit-form" = "/admin/beeshop/order/orders/list/{bs_order}/shipments/{bs_shipment}/edit",
 *     "by-order-collection" = "/admin/beeshop/order/orders/list/{bs_order}/shipments",
 *   },
 *   bundle_entity_type = "bs_shipment_type",
 *   field_ui_base_route = "entity.bs_shipment_type.edit_form",
 * )
 */
class Shipment extends ContentEntityBase implements ShipmentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(OrderInterface $order) {
    $this->set('order_id', $order);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->get('order_id')->target_id;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function getShippingProfile() {
//     return $this->get('shipping_profile')->entity;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function setShippingProfile(ProfileInterface $profile) {
//     $this->set('shipping_profile', $profile);
//     return $this;
//   }

  /**
   * {@inheritdoc}
   */
  public function getTrackingCode() {
    //return $this->get('tracking_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrackingCode($tracking_code) {
    //$this->set('tracking_code', $tracking_code);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key, $default = NULL) {
    $data = [];
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
    }
    return isset($data[$key]) ? $data[$key] : $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setData($key, $value) {
    $this->get('data')->__set($key, $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  public function getShippingMethodId() {
    return $this->get('shipping_method')->target_id;
  }

  public function getShippingMethod() {
    $methods = $this->get('shipping_method')->referencedEntities();
    return reset($methods);
  }

  /**
   * {@inheritdoc}
   */
  public function getShippedTime() {
    return $this->get('shipped')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShippedTime($timestamp) {
    $this->set('shipped', $timestamp);
    return $this;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function preSave(EntityStorageInterface $storage) {
//     parent::preSave($storage);

//     if (empty($this->getPackageType()) && !empty($this->getShippingMethodId())) {
//       $default_package_type = $this->getShippingMethod()->getPlugin()->getDefaultPackageType();
//       $this->set('package_type', $default_package_type->getId());
//     }
//     $this->recalculateWeight();

//     foreach (['order_id', 'items'] as $field) {
//       if ($this->get($field)->isEmpty()) {
//         throw new EntityMalformedException(sprintf('Required shipment field "%s" is empty.', $field));
//       }
//     }
//   }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The order backreference.
    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setDescription(t('The parent order.'))
      ->setSetting('target_type', 'bs_order')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['shipping_method'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shipping method'))
    ->setRequired(TRUE)
    ->setDescription(t('The shipping method'))
    ->setSetting('target_type', 'bs_shipping_method')
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

//     $fields['tracking_code'] = BaseFieldDefinition::create('string')
//       ->setLabel(t('Tracking code'))
//       ->setDescription(t('The shipment tracking code.'))
//       ->setDefaultValue('')
//       ->setSetting('max_length', 255)
//       ->setDisplayConfigurable('form', TRUE)
//       ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the shipment was created.'))
      ->setRequired(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the shipment was last updated.'))
      ->setRequired(TRUE);

    $fields['shipped'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Shipped'))
      ->setDescription(t('The time when the shipment was shipped.'));



    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if (!$this->get('order_id')->isEmpty()) {
      $uri_route_parameters['bs_order'] = $this->get('order_id')->target_id;
    }

    return $uri_route_parameters;
  }
}
