<?php

namespace Drupal\bs_shipping\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\bs_shipping\ShippingMethodInterface;

/**
 * Defines the interface for shipments.
 */
interface ShipmentInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the parent order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order, or NULL if unknown.
   */
  public function getOrder();


  /**
   * @param OrderInterface $order
   *
   * @return \Drupal\bs_shipping\Entity\ShipmentInterface
   */
  public function setOrder(OrderInterface $order);

  /**
   * Gets the parent order ID.
   *
   * @return int|null
   *   The order ID, or NULL if unknown.
   */
  public function getOrderId();

//   /**
//    * Gets the shipping method.
//    *
//    * @return \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null
//    *   The shipping method, or NULL if unknown.
//    */
//   public function getShippingMethod();

//   /**
//    * Sets the shipping method.
//    *
//    * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method
//    *   The shipping method.
//    *
//    * @return $this
//    */
//   public function setShippingMethod(ShippingMethodInterface $shipping_method);

//   /**
//    * Gets the shipping method ID.
//    *
//    * @return int|null
//    *   The shipping method ID, or NULL if unknown.
//    */
//   public function getShippingMethodId();

//   /**
//    * Sets the shipping method ID.
//    *
//    * @param int $shipping_method_id
//    *   The shipping method ID.
//    *
//    * @return $this
//    */
//   public function setShippingMethodId($shipping_method_id);

//   /**
//    * Gets the shipping profile.
//    *
//    * @return \Drupal\profile\Entity\ProfileInterface
//    *   The shipping profile.
//    */
//   public function getShippingProfile();

//   /**
//    * Sets the shipping profile.
//    *
//    * @param \Drupal\profile\Entity\ProfileInterface $profile
//    *   The shipping profile.
//    *
//    * @return $this
//    */
//   public function setShippingProfile(ProfileInterface $profile);

  /**
   * Gets the shipment tracking code.
   *
   * Only available if shipping method supports tracking and the shipment
   * itself has been shipped.
   *
   * @return string|null
   *   The shipment tracking code, or NULL if unknown.
   */
  public function getTrackingCode();

  /**
   * Sets the shipment tracking code.
   *
   * @param string $tracking_code
   *   The shipment tracking code.
   *
   * @return $this
   */
  public function setTrackingCode($tracking_code);

  /**
   * Gets a shipment data value with the given key.
   *
   * Used to store temporary data.
   *
   * @param string $key
   *   The key.
   * @param mixed $default
   *   The default value.
   *
   * @return array
   *   The shipment data.
   */
  public function getData($key, $default = NULL);

  /**
   * Sets a shipment data value with the given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   *
   * @return $this
   */
  public function setData($key, $value);

  /**
   * Gets the shipment creation timestamp.
   *
   * @return int
   *   The shipment creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the shipment creation timestamp.
   *
   * @param int $timestamp
   *   The shipment creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the shipment shipped timestamp.
   *
   * @return int
   *   The shipment shipped timestamp.
   */
  public function getShippedTime();


  /**
   * @return \Drupal\bs_shipping\ShippingMethodInterface;
   */
  public function getShippingMethod();


  public function getShippingMethodId();

  /**
   * Sets the shipment shipped timestamp.
   *
   * @param int $timestamp
   *   The shipment shipped timestamp.
   *
   * @return $this
   */
  public function setShippedTime($timestamp);

}
