<?php
namespace Drupal\bs_shipping\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Shipping method annotation object.
 *
 * @ingroup beeshop_shipping_api
 *
 * @Annotation
 */
class ShippingMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label of the shipping method.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

  /**
   * The category in the admin UI where the shipping method will be listed.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category = '';

}

