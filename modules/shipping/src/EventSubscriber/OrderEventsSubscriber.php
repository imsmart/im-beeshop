<?php

/**
* @file
* Contains \Drupal\bs_order\EventSubscriber.
*/

namespace Drupal\bs_shipping\EventSubscriber;

use Drupal\bs_order\Event\OrderEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\bs_order\Event\OrderEvents;
use Drupal\bs_shipping\Entity\ShipmentInterface;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\bs_price\PriceAdjustmentItemList;


/**
 * Class OrderEventsSubscriber.
 *
 * @package Drupal\bs_order
 */
class OrderEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrderEventsSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      OrderEvents::ORDER_DELETE => ['onOrderDelete', -100],
      OrderEvents::ORDER_INSERT => ['onOrderInsert', -100],
      OrderEvents::ORDER_PRESAVE => ['onOrderPresave', -100],
      OrderEvents::ORDER_CREATE => ['onOrderCreate', -100],
    ];
    return $events;
  }

  public function onOrderDelete(OrderEvent $event) {

    $order = $event->getOrder();

    $shipments_storage = $this->entityTypeManager->getStorage('bs_shipment');

    if ($order_shipments = $shipments_storage->loadByProperties(['order_id' => $order->id()])) {
      $shipments_storage->delete($order_shipments);
    }

  }

  public function onOrderCreate(OrderEvent $event) {
    /**
     * @var OrderInterface $order
     */
    $order = $event->getOrder();

    if ($cart = $order->cart) {
      /**
       * @var \Drupal\bs_cart\CartInterface $cart
       */

      if ($cart->hasField('shipping_method') && !$cart->get('shipping_method')->isEmpty()
        && ($shipping_method_id = $cart->get('shipping_method')->target_id)) {

          $order->set('shipping_method', $shipping_method_id);

        }
    }

    self::appendShipmentAdjustpent($order);

  }

  public function onOrderInsert(OrderEvent $event) {
    /**
     * @var OrderInterface $order
     */
    $order = $event->getOrder();
    self::appendShipmentAdjustpent($order);

    if ($order->hasField('shipments') && !$order->hasAdjustment('bs_shipping_order_price_adjustment')) {

      foreach ($order->get('shipments')->referencedEntities() as $shipment) {
        /**
         * @var ShipmentInterface $shipment
         */

        if ($shipment->get('order_id')->isEmpty()) {
          $shipment->setOrder($order);
          $shipment->save();
        }

      }

    }

  }

  public function onOrderPresave(OrderEvent $event) {
    $order = $event->getOrder();
    if ($order->isNew()) {
      self::appendShipmentAdjustpent($order);
    }

  }

  public static function appendShipmentAdjustpent(OrderInterface $order) {

    $shipping_price = NULL;

    if ($order->hasField('shipping_method') && !$order->get('shipping_method')->isEmpty()) {

      if ($shipping_method_plugin = $order->get('shipping_method')->first()->entity->getPlugin()) {
        $shipping_price = $shipping_method_plugin->getShippingPrice($order);
      }

    }

    if ($shipping_price) {
      $shipping_order_adjustment = [
        'type' => 'bs_shipping_order_price_adjustment',
        'label' => t('Shipping'),
        'amount' => $shipping_price->getPriceValue(),
      ];

      $order->get('adjustments')->appendOrReplaceItem($shipping_order_adjustment, TRUE);

    }
  }

}

