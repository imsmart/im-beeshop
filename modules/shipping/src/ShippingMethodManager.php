<?php

namespace Drupal\bs_shipping;

use Drupal\bs_shipping\ShippingMethodManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Context\ContextAwarePluginManagerTrait;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;

class ShippingMethodManager extends DefaultPluginManager implements ShippingMethodManagerInterface {

  use CategorizingPluginManagerTrait {
    getSortedDefinitions as traitGetSortedDefinitions;
  }
  use ContextAwarePluginManagerTrait;

  /**
   * Constructs a new \Drupal\bs_shipping\ShippingMethodManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Besshop/Shipping/methods',
      $namespaces,
      $module_handler,
      'Drupal\bs_shipping\ShippingMethodPluginInterface',
      'Drupal\bs_shipping\Annotation\ShippingMethod');

    $this->alterInfo('beeshop_shipping_method');
    $this->setCacheBackend($cache_backend, 'beeshop_shipping_method_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getSortedDefinitions(array $definitions = NULL) {
    // Sort the plugins first by category, then by admin label.
    $definitions = $this->traitGetSortedDefinitions($definitions, 'label');
    // Do not display the 'broken' plugin in the UI.
    unset($definitions['broken']);
    return $definitions;
  }

}

