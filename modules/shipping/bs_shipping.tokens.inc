<?php

/**
 * @file
 * Builds placeholder replacement tokens for node-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\bs_price\Price;

/**
 * Implements hook_token_info_alter().
 */
function bs_shipping_token_info_alter(&$data) {

  $data['tokens']['bs_order']['shipping_price'] = [
    'name' => t("Order shipping price"),
    'description' => t("Order shipping price"),
  ];

}

/**
 * Implements hook_tokens().
 */
function bs_shipping_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'bs_order' && !empty($data['bs_order'])) {
    /** @var OrderInterface $bs_order */
    $bs_order = $data['bs_order'];

    foreach ($tokens as $name => $original) {

      switch ($name) {
        case 'shipping_price':
          $shipping_price = 0;

          foreach ($bs_order->get('adjustments') as $adjustment) {
            if ($adjustment->type != 'bs_shipping_order_price_adjustment') {
              continue;
            }

            $shipping_price += $adjustment->amount;
          }

          $shipping_price = new Price($shipping_price, $bs_order->getOrderCurrencyCode());
          $replacements[$original] = preg_replace('#\.0+$#', '', $shipping_price->__toString());

          break;
      }
    }
  }

  return $replacements;
}

