<?php

/**
 * @file
 * Implements the shopping cart system and add to cart features.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_cart\AddToCartVariationsWidgetPluginInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Implements hook_entity_base_field_info
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
 * @return \Drupal\Core\Field\BaseFieldDefinition[]
 */
function bs_cart_entity_base_field_info(\Drupal\Core\Entity\EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'bs_product') {
    $fields = array();

    $fields['add_to_cart'] = BaseFieldDefinition::create('bs_add_to_cart')
    ->setLabel(t('Add to cart form'))
    ->setDescription(t('Add to cart'))
    ->setDisplayConfigurable('form', FALSE)
    ->setDisplayConfigurable('view', TRUE)
    ->setComputed(TRUE);

    return $fields;
  }
}

/**
 * Implements hook_field_widget_form_alter.
 */
function bs_cart_field_widget_form_alter(&$element, \Drupal\Core\Form\FormStateInterface $form_state, $context) {

//   dpm($element);

//   // Add a css class to widget form elements for all fields of type mytype.
//   $field_definition = $context['items']
//   ->getFieldDefinition();
//   if ($field_definition
//     ->getType() == 'mytype') {

//       // Be sure not to overwrite existing attributes.
//       $element['#attributes']['class'][] = 'myclass';
//     }
}

/**
 * Implements hook_form_FORM_ID_alter() for 'field_config_edit_form'.
 */
function bs_cart_form_field_config_edit_form_alter(array &$form, FormStateInterface $form_state) {

  /**
   * @var FieldConfig $field
   */
  $field = $form_state->getFormObject()->getEntity();
  $field_type = $field->getType();

  switch ($field->getTargetEntityTypeId()) {
    case 'bs_cart':
      $form['third_party_settings']['bs_cart'] = [
      '#type' => 'container',
      '#weight' => -20,
      ];

      $form['third_party_settings']['bs_cart']['copy_to_order'] = [
        '#type' => 'checkbox',
        '#title' => t('Copy field value to order'),
        '#default_value' => $field->getThirdPartySetting('bs_cart', 'copy_to_order', FALSE),
      ];
      break;
    case 'bs_product':
      $product_type_id = $field->getTargetBundle();
      $product_type = \Drupal::entityTypeManager()->getStorage('bs_product_type')->load($product_type_id);

      if (!$product_type->isVariationsSuport()) return;

      $form['third_party_settings']['bs_cart'] = [
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#title' => 'Settings for "Add to cart" form',
        '#weight' => 20,
        '#prefix' => '<div id="third-party-settings-bs-cart">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [
            ':input[name="third_party_settings[bs_product][isVariationCharacteristicField]"]' => ['checked' => TRUE],
          ]
        ]
      ];

      /**
       * @var Drupal\bs_cart\AddToCartVariationsWidgetPluginManager $widgets_plugin_mager
       */
      $widgets_plugin_mager = \Drupal::service('plugin.manager.add_to_cart_variations_widget_plugin');

      $add_to_cart_variation_widget_settings = $field->getThirdPartySetting('bs_cart', 'add_to_cart_variation_widget', []);

      $add_to_cart_variation_widget_settings += [
        'configuration' => [
          'field_definition' => $field,
        ],
      ];

      $form['third_party_settings']['bs_cart']['add_to_cart_variation_widget'] = [
        'type' => [
          '#type' => 'select',
          '#title' => t('Widget plugin for "Add to cart form"'),
          //'#title_display' => 'invisible',
          '#options' => $widgets_plugin_mager->getApplicablePluginOptions($field),
          '#default_value' => !empty($add_to_cart_variation_widget_settings['type']) ? $add_to_cart_variation_widget_settings['type'] : '',
          '#empty_option' => t('Select plugin for "Add to cart form"'),
          '#ajax' => [
            'callback' => 'bs_cart_ajax_field_third_party_settings',
            'wrapper' => 'third-party-settings-bs-cart',
          ],
        ],
      ];

      if (!empty($add_to_cart_variation_widget_settings['type'])) {
        /**
         * @var AddToCartVariationsWidgetPluginInterface $plugin
         */
        $add_to_cart_variation_widget_settings['configuration']['field_definition'] = $field;
        $plugin = $widgets_plugin_mager->createInstance($add_to_cart_variation_widget_settings['type'], $add_to_cart_variation_widget_settings['configuration']);

        if ($widget_settings_form = $plugin->settingsForm($form, $form_state, $field)) {
          $form['third_party_settings']['bs_cart']['add_to_cart_variation_widget']['configuration'] = $widget_settings_form;
        }
      }
      break;
  }

//   $label_content_options = [
//     '' => t('Rendered variation entity'),
//   ];

//   if ($field_type == 'entity_reference') {
//     $label_content_options['rendered_referenced_entity'] = t('Rendered referenced entity');
//   }

//   $form['third_party_settings']['bs_cart']['add_to_cart_form_output_label_mode'] = [
//     '#type' => 'select',
//     '#title' => t('Label content'),
//     '#options' => $label_content_options,
//     '#default_value' => $field->getThirdPartySetting('bs_cart', 'add_to_cart_form_output_label_mode', 'value_or_entity_label'),
//     '#ajax' => [
//       'callback' => 'bs_cart_ajax_field_third_party_settings',
//       'wrapper' => 'third-party-settings-bs-cart',
//     ],
//   ];

//   $rendered_entity_output_modes = [
//     'rendered_variation_entity',
//     'rendered_referenced_entity'
//   ];

//   $entity_display_repository = \Drupal::service('entity_display.repository');

//   $label_mode = !empty($form['third_party_settings']['bs_cart']['add_to_cart_form_output_label_mode']['#value']) ?
//                 $form['third_party_settings']['bs_cart']['add_to_cart_form_output_label_mode']['#value'] :
//                 $form['third_party_settings']['bs_cart']['add_to_cart_form_output_label_mode']['#default_value'];

//   if (in_array($label_mode, $rendered_entity_output_modes)) {

//      $options = [];

//      switch ($label_mode) {
//        case 'rendered_variation_entity':
//          $options = $entity_display_repository->getViewModeOptions('bs_product');
//          break;
//        case 'rendered_referenced_entity':
//          if ($target_type = $field->getFieldStorageDefinition()->getSetting('target_type')) {
//            $options = $entity_display_repository->getViewModeOptions($target_type);
//          }
//          break;
//      }

//      if ($options) {
//        $form['third_party_settings']['bs_cart']['add_to_cart_form_output_label_mode_id'] = [
//          '#type' => 'select',
//          '#title' => t('Mode'),
//          '#options' => $options,
//          '#default_value' => $field->getThirdPartySetting('bs_cart', 'add_to_cart_form_output_label_mode_id', NULL),
//          '#empty_option' => t('Select entity view mode'),
//        ];
//      }
//      else {
//        \Drupal::messenger()->addWarning('Cant resolve view mode for entity');
//      }

//   }

//   dpm($form_state);

}

function bs_cart_ajax_field_third_party_settings(array &$form, FormStateInterface $form_state) : array {
  return $form['third_party_settings']['bs_cart'];
}

function bs_cart_entity_type_alter(array &$entity_types) {

  /** @var $entity_types \Drupal\Core\Entity\EntityTypeInterface[] */
  $entity_types['bs_product']->setLinkTemplate('add-to-cart', '/product/{bs_product}/add-to-cart');

}

function bs_cart_entity_extra_field_info() {
  $extra['bs_cart']['bs_cart']['form']['actions'] = [
      'label' => t('Actions'),
      'weight' => 0,
      'visible' => TRUE,
    ];
  return $extra;
}


/**
 * Implements hook_theme().
 */
function bs_cart_theme($existing, $type, $theme, $path) {
  return [
    'bs_cart_block' => [
//       'render element' => 'elements',
      'variables' => [
        'cart' => NULL,
        'is_empty' => TRUE,
        'items_count' => 0,
        'count_text' => '',
        'url' => NULL,
        'id' => NULL,
      ],
    ],
    'bs_cart_summary' => [
      'variables' => [
        'cart' => NULL,
      ],
    ],
    'bs_cart_empty_page' => [
      'render element' => 'element',
    ],
  ];
}

function template_preprocess_bs_cart_summary(&$variables) {

  $cart = $variables['cart'];

  $variables['cart_total_amount'] = [
    '#theme' => 'bs_price_item',
    '#price_item' => $cart->getCartTotalAmount(),
  ];

}

/**
 * Prepares variables for the cart block element template.
 */
function template_preprocess_bs_cart_block(&$variables) {

  /**
   * @var \Drupal\bs_cart\CartInterface $cart
   */
  $cart = $variables['cart'];

  $variables['attributes']['class'][] = 'cart-block';

  if ($cart->isEmpty()) {
    $variables['attributes']['class'][] = 'cart-block__cart-is-empty';
  }

  $variables['attributes']['data-block-id'] = $variables['id'];
}

function template_preprocess_bs_cart_page(&$variables) {

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}

/**
 * Implements hook_views_data().
 */
function bs_cart_views_data() {
  $data['views']['bs_cart_summary'] = [
    'title' => t('Cart summary'),
    'help' => t('Displays the sopping cart summary info, requires an Cart ID argument.'),
    'area' => [
      'id' => 'bs_cart_summary',
    ],
  ];
  return $data;
}

function bs_cart_views_data_alter(array &$data) {


  $data['bs_product_field_data']['add_to_cart']['field'] = [
    'title' => t('Add to cart form'),
    'help' => t('Adds a "Add to cart form".'),
    'id' => 'bs_cart_add_to_cart_form_field',
  ];

}

function bs_cart_page_attachments(array &$attachments) {

  $route = \Drupal::routeMatch()->getRouteObject();

  if (\Drupal::service('router.admin_context')->isAdminRoute($route)) {
    return;
  }

  $attachments['#attached']['library'][] = 'bs_cart/cart.main';

  $attachments['#attached']['drupalSettings']['BeeShop']['cartDataFetchPath'] = Url::fromRoute('bs_cart.ajax.cart_data')->toString();

}
