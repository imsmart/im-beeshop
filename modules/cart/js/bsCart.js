/**
 *
 */
(function (window, Drupal, drupalSettings) {

  window.dataLayer = window.dataLayer || [];

  Drupal.BeeShop.cart = function() {
  	cartData = null;

  	cartItems = {}

  	this.fetchCartData();

  };

  window.addEventListener('BeeShop:productAddedToCart', function(e) {
  	window.dataLayer = window.dataLayer || [];
  	window.dataLayer.push({
  	 'ecommerce': {
  	    'add': {
  	      'products': [e.detail.args.product[0]]
  	    }
  	 },
  	 'event': 'ee-event',
  	 'ee-event-category': 'Enhanced Ecommerce',
  	 'ee-event-action': 'Product added to a Shopping Cart',
  	 'ee-event-label': e.detail.args.product[0].name,
  	 'ee-event-ni': 'False',
  	});

  	window['BsCart'].fetchCartData();
  });

  Drupal.BeeShop.cart.prototype.processProductsForoms = function () {
    if (typeof this.cartData == 'undefined' || typeof this.cartData['items'] == 'undefined') {
      return;
    }
    for (const [pid, item] of Object.entries(this.cartData['items'])) {
      document.querySelectorAll(`form[data-base-product-id="${pid}"]`).forEach(form => {
        form.classList.add('allready-in-cart');
        form.querySelectorAll('input.button--add-to-cart').forEach(addToCartButton => {
          addToCartButton.value = 'Купить еще';
          addToCartButton.classList.add('add-more');
        });
      });
      document.querySelectorAll(`a.add-to-cart--link[data-pid="${pid}"]`).forEach(anchor => {
        anchor.classList.add('allready-in-cart');
        anchor.classList.add('add-more');
        anchor.textContent = 'Купить еще';
      });
    }
  }

  Drupal.BeeShop.cart.prototype.getItemData = function (pid) {
    return this.cartData.items[pid];
  }

  Drupal.BeeShop.cart.prototype.fetchCartData = function () {
  	var cart = this;
  	if (drupalSettings.BeeShop.cartDataFetchPath) {
  		var xhr = new XMLHttpRequest();

  		xhr.onreadystatechange = function() {
  	    if (this.readyState == 4 && this.status == 200) {
  	    	cart.cartData = JSON.parse(this.responseText);

  	    	var event = new CustomEvent('BeeShop.cart.cartDataFetched', {
  	    		detail: { cart: cart }
  	    	});
  	    	window.dispatchEvent(event);

  	    	cart.processProductsForoms();
		    }
		  };

  		xhr.open('GET', drupalSettings.BeeShop.cartDataFetchPath, true);
  		xhr.send();
  	}
  }

  Drupal.BeeShop.cart.prototype.getData = function () {
    return this.cartData;
  }

  window.addEventListener("BeeShop.cart.cartDataFetched", function() {
  	document.querySelectorAll('input[type="number"].input--add-to-cart-count-spinner').forEach( el => {
      if (el.dataset['pid']) {
        if (itemData = window.BsCart.getItemData(el.dataset['pid'])) {
          el.value = itemData.quantity.replace(/\.0+$/,'');
        }
      }
    });
  });

  document.addEventListener("DOMContentLoaded", function() {
  	window['BsCart'] = new Drupal.BeeShop.cart();
  	var event = new CustomEvent('BeeShop.cart.cart_initilized', {
  		detail: { cart: window['BsCart'] }
  	});
  	window.dispatchEvent(event);
  });

  Drupal.behaviors.bsCart = {
	  attach: function (context, settings) {

	    if (typeof window['BsCart'] != 'undefined') {

	    }

      if (typeof ISpin != 'undefined') {
        context.querySelectorAll('input[type="number"].input--add-to-cart-count-spinner').forEach( el => {
          var spinner = new ISpin(el, {
            // options with defaults
            wrapperClass: 'ispin-wrapper',
            buttonsClass: 'ispin-button',
            step: 1,
            pageStep: 10,
            disabled: false,
            repeatInterval: 200,
            wrapOverflow: false,
            parse: Number,
            format: String
          });
        });
      }

	  },
  };

})(window, Drupal, drupalSettings);