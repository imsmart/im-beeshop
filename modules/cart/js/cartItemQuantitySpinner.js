/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {
  
  Drupal.behaviors.bsCartItemQuantitySpinner = {
      attach: function (context, settings) {
      	
        var typingTimer;
        var doneTypingInterval = 2000;
        
        function clearTypingTimer() {
          clearTimeout(typingTimer);
        }
        
      	if (typeof $.fn.spinner != 'undefined') {
      		$('input.cart-item-quantity', context).once('itemQuantitySpinner').each(function (index, element) {
      		  
      		  element.addEventListener('keydown', clearTypingTimer);
      		  element.addEventListener('keypress', clearTypingTimer);
      		  element.addEventListener('keyup', clearTypingTimer);
      		  
          	$(element).spinner({
          		start: function (event, ui) {
          			var val = $(element).val();
          			$(element).data('prevVal', val);
          		},
          		stop: function(event, ui) {
          			var val = $(element).val(),
          			    prevValue = $(element).data('prevVal');
          			
          			if (val != "" && (!prevValue || prevValue != val)) {
                  typingTimer = setTimeout(doneTyping, doneTypingInterval);
                  function doneTyping() {
                    $(element).change();
                  }
          			}
          			
              },
          	});
          });
      	}
      	
      },
  };
  
})(jQuery, window, Drupal, drupalSettings);