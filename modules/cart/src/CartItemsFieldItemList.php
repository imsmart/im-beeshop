<?php
namespace Drupal\bs_cart;

use Drupal\Core\Field\EntityReferenceFieldItemList;

class CartItemsFieldItemList extends EntityReferenceFieldItemList {

  public function getItemByProductId($product_id) {
    if (isset($this->list)) {

      $result = array_filter($this->list, function ($item) use ($product_id) {
        if ($item->entity && $item->entity->getPid() == $product_id ) {
          return TRUE;
        }
      });

      if ($result) {
        $item = reset($result);
        return $item->entity;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    if ($this->isEmpty()) {
      return [];
    }

    $target_entities = [];
    foreach ($this->list as $delta => $item) {
      if ($item->entity !== NULL) {
        $target_entities[$delta] = $item->entity;
      }
    }

    return $target_entities;
  }

}

