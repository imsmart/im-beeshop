<?php
namespace Drupal\bs_cart;

use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_module_test\Plugin\ImageEffect\NullTestImageEffect;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_product\ProductFunctionsTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Render\MainContent\MainContentRendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\bs_cart\Event\AddToCartFormEvents;
use Drupal\bs_cart\Event\AddToCartFormAjaxVariationChangedEvent;
use Drupal\bs_product\ProductVariationsFieldItemListInterface;
use Drupal\dblog\Plugin\views\wizard\Watchdog;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

abstract class AddToCartVariationsWidgetBase extends PluginBase implements AddToCartVariationsWidgetInterface {

  use VariationsRendererTrait;

  /**
   * @var PluginManagerInterface
   */
  protected $variationItemFormatterManager;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, array $configuration, PluginManagerInterface $variation_item_formatter_plugin_manager, PluginManagerInterface $variation_widget_plugin_manager) {
    parent::__construct([], $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->variationItemFormatterManager = $variation_item_formatter_plugin_manager;
    $this->variationWidgetPluginManager = $variation_widget_plugin_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('plugin.manager.add_to_cart_variation_item_label_formatter'),
      $container->get('plugin.manager.add_to_cart_variations_widget_plugin'),
      $container->get('main_content_renderer.ajax'),
      $container->get('current_route_match')
      );
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Config\Entity\ThirdPartySettingsInterface::setThirdPartySetting()
   */
  public function setThirdPartySetting($module, $key, $value) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Config\Entity\ThirdPartySettingsInterface::getThirdPartySetting()
   */
  public function getThirdPartySetting($module, $key, $default = NULL) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Config\Entity\ThirdPartySettingsInterface::getThirdPartySettings()
   */
  public function getThirdPartySettings($module) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Config\Entity\ThirdPartySettingsInterface::unsetThirdPartySetting()
   */
  public function unsetThirdPartySetting($module, $key) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Config\Entity\ThirdPartySettingsInterface::getThirdPartyProviders()
   */
  public function getThirdPartyProviders() {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   *
   * @return FieldDefinitionInterface or NULL
   */
  public function getFieldDefinition() {
    return !empty($this->configuration['field_definition']) ? $this->configuration['field_definition'] : NULL;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Component\Plugin\ConfigurablePluginInterface::setConfiguration()
   */
  public function setConfiguration(array $configuration) {
    // TODO Auto-generated method stub
    $this->configuration = $configuration;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Component\Plugin\ConfigurablePluginInterface::defaultConfiguration()
   */
  public function defaultConfiguration() {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function calculateDependencies() {
    return [];
  }

  public function isCustomTitleSupport() {
    $definition = $this->getPluginDefinition();

    return !empty($definition['custom_title']);
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = [];

    if (!$this->isCustomTitleSupport()) return $element;

    $conf = $this->getConfiguration();

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title for "Add to cart form"'),
      '#description' => t('If empty main field label was used for "Add to cart form"'),
      '#default_value' => !empty($conf['title']) ? $conf['title'] : '',
    ];


    $element['label_formatter']['type'] = [
      '#type' => 'select',
      '#title' => t('Items label formatter'),
      '#options' => $this->variationItemFormatterManager->getApplicablePluginOptions($this->getFieldDefinition()),
      '#default_value' => !empty($this->configuration['label_formatter']['type']) ? $this->configuration['label_formatter']['type'] : null,
      '#empty_option' => $this->t('None'),
      '#ajax' => [
        'callback' => 'bs_cart_ajax_field_third_party_settings',
        'wrapper' => 'third-party-settings-bs-cart',
      ],
    ];

    $element['label_formatter']['configuration_container'] = [
      '#type' => 'container'
    ];

    if (!empty($this->configuration['label_formatter']['type'])) {
      $configuration['field_definition'] = $this->getFieldDefinition();
      $plugin = $this->variationItemFormatterManager->createInstance($this->configuration['label_formatter']['type'], $configuration);

      if ($formatter_settings_form = $plugin->settingsForm($form, $form_state)) {
        $element['label_formatter']['configuration'] = $formatter_settings_form;
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // By default, widgets are available for all fields.
    return TRUE;
  }

  public static function ajaxVariationChanged(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\Core\Render\MainContent\MainContentRendererInterface $ajax_renderer */
    $ajax_renderer = \Drupal::service('main_content_renderer.ajax');
    $request = \Drupal::request();
    $route_match = \Drupal::service('current_route_match');
    /** @var \Drupal\Core\Ajax\AjaxResponse $response */
    $response = $ajax_renderer->renderResponse($form, $request, $route_match);

    if (($triggering_element = $form_state->getTriggeringElement()) && !empty($triggering_element['#is_variation'])) {
      $id = $triggering_element['#value'];

      // Allow modules to add arbitrary ajax commands to the response.
      $event = new AddToCartFormAjaxVariationChangedEvent($form, $id, $response);
      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event_dispatcher->dispatch(AddToCartFormEvents::ADD_TO_CART_FORM_AJAX_VARIATION_CHANGED, $event);

//       $renderer = \Drupal::service('renderer');

//       if ($product = Product::load($id)) {

//         $render_controller = \Drupal::entityTypeManager()
//         ->getViewBuilder($product
//           ->getEntityTypeId());

//         $product->addToCartFormPlaceholderId = 'add-to-cart--' . $form['#pid'];

//         $entity_render_array = $render_controller
//         ->view($product, 'default');

//         $response->addCommand(new HtmlCommand('[data-product-id="' . $form['#pid'] . '"]', $entity_render_array), TRUE);
//       }

    }

    return $response;
  }

  protected function getTitle() {

    if (!$this->getFieldDefinition()) return '';

    if (!$this->isCustomTitleSupport()) return $this->getFieldDefinition()->getLabel();

    $conf = $this->getConfiguration();

    $title = !empty($conf['title']) ? $conf['title'] : '';

    return !$title ? $this->getFieldDefinition()->getLabel() :
           ($title == '<none>' ? '' : $title);
  }

  /**
   * @return Drupal\bs_cart\AddToCartVariationItemLabelFormatterInterface
   */
  protected function getItemLabelFormatter() {

    if (empty($this->configuration['label_formatter']['type'])) return NULL;

    $formatter_plugin_options = [
      'type' => $this->configuration['label_formatter']['type'],
      'configuration' => !empty($this->configuration['label_formatter']['configuration']) ? $this->configuration['label_formatter']['configuration'] : [],
    ];

    $formatter_plugin_options['configuration']['field_definition'] = $this->getFieldDefinition();

    return $this->variationItemFormatterManager->getInstance($formatter_plugin_options);
  }

  protected function getItemLabel(ProductInterface $variation) {
    if ($formatter = $this->getItemLabelFormatter()) {
      return $formatter->getItemLabel($variation);
    }

    return '';
  }

}

