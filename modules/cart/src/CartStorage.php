<?php
namespace Drupal\bs_cart;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\bs_cart\Entity\CartItem;

class CartStorage extends SqlContentEntityStorage implements CartStorageInterface {


  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $entities = parent::loadMultiple($ids);

    foreach ($entities as $cart_entity) {
      //       $cart_entity->checkCartItems();
      $cart_entity->validateAndFix();
    }

    return $entities;
  }

  public function getCurrentUserCart() {

    $cart = &drupal_static('user_cart', NULL);

    if ($cart) {
      return $cart;
    }

    $session = \Drupal::request()->getSession();
    if (($cart_id = $session->get('cart_uuid')) &&
        ($carts = $this->loadByProperties([
          'uuid' => $cart_id,
        ]))) {

          return reset($carts);
    }

    $current_user = \Drupal::currentUser();

    if ($current_user->isAuthenticated() && ($stored_user_carts = $this->loadByProperties([
          'uid' => \Drupal::currentUser()->id()
        ]))) {
          $cart = reset($stored_user_carts);
          return $cart;
    }


    // No stored carts find, create new cart entity
    $cart = $this->create();
    $cart->save();


    return $cart;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFromStorage(array $ids = NULL) {
    $entities = parent::getFromStorage($ids);

    foreach ($entities as $cid => $cart_entity) {
      $cart_items = $this->loadCartItems($cid);
      $cart_entity->set('cart_items', $cart_items);
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function loadCartItems($cart_id) {
    $query = $this->database->select('bs_users_carts_items', 'ci');
    $query->condition('ci.cart_id', $cart_id);
    $query->fields('ci', ['cart_item_id']);

    $cart_items_ids = $query->execute()->fetchCol(0);

    $cart_items = CartItem::loadMultiple($cart_items_ids);
    return $cart_items;
  }

}

