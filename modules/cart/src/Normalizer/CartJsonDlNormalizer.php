<?php
namespace Drupal\bs_cart\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;
use Drupal\bs_cart\CartInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Psr\Log\LoggerInterface;

class CartJsonDlNormalizer extends ContentEntityNormalizer {

  /**
   * The formats that the Normalizer can handle.
   *
   * @var array
   */
  protected $format = ['json_dl'];

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = CartInterface::class;

  /**
   *
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  public function __construct(EntityManagerInterface $entity_manager, ModuleHandlerInterface $module_handler, LoggerInterface $logger) {
    parent::__construct($entity_manager);

    $this->moduleHandler = $module_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {

    /**
     * @var CartInterface $entity
     */

    $result = [];

    $items = [];

    foreach ($entity->getItems() as $cart_item) {
      if ($product = $cart_item->getProduct()) {
        $item = [
          'pid' => $product->id(),
          'quantity' => $cart_item->getQuantity(),
          'price' => $product->getActualPrice(),
        ];

        $items[$product->id()] = $item;
      }
    }

    $result['items'] = $items;
    $result['items_count'] = $entity->getCartItemsCount();
    $result['total_amount']['by_items'] = $entity->getTotalByItems() ? $entity->getTotalByItems()->toArray() : NULL;

    $this->moduleHandler->alter('bs_cart_json_dl', $result, $entity, $context);

    return $result;
  }

}

