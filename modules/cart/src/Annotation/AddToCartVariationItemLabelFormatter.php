<?php
namespace Drupal\bs_cart\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 *
 * @Annotation
 *
 */
class AddToCartVariationItemLabelFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the widget type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short description of the widget type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * An integer to determine the weight of this widget relative to other widgets
   * in the Field UI when selecting a widget for a given field.
   *
   * @var int optional
   */
  public $weight = NULL;

}

