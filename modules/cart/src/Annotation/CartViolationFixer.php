<?php
namespace Drupal\bs_cart\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for BeeShop cart violation fixer plugins.
 *
 * @see \Drupal\bs_cart\Plugin\Beeshop\Cart\CartViolationFixerPluginBase
 *
 * @Annotation
 */
class CartViolationFixer extends Plugin {

  /**
   * The plugin title used in the views UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title = '';

  /**
   * An array of cart violations causes, that the plug in can fix.
   *
   * @var array
   */
  protected $causes = [];

}

