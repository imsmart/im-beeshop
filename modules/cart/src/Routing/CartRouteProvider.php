<?php
namespace Drupal\bs_cart\Routing;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class CartRouteProvider extends DefaultHtmlRouteProvider {

  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('edit-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('edit-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'edit';
      $route
      ->setDefaults([
        '_entity_form' => "{$entity_type_id}.{$operation}",
        '_title' => 'Shopping cart'
          ])
          ->setRequirement('_permission', 'access cart')
//           ->setOption('parameters', [
//             $entity_type_id => ['type' => 'entity:' . $entity_type_id],
//           ])
          ->setOption('no_cache', TRUE)
          ;

          // Entity types with serial IDs can specify this in their route
          // requirements, improving the matching process.
          if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
            $route->setRequirement($entity_type_id, '\d+');
          }
          return $route;
    }
  }


}

