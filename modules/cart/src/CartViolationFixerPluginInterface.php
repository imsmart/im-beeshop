<?php
namespace Drupal\bs_cart;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Symfony\Component\Validator\ConstraintViolation;

interface CartViolationFixerPluginInterface extends PluginInspectionInterface {

  /**
   * @param \Symfony\Component\Validator\ConstraintViolation $violation
   */
  public function fixViolation($violation);

}

