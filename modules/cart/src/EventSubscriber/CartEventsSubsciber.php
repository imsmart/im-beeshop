<?php
namespace Drupal\bs_cart\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\bs_cart\Event\CartEvents;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\beeshop\Ajax\BeeShopUpdateSettings;
use Drupal\Core\Form\FormState;
use Symfony\Component\EventDispatcher\Event;
use Drupal\bs_cart\Event\CartEvent;
use Drupal\beeshop\Ajax\BeeShopEventCommand;

class CartEventsSubsciber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      CartEvents::CART_UPDATED => ['onCartUpdated', 10],
      CartEvents::PRODUCT_ADDED_TO_CART => ['onProductAddedToCart', 10],
    ];
    return $events;
  }

  public function onProductAddedToCart(CartEvent $event) {

    if ($event instanceof CartAjaxEvent) {
      $this->ajaxOnProductAddedToCart($event);
      return;
    }


  }

  public function onCartUpdated(CartEvent $event) {
    if ($event instanceof CartAjaxEvent) {
      $this->ajaxOnCartUpdated($event);
      return;
    }

  }

  protected function addUpdateCartBlocksCommand(CartAjaxEvent $event) {
    if ($ajax_page_state = $event->getRequest()->get('ajax_page_state')) {
      if (!empty($ajax_page_state['beeshop']['cartBlocks'])) {
        foreach ($ajax_page_state['beeshop']['cartBlocks'] as $id => $config) {
          $block_manager = \Drupal::service('plugin.manager.block');
          $plugin_block = $block_manager->createInstance('bs_cart_block', $config);
          $render = $plugin_block->build();

          $event->getResponse()->addCommand(new ReplaceCommand('.block-bs-cart .block-content [data-block-id="' . $id . '"]', $render));
        }
      }
    }
  }

  protected function onProductAddedToCartNotAjax(CartEvent $event) {

  }

  protected function ajaxOnProductAddedToCart(CartAjaxEvent $event) {

    if ($product = $event->getData(CartEvents::DATA_ADDED_PRODUCT)) {

      debug('ajaxOnProductAddedToCart:' . $product->id());

      $response = $event->getResponse();

      $product_data_array = [
        'name' => $product->getTitle(),
        'id' => $product->getRootParent() ? $product->getRootParent()->id() : $product->id(),
        'price' => $product->getActualPrice()->value,
        'quantity' => 1,
      ];

      if ($product->getRootParent()) {
        $product_data_array['variant'] = $product->label();
      }

      $response->addCommand(new BeeShopEventCommand('BeeShop:productAddedToCart', [
        'product' => [
          $product_data_array
        ]
      ]));
    }

  }

  protected function ajaxOnCartUpdated(CartAjaxEvent $event) {
    $cart = $event->getCart();

    if ($ajax_page_state = $event->getRequest()->get('ajax_page_state')) {
      if (!empty($ajax_page_state['beeshop']['cartEditForm']['wrapperId'])) {
        // Cart form present on page we need to replace it by updated cart form
        $form = \Drupal::service('entity.form_builder')->getForm($event->getCart(), 'default');

        $replace_cart_form_command = new ReplaceCommand('#' . $ajax_page_state['beeshop']['cartEditForm']['wrapperId'], $form);
        $event->getResponse()->addCommand($replace_cart_form_command);
      }
    }

    $cart_data = $cart->getCartDataForAjax();
    $event->getResponse()->addCommand(new BeeShopUpdateSettings('customerCart', $cart_data, TRUE));

    $this->addUpdateCartBlocksCommand($event);
  }

}

