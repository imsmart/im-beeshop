<?php

/**
* @file
* Contains \Drupal\bs_cart\EventSubscriber.
*/

namespace Drupal\bs_cart\EventSubscriber;

use Drupal\bs_shop\Event\CurrentShopEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\bs_shop\Event\ShopEvents;
use Drupal\bs_cart\CartProvider;
use Drupal\bs_cart\CartProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;


/**
 * Class OrderEventsSubscriber.
 *
 * @package Drupal\bs_order
 */
class CurrentShopEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The cart provider
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrderReceiptSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [ShopEvents::CURRENT_SHOP_SWITCHED => ['onCurrentShopSwitched', 10]];
    return $events;
  }

  public function onCurrentShopSwitched(CurrentShopEvent $event) {

    $cart = $this->entityTypeManager->getStorage('bs_cart')->getCurrentUserCart();
    $cart->checkCartItems();
  }

}

