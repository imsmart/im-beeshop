<?php
namespace Drupal\bs_cart\Form;

use Drupal\beeshop\Ajax\BeeShopEventCommand;
use Drupal\beeshop\Ajax\BeeShopUpdateSettings;
use Drupal\bs_cart\CartProvider;
use Drupal\bs_cart\CartProviderInterface;
use Drupal\bs_cart\Entity\Cart;
use Drupal\bs_cart\AddToCartFormInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\bs_cart\AddToCartVariationsWidgetPluginInterface;
use Drupal\bs_product\ProductFunctionsTrait;
use Drupal\bs_cart\VariationsRendererTrait;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Drupal\bs_cart\Event\CartEvents;



class AddToCartBySpinnerForm extends FormBase {

  use VariationsRendererTrait;

  protected $entityTypeManager;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The plugin objects used for this display, keyed by field name.
   *
   * @var array
   */
  protected $plugins = [];

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, PluginManagerInterface $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = \Drupal::logger('bs_cart');
    $this->setMessenger($messenger);
    $this->variationWidgetPluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('plugin.manager.add_to_cart_variations_widget_plugin')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ProductInterface $product = NULL, $mode = 'default') {

    $this->product = $product;
    $form['#parents'] = [];

    $form['#id'] = 'add-to-cart--' . $product->id();

    $form['#mode'] = $mode;

    $form['#pid'] = $product->id();

    $form['#attributes']['data-base-product-id'] = $product->id();

    $add_value = $this->t('Add to cart');

    if ($product_state = $product->getAvState()) {
      if ($product_state->get('ad_to_cart_button_text') &&
        $product_state->get('ad_to_cart_button_text')->value) {
          $add_value = $product_state->get('ad_to_cart_button_text')->value;
      }
    }

    $product_fo_check_availability = $product;

    $product_fo_check_availability = $product->getActiveVariation() ?: $product;

    if ($product_fo_check_availability->isAvailable()) {

      $form['in_cart_count'] = [
        '#type' => 'number',
        '#default_value' => 0,
        '#min' => 0,
        '#ajax' => [
          //'callback' => 'Drupal\bs_cart\Form\AddToCartForm::ajaxSubmit',
          'url' => \Drupal\Core\Url::fromRoute('bs_cart.ajax.set_item_quantity'),
          'event' =>'change',
          'options' => [
            'query' => ['pid' => $product->id()],
          ]
        ],
        '#attributes' => [
          'class' => [
            'input--add-to-cart-count-spinner'
          ],
          'data-pid' => $product->id(),
          'data-disable-refocus' => 1,
        ],
        '#weight' => 1000,
      ];

    }
    else {
      foreach ($product_fo_check_availability->getLastGroupValidationViolations('availability') as $violation) {
        if ($violation->getCode() == 403) {
          return [];
        }
      }

      $sold_out_text = $this->t('SOLD OUT');

      $form['actions']['unavailable'] = [
        '#violations' => $product_fo_check_availability->getLastGroupValidationViolations('availability'),
        '#markup' => '<div class="unavailable_status">' . $sold_out_text . '</div>',
      ];

    }

    $form['#attached']['library'][] = 'bs_cart/cart.main';
    $form['#build_id'] = 'addto-cart-' . $product->id();
    $form['#product'] = $product;

    $form_state->disableCache();

    return $form;
  }



  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'add_to_cart_by_spinner';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (($triggering_element = $form_state->getTriggeringElement()) &&
      !empty($triggering_element['#is_variation'])) {
      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    if (empty($values['pid'])) {
      $this->messenger()->addError('Error while adding product to cart');
      return;
    }

    $pid = $values['pid'];

    $cart = $this->entityTypeManager->getStorage('bs_cart')->getCurrentUserCart();

    if ($cart->addProduct($pid)) {
      $form['#added_pid'] = $pid;
    }
    else {
      $this->messenger()->addError('Error while adding product to cart.');
    }

  }

  public static function ajaxSubmit(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();
    $cart = \Drupal::service('current_user_cart');

    if (empty($form['#added_pid'])) {
      return $response;
    }

    if ($product = $cart->getCartItemProduct($form['#added_pid'])) {

//       $dispatcher = \Drupal::service('event_dispatcher');
//       $event = new CartAjaxEvent($cart, $response);

//       $event->setData(CartEvents::DATA_ADDED_PRODUCT, $product);

//       debug('-');
//       $dispatcher->dispatch(CartEvents::PRODUCT_ADDED_TO_CART, $event);
//       debug('--');
//       $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

      return $response;
    }

    // Что-то пошло не так

    return $response;
  }

  /**
   *
   */
  public function buildVariationsSelect(&$form, $form_state) {

    $product_type = $this->getProductBundleEntity();

    $current_level = $this->product->getLevel();
    $variations_hierarchy = $product_type->getVariationFieldsHierarchy();

    $current_level_variation_field_name = NULL;

    if ($current_level_variation_fields = $product_type->getVariationLevelFields($this->product->getLevel() + 1)) {
      $keys = array_keys($current_level_variation_fields);
      $current_level_variation_field_name = reset($keys);
    }

    if (!$current_level_variation_field_name) {
      return;
    }

    if ($widget = $this->getVariationsRenderer($current_level_variation_field_name)) {

      if (empty($form['variations'])) {
        $form['variations'] = [
          '#type' => 'container',
          '#id' => 'variations',
          '#parents' => ['variations'],
        ];
      }

      $widget->form($form['variations'], $form, $form_state,
        $this->product->get('variations'));
    }

  }

}

