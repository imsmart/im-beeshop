<?php
namespace Drupal\bs_cart\Form;

use Drupal\bs_cart\CartInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

class CartForm extends ContentEntityForm {

  /**
   * @var CartInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    \Drupal::moduleHandler()->invokeAll('cart_prebiuld_form', [$this->entity]);

    if ($this->entity->isEmpty()) {
      return [
        '#theme' => 'bs_cart_empty_page',
      ];
    }

    $wrapper_id = Html::getUniqueId('bs-cart-edit-form-wrapper');
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';
    $form['#wrapper_id'] = $wrapper_id;

    $form['#attached']['drupalSettings']['ajaxPageState']['beeshop']['cartEditForm']['wrapperId'] = $wrapper_id;

    $form = parent::buildForm($form, $form_state);

    // TODO: need settings for hiding save button
    if (!empty($form['actions']['submit'])) {
      unset($form['actions']['submit']);
    }

    $form['#action'] = '/shop/cart';

    $form['#attached']['drupalSettings']['BeeShop']['dataLayer']['cart']['products'] = $this->entity->getProductsListForDL();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter($entity_type_id) !== NULL) {
      $entity = $route_match->getParameter($entity_type_id);
    }
    else {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->getCurrentUserCart();
    }

    return $entity;
  }


  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    if ($this->entity->isEmpty()) return [];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm', '::save'],
    ];

//     $actions['clear_cart'] = [
//       '#type' => 'submit',
//       '#value' => $this->t('Clear cart'),
//       '#submit' => ['::clearCart'],
//       '#attributes' => [
//         'class' => [
//           'clear-cart'
//         ]
//       ],
//     ];

    // @todo Add empty cart button.

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    $element['#type'] = 'actions';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->unsetValue('cart_items');

    parent::submitForm($form, $form_state);

  }

  public function clearCart(array &$form, FormStateInterface $form_state) {

    $this->entity->clear();
    $this->entity->save();

  }

  public static function saveCartAjax (array $form, FormStateInterface $form_state) {
    $cart = $form_state->getFormObject()->buildEntity($form, $form_state);//$form_state->getFormObject()->getEntity();
    $cart->save();

    $request = \Drupal::request();
    $rm = \Drupal::service('current_route_match');
    $ajax_renderer = \Drupal::service('main_content_renderer.ajax');

    $response = $ajax_renderer->renderResponse($form, $request, $rm);

    return $response;

  }

}

