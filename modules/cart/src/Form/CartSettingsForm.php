<?php
namespace Drupal\bs_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;

class CartSettingsForm extends ConfigFormBase {
  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
      // TODO Auto-generated method stub
      return 'cart_settings';
  }

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\DependencyInjection\ContainerInjectionInterface::create()
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
      // TODO Auto-generated method stub
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['beeshop.cart'];
  }


}

