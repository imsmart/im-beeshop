<?php
namespace Drupal\bs_cart;

use Drupal\Core\Entity\ContentEntityStorageInterface;

interface CartStorageInterface extends ContentEntityStorageInterface {

  /**
   * @return \Drupal\bs_cart\CartInterface
   */
  public function getCurrentUserCart();

}

