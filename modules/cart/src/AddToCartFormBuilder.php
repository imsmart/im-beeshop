<?php
namespace Drupal\bs_cart;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Cache\CacheableJsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\bs_cart\Event\AddToCartFormAjaxVariationChangedEvent;
use Drupal\bs_cart\Event\AddToCartFormEvents;
use Drupal\bs_product\Entity\ProductInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\beeshop\CacheableAjaxResponce;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Ajax\InsertCommand;

class AddToCartFormBuilder implements ContainerInjectionInterface {

  protected $entityTypeManager;

  protected $request;

  protected $eventDispatcher;

  protected $formBuilder;

  /**
   * An associative array of form value keys to be removed by cleanValues().
   *
   * Any values that are temporary but must still be displayed as values in
   * the rendered form should be added to this array using addCleanValueKey().
   * Initialized with internal Form API values.
   *
   * This property is uncacheable.
   *
   * @var array
   */
  protected $cleanValueKeys = [
    'form_id',
    'form_token',
    'form_build_id',
    'op',
  ];

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $formsCache;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, FormBuilderInterface $form_builder, EventDispatcherInterface $event_dispatcher, CacheBackendInterface $forms_cache) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request_stack->getCurrentRequest();
    $this->formBuilder = $form_builder;
    $this->eventDispatcher = $event_dispatcher;
    $this->formsCache = $forms_cache;
  }

  /**
  * {@inheritDoc}
  * @see \Drupal\Core\DependencyInjection\ContainerInjectionInterface::create()
  */
  public static function create(ContainerInterface $container) {
   return new static (
     $container->get('entity_type.manager'),
     $container->get('request_stack'),
     $container->get('form_builder'),
     $container->get('event_dispatcher'),
     $container->get('cache.add_to_cart_forms')
   );
  }

  public function getForm(ProductInterface $product, $view_mode) {

    $cid_parts = [$view_mode, $product->id()];

    $main_cid = implode(':', $cid_parts);

    $product->getActiveVariation();

//     if (!$variation_id && ($def_var = $product->getDefaultVariation()) && $def_var->id() != $product->id()) {
//       $variation_id = $def_var->id();
//     }

//     if ($variation_id) {
//       $cid_parts[] = $variation_id;
//     }

//     $cid = implode(':', $cid_parts);

//     $form_cid = 'form:' . $cid;
//     $form_state_cid = 'form_state:' . $cid;

    $form_state = new FormState();
    $form_state->addBuildInfo('args', array_values([$product, $view_mode]));
    $input = $this->request->query->all();
    $form_state->setUserInput($input);
    $form_state->setValues($input);


//     if ($cached_form = $this->formsCache->get($form_cid)) {
//       $form = $cached_form->data;
//       return $form;
//     }
//     elseif ($main_cid != $cid &&
//       ($cached_form = $this->formsCache->get('form:' . $main_cid)) && ($form_state = $this->formsCache->get('form_state:' . $main_cid)->data)) {
//       $form = $cached_form->data;
//       $form_obj = $form_state->getFormObject();

//       $form_state->cleanValues();
//       $form_state->setUserInput($input);
//       $form_state->setValues($input);

//       $form_obj->buildVariationsSelect($form, $form_state);

//       $this->formBuilder->doBuildForm('', $form, $form_state);
//     }
//     else {
      $form = $this->formBuilder->buildForm('\Drupal\bs_cart\Form\AddToCartForm', $form_state);

//     }

//     $form['#cache_cid'] = $cid;

    $this->cleanValues($form);

//     $this->formsCache->set($form_cid, $form, CacheBackendInterface::CACHE_PERMANENT, $product->getCacheTags());
//     $this->formsCache->set($form_state_cid, $form_state, CacheBackendInterface::CACHE_PERMANENT, $product->getCacheTags());

    return $form;
  }

  public function ajaxGetForm() {
    $response = new CacheableAjaxResponce();

    if ($product = Product::load($this->request->get('product'))) {

     if ($product->getBundleEntity()->getMaxChildLevel() > $this->request->get('variation_level')) {
        $form = $this->getForm($product, $this->request->get('mode'), $this->request->get('variation'));
        $rendered_form = drupal_render_root($form);
        $response->addAttachments($form['#attached']);
        $response->addCommand(new InsertCommand(NULL, $rendered_form));
     }

    }

//     $expired = new \DateTime();
//     $expired->modify('+ 30 day');
//     $response->setExpires($expired);

    if ($id = $this->request->get('variation')) {

        $form['#pid'] = $this->request->get('product');
        $form['#mode'] = $this->request->get('mode');

//         Allow modules to add arbitrary ajax commands to the response.
        $event = new AddToCartFormAjaxVariationChangedEvent($form, $id, $response);
        $this->eventDispatcher->dispatch(AddToCartFormEvents::ADD_TO_CART_FORM_AJAX_VARIATION_CHANGED, $event);

    }

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->addCacheContexts(['url.query_args']);

    $response->addCacheableDependency($cache_metadata);

    return $response;
  }


  protected function cleanValues(&$form) {
    foreach ($this->getCleanValueKeys() as $value) {
      if (isset($form[$value])) {
        unset($form[$value]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCleanValueKeys() {
    return $this->cleanValueKeys;
  }
}

