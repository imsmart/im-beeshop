<?php
namespace Drupal\bs_cart;

use Drupal\bs_product\ProductFunctionsTrait;

trait VariationsRendererTrait {

  use ProductFunctionsTrait;

  /**
   * The plugin manager used by this entity type.
   *
   * @var \Drupal\Component\Plugin\PluginManagerBase
   */
  protected $variationWidgetPluginManager;

  protected $variationPlugins = [];

  /**
   * {@inheritdoc}
   *
   * @return AddToCartVariationsWidgetPluginInterface or NULL
   */
  public function getVariationsRenderer($field_name) {
    if (isset($this->variationPlugins[$field_name])) {
      return $this->variationPlugins[$field_name];
    }

    //     // Instantiate the widget object from the stored display properties.
    if ($definition = $this->getProductFieldDefinition($field_name)) {
      $widget = $this->variationWidgetPluginManager->getInstance([
        'field_definition' => $definition,
        //         'form_mode' => $this->originalMode,
      //         // No need to prepare, defaults have been merged in setComponent().
      //         'prepare' => FALSE,
      //         'configuration' => $configuration
      ]);
    }
    else {
      $widget = NULL;
    }

    // Persist the widget object.
    $this->variationPlugins[$field_name] = $widget;
    return $widget;
  }

}

