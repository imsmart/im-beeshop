<?php
namespace Drupal\bs_cart;

use Drupal\Core\Entity\EntityViewBuilder;

class CartViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\bs_cart\Entity\Cart [] $entities */
    if (empty($entities)) {
      return;
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);

  }

  /**
   * {@inheritdoc}
   */
  public function build(array $build) {
    $build = parent::build($build);

    $cart = $build['#bs_cart'];

    if (!$cart->isEmpty()) {

      $build['bs_cart_page'] = [
        '#theme' => 'bs_cart_page',
      ];

      $build['bs_cart_page']['cart_form'] = [
        '#prefix' => '<div class="cart-form">',
        '#suffix' => '</div>',
        '#type' => 'view',
        '#name' => 'cart',
        '#display_id' => 'block_cart_form',
        '#arguments' => [$cart->id()],
        '#embed' => TRUE,
      ];

    }
    else {
      $build['empty'] = [
        '#theme' => 'bs_cart_empty_page',
      ];

      if ($cart->containsAnavailableItems()) {
        //         $build['unavailable_items'] = [
        //           '#prefix' => '<div class="cart-unavailable-items">',
        //           '#suffix' => '</div>',
        //           '#type' => 'view',
        //           '#name' => 'cart',
        //           '#display_id' => 'block_cart_unavailable_items_list',
        //           '#arguments' => [$cart->id()],
        //           '#embed' => TRUE,
        //         ];
      }
    }

    return $build;
  }
}

