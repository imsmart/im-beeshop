<?php
namespace Drupal\bs_cart;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\bs_cart\Event\CartEvent;
use Drupal\bs_cart\Event\CartAjaxEvent;

interface CartInterface extends ContentEntityInterface {

  /**
   *
   * @param int|ProductInterface $pid
   * @param number $quantity
   */
  public function addProduct($pid, $quantity = 1);

  /**
   * Clear cart
   */
  public function clear();

  /**
   * Gets whether the cart is empty..
   *
   * @return bool
   *   TRUE if the cart is empty (has no items).
   */
  public function isEmpty();

  /**
   * @return \Drupal\bs_cart\CartItemInterface[]
   */
  public function getItems();

  /**
   *
   * @param AjaxResponse $response
   *
   * @return CartEvent||CartAjaxEvent
   */
  public function getCartEvent(AjaxResponse $response = NULL);

  /**
   * Gets an cart data value with the given key.
   *
   * Used to store temporary data.
   *
   * @param string $key
   *   The key.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *   The value.
   */
  public function getData($key, $default = NULL);

  /**
   * Sets an order data value with the given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   *
   * @return $this
   */
  public function setData($key, $value);

  public function validateAndFix();

  /**
   * Get cart total amount
   *
   * @return \Drupal\bs_price\Price
   *
   * @param bool $without_discount
   */
  public function getTotalByItems($without_discount = FALSE);

  /**
   * @return array
   */
  public function getProductsListForDL();

}

