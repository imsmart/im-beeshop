<?php
namespace Drupal\bs_cart;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\bs_price\PriceItemList;

class CartItemPriceItemList extends PriceItemList {
  use ComputedItemListTrait;

  /**
   *
   * {@inheritdoc}
   *
   * @see \Drupal\Core\TypedData\ComputedItemListTrait::computeValue()
   */
  protected function computeValue() {
    /**
     * @var CartItemInterface $cart_item
     */
    $cart_item = $this->getEntity();

    if ($product = $cart_item->getProduct()) {
      if ($pice = $product->getActualPrice()) {
        $this->list[0] = $pice;
      }
    }

  }

  public function setValueComputed(bool $flag) {
    $this->valueComputed = $flag;
  }

}

