<?php
namespace Drupal\bs_cart;

use Drupal\Core\Entity\ContentEntityInterface;

interface CartItemInterface extends ContentEntityInterface {

  /**
   * Get referenced cart
   *
   * @return CartInterface
   */
  public function getCart();

  /**
   * Gets the cart item quantity.
   *
   * @return string
   *   The cart item quantity
   */
  public function getQuantity();

  /**
   * Sets the cart item quantity.
   *
   * @param string $quantity
   *   The cart item quantity.
   *
   * @return $this
   */
  public function setQuantity($quantity);

  /**
   * @return \Drupal\bs_product\Entity\ProductInterface | null
   */
  public function getProduct();

}

