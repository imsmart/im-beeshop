<?php
namespace Drupal\bs_cart\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "cart_item_actuality",
 *   label = @Translation("Cart item actual data", context = "Validation"),
 *   type = {"entity"}
 * )
 */
class CartItemActualityState extends Constraint {

  public $priceChanged = 'Price of product %product_title has been changed since your last access';

  public $notAvailableNow = 'Product "%product_title" is not available now or not available for selected shop';

  public $notAvailableIsAvailableNow = 'Product "%product_title" that has been unavailable is available again';

  public $productNotFound = 'Product not found';

  public $productQuantityLimitReached = 'At this moment You can buy no more than @max_count @count_unit of a %product_title';
}

