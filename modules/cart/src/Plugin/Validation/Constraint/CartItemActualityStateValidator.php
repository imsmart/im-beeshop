<?php
namespace Drupal\bs_cart\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\bs_cart\CartItemInterface;


class CartItemActualityStateValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    /**
     * @var CartItemActualityState $cart
     */

    $cart = $items->getEntity();

    foreach ($items as $delta => $item) {
      if ($cart_item_entity = $item->entity) {
        /**
         * @var CartItemInterface $cart_item_entity
         */
        if ($product = $cart_item_entity->getProduct()) {
          $product->setPriceResolvingContext('bs_cart');
          $product->setPriceResolvingContextEntity('bs_cart', $cart);
          if ($product->isAvailable()) {

            if (!$cart_item_entity->isAvailable()) {
              $this->context->buildViolation($constraint->notAvailableIsAvailableNow, ['%product_title' => $product->getTitle()])
              ->atPath($delta)
              ->setInvalidValue($item)
              ->setCause('bs_cart:unavailable_product_is_available')
              ->addViolation();
            }

            if ($product->hasField('ctrl_quantity_in_wh') && $product->get('ctrl_quantity_in_wh')->value) {
              // Need to check available quantity of product
              $av_product_quantity = $product->get('quantity_in_wh')->getAllWhCountSum();

              if ($cart_item_entity->getQuantity() > $av_product_quantity) {
                $this->context->buildViolation($constraint->productQuantityLimitReached, ['%product_title' => $product->getTitle()])
                ->atPath($delta)
                ->setInvalidValue($item)
                ->setCause('bs_cart:productQuantityLimitReached')
                ->addViolation();

                $cart_item_entity->setQuantity($av_product_quantity);
              }

            }

            if ($actual_price = $product->getActualPrice()) {

              try {
                if ($actual_price->toPrice() != $cart_item_entity->get('price')->first()->toPrice()) {

                  $this->context->buildViolation($constraint->priceChanged, ['%product_title' => $product->getTitle()])
                  ->atPath($delta)
                  ->setInvalidValue($item)
                  ->setCause('bs_cart:product_actual_price_changed')
                  ->addViolation();
                }
              }
              catch (\Exception $e) {
                $this->context->buildViolation($constraint->priceChanged, ['%product_title' => $product->getTitle()])
                ->atPath($delta)
                ->setInvalidValue($item)
                ->setCause('bs_cart:product_actual_price_changed')
                ->addViolation();
              }
            }
            else {
              $this->context->buildViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()])
              ->atPath($delta)
              ->setInvalidValue($item)
              ->setCause('bs_cart:product_not_available_for_order')
              ->addViolation();
            }

          }
          else {
            $this->context->buildViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()])
            ->atPath($delta)
            ->setInvalidValue($item)
            ->setCause('bs_cart:product_not_available_for_order')
            ->addViolation();
          }

          //$product->setPriceResolvingContext();
        }
        else {
          $this->context->buildViolation($constraint->productNotFound)
          ->atPath($delta)
          ->setInvalidValue($item)
          ->setCause('bs_cart:product_not_found')
          ->addViolation();
        }
      }
    }
  }

  protected function setItemAsAnavailable(CartItemInterface $cart_item_entity, Constraint $constraint) {

    $product = $cart_item_entity->getProduct();

    $cart_item_entity->setUnavailableSatatus(TRUE);
    $cart_item_entity->set('price', [
      'value' => NULL,
      'currency_code' => NULL,
    ]);

    //$cart_item_entity->save();

    $this->context->addViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()]);
  }

}

