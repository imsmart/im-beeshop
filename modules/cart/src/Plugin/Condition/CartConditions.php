<?php

namespace Drupal\bs_cart\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_cart\CartInterface;

/**
 * Provides a 'Order total by products' condition.
 *
 * @Condition(
 *   id = "bs_cart_conditions",
 *   label = @Translation("Cart"),
 *   context = {
 *     "bs_cart" = @ContextDefinition(
 *       "entity:bs_cart",
 *       label = @Translation("Current user"),
 *       required = false
 *     )
 *   }
 * )
 */
class CartConditions extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['cart_not_empty' => FALSE] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {

    /**
     * @var CartInterface $cart
     */
    $cart = \Drupal::service('current_user_cart');//$this->getContextValue('bs_cart');

    if ($this->configuration['cart_not_empty']) {
      return !$cart->isEmpty();
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

    return '****';

//     if (count($this->configuration['bundles']) > 1) {
//       $bundles = $this->configuration['bundles'];
//       $last = array_pop($bundles);
//       $bundles = implode(', ', $bundles);
//       return $this->t('The node bundle is @bundles or @last', ['@bundles' => $bundles, '@last' => $last]);
//     }
//     $bundle = reset($this->configuration['bundles']);
//     return $this->t('The node bundle is @bundle', ['@bundle' => $bundle]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['cart_not_empty'] = [
      '#type' => 'checkbox',
      '#title' => 'Cart is not empty',
      '#default_value' => $this->configuration['cart_not_empty'],
    ];

    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['cart_not_empty'] = $form_state->getValue('cart_not_empty');
    parent::submitConfigurationForm($form, $form_state);
  }

}

