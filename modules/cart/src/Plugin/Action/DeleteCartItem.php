<?php

namespace Drupal\bs_cart\Plugin\Action;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Action\Plugin\Action\EntityActionBase;
use Drupal\Core\Access\AccessResult;
use Drupal\bs_order\Entity\OrderItemInterface;

/**
 * Delete order item.
 *
 * @Action(
 *   id = "bs_cart_item_delete",
 *   label = @Translation("Delete cart"),
 *   type = "bs_cart_item"
 * )
 */
class DeleteCartItem extends EntityActionBase {

//   /**
//    *
//    * @var CartInterface
//    */
//   protected $cart;

//   /**
//    * Constructs a new DeleteAction object.
//    *
//    * @param array $configuration
//    *   A configuration array containing information about the plugin instance.
//    * @param string $plugin_id
//    *   The plugin ID for the plugin instance.
//    * @param mixed $plugin_definition
//    *   The plugin implementation definition.
//    * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
//    *   The entity type manager.
//    * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
//    *   The tempstore factory.
//    * @param \Drupal\Core\Session\AccountInterface $current_user
//    *   Current user.
//    */
//   public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CartInterface $current_user_cart) {
//     $this->cart = $current_user_cart;

//     parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
//     return new static(
//       $configuration,
//       $plugin_id,
//       $plugin_definition,
//       $container->get('entity_type.manager'),
//       $container->get('current_user_cart')
//       );
//   }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    /**
     * @var OrderItemInterface $object
     */
//     dpm($object);
    $object->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

}
