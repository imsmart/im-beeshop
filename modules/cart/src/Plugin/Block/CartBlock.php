<?php

namespace Drupal\bs_cart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\MessagesBlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\bs_cart\CartInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Component\Utility\Html;

/**
 * Provides a block to display the messages.
 *
 * @see drupal_set_message()
 *
 * @Block(
 *   id = "bs_cart_block",
 *   admin_label = @Translation("Cart block")
 * )
 */
class CartBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var CartInterface
   */
  protected $cart;

  /**
   * Constructs a new CartBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartInterface $cart) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cart = $cart;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user_cart')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $items_count = $this->cart->get('cart_items')->count();

    $wrapper_id = Html::getUniqueId('bs-cart-block');

    $build = [
      '#theme' => 'bs_cart_block',
      '#cart' => $this->cart,
      '#is_empty' => $this->cart->isEmpty(),
      '#id' => $wrapper_id,
      '#items_count' => $items_count,
      '#count_text' => $this->formatPlural($items_count, '@count product', '@count products'),
      '#url' => Url::fromRoute('entity.bs_cart.edit_form')->toString(),
      '#cache' => [
        'contexts' => ['bs_cart'],
      ],
      '#attached' => [
        'drupalSettings' => [
          'ajaxPageState' => [
            'beeshop' => [
              'cartBlocks' => [
                $wrapper_id => $this->configuration,
              ]
            ]
          ]
        ]
      ]
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['bs_cart']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags = parent::getCacheTags();
    return Cache::mergeTags($cache_tags, $this->cart->getCacheTags());
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function getCacheMaxAge() {
//     // The messages are session-specific and hence aren't cacheable, but the
//     // block itself *is* cacheable because it uses a #lazy_builder callback and
//     // hence the block has a globally cacheable render array.
//     return 0;
//   }

}
