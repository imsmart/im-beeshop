<?php

namespace Drupal\bs_cart\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'bs_add_to_cart_as_link' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_add_to_cart_as_link",
 *   label = @Translation("Add to cart as link"),
 *   field_types = {
 *     "bs_add_to_cart"
 *   }
 * )
 */
class ProductAddToCartAsLinkFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [
      'link_text' => NULL,
      'if_unavailable_link_text' => NULL,
      'on_click' => 'go_to_product_page',
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    /**
     * @var ProductInterface $product
     */
    $product = $items->getEntity();

    $title = $this->getSetting('link_text') ?: $this->t('Buy');

    if (!$product->isAvailable() && $this->getSetting('if_unavailable_link_text')) {
      $title = $this->getSetting('if_unavailable_link_text');
    }

    $item = [
      '#type' => 'link',
      '#title' => $title,
    ];

    switch ($this->getSetting('on_click')) {
      case 'go_to_product_page':
        $item['#url'] = $product->toUrl();
        $item['#attributes']['class'][] = 'add-to-cart--link-to-product-page';
        break;
      case 'add_to_cart':
        if ($product->isAvailable()) {
          $item['#url'] = Url::fromRoute('bs_cart.ajax.add_to_cart', [], ['query' => [
            'pid' => $product->id(),
          ]]);
          $item['#attributes']['class'][] = 'use-ajax';
          $item['#attributes']['class'][] = 'add-to-cart--link';
          $item['#attributes']['data-pid'] = $product->id();
        }
        else {
          $item['#url'] = $product->toUrl();
          $item['#attributes']['class'][] = 'add-to-cart--link-to-product-page';
        }
        break;
    }

    \Drupal::moduleHandler()->alter('product_add_to_cart_link', $item, $product);

    $build[] = $item;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->getSetting('link_text'),
      '#description' => 'Default link text, if blank "Buy" value will be used',
    ];

    $form['if_unavailable_link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text for unavailable products'),
      '#default_value' => $this->getSetting('if_unavailable_link_text'),
      '#description' => 'Link text for unavailable products, if blank - "Buy" or defined default value will be used',
    ];

    $form['on_click'] = [
      '#type' => 'select',
      '#title' => $this->t('On click action'),
      '#default_value' => $this->getSetting('on_click'),
      '#options' => [
        'go_to_product_page' => $this->t('Go to product page'),
        'add_to_cart' => $this->t('Add product to cart'),
      ],
    ];

    return $form;
  }

  //TODO: settingsSummary

//   /**
//    * {@inheritdoc}
//    */
//   public function settingsSummary() {
//     $summary = [];
//     $setting = $this->getSetting('format');

//     if ($setting == 'grouped_by_dif_fields') {
//       $summary[] = $this->t('Display mode: grouped_by_dif_fields', []);
//     }
//     else {
//       $summary[] = $this->t('Display mode: Single list', []);
//     }

//     return $summary;
//   }

}
