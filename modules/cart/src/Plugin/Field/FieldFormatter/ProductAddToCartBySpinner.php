<?php

namespace Drupal\bs_cart\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'bs_add_to_cart_default' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_add_to_cart_by_spinner",
 *   label = @Translation("Add to cart by spinner"),
 *   field_types = {
 *     "bs_add_to_cart"
 *   }
 * )
 */
class ProductAddToCartBySpinner extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $product = $items->getEntity();

    if (!$product->addToCartFormPlaceholderId) {

      $form_state = new FormState();
      $form_state->addBuildInfo('args', array_values([$product, $this->viewMode]));
      $input = \Drupal::request()->query->all();
      $form_state->setUserInput($input);
      $form_state->setValues($input);

      $form = \Drupal::service('form_builder')->buildForm('\Drupal\bs_cart\Form\AddToCartBySpinnerForm', $form_state);
    }
    else {
      $form = [
        '#markup' => '<div id="' . $product->addToCartFormPlaceholderId . '"></div>',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Variations list format'),
      '#default_value' => $this->getSetting('format'),
      '#options' => [
        'grouped_by_dif_fields' => $this->t('Grouped by different fields'),
        'single_list' => $this->t('Single list'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $setting = $this->getSetting('format');

    if ($setting == 'grouped_by_dif_fields') {
      $summary[] = $this->t('Display mode: grouped_by_dif_fields', []);
    }
    else {
      $summary[] = $this->t('Display mode: Single list', []);
    }

    return $summary;
  }

}
