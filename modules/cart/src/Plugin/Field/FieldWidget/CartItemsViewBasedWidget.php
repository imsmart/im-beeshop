<?php
namespace Drupal\bs_cart\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Component\Utility\Html;
use Drupal\views\Views;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\bs_cart\Entity\Cart;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Drupal\bs_cart\Event\CartEvents;
use Drupal\Core\Render\MainContent\AjaxRenderer;
use Drupal\beeshop\Ajax\BeeshopSowAlert;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;


/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "bs_cart_items_view_based",
 *   label = @Translation("Cart items (view based)"),
 *   field_types = {
 *     "bs_cart_items"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CartItemsViewBasedWidget extends WidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(\Drupal\Core\Field\FieldItemListInterface $items, $delta, array $element, array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

    $class = get_class($this);

    $cart = $items->getEntity();

    $cart_items = [
      '#type' => 'view',
      '#name' => 'cart',
      '#display_id' => 'block_cart_form',
      '#arguments' => [$cart->id()],
      '#embed' => TRUE,
      '#rows' => TRUE,
    ];

    $element['items'] = $cart_items;

    foreach ($items->referencedEntities() as $cart_item) {

      /**
       * @var ProductInterface $product
       */
      $product = $cart_item->getProduct();
      $quantity = $cart_item->getQuantity();

      $element['items_quantity'][$cart_item->id()] = [
        '#type' => 'number',
        '#title' => $this->t('Quantity'),
        '#title_display' => 'invisible',
        '#default_value' => round($quantity),
        '#item_id' => $cart_item->id(),
        '#product_title' => $product->label(),
//         '#cart_item' => $cart_item,
        '#size' => 4,
        '#min' => 0,
//         '#max' => 9999,
        '#step' => 1,
        '#attributes' => [
          'class' => [
            'cart-item-quantity'
          ]
        ],
        '#ajax' => [
          'callback' => $class . '::ajaxCartUpdateItemQuantity',
          'event' =>'change',
          'wrapper' => !empty($form['#wrapper_id']) ?  $form['#wrapper_id'] : 'bs-cart-edit-form-wrapper',
        ],
        '#element_validate' => [[$this, 'validateQuantityValue']]
      ];

      $max = $cart_item->getMaxAvQuantity();
      if (is_numeric($max)) {
        $element['items_quantity'][$cart_item->id()]['#max'] = $max;
      }

      $params = [
        'item_id' => $cart_item->id(),
      ];
      $link_url = Url::fromRoute('beeshop.cart.ajax.remove_item', $params);


      $element['remove_item_links'][$cart_item->id()] = [
        '#type' => 'link',
        '#title' => $this->t('Remove'),
        '#url' => $link_url,
        '#ajax' => [
          'progress' => [
            'type' => 'throbber',
            'message' => NULL,
          ],
        ],
        '#attributes' => [
          'title' => $this->t('Remove'),
          'class' => [
            'remove-cart-item-link'
          ]
        ]
      ];

      $element['remove_item_checkbox'][$cart_item->id()] = [
        '#type' => 'checkbox',
        '#title' => ' ',
      ];
    }

    $element['remove_selected'] = [
      '#type' => 'button',
      '#value' => $this->t('Remove selected'),
      '#action' => 'remove_selected',
      '#ajax' => [
        'callback' => $class . '::ajaxRemoveSelected',
        'wrapper' => !empty($form['#wrapper_id']) ?  $form['#wrapper_id'] : 'bs-cart-edit-form-wrapper',
      ],
    ];

    $element['#pre_render'] = [
      [$class, 'preRender'],
    ];


    return $element;
  }

  public static function validateQuantityValue(&$element, FormStateInterface $form_state, &$complete_form) {
    if ($element['#value'] < 0) {
      $form_state->setValueForElement($element, 0);
    }

    if (!empty($element['#max']) && $element['#value'] > $element['#max']) {
      $element['#value'] = $element['#max'];
      $form_state->setValueForElement($element, $element['#max']);
    }
  }

  public static function preRender($element) {

    $search = [];
    $replace = [];

    foreach (Element::children($element['items_quantity']) as $item_id) {
      $search[] = "<!--form-item-edit_quantity--{$item_id}-->";
      $replace[] = \Drupal::service('renderer')->render($element['items_quantity'][$item_id]);

      $search[] = "<!--form-item-remove-link--{$item_id}-->";
      $replace[] = \Drupal::service('renderer')->render($element['remove_item_links'][$item_id]);

      if (!empty($element['remove_item_checkbox'][$item_id])) {
        $search[] = "<!--form-item-remove-checkbox--{$item_id}-->";
        $replace[] = \Drupal::service('renderer')->render($element['remove_item_checkbox'][$item_id]);
      }
    }

    if (!empty($element['remove_selected']) && !empty($element['remove_item_checkbox'])) {
      $search[] = "<!--remove-selected-->";
      $replace[] = \Drupal::service('renderer')->render($element['remove_selected']);
    }

    $rendered_view = \Drupal::service('renderer')->render($element['items']);

    $items_output = str_replace($search, $replace, $rendered_view);

    $element['items'] = [
      '#markup' => ViewsRenderPipelineMarkup::create($items_output),
    ];

    return $element;
  }

  public static function ajaxRemoveSelected(array $form, FormStateInterface $form_state) {

    /**
     * @var Cart $cart
     */
    $cart = $form_state->getFormObject()->getEntity();
    $cart->save();

    $form = \Drupal::service('form_builder')->rebuildForm('bs_cart_edit_form', $form_state, $form);

    $request = \Drupal::request();
    $rm = \Drupal::service('current_route_match');
    $ajax_renderer = \Drupal::service('main_content_renderer.ajax');

    /**
     * @var AjaxResponse $response
     */
    $response = $ajax_renderer->renderResponse($form, $request, $rm);

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = $cart->getCartEvent($response);
    $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

    return $response;
    //return $form;
  }

  public static function ajaxCartUpdateItemQuantity(array $form, FormStateInterface $form_state, Request $request) {

    $cart = $form_state->getFormObject()->getEntity();

    $cart->save();

    $commands = [];

    if (!empty($form['cart_items']['widget']['items_quantity'])) {
      foreach (Element::children($form['cart_items']['widget']['items_quantity']) as $key) {
        if (isset($form['cart_items']['widget']['items_quantity'][$key]['#value'])) {
          if ($form['cart_items']['widget']['items_quantity'][$key]['#value'] <= 0) {
            $form['cart_items']['widget']['items_quantity'][$key]['#value'] = 0;
          }
          if (isset($form['cart_items']['widget']['items_quantity'][$key]['#max']) && $form['cart_items']['widget']['items_quantity'][$key]['#max'] < $form['cart_items']['widget']['items_quantity'][$key]['#value']) {
            $form['cart_items']['widget']['items_quantity'][$key]['#value'] = $form['cart_items']['widget']['items_quantity'][$key]['#max'];

            if ($form['cart_items']['widget']['items_quantity'][$key]['#value'] > 0) {
              $plural_tramslatable = new PluralTranslatableMarkup($form['cart_items']['widget']['items_quantity'][$key]['#value'],
                'At this moment You can buy only 1 item of "@product_title"',
                'At this moment You can buy only @count items of "@product_title',
                ['@product_title' => $form['cart_items']['widget']['items_quantity'][$key]['#product_title']]);
            }
            else {
              $plural_tramslatable = new TranslatableMarkup('Sorry! Product "@product_title" is out of stock.',
                ['@product_title' => $form['cart_items']['widget']['items_quantity'][$key]['#product_title']]);
            }

            $commands[] = new BeeshopSowAlert([
              'title' => t('We are sorry'),
              'text' => $plural_tramslatable,
              'icon' => 'warning',
              'type' => 'warning',
              'focusConfirm' => TRUE
            ]);
          }
        }
      }
    }

    $rm = \Drupal::service('current_route_match');
    /**
     * @var AjaxRenderer $ajax_renderer
     */
    $ajax_renderer = \Drupal::service('main_content_renderer.ajax');

    /**
     * @var AjaxResponse $response
     */
    $response = $ajax_renderer->renderResponse($form, $request, $rm);

    foreach ($commands as $command) {
      $response->addCommand($command);
    }

    $event = new CartAjaxEvent($cart, $response);
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

    return $response;
    //return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    $cart = \Drupal::entityTypeManager()->getStorage('bs_cart')->getCurrentUserCart();

    $triggered_element = $form_state->getTriggeringElement();

    if (!empty($triggered_element['#action']) && $triggered_element['#action'] == 'remove_selected' && !empty($values['remove_item_checkbox'])) {
      foreach ($values['remove_item_checkbox'] as $item_id => $remove) {
        if ($remove && ($cart_item = $cart->getCartItemById($item_id))) {
          $cart_item->delete();
        }
      }
    }

    if (!empty($values['items_quantity'])) {
      foreach ($values['items_quantity'] as $item_id => $quantity) {
        if ($cart_item = $cart->getCartItemById($item_id)) {
          $quantity = $quantity >= 0 ? $quantity : 1;
          $cart_item->setQuantity($quantity);
          $values['items_quantity'][$item_id] = $quantity;
        }
      }
    }

//     $form_state->setValues($values);

//     debug($values);

    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(\Drupal\Core\Field\FieldDefinitionInterface $field_definition) {
      // TODO Auto-generated method stub
      return TRUE;
  }

//   /**
//    * {@inheritDoc}
//    */
//   public static function getWidgetState(array $parents, $field_name, \Drupal\Core\Form\FormStateInterface $form_state) {
//       // TODO Auto-generated method stub

//   }

//   /**
//    * {@inheritDoc}
//    */
//   public static function setWidgetState(array $parents, $field_name, \Drupal\Core\Form\FormStateInterface $form_state, array $field_state) {
//       // TODO Auto-generated method stub

//   }

//   /**
//    * {@inheritDoc}
//    */
//   public static function defaultSettings() {
//       // TODO Auto-generated method stub

//   }




}

