<?php

namespace Drupal\bs_cart\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'yandex_share' field type.
 *
 * @FieldType(
 *   id = "bs_add_to_cart",
 *   label = @Translation("Add to cart form"),
 *   description = @Translation("Add to cart form"),
 *   category = @Translation("BeeShop"),
 *   default_formatter = "bs_add_to_cart_default",
 *   no_ui = TRUE,
 * )
 */
class AddToCartFormItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

}
