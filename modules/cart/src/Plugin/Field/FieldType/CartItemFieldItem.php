<?php
namespace Drupal\bs_cart\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'product_variations' entity field type.
 *
 * @FieldType(
 *   id = "bs_cart_items",
 *   label = @Translation("Cart items"),
 *   description = @Translation("An bs_cart entity field containing an items reference."),
 *   category = @Translation("Cart items"),
 *   default_widget = "bs_cart_items_view_based",
 *   list_class = "\Drupal\bs_cart\CartItemsFieldItemList",
 *   no_ui = TRUE,
 * )
 */
class CartItemFieldItem extends EntityReferenceItem {
}

