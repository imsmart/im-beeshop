<?php
namespace Drupal\bs_cart\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Factory\DefaultFactory;

class BsCartViolationFixerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      "Plugin/Beeshop/Cart/ViolationFixer",
      $namespaces,
      $module_handler,
      'Drupal\bs_cart\Plugin\Beeshop\Cart\CartViolationFixerPluginInterface',
      'Drupal\bs_cart\Annotation\CartViolationFixer');

      $this->alterInfo('bs_cart_violation_fixer_plugin');
      $this->setCacheBackend($cache_backend, "beesdhop:cart:violation_fixer");
      $this->factory = new DefaultFactory($this->getDiscovery());
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsForCauses() {
    $definitions = parent::getDefinitions();

    $definitions_by_causes = [];

    foreach ($definitions as $plugin_id => $definition) {
      foreach ($definition['causes'] as $cause => $priority) {
        $definitions_by_causes[$cause][$priority] = $plugin_id;
      }
    }

    foreach ($definitions_by_causes as $cause => $plugins) {
      if (count($plugins) > 1) {
        ksort($definitions_by_causes[$cause]);
      }
    }

    return $definitions_by_causes;
  }
}

