<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\ItemLabel\Formatter;

use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_cart\Annotation\AddToCartVariationItemLabelFormatter;
use Drupal\Core\Annotation\Translation;

/**
 * Value or referenced entity label variation item label formatter.
 *
 * @AddToCartVariationItemLabelFormatter(
 *   id="rendered_variation_entity",
 *   label = @Translation("Rendered variation entity"),
 *   weight = 0,
 *   entity_type = "bs_product",
 * )
 */
class RenderedVariationEntity extends RenderedEntityBase {


  /**
   * {@inheritDoc}
   */
  public function getItemLabel(ProductInterface $variation) {
    $result = $this->getRenderedEntity($variation, $this->getEntityViewModeId());

    return $result;
  }


}

