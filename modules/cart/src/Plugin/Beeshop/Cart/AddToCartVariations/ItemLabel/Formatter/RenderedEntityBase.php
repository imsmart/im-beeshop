<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\ItemLabel\Formatter;

use Drupal\bs_cart\AddToCartVariationItemLabelFormatterBase;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_cart\Annotation\AddToCartVariationItemLabelFormatter;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Value or referenced entity label variation item label formatter.
 *
 */
abstract class RenderedEntityBase extends AddToCartVariationItemLabelFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * @var RendererInterface
   */
  protected $renderer;

  public function __construct($plugin_id, $plugin_definition, array $configuration, EntityDisplayRepositoryInterface $entity_display_repository, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $configuration);

    $this->entityDisplayRepository = $entity_display_repository;
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_display.repository'),
      $container->get('renderer')
      );
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['display_mode_id'] = [
      '#type' => 'select',
      '#title' => 'Display mode',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getEntityTypeId()),
    ];

    return $element;
  }

  protected function getEntityTypeId() {
    return !empty($this->pluginDefinition['entity_type']) ? $this->pluginDefinition['entity_type'] : '';
  }

  protected function getRenderedEntity(EntityInterface $entity, $view_mode, $reset = FALSE) {

    $clone = clone $entity;

    $render_controller = \Drupal::entityTypeManager()
    ->getViewBuilder($clone
      ->getEntityTypeId());
    if ($reset) {
      $render_controller
      ->resetCache([
        $clone,
      ]);
    }

    $entity_render_array = $render_controller
    ->view($clone, $view_mode);

    return $this->renderer->render($entity_render_array);
  }

  protected function getEntityViewModeId() {
    return !empty($this->configuration['display_mode_id']) ? $this->configuration['display_mode_id'] : 'default';
  }

}

