<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\ItemLabel\Formatter;

use Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\ItemLabel\Formatter\RenderedEntityBase;
use Drupal\bs_product\Entity\ProductInterface;

/**
 * Value or referenced entity label variation item label formatter.
 *
 * @AddToCartVariationItemLabelFormatter(
 *   id="rendered_referenced_entity",
 *   label = @Translation("Rendered referenced entity"),
 *   weight = 0,
 * )
 */
class RenderedReferencedEntity extends RenderedEntityBase {

  /**
   * {@inheritDoc}
   */
  public function getItemLabel(ProductInterface $variation) {

    $field_name = $this->getFieldDefinition()->getName();

    if ($referenced_entities = $variation->get($field_name)->referencedEntities()) {
      $entity_to_render = reset($referenced_entities);

      $result = $this->getRenderedEntity($entity_to_render, $this->getEntityViewModeId());

      return $result;
    }

    return '';
  }

}

