<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\ItemLabel\Formatter;

use Drupal\Core\Annotation\Translation;
use Drupal\bs_cart\AddToCartVariationItemLabelFormatterBase;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_cart\Annotation\AddToCartVariationItemLabelFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Psr\Container\ContainerInterface;

/**
 * Value or referenced entity label variation item label formatter.
 *
 * @AddToCartVariationItemLabelFormatter(
 *   id="value_or_entity_label",
 *   label = @Translation("Value or referenced entity label"),
 *   weight = -10,
 * )
 */
class ValueOrEntityLabel extends AddToCartVariationItemLabelFormatterBase {




  public function getItemLabel(ProductInterface $variation) {

    $field_name = $this->getFieldDefinition()->getName();

    $main_property_name = $this->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();

    $settable_options = [];

    if ($options_provider = $this->getFieldDefinition()
        ->getFieldStorageDefinition()
        ->getOptionsProvider($main_property_name, $variation)) {
          $settable_options = $options_provider->getSettableOptions();
    }
    else {
      $items = $variation->get($field_name);
      foreach ($items as $item) {
        $settable_options[$variation->get($field_name)->$main_property_name] = $item->{$main_property_name};
      }

    }

//     $settable_options = $this->getFieldDefinition()
//       ->getFieldStorageDefinition()
//       ->getOptionsProvider($main_property_name, $variation)
//       ->getSettableOptions();

    $result = !empty($settable_options[$variation->get($field_name)->$main_property_name]) ? ltrim($settable_options[$variation->get($field_name)->$main_property_name], '-') : '-';

    return $result;
  }


}

