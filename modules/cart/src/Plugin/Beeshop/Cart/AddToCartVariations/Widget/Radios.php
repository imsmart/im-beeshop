<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\AddToCartVariations\Widget;

use Drupal\bs_cart\AddToCartVariationsWidgetBase;
use Drupal\bs_cart\Annotation\AddToCartVariationsWidget;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\bs_product\ProductVariationsFieldItemList;
use Drupal\bs_product\ProductVariationsFieldItemListInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Radios widget fo variations on "Add to cart" form.
 *
 * @AddToCartVariationsWidget(
 *   id="radios",
 *   label = @Translation("Radios"),
 *   weight = -10,
 * )
 */
class Radios extends AddToCartVariationsWidgetBase {

  /**
   * {@inheritDoc}
   * @see \Drupal\Component\Plugin\ConfigurablePluginInterface::defaultConfiguration()
   */
  public function defaultConfiguration() {
    // TODO Auto-generated method stub
    return [
      'label_formatter' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = parent::settingsForm($form, $form_state);

    $element['no_groups'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display all variations in one list (do not group variations)')
    ];

    return $element;
  }

  public function form(array &$element, array &$form, FormStateInterface $form_state, ProductVariationsFieldItemListInterface $variations_items) {

    /**
     * @var ProductInterface $parent_product
     */
    $this->product = $variations_items->getEntity();
    $product_type = $this->product->getBundleEntity();

    $current_level = $this->product->getlevel() + 1;
    $sub_level_childs_wrapper_id =  'level-' . $current_level . '-childs';

    $element_name = $current_level == $product_type->getMaxChildLevel() ? 'pid' : 'items--level-' . $current_level;

    if (empty($element[$element_name]['#parent_pid']) || $element[$element_name]['#parent_pid'] != $variations_items->getEntity()->id()) {

      $variation_field_name = $this->configuration['field_definition']->getName();

      $element[$element_name] = [
        '#type' => 'radios',
        '#title' => $this->getTitle(),
        '#items_level' => $current_level,
        '#parent_pid' => $variations_items->getEntity()->id(),
        //       '#ajax' => [
        //         //'callback' => [get_class($this), 'ajaxVariationChanged'],
        // //         'url' => $ajax_url,
        // //         'wrapper' => $form['#id'],
        //       ],
        '#attributes' => [
          'data-field-name' => $this->configuration['field_definition']->getName(),
          'class' => [
            'variations-set'
          ]
        ],
      ];

      $default_variation = NULL;

      $variations = $variations_items->referencedEntities();

      foreach ($variations as $variation) {
        /**
         * @var ProductInterface $variation
         */

        if (!$variation->access('view')) {
          continue;
        }

        $is_available = $variation->isAvailable();

        if (!$is_available) {
          foreach ($variation->getLastGroupValidationViolations('availability') as $violation) {
            if ($violation->getCode() == 403) {
              $element[$element_name][$variation->id()]['#access'] = FALSE;
            }
          }

          //$element[$element_name][$variation->id()]['#attributes']['disabled'] = TRUE;
          $element[$element_name][$variation->id()]['#attributes']['data-available'] = FALSE;
        }
        else {
          if (!$default_variation) {
            $default_variation = $variation;
          }
        }

        $element[$element_name][$variation->id()]['#attributes']['data-available'] = $is_available ? 'TRUE' : 'FALSE';

        if (!$is_available && ($product_type->getMaxChildLevel() == $current_level)) {
          $element[$element_name][$variation->id()]['#disabled'] = TRUE;
        }

        // @TODO:
        if ($variation->get($variation_field_name) instanceof EntityReferenceFieldItemList && ($referenced_entity = $variation->get($variation_field_name)->entity)) {
          $variation->variationLabelTitle = $referenced_entity->label();
        }

        $item_label = $this->getItemLabel($variation);

        $element[$element_name]['#options'][$variation->id()] = $item_label;
        $element[$element_name][$variation->id()]['#items_level'] = $current_level;
        $element[$element_name][$variation->id()]['#is_variation'] = TRUE;

        $element[$element_name][$variation->id()]['#ajax'] = [
          //'callback' => [get_class($this), 'ajaxVariationChanged'],
          'url' => new Url('beeshop.cart.ajax.add_to_cart_form_update', [
            'product' => $form['#pid'],
            'variation_level' => $current_level,
            'variation' => $variation->id(),
            $element_name => $variation->id(),
            'mode' => $form['#mode'],
          ]),
          'wrapper' => $form['#id'],
        ];

        $element[$element_name][$variation->id()]['#attributes']['data-ajax-request-type'] = 'GET';


      }

    }

    /*
//     if (($input_value = intval($form_state->getValue($element_name))) &&
//         !empty($element[$element_name]['#options'][$input_value])) {
//       $element[$element_name]['#default_value'] = $input_value;
//       $default_variation = $variations_items->getVariation($input_value);

//     }
//     else {

//       if ($product_default_variation = $this->product->getDefaultVariation()) {

//         if ($variations_items->getVariation($product_default_variation->id())) {
//           $default_variation = $product_default_variation;
//         }
//       }
//     }

*/

    $default_value = NULL;

    if (!empty($form['#active_ids'])) {
      foreach ($element[$element_name]['#options'] as $id => $label) {
        if (in_array($id, $form['#active_ids'])) {
          $element[$element_name]['#default_value'] = $id;
          $default_value = $id;
          $element[$element_name][$id]['#attributes']['checked'] = true;
          $default_variation = $variations_items->getVariation($id);
          break;
        }
      }
    }


/*
    if ($default_variation && (
        isset($element[$element_name]['#options'][$default_variation->id()]))) {
      $element[$element_name]['#default_value'] = $default_variation->id();
      $element[$element_name][$default_variation->id()]['#attributes']['checked'] = true;
    }

    if ($default_variation) {

      if ($form['#build_id']) {
        $form['#build_id'] .= '-' . $default_variation->id();
      }
      else {
        $form['#build_id'] = $default_variation->id();
      }

      $form['#default_variation'] = $default_variation;
    }
*/

    $child_variation_field_name = NULL;

    if ($product_type->getMaxChildLevel() >=  $current_level + 1) {
      $child_variation_field_name = $product_type->getVariationLevelField($current_level + 1);
    }

    if ($child_variation_field_name && $default_variation) {

      if (empty($element['childs']['#parent_id']) || $element['childs']['#parent_id'] != $default_value) {

        if ($widget = $this->getVariationsRenderer($child_variation_field_name)) {
          $element['#processed'] = FALSE;

          $element['childs'] = [
            '#type' => 'container',
            '#id' => $sub_level_childs_wrapper_id,
            '#attributes' => [
              'data-variations-level' => $current_level + 1,
              'class' => [
                'sub-variations-set'
              ]
            ],
            '#parent_id' => $default_value
          ];
          $widget->form($element['childs'], $form, $form_state,
            $default_variation->get('variations'));

        }
      }

    }

    $element['#parent_level'] = $current_level - 1;

    return $element;
  }



}

