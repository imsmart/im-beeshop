<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart;

use Drupal\Component\Plugin\PluginBase;

abstract class CartViolationFixerPluginBase extends PluginBase implements CartViolationFixerPluginInterface {



  public function getCauses() {
    return [];
  }

}

