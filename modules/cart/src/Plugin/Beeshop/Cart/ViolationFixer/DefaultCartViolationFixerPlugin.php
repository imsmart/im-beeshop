<?php
namespace Drupal\bs_cart\Plugin\Beeshop\Cart\ViolationFixer;

use Drupal\bs_cart\Plugin\Beeshop\Cart\CartViolationFixerPluginBase;

/**
 * CML
 *
 * @ingroup bs_data_exchange_processor_plugins
 *
 * @CartViolationFixer(
 *   id="default_cart_violation_fixer",
 *   title = @Translation("Default cart violation fixer"),
 *   causes = {
 *     "bs_cart:product_not_found" = 1000,
 *     "bs_cart:product_not_available_for_order" = 1000,
 *     "bs_cart:product_actual_price_changed" = 1000,
 *     "bs_cart:unavailable_product_is_available" = 1000
 *   },
 * )
 */
class DefaultCartViolationFixerPlugin extends CartViolationFixerPluginBase {

  /**
   * {@inheritDoc}
   */
  public function fixViolation($violation) {

    $cart = $violation->getRoot()->getValue();
    $invalid_item = $violation->getInvalidValue();
    $cart_item_entity = $invalid_item->entity;

    switch($violation->getCause()) {
      case 'bs_cart:product_actual_price_changed':
        if ($product = $cart_item_entity->getProduct()) {
          $product->setPriceResolvingContext('bs_cart');
          $product->setPriceResolvingContextEntity('bs_cart', $cart);
          if ($product_actual_price = $product->getActualPrice()) {
            $cart_item_entity->set('price', $product_actual_price->toPrice()->toArray());
            $cart_item_entity->save();
          }
        }
        break;
      case 'bs_cart:product_not_available_for_order':
        $cart_item_entity->setUnavailableSatatus(TRUE);
        $cart_item_entity->save();
        break;
      case 'bs_cart:product_not_found':
        $cart_item_entity->delete();
        break;
      case 'bs_cart:unavailable_product_is_available':
        $cart_item_entity->setUnavailableSatatus(FALSE);
        $cart_item_entity->save();
        break;
    }

    return TRUE;
  }

}

