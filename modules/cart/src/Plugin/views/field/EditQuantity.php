<?php

namespace Drupal\bs_cart\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Defines a form element for editing the cart item quantity.
 *
 * @ViewsField("bs_cart_item_edit_quantity")
 */
class EditQuantity extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    parent::buildOptionsForm($form, $form_state);

    $form['use_jquery_spinner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use jQueryUI Spinner'),
      '#description' => $this->t('If checked, jQueryUI spinner will be attached to this field.'),
      '#default_value' => $this->options['use_jquery_spinner'],
    ];

    $form['use_ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Ajax for cart update on quantity change'),
      //'#description' => $this->t('If checked, jQueryUI spinner will be attached to this field.'),
      '#default_value' => $this->options['use_ajax'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $cart_item = $this->getEntity($row);

    if ($this->options['use_jquery_spinner']) {
      $this->view->element['#attached']['library'][] = 'bs_cart/cartItemQuantitySpinner';
    }

    return '<!--form-item-' . $this->options['id'] . '--' . $cart_item->id() . '-->';
  }

//   public function form_element_row_id($row_id) {
//     $views_result_row = $this->view->result[$row_id];
//     $cart_item = $this->getEntity($views_result_row);
//     return $cart_item->id();
//   }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['use_jquery_spinner'] = ['default' => FALSE];
    $options['use_ajax'] = ['default' => FALSE];
    $options['allow_zero'] = ['default' => FALSE];
    return $options;
  }
}
