<?php
namespace Drupal\bs_cart\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;

/**
 * Defines a form element for removing the order item.
 *
 * @ViewsField("bs_cart_item_remove_link")
 */
class RemoveCartItemLink extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

//   /**
//    * {@inheritdoc}
//    */
//   public function render(ResultRow $values) {

//     $params = [
//       'item_id' => $values->_entity->id(),
//     ];
//     $link_url = Url::fromRoute('beeshop.cart.ajax.remove_item', $params);

//     $link = [
//       '#type' => 'link',
//       '#title' => $this->t('Remove'),
//       '#url' => $link_url,
//       '#ajax' => [
//         'progress' => [
//           'type' => 'throbber',
//           'message' => NULL,
//         ],
//       ],
//       '#attributes' => [
//         'title' => $this->t('Remove'),
//         'class' => [
//           'remove-cart-item-link'
//         ]
//       ]
//     ];

//     //$value = $this->getValue($values);
//     return $link;
//   }



  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $cart_item = $this->getEntity($row);
    return Markup::create('<!--form-item-remove-link--' . $cart_item->id() . '-->');
    //return '<!--form-item-' . $this->options['id'] . '--' . $cart_item->id() . '-->';
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }
}

