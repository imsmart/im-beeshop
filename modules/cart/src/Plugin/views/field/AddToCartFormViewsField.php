<?php

namespace Drupal\bs_cart\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element for editing the cart item quantity.
 *
 * @ViewsField("bs_cart_add_to_cart_form_field")
 */
class AddToCartFormViewsField extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  protected $forms = [];

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition/*, CartManagerInterface $cart_manager*/) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

//     $this->cartManager = $cart_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition//,
//       $container->get('commerce_cart.cart_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function getValue(ResultRow $row, $field = NULL) {

//     return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';

//   }

  public function advancedRender(ResultRow $row) {
    $product = $row->_entity;
    $form = \Drupal::formBuilder()->getForm('\Drupal\bs_cart\Form\AddToCartForm', $product, 'views_row');
//     $rendered_form = $this->getRenderer()->render($form);

//     $this->forms[$row->index] = $rendered_form;

    return $this->getRenderer()->render($form);

//     return parent::advancedRender($row);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    //dpm($values);
    return $this->sanitizeValue($value);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
