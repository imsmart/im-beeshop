<?php
namespace Drupal\bs_cart\Plugin\views\field;

use Drupal\Core\Render\Element;
use Drupal\views\Plugin\views\style\Table;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("bs_cart_items_bulk_remove_form")
 */
class CartItemsBulkForm extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$values) {

    //parent::preRender($values);

    // If the view is using a table style, provide a placeholder for a
    // "select all" checkbox.
    if (!empty($this->view->style_plugin) && $this->view->style_plugin instanceof Table) {
      // Add the tableselect css classes.
      $this->options['element_label_class'] .= 'select-all';
      // Hide the actual label of the field on the table header.
      $this->options['label'] = '';

      $this->view->element['#attached']['library'][] = 'core/drupal.tableselect';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    $cart_item = $this->getEntity($row);
    return '<!--form-item-remove-checkbox--' . $cart_item->id() . '-->';
  }


}

