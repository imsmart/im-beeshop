<?php
namespace Drupal\bs_cart;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @author andrey
 *
 */
abstract class AddToCartVariationItemLabelFormatterBase extends PluginBase implements AddToCartVariationItemLabelFormatterInterface {

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, array $configuration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function setThirdPartySetting($module, $key, $value) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function getThirdPartySetting($module, $key, $default = NULL) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function getThirdPartySettings($module) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function unsetThirdPartySetting($module, $key) {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function getThirdPartyProviders() {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   *
   * @return FieldDefinitionInterface or NULL
   */
  public function getFieldDefinition() {
    return !empty($this->configuration['field_definition']) ? $this->configuration['field_definition'] : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $configuration) {
    // TODO Auto-generated method stub
    $this->configuration = $configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function calculateDependencies() {
    return [];
  }

//   /**
//    * {@inheritDoc}
//    */
//   public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
//     // TODO Auto-generated method stub

//   }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // By default, widgets are available for all fields.
    return TRUE;
  }

}

