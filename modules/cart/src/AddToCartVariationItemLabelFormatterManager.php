<?php
namespace Drupal\bs_cart;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 *
 */
class AddToCartVariationItemLabelFormatterManager extends DefaultPluginManager {

  /**
   * An array of formatter options.
   *
   * @var array
   */
  protected $formatterOptions;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Beeshop/Cart/AddToCartVariations/ItemLabel/Formatter',
      $namespaces,
      $module_handler,
      'Drupal\bs_cart\AddToCartVariationItemLabelFormatterInterface',
      'Drupal\bs_cart\Annotation\AddToCartVariationItemLabelFormatter'
      );
    $this->setCacheBackend($cache_backend, 'add_to_cart_variations_item_label_formatter_plugins');
    $this->alterInfo('add_to_cart_variations_item_label_formatter_plugin_info');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

  /**
   * Returns an array of widget type options.
   *
   * @return array
   *   Returns a nested array of all widget types
   */
  public function getOptions() {
    if (!isset($this->formatterOptions)) {
      $options = [];
      $formatter_types = $this->getDefinitions();
      uasort($formatter_types, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
      foreach ($formatter_types as $name => $widget_type) {
        $options[$name] = $widget_type['label'];
      }
      $this->formatterOptions = $options;
    }

    return $this->formatterOptions;
  }

  public function getApplicablePluginOptions(FieldDefinitionInterface $field_definition) {
    $options = $this->getOptions();
    $applicable_options = [];
    foreach ($options as $option => $label) {
      $plugin_class = DefaultFactory::getPluginClass($option, $this->getDefinition($option));
      if ($plugin_class::isApplicable($field_definition)) {
        $applicable_options[$option] = $label;
      }
    }
    return $applicable_options;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition);

    // If the plugin provides a factory method, pass the container to it.
    if (is_subclass_of($plugin_class, 'Drupal\Core\Plugin\ContainerFactoryPluginInterface')) {
      return $plugin_class::create(\Drupal::getContainer(), $configuration, $plugin_id, $plugin_definition);
    }

    return new $plugin_class($plugin_id, $plugin_definition, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {

    $configuration = !empty($options['configuration']) ? $options['configuration'] : [];

//     if (empty($options['field_definition'])) {
//       return NULL;
//     }

//     $bs_cart_field_settings = $options['field_definition']->getThirdPartySettings('bs_cart');

//     if (!$bs_cart_field_settings || empty($bs_cart_field_settings['add_to_cart_variation_widget']['type'])) {
//       return NULL;
//     }

    $plugin_id = $options['type'];

//     $configuration = !empty($bs_cart_field_settings['add_to_cart_variation_widget']['configuration']) ? $bs_cart_field_settings['add_to_cart_variation_widget']['configuration'] : [];
//     $configuration['field_definition'] = $options['field_definition'];

    return $this->createInstance($plugin_id, $configuration);

  }

}

