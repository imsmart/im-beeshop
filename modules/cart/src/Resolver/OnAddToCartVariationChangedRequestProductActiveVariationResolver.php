<?php

namespace Drupal\bs_cart\Resolver;

use Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\bs_product\Resolver\ProductActiveVariationResolverBase;

class OnAddToCartVariationChangedRequestProductActiveVariationResolver extends ProductActiveVariationResolverBase implements ProductActiveVariationResolverInterface {

  /**
   * @var RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var Request
   */
  protected $request;

  protected $variationIdsInRequest;

  protected $resolvedVariation;

  public function __construct(RequestStack $request_stack, RouteMatchInterface $route_match) {
    $this->request = $request_stack->getCurrentRequest();
    $this->routeMatch = $route_match;
  }

  /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::applies()
  */
  public function applies(ProductInterface $entity) {
    return $this->routeMatch->getRouteName() == 'beeshop.cart.ajax.add_to_cart_form_update';
  }

  /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::resolve()
  */
  public function resolve(ProductInterface $entity) {

    foreach ($this->request->request->all() as $key => $value) {
      $matches = NULL;
      if ($key == 'pid' || preg_match('#items--level-(?<level>\d{1,})#ui', $key, $matches)) {
        if ($matches) {
          $this->variationIdsInRequest[intval($matches['level'])] = intval($value);
        }
        else {
          $this->variationIdsInRequest[100] = intval($value);
        }
      }
    }

    ksort($this->variationIdsInRequest);

    $matches = [];

    $this->resolvedVariation = $entity->getVariationByIdsPath($this->variationIdsInRequest, $matches);

    $this->resolvedVariation = $this->resolvedVariation ?: end($matches);

    return $this->resolvedVariation;

  }


  public function breakResolving() {
    return ($this->resolvedVariation && $this->resolvedVariation->isFinalPoint()) ? TRUE : FALSE;
  }


}

