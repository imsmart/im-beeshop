<?php
namespace Drupal\bs_cart\Entity;

use Drupal\bs_cart\CartInterface;
use Drupal\bs_cart\Event\CartEvent;
use Drupal\bs_price\Price;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_cart\Event\CartEvents;
use Drupal\bs_cart\CartItemInterface;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Symfony\Component\HttpFoundation\Response;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Defines the cart entity class.
 *
 * @ContentEntityType(
 *   id = "bs_cart",
 *   label = @Translation("Cart"),
 *   handlers = {
 *     "storage" = "Drupal\bs_cart\CartStorage",
 *     "views_data" = "Drupal\bs_cart\CartViewsData",
 *     "view_builder" = "Drupal\bs_cart\CartViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_cart\Form\CartForm",
 *       "edit" = "Drupal\bs_cart\Form\CartForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_cart\Routing\CartRouteProvider",
 *     },
 *   },
 *   admin_permission = "access cart",
 *   base_table = "bs_users_carts",
 *   entity_keys = {
 *     "id" = "cid",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/shop/cart",
 *   },
 *   common_reference_target = TRUE,
 *   field_ui_base_route = "beeshop.config.cart",
 * )
 */
class Cart extends ContentEntityBase implements CartInterface {

  protected $lastAffectedItem = NULL;

  /**
   * @var Response
   */
  protected $onEventResponse;

  /**
   * {@inheritdoc}
   */
  public function addProduct($pid, $quantity = 1) {

    $product = NULL;

    if ($pid instanceof ProductInterface) {
      $product = $pid;
      $pid = $pid->id();
    }
    elseif (is_numeric($pid)) {
      $product = Product::load($pid);
    }

    $logger_context = [];

    $affected_cart_item = FALSE;

    if ($cart_item = $this->cart_items->getItemByProductId($pid)) {
      $cart_item->increaseQuantity($quantity);
      $cart_item->save();
    }
    elseif ($product && $product->isAvailable(FALSE)) {

      $logger_context['@product_title'] = $product->label();
      $product_link = $product->toLink(t('Edit'), 'edit-form');
      $logger_context['link'] = $product_link->toString();

        $cart_items_storage = \Drupal::entityTypeManager()->getStorage('bs_cart_item');

        $product->setPriceResolvingContext('cart');
        $actual_price = $product->getActualPrice();

        if (!$actual_price) {
          \Drupal::logger('bs_cart')->warning('Product "@product_title" can\'t be added to cart: actual price can\'t be resolved!', $logger_context);
          return NULL;
        }

        $cart_item = $cart_items_storage->create([
          'pid' => $pid,
          'quantity' => $quantity,
          'cart_id' => $this->id(),
          'price' => $actual_price,
        ]);

        $cart_item->set('price', [
          'value' => $actual_price->value,
          'currency_code' => $actual_price->currency_code,
        ]);

        $this->cart_items->set(count($this->cart_items), $cart_item);

    }

    $event = $this->getCartEvent();

    if (isset($cart_item)) {
      $this->save();
      $dispatcher = \Drupal::service('event_dispatcher');
      $this->lastAffectedItem = $affected_cart_item;
      $dispatcher->dispatch(CartEvents::PRODUCT_ADDED_TO_CART, $event);
      $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);
    }
    else {
      $dispatcher = \Drupal::service('event_dispatcher');
      $this->lastAffectedItem = $affected_cart_item;
      $dispatcher->dispatch(CartEvents::PRODUCT_DOES_NOT_ADDED_TO_CART_AJAX, $event);
    }

    return $cart_item;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Customer'))
    ->setDescription(t('The customer.'))
    ->setSetting('target_type', 'user')
    ->setSetting('handler', 'default')
    ->setDefaultValueCallback('Drupal\bs_cart\Entity\Cart::getCurrentUserId');

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The time when the cart was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
    ->setLabel(t('Changed'))
    ->setDescription(t('The time when the cart was last edited.'));

    $fields['cart_items'] = BaseFieldDefinition::create('bs_cart_items')
    ->setLabel(t('Cart items'))
    ->setDescription(t('The cart items.'))
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->addConstraint('cart_item_actuality')
    ->setSetting('clearOnCartClear', TRUE)
    ->setSetting('target_type', 'bs_cart_item')
    ->setSetting('handler', 'default')
    ->setDisplayConfigurable('form', TRUE)
    ->setCustomStorage(TRUE);

    return $fields;
  }

  public function clear() {

    foreach ($this->getItems() as $cart_item) {
      $cart_item->delete();
    }

    $this->set('cart_items', []);

    foreach ($this->getFieldDefinitions() as $cart_field_definition) {
      if ($cart_field_definition->getSetting('clearOnCartClear')) {
        $this->set($cart_field_definition->getName(), []);
      }
    }

    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return count($this->cart_items->referencedEntities()) == 0;
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    return 'Cart';
  }

  public function containsAnavailableItems() {
    foreach ($this->cart_items as $cart_item) {
      if ($cart_item->entity || $cart_item->entity->isAvailable()) return TRUE;
    }
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getLastAffectedItem() {
    return $this->lastAffectedItem ? $this->lastAffectedItem : FALSE;
  }

  public function getCartEvent(AjaxResponse $response = NULL) {

    $cart_event = &drupal_static('cart_event', NULL);

    if ($cart_event) {
       if ($response && $cart_event instanceof CartAjaxEvent) {
         $cart_event->setResponse($response);
       }
      return $cart_event;
    }

    $request = \Drupal::request();

    if ($request->isXmlHttpRequest()) {
      $cart_event = new CartAjaxEvent($this, $response);
    }
    else {
      $cart_event = new CartEvent($this);
    }

    return $cart_event;
  }

  public function getCartDataForAjax() {
    $data = [];

    $data['items_count'] = $this->getCartItemsCount();

    $data['total_amount'] = NULL;

    if ($cart_total = $this->getCartTotalAmount()) {
      $data['total_amount']['value'] = $cart_total->getPriceValue();
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemProduct($pid) {
    foreach ($this->cart_items as $cart_item) {
      $cart_item_entity = $cart_item->entity;
      if ($cart_item_entity->getPid() == $pid) {
        return $cart_item_entity->getProduct();
      }
    }
  }

  public function getCartItemById($item_id) {
    if ($items = $this->cart_items->referencedEntities()) {
      foreach ($items as $index => $cart_item) {
        if ($cart_item->id() == $item_id) {
          //$cart_item->index = $index;
          return $cart_item;
        }
      }
    }
  }

  public function getItems() {
    return $this->get('cart_items')->referencedEntities();
  }

  public function getCartItemsCount($only_available = TRUE) {
    $count = 0;

    foreach ($this->getItems() as $cart_item) {
      if ($only_available && !$cart_item->isAvailable()) continue;
      $count++;
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getCartTotalAmount() {

    $cart_total = $this->getTotalByItems();

    return $cart_total;
  }

  public function getTotalByItems($without_discount = FALSE) {

    $cart_total = NULL;

    foreach ($this->getItems() as $cart_item) {

      /**
       * @var CartItemInterface $cart_item
       */
      if ($item_total = $cart_item->getItemTotal($without_discount)) {
        if ($cart_total) {
          $cart_total->add($item_total);
        }
        else {
          $cart_total = $item_total;
        }
      }
    }

    return $cart_total;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key, $default = NULL) {
    $data = [];
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
    }
    return isset($data[$key]) ? $data[$key] : $default;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function setData($key, $value) {
    $this->get('data')->__set($key, $value);
    return $this;
  }

  public function setOnEventsResponse(Response $response) {
    $this->onEventResponse = $response;
    return $this;
  }

  public function setItemQuantity($item_id, $quatnity) {

  }

  public function setItemQuantityByPid($pid, $quantity) {
    if ($item = $this->getItemByPid($pid)) {
      $item->setQuantity($quantity);
    }
    else {
      // item with defined product does not exist - add it
      $this->addProduct($pid, $quantity);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemByPid($pid) {
    foreach ($this->getItems() as $cart_item) {
      if ($product = $cart_item->getProduct()) {

        if ($product->id() == $pid) {
          return $cart_item;
        }

      }
    }

    return FALSE;
  }

  public function getProductsListForDL() {
    $products = [];


    foreach ($this->getItems() as $cart_item) {
     if ($product = $cart_item->getProduct()) {

       $norm_product = $product->toDataLayerVar();
       $norm_product['quantity'] = intval($cart_item->getQuantity());
       $norm_product['price'] = $cart_item->get('price')->value;
       $products[] = $norm_product;

     }
    }

    return $products;
  }

  public function validateAndFix() {
    if ($this->validated) {
      return;
    }

    $violations = $this->validate();

    if ($violations->count()) {

      $fixer_plugin_manager = $this->violationFixerPluginManager();
      $definitions = $fixer_plugin_manager->getDefinitionsForCauses();

      foreach ($violations as $offset => $violation) {
        if (!$violation->getCause() || empty($definitions[$violation->getCause()])) continue;

        foreach ($definitions[$violation->getCause()] as $priority => $plugin_id) {
          $plugin_instance = $fixer_plugin_manager->createInstance($plugin_id);
          if ($plugin_instance->fixViolation($violation)) {
            $violations->remove($offset);
          }
        }
      }
    }

    return $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function checkCartItems() {
    foreach ($this->cart_items as $cart_item) {
      if ($cart_item_entity = $cart_item->entity) {
        if ($product = $cart_item_entity->getProduct()) {
          if ($product_actual_price = $product->getActualPrice()) {
            $cart_item_entity->set('price', [
              'value' => $product_actual_price->value,
              'currency_code' => $product_actual_price->currency_code,
            ]);
            $cart_item_entity->setUnavailableSatatus(FALSE);
            $cart_item_entity->save();
          }
          else {
            $link = \Drupal\Core\Link::fromTextAndUrl($product->getTitle(), $product->toUrl('canonical'));
            drupal_set_message(t('Product "@product_name_link" is unavailable for chosen shop.', ['@product_name_link' => $link->toString()]), 'warning');
            $cart_item_entity->setUnavailableSatatus(TRUE);
            $cart_item_entity->set('price', [
              'value' => NULL,
              'currency_code' => NULL,
            ]);
            $cart_item_entity->save();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {

    $fields_from_data = [];


    foreach (\Drupal::service('entity_field.manager')->getFieldDefinitions('bs_cart', 'bs_cart') as $field_definition) {
      if ($field_definition->getSetting('store_in_data_field')) {
        $fields_from_data[] = $field_definition->getName();
      }
    }

    foreach ($entities as $cart_entity) {
      //$cart_entity->checkCartItems();

      foreach ($fields_from_data as $field_name) {
        if ($value = $cart_entity->getData($field_name, NULL)) {
          $cart_entity->set($field_name, $value);
        }
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    parent::postSave($storage, $update);

    if (!$update) {
      $session = \Drupal::request()->getSession();
      $session->set('cart_uuid', $this->uuid());
    }

    foreach ($this->getItems() as $item) {

      /**
       * @var CartItemInterface $item
       */
      if ($item->getCartId() != $this->id()) {
        $item->setCartId($this->id());
      }

      if ($item->getQuantity() > 0) {
        $item->save();
      }
      else {
        $item->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    foreach ($this->getFieldDefinitions() as $field_definition) {
      if ($field_definition->getSetting('store_in_data_field')) {
        $field_name = $field_definition->getName();
        $this->setData($field_name, $this->get($field_name)->getValue());
      }
    }

    $this->validate();

    parent::preSave($storage);
    $this->removeNotExitingItems();
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem($cartItem) {
    foreach ($this->cart_items as $index => $item) {
      if ($item->target_id == $cartItem->id()) {
        $this->cart_items->removeItem($index);
        $cartItem->delete();
      }
    }
  }

  public function removeItemByItemId() {

  }

  protected function removeNotExitingItems() {
    foreach ($this->cart_items as $index => $item) {
      if ($cart_item = $item->entity) {
        $product = $cart_item->getProduct();

        if (!$product) {
          $this->cart_items->removeItem($index);
          $cart_item->delete();
        }
      }
    }
  }

  /**
   * @return Drupal\bs_cart\Plugin\BsCartViolationFixerPluginManager
   */
  protected function violationFixerPluginManager() {
    return \Drupal::service('plugin.manager.bs_cart.violation_fixer');
  }
}

