<?php
namespace Drupal\bs_cart\Entity;

use Drupal\bs_cart\CartItemInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_price\Price;
use Drupal\bs_cart\Event\CartEvents;

/**
 * Defines the cart item entity class.
 *
 * @ContentEntityType(
 *   id = "bs_cart_item",
 *   label = @Translation("Cart item"),
 *   admin_permission = "access cart",
 *   handlers = {
 *     "views_data" = "Drupal\bs_cart\CartItemsViewsData",
 *   },
 *   base_table = "bs_users_carts_items",
 *   entity_keys = {
 *     "id" = "cart_item_id",
 *   },
 *   common_reference_target = TRUE
 * )
 */
class CartItem extends ContentEntityBase implements CartItemInterface {

  protected $changedFields = [];

  /**
   * {@inheritdoc}
   */
  public function onChange($name) {
    parent::onChange($name);

    if (!in_array($name, $this->changedFields)) {
      $this->changedFields[] = $name;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function delete() {

    $cart = $this->getCart();
    $cart->removeItem($this);

    parent::delete();

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = $cart->getCartEvent();
    $dispatcher->dispatch(CartEvents::PRODUCT_REMOVED_FROM_CART, $event);
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The order backreference, populated by Order::postSave().
    $fields['cart_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Cart'))
    ->setDescription(t('The cart id.'))
    ->setSetting('target_type', 'bs_cart')
    ->setRequired(TRUE)
    ->setReadOnly(TRUE);

    $fields['pid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Product'))
    ->setDescription(t('The reference to product.'))
    ->setSetting('target_type', 'bs_product')
    ->setSetting('handler', 'base:bs_product')
    ->setRequired(TRUE);

    $fields['quantity'] = BaseFieldDefinition::create('decimal')
    ->setLabel(t('Quantity'))
    ->setDescription(t('The number of units.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE)
    ->setDefaultValue(1)
    ->setDisplayOptions('form', [
      'type' => 'number',
      'weight' => 1,
    ]);

    $fields['price'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Item price'))
    ->setDescription(t('The price of a single unit.'))
    ->setComputed(TRUE)
    ->setClass('Drupal\bs_cart\CartItemPriceItemList')
    ->setReadOnly(TRUE);

    $fields['total_price'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Total price'))
    ->setDescription(t('The total price of the item in cart.'))
    ->setReadOnly(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    $fields['unavailable'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Item is anavailable'))
    ->setDescription(t('Whether the cart item is unavailable for ordering.'))
    ->setDefaultValue(FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCart() {
    return $this->get('cart_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCartId() {
    return $this->get('cart_id')->target_id;
  }

  public function setCartId($cart_id) {
    $this->set('cart_id', $cart_id);
    return $this;
  }

  public function resetChangedFieldsList() {
    $this->changedFields = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPid() {
    return $this->get('pid')->target_id;
  }


  /**
   * @return ProductInterface | null
   */
  public function getProduct() {
    return $this->get('pid')->entity;
  }

  public function getMaxAvQuantity() {
    $product = $this->getProduct();
    return $product->getMaxAvQuantity();
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function get($field_name) {

//     $result = parent::get($field_name);

//     if ($field_name == 'price') {
//       $result->getValue();
//     }

//     return $result;
//   }

  /**
   * @return Price
   */
  public function getActualItemPrice() {

    $cart = &drupal_static('user_cart', NULL);
    $cart = $this->getCart();

    $this->get('price')->getValue();
    $actual_price =  $this->get('price')->first()->toPrice();
    return $actual_price ?: NULL;
//     if ($product = $this->getProduct()) {
//       if ($product_actual_price = $product->getActualPrice()) {
//         return $product_actual_price->toPrice();
//       }
//     }

//     return NULL;
  }

  /**
   * @return Price
   */
  public function getItemTotal($without_discount = FALSE) {
    if ($item_price = $this->getActualItemPrice()) {
      $quantity = $this->getQuantity();

      if ($without_discount && $item_price->getOldPrice()) {
        $item_price = $item_price->getOldPrice();
      }

      return $item_price->multiply($quantity);
    }

    return NULL;
  }

//   public function getPrice() {

//   }

  /**
   * {@inheritdoc}
   */
  public function getQuantity() {
    return (string) $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable() {
    return !$this->get('unavailable')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $this->get('price')->setValueComputed(FALSE);

    if ($total_by_item = $this->getItemTotal()) {
      $this->set('total_price', $total_by_item);
    }
    else {
      throw new \Exception('Error while calculating cart item total');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    foreach ($entities as $entity) {
      $entity->resetChangedFieldsList();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity($quantity) {

    if ($this->getQuantity() == $quantity) {
      return $this;
    }

    $this->set('quantity', (string) $quantity);

    if ($total_by_item = $this->getItemTotal()) {
      $this->set('total_price', $total_by_item);
    }

    return $this;
  }

  public function increaseQuantity($add_value) {
    $this->set('quantity', $this->get('quantity')->value + $add_value);
    return $this;
  }

  /**
   * @return boolean
   */
  public function hasChanges() {
    return !empty($this->changedFields);
  }


  public function setUnavailableSatatus($value) {
    $this->set('unavailable', $value ? 1 : 0);
    return  $this;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public static function postDelete(EntityStorageInterface $storage, array $entities) {
//     parent::postDelete($storage, $entities);
//   }
}

