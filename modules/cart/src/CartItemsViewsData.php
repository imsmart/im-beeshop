<?php
namespace Drupal\bs_cart;

use Drupal\views\EntityViewsData;

class CartItemsViewsData extends EntityViewsData {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bs_users_carts_items']['edit_quantity']['field'] = [
      'title' => t('Quantity text field'),
      'help' => t('Adds a text field for editing the quantity.'),
      'id' => 'bs_cart_item_edit_quantity',
    ];

    $data['bs_users_carts_items']['remove_button']['field'] = [
      'title' => t('Remove button'),
      'help' => t('Adds a button for removing the cart item.'),
      'id' => 'bs_cart_item_item_remove_button',
    ];

    $data['bs_users_carts_items']['remove_link']['field'] = [
      'title' => t('Remove item link'),
      'help' => t('Adds a link for removing the cart item.'),
      'id' => 'bs_cart_item_remove_link',
    ];

    $data['bs_users_carts_items']['price'] = [
      'title' => $this->t('Price'),
      'help' => $this->t('Cart item price'),
      'field' => [
        'id' => 'field',
        'default_formatter' => 'bs_product_price_default',
        'field_name' => 'price',
      ],
    ];

    $data['bs_users_carts_items']['bs_cart_items_bulk_remove_form'] = [
      'title' => $this->t('Cart items bulk remove form'),
      'help' => $this->t('Add a form element that lets you remove multiple items from cart.'),
      'field' => [
        'id' => 'bs_cart_items_bulk_remove_form',
      ],
    ];

    return $data;
  }
}

