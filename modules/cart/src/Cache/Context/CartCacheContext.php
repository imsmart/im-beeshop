<?php

namespace Drupal\bs_cart\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\bs_cart\CartInterface;

/**
 * Defines the CartCacheContext service, for "per cart" caching.
 *
 * Cache context ID: 'cart'.
 */
class CartCacheContext implements CacheContextInterface {


  /**
   * The cart provider service.
   *
   * @var \Drupal\bs_cart\CartInterface
   */
  protected $cart;

  /**
   * Constructs a new CartCacheContext object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider service.
   */
  public function __construct(CartInterface $cart) {
    $this->cart = $cart;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Current cart IDs');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->cart->isEmpty() ? 'empty-cart' : $this->cart->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    $metadata = new CacheableMetadata();
    $metadata->addCacheableDependency($this->cart);
    return $metadata;
  }

}
