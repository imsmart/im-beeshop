<?php
namespace Drupal\bs_cart\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_cart\CartInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\beeshop\Ajax\BeeShopEventCommand;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Drupal\bs_cart\Event\CartEvents;

class CartAjaxOperationsController implements ContainerInjectionInterface {

  protected $cart;

  public function __construct(CartInterface $cart) {
    $this->cart = $cart;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user_cart')
    );
  }

  public function removeItemFromCart($item_id) {
    $response = new AjaxResponse();

    if ($item = $this->cart->getCartItemById($item_id)) {
      $this->cart->removeItem($item);
    }

    $response->addCommand(new BeeShopEventCommand('BeeShop:cartUpdated'));

    // Allow modules to add arbitrary ajax commands to the response.
    $event = new CartAjaxEvent($this->cart, $response);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

//     $cart_data = $cart->getCartDataForAjax();
//     $response->addCommand(new BeeShopUpdateSettings('customerCart', $cart_data, TRUE));

//     $block_manager = \Drupal::service('plugin.manager.block');
//     $config = [];
//     $plugin_block = $block_manager->createInstance('bs_cart_cart_items_block', $config);
//     $render = $plugin_block->build();

//     $response->addCommand(new HtmlCommand('.block-bs-cart .block-content', $render));

//     $cart_page = $this->cartPage();
//     $response->addCommand(new ReplaceCommand('.user-sopping-cart', $cart_page));

    return $response;
  }

}

