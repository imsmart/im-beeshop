<?php

namespace Drupal\bs_cart\Controller;

use Drupal\beeshop\Ajax\BeeShopEventCommand;
use Drupal\beeshop\Ajax\BeeShopUpdateSettings;
use Drupal\bs_cart\CartInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_cart\CartProviderInterface;
use Drupal\bs_cart\Entity\Cart;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\bs_product\Entity\Product;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\bs_cart\Event\CartAjaxEvent;
use Drupal\bs_cart\Event\CartEvents;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides the cart page.
 */
class CartController extends ControllerBase {

  /**
   * The cart provider.
   *
   * @var CartInterface
   */
  protected $cart;

  protected $request;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   */
  public function __construct(CartInterface $cart, RequestStack $request_stack) {
    $this->cart = $cart;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user_cart'),
      $container->get('request_stack')
    );
  }

  public function ajaxGetCart() {

    $response = new JsonResponse();

    $serializer = \Drupal::service('serializer');
    $output = $serializer->normalize($this->cart, 'json_dl');

    $response->setData($output);

    return $response;
  }

  public function ajaxCartData() {

    $response = new AjaxResponse();

    $cart = $this->cartProvider->getCart();

    $cart_data = $cart->getCartDataForAjax();

    $response->addCommand(new BeeShopUpdateSettings('customerCart', $cart_data, TRUE));

    return $response;
  }

  public function ajaxSetItemQuantity() {
    $response = new AjaxResponse();

    /**
     * @var Cart $cart
     * */
    $cart = \Drupal::service('current_user_cart');
    $request = \Drupal::request();
    $event = new CartAjaxEvent($cart, $response);
    $dispatcher = \Drupal::service('event_dispatcher');

    if ($pid = $request->get('pid')) {
      $quantity = $request->get('in_cart_count', 0);
      $cart->setItemQuantityByPid($pid, $quantity);
      $cart->save();

      if ($product = Product::load($pid)) {
        $event->setData(CartEvents::DATA_ADDED_PRODUCT, $product);
      }
    }

    $response->addCommand(new BeeShopEventCommand('BeeShop:cartUpdated'));

    $dispatcher->dispatch(CartEvents::PRODUCT_ADDED_TO_CART, $event);
    $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

    return $response;
  }

  public function ajaxCartUpdateItemQuantity(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();

    if ($trigrerring_element = $form_state->getTriggeringElement()) {

      if (!empty($trigrerring_element['#item_id'])) {
        $item_id = $trigrerring_element['#item_id'];

        $cart_provuder = \Drupal::service('bs_cart.cart_provider');
        $cart = $cart_provuder->getCart();

        if ($cart_item = $cart->getCartItemById($item_id)) {
          $cart_item->setQuantity($trigrerring_element['#value']);
          $cart_item->save();
          $cart->save();
        }

        $request = \Drupal::requestStack()->getCurrentRequest();

        if ($request->query->has(FormBuilder::AJAX_FORM_REQUEST)) {
          $request->query->remove(FormBuilder::AJAX_FORM_REQUEST);
        }

        $b = \Drupal::entityTypeManager()->getViewBuilder('bs_cart');
        $cart_page = $b->view($cart);


        $response->addCommand(new BeeShopEventCommand('BeeShop:cartUpdated'));

        $response->addCommand(new ReplaceCommand('.user-sopping-cart', $cart_page));
      }
    }

    $event = new CartAjaxEvent($cart, $response);
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

    return $response;
  }

  public function removeItemFromCart($item_id) {
    $response = new AjaxResponse();

    $cart = $this->cartProvider->getCart();

    if ($item = $cart->getCartItemById($item_id)) {
      $cart->removeItem($item);
    }

    $response->addCommand(new BeeShopEventCommand('BeeShop:cartUpdated'));

    $cart_data = $cart->getCartDataForAjax();
    $response->addCommand(new BeeShopUpdateSettings('customerCart', $cart_data, TRUE));

    $block_manager = \Drupal::service('plugin.manager.block');
    $config = [];
    $plugin_block = $block_manager->createInstance('bs_cart_cart_items_block', $config);
    $render = $plugin_block->build();

    $response->addCommand(new HtmlCommand('.block-bs-cart .block-content', $render));

    $cart_page = $this->cartPage();
    $response->addCommand(new ReplaceCommand('.user-sopping-cart', $cart_page));

    return $response;
  }

  /**
   * @param Product $bs_product
   * @return \Drupal\Core\Ajax\AjaxResponse|unknown|\Drupal\Core\Ajax\AjaxResponse
   */
  public function addToCart($bs_product) {
    //dpm($bs_product->variations);

    $response = new AjaxResponse();

    $pid = NULL;

    if ($bs_product->isGroup()) {
      if ($bs_product->variations->count() > 1) {
        $redirect_command = new RedirectCommand($bs_product->toUrl()->toString());
        $response->addCommand($redirect_command);

        return $response;
      }

      $variation = $bs_product->variations->first()->entity;

      $pid = $variation->id();
    }
    else {
      $pid = $bs_product->id();
    }

    $this->request->query->add(['pid' => $pid]);

    return $this->ajaxAddToCart();
  }

  public function ajaxAddToCart() {

    $response = new AjaxResponse();

    if ($pid = $this->request->get('pid')) {

      if ($cart_item = $this->cart->addProduct($pid)) {

        $product = $cart_item->getProduct();

        $event = new CartAjaxEvent($this->cart, $response);
        $event->setData(CartEvents::DATA_ADDED_PRODUCT, $product);
        $dispatcher = \Drupal::service('event_dispatcher');
        $dispatcher->dispatch(CartEvents::PRODUCT_ADDED_TO_CART, $event);
        $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);
//         $response->addCommand(new BeeShopEventCommand('BeeShop:productAddedToCart', [
//           'product' => [
//             'title' => $product->getTitle(),
//             'price' => $product->getActualPrice()->value,
//             'quantity' => 1,
//           ]
//         ]));

//         $dispatcher = \Drupal::service('event_dispatcher');
//         $event = new CartAjaxEvent($this->cart, $response);
//         $dispatcher->dispatch(CartEvents::PRODUCT_ADDED_TO_CART_AJAX, $event);
//         $dispatcher->dispatch(CartEvents::CART_UPDATED_AJAX, $event);
      }


    }

    return $response;
  }

  public function clearCartAjax() {
    $response = new AjaxResponse();

    if ($this->cart) {

      $this->cart->clear();

      $event = new CartAjaxEvent($this->cart, $response);
      $dispatcher = \Drupal::service('event_dispatcher');
      $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

//       $cart_page = $this->cartPage();
//       $response->addCommand(new ReplaceCommand('.user-sopping-cart', $cart_page));

    }

    return $response;
  }

  /**
   * Gets the cart views for each cart.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface[] $carts
   *   The cart orders.
   *
   * @return array
   *   An array of view ids keyed by cart order ID.
   */
  protected function getCartViews(array $carts) {
//     $order_type_ids = array_map(function ($cart) {
//       /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
//       return $cart->bundle();
//     }, $carts);
//     $order_type_storage = $this->entityTypeManager()->getStorage('commerce_order_type');
//     $order_types = $order_type_storage->loadMultiple(array_unique($order_type_ids));
//     $cart_views = [];
//     foreach ($order_type_ids as $cart_id => $order_type_id) {
//       /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
//       $order_type = $order_types[$order_type_id];
//       $cart_views[$cart_id] = $order_type->getThirdPartySetting('commerce_cart', 'cart_form_view', 'commerce_cart_form');
//     }

//     return $cart_views;
  }

}
