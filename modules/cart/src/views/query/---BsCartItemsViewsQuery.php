<?php

namespace Drupal\bs_cart\Plugin\views\query;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\views\Plugin\views\join\JoinPluginBase;
use Drupal\views\Plugin\views\HandlerBase;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views query plugin for an SQL query.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "bs_cart_items_views_query",
 *   title = @Translation("Beeshop products SQL Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class BsCartItemsViewsQuery extends Sql {


  public function query($get_count = FALSE) {
    $target = 'default';
    $key = 'default';

    $cart_items_temp_table_name = Database::getConnection($target, $key)
    ->queryTemporary('SELECT pid FROM \'bs_product_field_data\'');

    $query = Database::getConnection($target, $key)
    ->select($cart_items_temp_table_name, 'cart_items');

    return $query;
  }

}
