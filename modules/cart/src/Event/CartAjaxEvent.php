<?php
namespace Drupal\bs_cart\Event;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\bs_cart\CartInterface;
use Drupal\beeshop\AjaxEventTrait;

class CartAjaxEvent extends CartEvent {

  use AjaxEventTrait;


  public function __construct(CartInterface $cart, AjaxResponse $response = NULL) {
    $this->cart = $cart;
    $this->response = $response ? $response : new AjaxResponse();
  }

}

