<?php
namespace Drupal\bs_cart\Event;

final class AddToCartFormEvents {

  const ADD_TO_CART_FORM_AJAX_VARIATION_CHANGED = 'bs_cart.add_to_cart_form.ajax.variation_changed';

}

