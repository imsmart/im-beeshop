<?php

namespace Drupal\bs_cart\Event;

use Drupal\bs_cart\CartInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Component\Utility\NestedArray;

/**
 * Defines the order event.
 *
 * @see \Drupal\commerce_order\Event\OrderEvents
 */
class CartEvent extends Event {

  /**
   * The cart.
   *
   * @var \Drupal\bs_cart\CartInterface
   */
  protected $cart;

  protected $data = [];

  /**
   * Constructs a new OrderEvent.
   *
   * @param \Drupal\bs_cart\CartInterface $cart
   *   The cart.
   */
  public function __construct(CartInterface $cart) {
    $this->cart= $cart;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Gets the order.
   */
  public function getCart() {
    return $this->cart;
  }

  public function getData($key, $default = NULL) {
    $exists = NULL;
    $result = NestedArray::getValue($this->data, (array) $key, $exists);
    return $exists ? $result : $default;
  }

  /**
   * Sets value for a specific key.
   *
   * @param string|array $key
   * @param mixed  $value
   */
  public function setData($key, $value) {
    NestedArray::setValue($this->data, (array) $key, $value, TRUE);
    return $this;
  }
}
