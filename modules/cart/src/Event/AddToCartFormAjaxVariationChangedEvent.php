<?php
namespace Drupal\bs_cart\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Ajax\AjaxResponse;

class AddToCartFormAjaxVariationChangedEvent extends Event {

  protected $form;

  protected $variationId;

  /**
   * The ajax response.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  protected $response;

  /**
   * Constructs a new OrderEvent.
   *
   * @param \Drupal\bs_cart\CartInterface $cart
   *   The cart.
   */
  public function __construct(array $form, $variation_id, AjaxResponse $response) {
    $this->form = $form;
    $this->variationId = $variation_id;
    $this->response = $response;
  }

  public function getForm() {
    return $this->form;
  }

  public function getVariationId() {
    return $this->variationId;
  }

  /**
   * The ajax response.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax reponse.
   */
  public function getResponse() {
    return $this->response;
  }
}

