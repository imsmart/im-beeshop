<?php

namespace Drupal\bs_cart\Event;

final class CartEvents {

  const CART_UPDATED = 'beeshop.cart.cart_updated';

  const CART_UPDATED_AJAX = 'beeshop.cart.ajax.cart_updated';

  const PRODUCT_DOES_NOT_ADDED_TO_CART_AJAX = 'beeshop.cart.ajax.product_does_ton_added';

  /**
   * Name of the event fired after creating a new order.
   *
   * Fired before the order is saved.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const PRODUCT_ADDED_TO_CART = 'beeshop.cart.product_added';

  const PRODUCT_ADDED_TO_CART_AJAX = 'beeshop.cart.ajax.product_added';

  const PRODUCT_REMOVED_FROM_CART = 'beeshop.cart.product_removed';

  const PRODUCT_REMOVED_FROM_CART_AJAX = 'beeshop.cart.ajax.product_removed';

  const DATA_ADDED_PRODUCT = 'addedProduct';

}
