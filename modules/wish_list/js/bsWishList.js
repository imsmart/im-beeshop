/**
 * 
 */
(function ($, Drupal, settings) {

  'use strict';
  
  Drupal.behaviors.bsWishListBase = {
      attach: function (context, settings) {
        
      	$('body').once('bs-wishlist-js-attached').each(function() {
          $('<div class="bs-wishlist-ajax" style="display:none"><a></a></div>').appendTo('body');
          var ajaxSettings = {
            url: '/beeshop/ajax/wish-list/get-data',
            base: 'bsCartBase',
          };
          
          var myAjaxObject = Drupal.ajax(ajaxSettings);
          
          myAjaxObject.execute();
        });
      	
      	//Добавить в избранное 'Удалить из избранного'
      	var linkText = Drupal.t('Add to wishlist');
        $('.product-fav-toggle-link').removeClass('in-favorits').attr('title', linkText).find('.link-text').text(linkText);
        if (settings.BeeShop && settings.BeeShop.customerWishList) {
          
        	if (typeof settings.BeeShop.customerWishList.count != 'undefined') {
        		$('[data-in-wis-list-count]').attr('data-in-wis-list-count', settings.BeeShop.customerWishList.count);
        	}
        	
        	if (typeof settings.BeeShop.customerWishList.pids != 'undefined') {
        		linkText = Drupal.t('Remove from wishlist');
            $.each(settings.BeeShop.customerWishList.pids, function (index, pid) {
            	var linkElement = $('.product-fav-toggle-link[data-product-id="'+ pid + '"]');
            	linkElement.addClass('in-favorits').attr('title', linkText).find('.link-text').text(linkText);
            });
        	}
        }
      }
  }

})(jQuery, Drupal, drupalSettings);