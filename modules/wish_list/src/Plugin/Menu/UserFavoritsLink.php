<?php
namespace Drupal\bs_wish_list\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Session\AccountInterface;

class UserFavoritsLink extends MenuLinkDefault {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new LoginLogoutMenuLink.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\StaticMenuLinkOverridesInterface $static_override
   *   The static override storage.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('current_user')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->t('My favorits');
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    if ($this->currentUser->isAuthenticated()) {
      return 'bs_wish_list.user_favorits_page';
    }
    else {
      return 'user.login';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters() {

    $route_params = parent::getRouteParameters();

    if ($this->currentUser->isAuthenticated()) {
      $route_params['user'] = $this->currentUser->id();
    }

    return $route_params;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {
    $url = parent::getUrlObject($title_attribute);

    if ($this->currentUser->isAuthenticated()) {
      $wish_list_provider = \Drupal::service('bs_wish_list.list_provider');
      $wish_list = $wish_list_provider->getList();

      $link_options = array(
        'attributes' => array(
          'class' => array(
            'user-favorits-link',
          ),
          'data-in-wis-list-count' => '0',
          'title' => $this->t('My favorits'),
        ),
        '#attached' => [
          'library' => [
            'bs_wish_list/base'
          ],
          'drupalSettings' => [
            'BeeShop' => [
              'customerWishList' => $wish_list ? $wish_list->getInfoForSettings() : NULL,
            ]
          ]
        ]
      );

      $url->setOptions($link_options);
    }

    return $url;
  }
}

