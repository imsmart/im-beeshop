<?php
namespace Drupal\bs_wish_list;

use Drupal\Core\Entity\ContentEntityStorageInterface;

interface CustomerWishListStorageInterface extends ContentEntityStorageInterface {
}

