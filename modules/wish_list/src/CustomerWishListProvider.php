<?php
namespace Drupal\bs_wish_list;

use Drupal\bs_cart\Entity\Cart;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

class CustomerWishListProvider implements CustomerWishListProviderInterface {

  /**
   * @var array Cart items
   */
  protected $cartItems = [];


  /**
   * Constructs a new Cart object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->listsStorage = $entity_type_manager->getStorage('bs_customer_wish_list');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getList() {

    if (!$this->currentUser->id()) {
      return FALSE;
    }

    $customer_lists = $this->listsStorage
    ->loadByProperties(['uid' => $this->currentUser->id()]);

    if ($customer_lists) {
      return reset($customer_lists);
    }

    $customer_list = $this->listsStorage->create();

    return $customer_list;
  }

  public function addProductToList($pid) {

//     $cart_item = array(
//       $pid => array(
//         'count' => 0,
//       ),
//     );

//     if (is_numeric($pid)) {
//       $product = Product::load($pid);

//       if (!$product) {
//         return FALSE;
//       }
//     }

//     $this->cartItems += $cart_item;
//     $this->cartItems[$pid]['count'] += $count;

//     dpm($this);
  }
}

