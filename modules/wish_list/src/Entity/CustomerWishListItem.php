<?php
namespace Drupal\bs_wish_list\Entity;

use Drupal\bs_wish_list\CustomerWishListItemInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the cart item entity class.
 *
 * @ContentEntityType(
 *   id = "bs_customer_wish_list_item",
 *   label = @Translation("Wish list item"),
 *   admin_permission = "access cart",
 *   handlers = {
 *     "views_data" = "Drupal\bs_wish_list\CustomerWishListItemsViewsData",
 *   },
 *   base_table = "bs_customer_wish_list_items",
 *   entity_keys = {
 *     "id" = "item_id",
 *   },
 *   common_reference_target = TRUE
 * )
 */
class CustomerWishListItem extends ContentEntityBase implements CustomerWishListItemInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The order backreference, populated by Order::postSave().
    $fields['list_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('List'))
    ->setDescription(t('The list id.'))
    ->setSetting('target_type', 'bs_customer_wish_list')
    ->setReadOnly(TRUE);

    $fields['pid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Product'))
    ->setDescription(t('The reference to product.'))
    ->setSetting('target_type', 'bs_product')
    ->setRequired(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getList() {
    return $this->get('list_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPid() {
    return $this->get('pid')->target_id;
  }

  public function getProduct() {
    return $this->get('pid')->entity;
  }

}

