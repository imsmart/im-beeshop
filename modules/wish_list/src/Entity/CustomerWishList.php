<?php
namespace Drupal\bs_wish_list\Entity;

use Drupal\bs_product\Entity\Product;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_wish_list\CustomerWishListInterface;

/**
 * Defines the cart entity class.
 *
 * @ContentEntityType(
 *   id = "bs_customer_wish_list",
 *   label = @Translation("Wish list"),
 *   handlers = {
 *     "storage" = "Drupal\bs_wish_list\CustomerWishListStorage",
 *     "views_data" = "Drupal\bs_wish_list\CustomerWishListViewsData",
 *     "route_provider" = {
 *       "default" = "Drupal\bs_wish_list\Routing\BsWishListRouteProvider",
 *     },
 *   },
 *   admin_permission = "access beeshop wish lists",
 *   base_table = "bs_customer_wish_list",
 *   entity_keys = {
 *     "id" = "lid",
 *     "uuid" = "uuid"
 *   },
 *   common_reference_target = TRUE
 * )
 */
class CustomerWishList extends ContentEntityBase implements CustomerWishListInterface {

  public function removeItem($item_to_remove) {

    foreach ($this->list_items as $index => $item) {
      if ($item->entity) {
        if ($item->entity == $item_to_remove) {
          $item_to_remove->delete();
          $this->list_items->removeItem($index);
          $this->save();
          return;
        }
      }
    }
  }

  public function toggleProduct($pid) {

    $op = 'remove';

    foreach ($this->list_items as $key => $list_item) {
      if ($list_item->entity && $list_item->entity->getPid() == $pid) {
        $list_item->entity->delete();
        $this->list_items->removeItem($key);
        $this->save();
        return $op;
      }
    }

    if ($product = Product::load($pid)) {
      $items_storage = \Drupal::entityTypeManager()->getStorage('bs_customer_wish_list_item');
      $new_item = $items_storage->create([
        'pid' => $pid,
      ]);

      $this->list_items->set(count($this->list_items), $new_item);

      $this->save();

      return 'add';
    }

  }

  /**
   * {@inheritdoc}
   */
  public function addProduct($pid, $quantity = 1) {
//     foreach ($this->cart_items as $cart_item) {
//       if ($cart_item->entity->getPid() == $pid) {
//         $current_quantity = $cart_item->entity->getQuantity();
//         $current_quantity += $quantity;
//         $cart_item->entity->setQuantity($current_quantity);
//         $cart_item->entity->save();
//         return TRUE;
//       }
//     }

//     if ($product = Product::load($pid)) {
//       $cart_items_storage = \Drupal::entityTypeManager()->getStorage('bs_cart_item');

//       $actual_price = $product->getActualPrice();

//       if (!$actual_price) return $this;

//       $new_cart_item = $cart_items_storage->create([
//         'pid' => $pid,
//         'quantity' => $quantity,
//         'cart_id' => $this->id(),
//         'price' => $actual_price,
//       ]);

//       $new_cart_item->set('price', [
//         'value' => $actual_price->value,
//         'currency_code' => $actual_price->currency_code,
//       ]);

//       $this->cart_items->set(count($this->cart_items), $new_cart_item);

//       $this->save();

//       return TRUE;
//     }

//     return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Customer'))
    ->setDescription(t('The customer.'))
    ->setSetting('target_type', 'user')
    ->setSetting('handler', 'default')
    ->setDefaultValueCallback('Drupal\bs_cart\Entity\Cart::getCurrentUserId');

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The time when the cart was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
    ->setLabel(t('Changed'))
    ->setDescription(t('The time when the cart was last edited.'));

    $fields['list_items'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Items'))
    ->setDescription(t('The list items.'))
    ->setSetting('target_type', 'bs_customer_wish_list_item')
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setSetting('handler', 'default')
    ->setCustomStorage(TRUE);

    return $fields;
  }

  public function clear() {
//     foreach ($this->cart_items as $cart_item) {
//       $cart_item->entity->delete();
//     }

//     $this->set('cart_items', []);

//     $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return count($this->list_items) > 0 ? FALSE : TRUE;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  public function getInfoForSettings($blank = TRUE) {

    $info = [];

    $info['count'] = 0;
    $info['pids'] = [];

    if (!$blank) {

      $info['count'] = count($this->list_items);

      foreach ($this->list_items as $key => $list_item) {
        if ($list_item->entity) {
          $info['pids'][] = $list_item->entity->getPid();
        }
      }
    }

    return $info;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function getCartTotalAmount() {

//     $cart_total = 0;

//     foreach ($this->cart_items as $cart_item) {
//       $item_quantity = $cart_item->entity->getQuantity();
//       $item_price = $cart_item->entity->get('price');
//       $item_price_value = $item_price->value;

//       $total_by_item = $item_price_value * $item_quantity;

//       $cart_total += $total_by_item;
//     }

//     return $cart_total;
//   }

//   public function getCartItemProduct($pid) {
//     foreach ($this->cart_items as $cart_item) {
//       $cart_item_entity = $cart_item->entity;
//       if ($cart_item_entity->getPid() == $pid) {
//         return $cart_item_entity->getProduct();
//       }
//     }
//   }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    if (!$this->list_items->isEmpty()) {
      foreach ($this->list_items as $index => $item) {
        if ($item->entity) {
          $item->entity->set('list_id', $this->id());
          $item->entity->save();
        }
      }
    }
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function removeItem($cartItem) {
//     foreach ($this->cart_items as $index => $item) {
//       if ($item == $cartItem) {
//         $this->cart_items->removeItem($index);
//       }
//       $cartItem->delete();
//       $this->save();
//     }
//   }
}

