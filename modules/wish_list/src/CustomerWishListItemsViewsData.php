<?php
namespace Drupal\bs_wish_list;

use Drupal\views\EntityViewsData;

class CustomerWishListItemsViewsData extends EntityViewsData {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

//     $data['bs_users_carts_items']['remove_button']['field'] = [
//       'title' => t('Remove button'),
//       'help' => t('Adds a button for removing the order item.'),
//       'id' => 'bs_cart_item_item_remove_button',
//     ];

    $data['bs_customer_wish_list_items']['remove_button']['field'] = [
      'title' => t('Remove button'),
      'help' => t('Adds a button for removing the list item.'),
      'id' => 'bs_customer_wish_list_item_remove_button',
    ];

    return $data;
  }
}

