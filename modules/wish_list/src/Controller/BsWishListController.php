<?php
namespace Drupal\bs_wish_list\Controller;

use Drupal\beeshop\Ajax\BeeShopEventCommand;
use Drupal\beeshop\Ajax\BeeShopUpdateSettings;
use Drupal\bs_wish_list\CustomerWishListProviderInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\SettingsCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Render\Markup;
use Drupal\bs_wish_list\Event\WishListEvents;
use Drupal\bs_wish_list\Event\BsWishListNeedLoginToAccessByAjax;


class BsWishListController extends ControllerBase {

  /**
   * The wish list provider.
   *
   * @var \Drupal\bs_wish_list\CustomerWishListProviderInterface
   */
  protected $wishListProvider;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\bs_wish_list\CustomerWishListProviderInterface
   *   The wish list provider.
   */
  public function __construct(CustomerWishListProviderInterface $cart_provider) {
    $this->wishListProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bs_wish_list.list_provider')
      );
  }

  public function userFavoritsList() {

    $wish_list = $this->wishListProvider->getList();

    if (!$wish_list) {
      $login_link = \Drupal\Core\Link::fromTextAndUrl($this->t('login'), Url::fromRoute('user.login'));
      $register_link = \Drupal\Core\Link::fromTextAndUrl($this->t('register'), Url::fromRoute('user.register'));
      $need_login_message = $this->t('To add products to favorites, you need to @register_link and/or @login_link on the site.', ['@register_link' => $register_link->toString(), '@login_link' => $login_link->toString()]);
      return [
        //'#markup' => '<div class="text-formatted"><p>Для добавления товаров в избранное, вам нужно зарегистрироваться и <a href="/user">войти</a> на сайт.</p></div>',
        '#markup' => '<div class="text-formatted"><p>' . $need_login_message . '</p></div>',
      ];
    }

    $build = [];
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->addCacheContexts(['user', 'session']);

    $build['wish_form'] = [
      '#prefix' => '<div class="customer-wish-list-form">',
      '#suffix' => '</div>',
      '#type' => 'view',
      '#name' => 'wish_list_items',
      '#display_id' => 'block_customer_wishlist_items',
      '#arguments' => [$wish_list->id()],
      '#embed' => TRUE,
    ];

    $cacheable_metadata->addCacheableDependency($wish_list);

    $build['#cache'] = [
      'contexts' => $cacheable_metadata->getCacheContexts(),
      'tags' => $cacheable_metadata->getCacheTags(),
      'max-age' => $cacheable_metadata->getCacheMaxAge(),
    ];

    return $build;

  }

  public function productInListToggle() {

    return [
      '#markup' => '***'
    ];
  }

  public function ajaxUserWishListData() {
    $response = new AjaxResponse();

    if ($wish_list = $this->wishListProvider->getList()) {
      $response->addCommand(new BeeShopUpdateSettings('customerWishList', $wish_list->getInfoForSettings(FALSE), TRUE));
    }

    return $response;
  }

  public function ajaxProductInListToggle() {

    $response = new AjaxResponse();

    /**
     * @var \Drupal\bs_wish_list\Entity\CustomerWishList $wish_list
     */
    $wish_list = $this->wishListProvider->getList();

    if (!$wish_list){
      $dispatcher = \Drupal::service('event_dispatcher');
      $event = new BsWishListNeedLoginToAccessByAjax($response);
      $dispatcher->dispatch(WishListEvents::NEED_LOGIN_TO_ACCESS_WISHLIST_ON_AJAX, $event);

      if (!$response->getCommands()) {
        $alert = new AlertCommand('Для использования избранного, Вы должны быть зарегистрированным пользователем');
        $response->addCommand($alert);
      }
      return $response;
    }

    $rm = \Drupal::routeMatch();
    $pid = $rm->getRawParameter('bs_product');

    $op = $wish_list->toggleProduct($pid);

    //BeeShopUpdateSettings
    $response->addCommand(new BeeShopUpdateSettings('customerWishList', $wish_list->getInfoForSettings(FALSE), TRUE));

    switch ($op) {
      case 'remove':
        $response->addCommand(new BeeShopEventCommand('BeeShop:productRemovedFromWishList', [], FALSE));
        break;
      case 'add':
        $response->addCommand(new BeeShopEventCommand('BeeShop:productAddedToWishList', [], FALSE));
        break;
    }


    return $response;
  }

}

