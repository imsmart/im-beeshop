<?php

namespace Drupal\bs_wish_list\Event;

final class WishListEvents {

  /**
   * Name of the event fired after creating a new order.
   *
   * Fired before the order is saved.
   *
   * @Event
   *
   * @see \Drupal\bs_order\Event\OrderEvent
   */
  const NEED_LOGIN_TO_ACCESS_WISHLIST_ON_AJAX = 'bs_wish_list.ajax.need_login_to_access_wish_list';


}
