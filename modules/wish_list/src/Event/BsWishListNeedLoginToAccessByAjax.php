<?php
namespace Drupal\bs_wish_list\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Ajax\AjaxResponse;

class BsWishListNeedLoginToAccessByAjax extends Event {


  /**
   * @var AjaxResponse
   */
  protected $response;

  /**
   * Constructs a new OrderEvent.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(AjaxResponse $response) {
    $this->response = $response;
  }

  public function getResponse() {
    return $this->response;
  }
}

