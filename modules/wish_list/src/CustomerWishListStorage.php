<?php
namespace Drupal\bs_wish_list;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\bs_cart\Entity\CartItem;

class CustomerWishListStorage extends SqlContentEntityStorage implements CustomerWishListStorageInterface {

  /**
   * {@inheritdoc}
   */
  protected function getFromStorage(array $ids = NULL) {
    $entities = parent::getFromStorage($ids);

    $items_storage = \Drupal::entityTypeManager()->getStorage('bs_customer_wish_list_item');

    foreach ($entities as $id => $list_entity) {
      if ($items = $items_storage->loadByProperties(['list_id' => $list_entity->id()])) {
        foreach ($items as $item) {
          $list_entity->list_items->set(count($list_entity->list_items), $item);
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function loadCartItems($cart_id) {
//     $query = $this->database->select('bs_users_carts_items', 'ci');
//     $query->condition('ci.cart_id', $cart_id);
//     $query->fields('ci', ['cart_item_id']);

//     $cart_items_ids = $query->execute()->fetchCol(0);

//     $cart_items = CartItem::loadMultiple($cart_items_ids);
//     return $cart_items;
  }

}

