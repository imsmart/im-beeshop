<?php
namespace Drupal\bs_product\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\serialization\Normalizer\EntityNormalizer;
use Drupal\bs_yaml\YaMLFileBuilder;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

class ProductDataLayerJsonNormalizer extends ContentEntityNormalizer {

  use StringTranslationTrait;

  /**
   * The formats that the Normalizer can handle.
   *
   * @var array
   */
  protected $format = ['datalayer_json'];

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = ProductInterface::class;

  /**
   *
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  public function __construct(EntityManagerInterface $entity_manager, ModuleHandlerInterface $module_handler, LoggerInterface $logger) {
    parent::__construct($entity_manager);

    $this->moduleHandler = $module_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {

    /**
     * @var ProductInterface $entity
     */

    $result = [];

    $clone = clone $entity;
    $clone->prepareForView();
    $result = $this->normalizeProduct($clone, $context);

    return $result;
  }


  protected function normalizeProduct(ProductInterface $product, $context) {

    $product_cat = $product->getMainCategory();

    $actual_price = $product->getActualPrice();

    $result = [
      'id' => $product->getRootParent() ? $product->getRootParent()->id() : $product->id(),
      'name' => $product->getRootParent() ? $product->getRootParent()->label() : $product->label(),
      'price' => $actual_price ? $actual_price->value : '',
    ];

    if ($product_cat) {
      $result['category'] = $product_cat->getNameWithParents();
    }

    if ($root_parent = $product->getRootParent()) {
      $result['variant'] = $product->getSKU();
    }

    $this->moduleHandler->alter('product_datalayer_json', $result, $product, $context);

    return $result;
  }

}

