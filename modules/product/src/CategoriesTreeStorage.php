<?php
namespace Drupal\bs_product;

use Drupal\Core\Database\Connection;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Provides a categories tree storage using the database.
 */
class CategoriesTreeStorage implements CategoriesTreeStorageInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Cache backend instance for the extracted tree data.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $categoriesCacheBackend;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Additional database connection options to use in queries.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The database table name.
   *
   * @var string
   */
  protected $table = 'bs_products_category_hierarchy';

  /**
   * Constructs a new \Drupal\bs_product\CategoriesTreeStorage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing configuration data.
   * @param array $options
   *   (optional) Any additional database connection options to use in queries.
   */
  public function __construct(Connection $connection, CacheBackendInterface $menu_cache_backend, CacheTagsInvalidatorInterface $cache_tags_invalidator, array $options = []) {
    $this->connection = $connection;
    $this->categoriesCacheBackend = $menu_cache_backend;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTreeData($shop_id = 0, MenuTreeParameters $parameters) {

    $categories = $this->loadCategories($shop_id, $parameters);

    $data['tree'] = [];

    return $data;
  }

  /**
   * Loads links in the given menu, according to the given tree parameters.
   *
   * @param string $menu_name
   *   A menu name.
   * @param \Drupal\Core\Menu\MenuTreeParameters $parameters
   *   The parameters to determine which menu links to be loaded into a tree.
   *   This method will set the absolute minimum depth, which is used in
   *   MenuTreeStorage::doBuildTreeData().
   *
   * @return array
   *   A flat array of menu links that are part of the menu. Each array element
   *   is an associative array of information about the menu link, containing
   *   the fields from the {menu_tree} table. This array must be ordered
   *   depth-first.
   */
  protected function loadCategories($shop_id, MenuTreeParameters $parameters) {
    $query = $this->connection->select('bs_products_category_field_data', 'c');
    $query->join($this->table, 'h', 'h.cid = c.cid');
    $query->addTag('bs_products_category_access')
    ->fields('c')
    ->fields('h', ['parent'])
    ->condition('c.sid', $shop_id)
    ->condition('c.default_langcode', 1)
    ->orderBy('c.weight')
    ->orderBy('c.name');

    $categories = $this->safeExecuteSelect($query)->fetchAllAssoc('cid', \PDO::FETCH_ASSOC);
    dpm($categories);
  }


}

