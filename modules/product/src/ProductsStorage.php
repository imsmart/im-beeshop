<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\bs_shop\Entity\Shop;
use Drupal\Core\Language\LanguageInterface;
use Drupal\bs_product\Entity\ProductType;

class ProductsStorage extends SqlContentEntityStorage implements ProductsStorageInterface {

  protected $sortByWeight = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function getFromStorage(array $ids = NULL) {
    $entities = parent::getFromStorage($ids);
    if (!$entities) return $entities;

    $categories_storage = $this->entityManager->getStorage('bs_products_category');
    foreach ($categories_storage->getProductsCategories($ids) as $pid => $product_categories) {
      $entities[$pid]->setCategoriesIds($product_categories);
    }

    if ($variations_pids = $this->getProductsVariationsIdsMultily($ids)) {
      $this->sortByWeight = TRUE;
      $variation_entities = parent::getFromStorage($variations_pids);

      if ($variation_entities) {
        $by_parents = [];

        foreach ($variation_entities as $variation) {
          if ($parent_id = $variation->getParentId()) {

            if (isset($entities[$parent_id])) {

              if ($entities[$parent_id]->hasField('variations')) {
                //$by_parents[$parent_id][] = $variation->id();
                $variation->setLevel($entities[$parent_id]->getLevel() + 1);
                $variation->set('parent', $entities[$parent_id]);
                $entities[$parent_id]->variations->appendItem($variation->id());
              }
              else {
                \Drupal::logger('bs_product:product_structure')->critical('Product structure error! PID#' . $entities[$parent_id]->id());
              }

            }
          }
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {

    $shops = Shop::loadMultiple();

    if (count($shops) == 1) {
      $default_shop = reset($shops);

      $values['shops'] = [
        $default_shop->id(),
      ];
    }

    $entity = parent::create($values);

    return $entity;
  }

  /**
   *
   * {@inheritDoc}
   */
  protected function loadFromDedicatedTables(array &$values, $load_from_revision) {

    $bundles = [];
    foreach ($values as $entity_values) {
      $bundles[$this->bundleKey ? $entity_values[$this->bundleKey][LanguageInterface::LANGCODE_DEFAULT] : $this->entityTypeId] = TRUE;
    }

    $altered_field_storage_definitions = [];

    foreach ($bundles as $bundle => $v) {
      if ($bundle_entity = ProductType::load($bundle)) {
        if ($bundle_entity->isVariationsSuport()) {
          $productFieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('bs_product', $bundle);

          $level = isset($values['level']) ? $values['level'] : 0;

          if ($fields = $bundle_entity->getFieldsForInhFromChildsForSave($level)) {
            foreach ($fields as $field_name) {
              $field_storage_def = $productFieldDefinitions[$field_name]->getFieldStorageDefinition();
              $field_storage_def->prevCardinality = $field_storage_def->getCardinality();
              $field_storage_def->setCardinality(-1);

              $altered_field_storage_definitions[] = $field_storage_def;
            }
          }
        }

      }
    }

    parent::loadFromDedicatedTables($values, $load_from_revision);

    if ($altered_field_storage_definitions) {
      foreach ($altered_field_storage_definitions as $field_storage_def) {
        if (isset($field_storage_def->prevCardinality)) {
          $field_storage_def->setCardinality($field_storage_def->prevCardinality);
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $entities = parent::loadMultiple($ids);

    return $entities;
  }

  public function getProductsVariationsIdsMultily(array $pids) {

    $variations = [];

    $query = $this->database->select($this->dataTable, 'p');
    $query->condition('p.parent', $pids, 'IN');
    $query->fields('p', ['pid']);

    $variations = $query->execute()->fetchCol(0);
    return $variations;
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    if (!$entity->mustBeDeletedOnParentSave()) {
      return parent::save($entity);
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Entity\Sql\SqlContentEntityStorage::buildQuery()
   */
  protected function buildQuery($ids, $revision_id = FALSE) {
    $query = parent::buildQuery($ids);

    if ($this->sortByWeight) {
      $query->leftJoin($this->dataTable, 'data', 'base.pid = data.pid');
      $query->fields('data', ['weight']);
      $query->orderBy('data.weight');
    }

    return $query;
  }


}

