<?php
namespace Drupal\bs_product;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

interface ProductAvailabilityStateStorageInterface extends ConfigEntityStorageInterface {
}

