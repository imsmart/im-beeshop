<?php

namespace Drupal\bs_product\Exception;

/**
 * Thrown when product type can't be loaded.
 */
class ProductTypeNotFoundException extends \RuntimeException {}
