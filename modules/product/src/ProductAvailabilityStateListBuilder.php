<?php
namespace Drupal\bs_product;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

class ProductAvailabilityStateListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['av_flag'] = $this->t('Is available for order');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['name']['data']['label_markup']['#markup'] = $entity->label();
    $row['av_flag']['data']['label_markup']['#markup'] = $entity->av_flag ? '+' : '';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_product_av_state_list_form';
  }



}

