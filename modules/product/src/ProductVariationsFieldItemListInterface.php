<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\bs_product\Entity\ProductInterface;

interface ProductVariationsFieldItemListInterface extends EntityReferenceFieldItemListInterface {

  /**
   * Get variation entity by it's ID
   * @param int $id
   *
   * @return \Drupal\bs_product\Entity\ProductInterface|NULL
   *   Variation entity or NULL if no variations found
   *
   */
  public function getVariation($id);

}

