<?php
namespace Drupal\bs_product\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

class ProductVariationsTaskAccessCheck implements AccessInterface {

  public function access(ProductInterface $bs_product) {

    return AccessResult::allowedIf($bs_product->hasField('variations'));
  }

}

