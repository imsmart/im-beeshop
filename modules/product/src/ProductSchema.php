<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class ProductSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['bs_product_variations_display'] = [
      'description' => 'Stores ...',
      'fields' => [
        'ptype' => [
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ],
        'level_for_category' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'level_for_page' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
      ],
      'primary key' => ['ptype'],
    ];

    return $schema;
  }

}

