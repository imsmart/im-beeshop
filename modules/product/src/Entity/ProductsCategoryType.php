<?php
namespace Drupal\bs_product\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntity;

/**
 * Defines the product type entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_products_category_type",
 *   label = @Translation("Products category type"),
 *   label_collection = @Translation("Products category types"),
 *   label_singular = @Translation("products category type"),
 *   label_plural = @Translation("products category types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count products category type",
 *     plural = "@count products category types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_product\ProductsCategoryTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_product\Form\ProductsCategoryTypeForm",
 *       "add" = "Drupal\bs_product\Form\ProductsCategoryTypeForm",
 *       "edit" = "Drupal\bs_product\Form\ProductsCategoryTypeForm",
 *       "delete" = "Drupal\bs_product\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "bs_products_category_type",
 *   admin_permission = "administer beeshop products category type",
 *   bundle_of = "bs_products_category",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/structure/categories/categories-types/add",
 *     "edit-form" = "/admin/beeshop/structure/categories/categories-types/{bs_products_category_type}/edit",
 *     "delete-form" = "/admin/beeshop/structure/categories/categories-types/{bs_products_category_type}/delete",
 *     "collection" = "/admin/beeshop/structure/categories/categories-types"
 *   }
 * )
 */
class ProductsCategoryType extends BeeShopBundleWithDescriptionEntity implements ProductsCategoryTypeInterface {
}

