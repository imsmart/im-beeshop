<?php
namespace Drupal\bs_product\Entity;

use Drupal\bs_product\ProductsCategoryStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\hierarchical_entity\HierarchicalContentEntityTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Products category entity class.
 *
 * @HierarchicalEntity(
 *   parent_relation = @ORM\ManyToOne(targetEntity="bs_products_category", inversedBy="children")
 * )
 *
 * @ContentEntityType(
 *   id = "bs_products_category",
 *   label = @Translation("Products category"),
 *   label_singular = @Translation("products category"),
 *   label_plural = @Translation("products categories"),
 *   label_count = @PluralTranslation(
 *     singular = "@count products category",
 *     plural = "@count products categories",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_product\ProductsCategoryStorage",
 *     "tree" = "Drupal\entity_tree\EntityTree",
 *     "storage_schema" = "Drupal\bs_product\ProductsCategorySchema",
 *     "list_builder" = "Drupal\bs_product\ProductCategoryListBuilder",
 *     "views_data" = "Drupal\bs_product\ProductsCategoryViewsData",
 *     "access" = "Drupal\bs_product\PoductsCategoryAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\bs_product\Form\ProductsCategoryForm",
 *       "add" = "Drupal\bs_product\Form\ProductsCategoryForm",
 *       "edit" = "Drupal\bs_product\Form\ProductsCategoryForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   base_table = "bs_products_category",
 *   data_table = "bs_products_category_field_data",
 *   hierarchy_table = "bs_products_category_hierarchy",
 *   common_reference_target = TRUE,
 *   admin_permission = "administer beeshop products categories",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "cid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "status" = "status",
 *     "published" = "status",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "parent" = "parent",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "canonical" = "/catalog/category/{bs_products_category}",
 *     "add-page" = "/admin/beeshop/structure/categories/add",
 *     "add-form" = "/admin/beeshop/structure/categories/add/{bs_products_category_type}",
 *     "edit-form" = "/catalog/category/{bs_products_category}/edit",
 *     "delete-form" = "/catalog/category/{bs_products_category}/delete",
 *     "collection" = "/admin/beeshop/structure/categories/tree",
 *   },
 *   bundle_entity_type = "bs_products_category_type",
 *   field_ui_base_route = "entity.bs_products_category_type.edit_form",
 * )
 */
class ProductsCategory extends ContentEntityBase implements ProductsCategoryInterface {

  use EntityPublishedTrait;

  use HierarchicalContentEntityTrait;

  protected $customUrlRouteParams;

  /**
   * {@inheritdoc}
   */
  public function isPublished() {

    $self_published = (bool) $this->getEntityKey('published');

    if (!$self_published) {
      return FALSE;
    }

    $parent = $this->getParent();

    if ($parent && !$parent->isPublished()) {
      return FALSE;
    }

    return $self_published;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentCid() {
    return intval($this->parent->target_id);
  }

  public function getChangedTime() {}

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    return $this->get('parent')->entity ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getNameWithParents($delimiter = '/') {
    $current = $this;

    $parts = [];

    while ($current) {
      $parts[] = $current->label();

      $current = $current->getParent();
    }

    $parts = array_reverse($parts);

    return implode($delimiter, $parts);
  }

  public function getDefaultMenuTitle() {
    if (!$this->get('menu_item_label')->isEmpty()) {
      return $this->get('menu_item_label')->value;
    }

    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentCid($parent_cid) {
    $this->get('parent')->setValue($parent_cid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['cid']->setLabel(t('Category ID'))
    ->setDescription(t('The category ID.'));

    $fields['uuid']->setDescription(t('The category UUID.'));

    $fields['langcode']->setDescription(t('The category language code.'));

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(new TranslatableMarkup('Name'))
    ->setTranslatable(TRUE)
    ->setRequired(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -5,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -10,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);;

    $fields['menu_item_label'] = BaseFieldDefinition::create('string')
    ->setLabel(new TranslatableMarkup('Menu item label'))
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -5,
    ])
    ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setLabel(new TranslatableMarkup('Description'))
    ->setTranslatable(TRUE)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_default',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
    ->setLabel(new TranslatableMarkup('Weight'))
    ->setDescription(t('The weight of this category in relation to other categories.'))
    ->setDefaultValue(0);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(new TranslatableMarkup('Category parent'))
    ->setDescription(t('The parent category of this category.'))
    ->setSetting('target_type', 'bs_products_category')
    ->setCardinality(1)
    ->setCustomStorage(TRUE);

    $fields['childCats'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Child categories'))
      ->setTranslatable(FALSE)
      ->setDescription(t('Child categories'))
      ->setSetting('target_type', 'bs_products_category')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE)
      ->setComputed(TRUE);

    $fields['sid'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Shop'))
    ->setDescription(t('Category shop ID'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('view', FALSE)
    ->setDisplayConfigurable('form', FALSE);

//     $fields['sid'] = BaseFieldDefinition::create('entity_reference')
//     ->setLabel(t('Shop'))
//     ->setDescription(t('Category shop.'))
//     ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
//     //->setRequired(TRUE)
//     ->setSetting('target_type', 'bs_shop')
//     ->setSetting('handler', 'default')
//     ->setTranslatable(TRUE)
//     ->setDisplayOptions('form', [
//       'type' => 'options_select',
//       'weight' => 2,
//     ])
//     ->setDisplayConfigurable('view', FALSE)
//     ->setDisplayConfigurable('form', TRUE);


    $fields['path'] = BaseFieldDefinition::create('path')
    ->setLabel(t('URL alias'))
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'path',
      'weight' => 30,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setComputed(TRUE);

    $fields['show_in_categories_menu'] = BaseFieldDefinition::create('boolean')
    ->setLabel(new TranslatableMarkup('Show in categories menu'))
    ->setRevisionable(TRUE)
    ->setDefaultValue(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => TRUE,
      ],
      'weight' => 120,
    ])
    ->setDisplayConfigurable('form', TRUE);

    $fields['status']
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => TRUE,
      ],
      'weight' => 120,
    ])
    ->setTranslatable(FALSE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['hasSubCats'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Has sub categries'))
      ->setDescription(new TranslatableMarkup('Indicates that the category has sub categories'))
      ->setComputed(TRUE)
      ->setDefaultValue(FALSE);

    $fields['category_products'] = BaseFieldDefinition::create('category_products')
    ->setLabel(new TranslatableMarkup('Category products'))
    ->setTranslatable(FALSE)
    ->setDisplayConfigurable('form', FALSE)
    ->setDisplayConfigurable('view', TRUE)
    ->setComputed(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    $childs = $storage->getSubCatsIds(array_keys($entities));

    foreach ($entities as $entity) {
      if (isset($childs[$entity->id()])) {
        $entity->set('childCats', $childs[$entity->id()]);
        $entity->set('hasSubCats', TRUE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {

    $cids = [];

    foreach ($entities as $category) {
      $cids[] = $category->id();
    }

    /**
     * @var ProductsCategoryStorageInterface $storage
     */
    $storage->deleteCategoryHierarchy($cids);
    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    $need_set_update_hierarchy = FALSE;

    if (!$this->get('parent')->isEmpty()) {
      if (!$this->original || intval($this->parent->target_id) != intval($this->original->parent->target_id)) {
        $need_set_update_hierarchy = TRUE;
      }
    }
    else {
      $this->set('parent', 0);
      $need_set_update_hierarchy = TRUE;
    }

    if ($need_set_update_hierarchy) {
      $storage->updateCategoryHierarchy($this);
    }
  }

  public function toMenuLink() {
    $menu_item = [
      'route_name' => 'entity.bs_products_category.canonical',
      'route_parameters' => [
        'bs_products_category' => $this->id()
      ],
      'id' => $this->id(),
      'title' => $this->getDefaultMenuTitle(),
      'description' => '',//$this->getDescription(),
      'enabled' => $this->isPublished(),
      'expanded' => TRUE,
      'weight' => $this->getWeight(),
      'menu_name' => 'bs_shop_categories',
      'metadata' => [
        'category_id' => $this->id(),
      ],
    ];

    if ($parent_id = $this->getParentId()) {
      $menu_item['parent'] = 'beeshop.categories:' . $parent_id;
    }

    return $menu_item;
  }

  public function setCustomUrlRouteParams($rel = 'canonical', $params) {
    foreach ($params as $key => $value) {
      $this->customUrlRouteParams[$rel][$key] = $value;
    }
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Entity\Entity::urlRouteParameters()
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if (!empty($this->customUrlRouteParams[$rel])) {
      foreach ($this->customUrlRouteParams[$rel] as $param_name => $value) {
        $uri_route_parameters[$param_name] = $value;
      }
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkipFromTranslationChangesCheck() {
    // @todo the current implementation of the parent field makes it impossible
    // for ::hasTranslationChanges() to correctly check the field for changes,
    // so it is currently skipped from the comparision and has to be fixed by
    // https://www.drupal.org/node/2843060.
    $fields = parent::getFieldsToSkipFromTranslationChangesCheck();
    $fields[] = 'parent';
    return $fields;
  }


}

