<?php

namespace Drupal\bs_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\bs_price\Plugin\Field\FieldType\ProductPriceItem;

/**
 * Defines the interface for products.
 *
 * @property \Drupal\bs_price\ProductPriceItemList price
 */
interface ProductInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface{

  /**
   * Get whether the product can be purchased.
   *
   * @return bool
   *   TRUE if the product can be purchased, FALSE otherwise.
   */
  public function canBePurchased();

  public function getSKU();

  /**
   * Gets the product title.
   *
   * @return string
   *   The product title
   */
  public function getTitle();

  /**
   * @param bool $include_sub_variations
   *
   * @return \Drupal\bs_product\Entity\ProductInterface[]
   */
  public function getVariations($include_sub_variations = FALSE, $final_only = FALSE);

  /**
   * Sets the product parent
   *
   * @param integer $parent_id
   *
   * @return $this
   */
  public function setParent($parent_id);

  /**
   * Sets the product title.
   *
   * @param string $title
   *   The product title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Returns whether or not the product is available and can be purchaised.
   *
   * @return bool TRUE if the product is available, FALSE otherwise.
   */
  public function isAvailable();

  /**
   * @return bool
   */
  public function isFinalPoint();

  /**
   * Get whether the product is group product
   *
   * @return bool
   *   TRUE if the product is group product
   */
  public function isGroup();

  /**
   * Get whether the product is support variations
   *
   * @return bool
   *   TRUE if the product is support variations
   */
  public function isVariationsSupport();

  /**
   * @return bool
   */
  public function hasVariations();

  /**
   *
   * @return $this
   */
  public function prepareForView();

  /**
   * @return \Drupal\bs_product\Entity\ProductInterface
   */
  public function getActiveVariation();

  /**
   *
   * @return ProductPriceItem | null
   */
  public function getActualPrice();

  /**
   * @return string
   */
  public function getAvailabilitySateLabel();

  /**
   * Gets the product creation timestamp.
   *
   * @return int
   *   The product creation timestamp.
   */
  public function getCreatedTime();


  /**
   * Get Product type entity
   *
   * @return \Drupal\bs_product\Entity\ProductTypeInterface
   */
  public function getBundleEntity();

  /**
   * Get product parent
   *
   * @return \Drupal\bs_product\Entity\ProductInterface|null
   *   The product parent product, or NULL if the product does not yet have an
   *   parent product.
   */
  public function getParent();


  public function getParentsIds($reverse_result);

  /**
   * Gets product shops array keyed by Shop ID
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   */
  public function getShopsKeyedByID();

  /**
   * Sets the product creation timestamp.
   *
   * @param int $timestamp
   *   The product creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);


  /**
   * Get product level (depth).
   *
   * @return int
   */
  public function getLevel();

  /**
   * @return ProductsCategoryInterface | null
   */
  public function getMainCategory();

  /**
   * Validate product by specifed validtion group name
   *
   * @param string $group
   *
   * @return \Drupal\Core\Entity\EntityConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation succeeded.
   */
  public function validateFor($group);

}
