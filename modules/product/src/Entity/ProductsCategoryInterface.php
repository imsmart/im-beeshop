<?php
namespace Drupal\bs_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\hierarchical_entity\HierarchicalEntityInterface;

interface ProductsCategoryInterface extends ContentEntityInterface, EntityPublishedInterface/*, HierarchicalEntityInterface*/ {

  /**
   * Gets the weight of this category.
   *
   * @return int
   *   The weight of the category.
   */
  public function getWeight();

  /**
   * Sets the weight of this category.
   *
   * @param int $weight
   *   The category weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   *
   * @return ProductsCategoryInterface|NULL
   */
  public function getParent();

  public function getParentCid();


  /**
   * Gets the name of the category.
   *
   * @return string
   *   The name of the category.
   */
  public function getName();

  public function getNameWithParents();

  /**
   * Sets the name of the category.
   *
   * @param int $name
   *   The category name.
   *
   * @return $this
   */
  public function setName($name);

}

