<?php

namespace Drupal\bs_product\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for products category types.
 */
interface ProductsCategoryTypeInterface extends BeeShopBundleWithDescriptionEntityInterface {
}

