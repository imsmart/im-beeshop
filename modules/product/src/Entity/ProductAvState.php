<?php
namespace Drupal\bs_product\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the product availability state entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_product_av_state",
 *   label = @Translation("Product availability state"),
 *   label_singular = @Translation("product availability state"),
 *   label_plural = @Translation("product availability states"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product availability state",
 *     plural = "@count product availability states",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_product\ProductAvailabilityStateStorage",
 *     "form" = {
 *       "default" = "Drupal\bs_product\Form\ProductAvailabilityStateForm",
 *       "add" = "Drupal\bs_product\Form\ProductAvailabilityStateForm",
 *       "edit" = "Drupal\bs_product\Form\ProductAvailabilityStateForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_product\ProductAvailabilityStateListBuilder",
 *   },
 *   admin_permission = "administer beeshop product av states",
 *   config_prefix = "av_state",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "uuid",
 *     "weight",
 *     "av_flag",
 *     "ad_to_cart_button_text",
 *     "add_to_cart_form_replacement_text",
 *     "add_to_cart_form_description_text",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/structure/products/product-states/add",
 *     "edit-form" = "/admin/beeshop/structure/products/product-states/{bs_product_av_state}/edit",
 *     "delete-form" = "/admin/beeshop/structure/products/product-states/{bs_product_av_state}/delete",
 *     "collection" = "/admin/beeshop/structure/products/product-states",
 *   }
 * )
 */
class ProductAvState extends ConfigEntityBase implements ProductAvStateInterface {

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($operation == 'view label') {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The product state label.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name ID'))
      ->setDescription(t('A unique machine-readable name for this state. It must only contain lowercase letters, numbers, and underscores.'))
      ->setSetting('max_length', 64)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('The product state description text.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['product_is_unavailable'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Product is unavailable with this state'))
      ->setDescription(t('If checked product can\'t be added to cart and ordered.'))
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['ad_to_cart_button_text'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Add to cart button text'))
      ->setDescription(t('...'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['add_to_cart_form_replacement_text'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Add to cart form replacement text'))
      ->setDescription(t('If product with current state is unavailable this text will be shown instead of the Add to cart form.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['add_to_cart_form_replacement_text_format'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Add to cart form replacement text format'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['add_to_cart_form_description_text'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Add to cart form replacement text'))
      ->setDescription(t('This text will be shown after Add to cart form.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['add_to_cart_form_description_text_format'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Add to cart form replacement text format'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The product state weight'))
      ->setDefaultValue(0);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->get('label')->value;
  }
}

