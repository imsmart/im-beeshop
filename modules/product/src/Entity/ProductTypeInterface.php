<?php

namespace Drupal\bs_product\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntityInterface;

/**
 * Defines the interface for product types.
 */
interface ProductTypeInterface extends BeeShopBundleWithDescriptionEntityInterface {

  /**
   *
   */
  public function isVariationsSuport();

  /**
   *
   */
  public function setVariationsSupport($variations_support);

  /**
   *
   */
  public function getVariationFieldHierarchyData($field_name);

}
