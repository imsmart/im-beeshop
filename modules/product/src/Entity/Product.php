<?php

namespace Drupal\bs_product\Entity;

use Drupal\beeshop\BeeshopEnityFieldDefinition;
use Drupal\bs_product\Exception\ProductTypeNotFoundException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\user\UserInterface;
use Drupal\bs_shop\Entity\Shop;
use Symfony\Component\Validator\Constraints as Assert;
use Drupal\beeshop\PurchasableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityConstraintViolationList;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\bs_product\ProductConstraintViolationList;
use Drupal\bs_price\Price;
use Drupal\bs_price\Plugin\Field\FieldType\ProductPriceItem;
use Drupal\bs_price\ProductPriceItemList;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\Serializer\Serializer;
use Drupal\Component\Utility\Timer;

/**
 * Defines the product entity class.
 *
 * @ContentEntityType(
 *   id = "bs_product",
 *   label = @Translation("Product"),
 *   label_collection = @Translation("Products"),
 *   label_singular = @Translation("product"),
 *   label_plural = @Translation("products"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product",
 *     plural = "@count products",
 *   ),
 *   bundle_label = @Translation("Product type"),
 *   handlers = {
 *     "storage" = "Drupal\bs_product\ProductsStorage",
 *     "storage_schema" = "Drupal\bs_product\ProductSchema",
 *     "view_builder" = "Drupal\bs_product\ProductViewBuilder",
 *     "views_data" = "Drupal\bs_product\ProductViewsData",
 *     "list_builder" = "Drupal\bs_product\ProductListBuilder",
 *     "access" = "Drupal\bs_product\ProductAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\bs_product\Form\ProductForm",
 *       "add" = "Drupal\bs_product\Form\ProductForm",
 *       "edit" = "Drupal\bs_product\Form\ProductForm",
 *       "clone" = "Drupal\bs_product\Form\ProductForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "variation_form" = {
 *       "default" = "Drupal\bs_product\Form\ProductForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_product\Routing\ProductRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer beeshop product",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   base_table = "bs_product",
 *   data_table = "bs_product_field_data",
 *   entity_keys = {
 *     "id" = "pid",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/catalog/product/{bs_product}",
 *     "edit-form" = "/catalog/product/{bs_product}/edit",
 *     "add-page" = "/catalog/product/add",
 *     "add-form" = "/catalog/product/add/{bs_product_type}",
 *     "clone-form" = "/catalog/product/{bs_product}/clone",
 *     "add-variation-form" = "/catalog/product/add/{bs_product_type}/{level}",
 *     "delete-form" = "/catalog/product/{bs_product}/delete",
 *   },
 *   bundle_entity_type = "bs_product_type",
 *   field_ui_base_route = "entity.bs_product_type.edit_form",
 *   common_reference_target = TRUE,
 *   constraints_by_groups = {
 *     "availability" = {
 *       "bs_product_access" = {
 *         "operation" = "view"
 *       },
 *       "bs_product_state" = {},
 *       "bs_product_av_by_price" = {
 *         "skipForParents" = TRUE
 *       },
 *       "bs_product_by_variations" = {
 *         "set" = "post",
 *       }
 *     },
 *   }
 * )
 *
 */
class Product extends ContentEntityBase implements ProductInterface, PurchasableEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  protected $_mainCategory;

  protected $needSave = FALSE;

  protected $basePrice = NULL;

  protected $actualPrices = [];

  protected $discountedPrice = NULL;

  /**
   *
   */
  protected $actualPrice = [];

  protected $avState = NULL;

  protected $deleteOnParentSave = FALSE;

  protected $groupValidationViolations = [];

  protected $preparedForView = [];

  protected $priceResolvingContext = 'product';

  protected $priceResolvingContexts = [];

  protected $variationFieldsCardinalityAltered = FALSE;

  protected $varaitionsInRequest;

  /**
   * @var ProductInterface
   */
  protected $activeVariation;

//   public function &__get($name) {
//     dpm($name);
//     return parent::__get($name);
//   }

  public function __call($name, $arguments) {
    $r = \Drupal::moduleHandler()->invokeAll('bs_product_call', [$this, $name, $arguments]);
    if ($r) {
      return reset($r);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  public function getTitleForAC() {

    $title_parts = [
      $this->label(),
      '(#' . $this->getSKU() . ')',
    ];

    if ($variations_fields = $this->getBundleEntity()->getVariationFieldsHierarchy()) {
      foreach ($variations_fields as $field_name => $field_data) {
        $title_parts[] = $this->getFieldDefinition($field_name)->getLabel() . ': [bs_product:' . $field_name . ']';
      }
    }

    $title = implode(' ', $title_parts);

    $token_data = [
      'bs_product' => $this,
    ];

    $title = \Drupal::token()->replace($title, $token_data);
    $title = strip_tags($title);

    return $title;
  }

  public function characteristicsLabel($include_field_label = TRUE, $delimiter = ' ') {

    $label_parts = [];

    if ($variations_fields = $this->getBundleEntity()->getVariationFieldsHierarchy()) {
      foreach ($variations_fields as $field_name => $field_data) {
        if ($this->getLevel() >= $field_data['level']) {
          $label_parts[] = ($include_field_label ? $this->getFieldDefinition($field_name)->getLabel() . ': ' : '') . '[bs_product:' . $field_name . ']';
        }
      }
    }

    $label = implode($delimiter, $label_parts);

    $token_data = [
      'bs_product' => $this,
    ];

    $label = \Drupal::token()->replace($label, $token_data);
    $label = strip_tags($label);

    return $label;
  }

  public function getTranslations() {
    return $this->translations;
  }

  /**
   * {@inheritdoc}
   */
  public function setSku($sku) {
    $this->set('sku', $sku);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    $uri = parent::toUrl($rel, $options);
    return $uri;
  }

  public function toDataLayerVar() {
    /**
     * @var Serializer $serializer
     */
    $serializer = \Drupal::service('serializer');

    return $serializer->normalize($this, 'datalayer_json');
  }

  public function setDeleteOnParentSave($delete = TRUE) {
    $this->deleteOnParentSave = $delete;
    return $this;
  }

  public function mustBeDeletedOnParentSave() {
    return $this->deleteOnParentSave;
  }

  public function needSave($needSave = NULL) {
    if ($needSave === NULL) {
      return $this->needSave;
    }

    $this->needSave = $needSave;
  }

  /**
  * {@inheritDoc}
  */
  public function isAvailable($up_recursion = TRUE, $revalidate = FALSE, $update_unav_reasons = FALSE) {

    if (!$revalidate) {
      return $this->get('product_unav_reasons')->isEmpty();
    }

    $result = $this->validateFor('availability', $up_recursion);

    if ($result->count() && $update_unav_reasons) {
      foreach ($this->getLastGroupValidationViolations('availability') as $violation) {
        $this->get('product_unav_reasons')->setValue(['source' => $violation->getCause(), 'reason' => $violation->getCause()]);
      }
    }

    return !$result->count();
  }


  public function isFinalPoint() {
    $max_level = $this->getBundleEntity()->getMaxChildLevel();

    return $this->getLevel() == $max_level;
  }

  public function isVariationsSupport() {
    return $this->hasField('variations');
  }

  public function hasVariations() {
    return $this->isVariationsSupport() && !$this->get('variations')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function isGroup() {
    if (!$this->hasField('variations')) {
      return FALSE;
    }
    return $this->variations->count() > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {

    $self_published = (bool) $this->getEntityKey('status');

    if (!$self_published) {
      return FALSE;
    }

    if ($main_cat = $this->getMainCategory()) {
      return $main_cat->isPublished();
    }

    return TRUE;
  }

  public function setFieldDefinitions($field_name, $field_definitions) {
    if (isset($this->fieldDefinitions[$field_name])) {
      $this->fieldDefinitions[$field_name] = $field_definitions;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLinkedVariations() {

    if (!$this->hasField('variations') || !$this->variations->count()) return;

    // Fields list that values must be coped to variations
    $fields_to_copy_down = [];

    foreach ($this->fieldDefinitions as $field_name => $field_definition) {
      /**
       * @var $field_definition \Drupal\Core\Field\FieldDefinitionInterface
       */
      if ($field_definition->getSetting('copy_to_variations')) {
        $fields_to_copy_down[] = $field_name;
      }
    }

    $parent_langcode = $this->language()->getId();

    // Ensure there's a back-reference on each product variation.
    foreach ($this->getVariations() as $variation) {

      if ($variation->mustBeDeletedOnParentSave()) {
        $variation->delete();
      }
      else {
        if ($variation->parent->isEmpty() || $variation->getParentId() != $this->id()) {
          $variation->parent = $this->id();
          $variation->needSave(TRUE);
        }

        if ($variation->get('level')->isEmpty() ||
          ($variation->get('level')->value != $this->getLevel() + 1)) {
            $variation->set('level', $this->getLevel() + 1);
            $variation->needSave(TRUE);
        }

        $root_parent = (!$this->getParentId()) ? $this->id() :
        (($this->getRootParentId()) ? $this->getRootParentId() : FALSE);

        if ($root_parent) {
          $variation->setRootParent($root_parent);
        }

        if ($variation->get('type')->entity->id() != $this->get('type')->entity->id()) {
          $variation->setBundleEntity($this->get('type')->entity);
          $variation->set('level', $this->getLevel() + 1);
          $variation->needSave(TRUE);
        }

        foreach ($fields_to_copy_down as $field_name) {
          $value = $this->get($field_name);
          $variation->set($field_name, []);
          foreach ($value as $item) {
            $variation->get($field_name)->appendItem($item->getValue());
          }
          $variation->setFieldDefinitions($field_name, $this->fieldDefinitions[$field_name]);
          $variation->needSave(TRUE);
        }

        if (!$variation->hasTranslation($parent_langcode)) {
          $variation->addTranslation($parent_langcode);
        }

        //$translation = $variation->getTranslation($parent_langcode);
        //$translation->setTitle($this->getTitle());
        $variation->needSave(TRUE);

        if ($variation->isNew() || $variation->needSave()) {
          $variation->save();
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Entity\Entity::urlRouteParameters()
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel != 'canonical' || !$this->getBundleEntity()->isVariationsSuport()) return $uri_route_parameters;

    $variations_h = $this->getBundleEntity()->getVariationFieldsHierarchy();

    if (!$variations_h) return $uri_route_parameters;

    /**
     * @var ProductPriceItem $actual_price;
     */
    $actual_price = $this->getActualPrice();

    if ($actual_price && $actual_price->getEntity()->id() != $this->id()) {
      $uri_route_parameters['variation'] = $actual_price->getEntity()->getTreePath(TRUE);
    }

    $standalone_page_level = 0;

    foreach ($variations_h as $field_data) {
      if (!empty($field_data['have_standalone_page']) && !empty($field_data['level'])) {
        $standalone_page_level = $field_data['level'];
      }
    }

    if (!$standalone_page_level) {
      $uri_route_parameters['bs_product'] = ($this->getLevel() == $standalone_page_level) ? $this->id() : $this->getRootParentId();

      if ($this->getLevel() > $standalone_page_level && empty($uri_route_parameters['variation'])) {
        $uri_route_parameters['variation'] = $this->id();
      }

      if (empty($uri_route_parameters['bs_product'])) {
        $uri_route_parameters['bs_product'] = $this->id();
      }

      return $uri_route_parameters;
    }

    if ($standalone_page_level == $this->getLevel()) return $uri_route_parameters;

    if ($this->getLevel() > $standalone_page_level) {
      if ($parent_by_level = $this->getParentByLevel($standalone_page_level)) {
        $uri_route_parameters['bs_product'] = $parent_by_level->id();
        return $uri_route_parameters;
      }
    }

    if (empty($uri_route_parameters['bs_product'])) {
      $uri_route_parameters['bs_product'] = $this->id();
    }

    return $uri_route_parameters;
  }


  /**
   * {@inheritdoc}
   */
  public function updateIsGroup() {
    $this->set('is_group', $this->isGroup());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCategories(array $categories) {
    $this->set('categories', $categories);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCategoriesIds(array $cids) {
    $this->set('categories', $cids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getShops() {

    if (!$this->hasParent()) {
      $shops = $this->get('shops')->referencedEntities();
    }
    else {
      $shops = $this->getParent()->getShops();
    }


    return $this->ensureTranslations($shops);
  }

  /**
   * {@inheritdoc}
   */
  public function getSKU() {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getShopsKeyedByID() {
    $shops = [];
    if ($shops_array = $this->getShops()) {
      foreach ($shops_array as $shop) {
        $shops[$shop->id()] = $shop;
      }
    }

    return $shops;
  }

  public function getVariations($include_sub_variations = FALSE, $final_only = FALSE) {
    $variations = [];

    if (!$this->hasField('variations')) return $variations;

    foreach ($this->get('variations')->referencedEntities() as $variation) {
      $variation->setLevel($this->getLevel() + 1);
      $variations[] = $variation;
      if ($include_sub_variations) {
        foreach ($variation->getVariations($include_sub_variations) as $s_variation) {
          $variations[] = $s_variation;
        }
      }
    }

    if ($final_only) {
      $variations = array_filter($variations, function ($variation) {
        return $variation->isFinalPoint();
      });
    }

    return $variations;
  }

  public function getVariationByIdsPath($ids = [], &$matches = []) {

    if (!$this->isVariationsSupport()) return FALSE;

    $current_parent = $this;

    $variation = FALSE;

    while ($id = array_shift($ids)) {
      if ($variation = $current_parent->variations->getVariation($id)) {
        $matches[$id] = $variation;
        $current_parent = $variation;
      }
    }

    return $variation;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  public function hasParent() {
    if ($this->parent->isEmpty()) return FALSE;
    return $this->getParentId() ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getShopIds() {
    $shop_ids = [];

    if (!$this->getParentId()) {
      foreach ($this->get('shops') as $store_item) {
        $shop_ids[] = $store_item->target_id;
      }
    }
    else {
      $shop_ids = $this->getParent()->getShopIds();
    }

    return $shop_ids;
  }

  /**
   * {@inheritDoc}
   */
  public function getCategoryIds() {


    $categories_ref_holder = $this->getRootParent() ?: $this;

    $cids = [];
    foreach ($categories_ref_holder->get('categories') as $category_item) {
      $cids[] = $category_item->target_id;
    }
    return $cids;
  }

  public function getAvailabilitySateLabel() {
    if ($av_state = $this->getAvState()) {
      return $av_state->label();
    }

    return $this->t('Available');
  }

  /**
   * @return unknown
   */
  public function getAvState() {

    if ($this->avState !== NULL) {
      return $this->avState;
    }

    if ($av_state_item = $this->get('av_state')->first()) {
      $this->avState = $av_state_item->entity;
    }

    return $this->avState;
  }


  public function getActualPrice() {
//     if (!empty($this->actualPrice[$this->priceResolvingContext])) {
//       return $this->actualPrice[$this->priceResolvingContext];
//     }

//     if ($this->activeVariation) {
//       return $this->activeVariation->getActualPrice();
//     }

    $actual_price = NULL;
    $min_price_resolved = FALSE;

    // TODO: Need to refactoring code below

    if (!$actual_price && $this->hasVariations()) {
      $min_variation_price = NULL;
      $current_variations = $this->getVariationIdsFromRequest();

      foreach ($this->getVariations() as $variation) {

        if (!$variation->isAvailable() ||
            ($current_variations &&
              (!in_array($variation->id(), $current_variations) && !in_array($variation->getParentId(), $current_variations))
            )) {
          continue;
        }

        if ($variation_price = $variation->getActualPrice()) {

          if ($current_variations && in_array($variation->id(), $current_variations)) {
            $actual_price = $variation_price;
            $min_price_resolved = FALSE;
            $min_variation_price = NULL;
            break;
          }

          $min_variation_price = $variation_price;

          if (!$min_variation_price) {
            $min_variation_price = $variation_price;
            continue;
          }

          if ($min_variation_price->getPriceValue() > $variation_price->getPriceValue()) {
            $min_variation_price = $variation_price;
            $min_price_resolved = TRUE;
          }
        }
      }

      $actual_price = $actual_price?: ($min_variation_price ?: NULL);
    }
    else {
      $price_resolver = $this->priceResolver();
      $actual_price = $price_resolver->resolve($this);
    }

    if ($min_price_resolved) {
      $actual_price->set('is_minimal', TRUE);
    }

    \Drupal::moduleHandler()->alter('product_actual_price', $actual_price);

    $this->actualPrice[$this->priceResolvingContext] = $actual_price;

    return $this->actualPrice[$this->priceResolvingContext];
  }

  public function _getActualPrice() {

    $current_shop_id = \Drupal::service('bs_shop.default_shop_resolver')->resolve()->id();

    if (!empty($this->actualPrices[$current_shop_id]['actualPrice'])) return $this->actualPrices[$current_shop_id]['actualPrice'];

    $this->discountedPrice = null;
    $this->actualPrice = null;

    if ($current_shop = Shop::load($current_shop_id)) {
      $price_types_tree = $current_shop->getShopPriceTypesTree();
      $active_price_type = null;

      foreach ($price_types_tree as $price_type) {
        if (empty($price_type->parent)) {
          $active_price_type = $price_type;
          break;
        }
      }

      if ($active_price_type) {
        foreach ($this->price as $price_item) {
          if ($price_item->shop_id == $current_shop_id &&
              ($price_item->price_type == $active_price_type->id() || ($active_price_type->isChildType($price_item->price_type)))) {
            if ($price_item->price_type == $active_price_type->id()) {
              $this->actualPrices[$current_shop_id]['basePrice'] = $price_item;
              $this->basePrice = &$this->actualPrices[$current_shop_id]['basePrice'];
              continue;
            }
            if ($active_price_type->isChildType($price_item->price_type)) {
              $this->actualPrices[$current_shop_id]['discountedPrice'] = $price_item;
              $this->discountedPrice = &$this->actualPrices[$current_shop_id]['discountedPrice'];
              continue;
            }
          }
        }
      }
    }

    $this->actualPrices[$current_shop_id]['actualPrice'] = !empty($this->actualPrices[$current_shop_id]['discountedPrice']) ?
                                                           $this->actualPrices[$current_shop_id]['discountedPrice'] :
                                                           (!empty($this->actualPrices[$current_shop_id]['basePrice']) ? $this->actualPrices[$current_shop_id]['basePrice'] : NULL);

    return $this->actualPrices[$current_shop_id]['actualPrice'];
  }

  public function getPrice() {
    return $this->get('price');
  }

  public function getPriceResolvingContext() {
    return $this->priceResolvingContext;
  }

  public function setPriceResolvingContext($price_resolving_context = 'product') {
    $this->priceResolvingContext = $price_resolving_context;
    return $this;
  }

  public function setPriceResolvingContextEntity($entity_type, EntityInterface $entity) {
    $this->priceResolvingContexts[$entity_type] = $entity;
    if (!empty($this->actualPrice[$entity_type])) {
      unset($this->actualPrice[$entity_type]);
    }
    return $this;
  }

  public function getPriceResolvingContextEntity($entity_type) {

    return !empty($this->priceResolvingContexts[$entity_type]) ? $this->priceResolvingContexts[$entity_type] : NULL;
  }

  public function hasPrice() {
    return !$this->get('price')->isEmpty();
  }

  public function setPrice($price) {

    if ($price instanceof ProductPriceItem) {
      $this->set('price', []);
      $this->get('price')->appendPriceItem($price);
      return $this;
    }

    $this->set('price', $price);

    return $this;
  }

  public function getPriceByPriceType($price_type_name) {
    foreach ($this->price as $price) {
      $price_values = $price->getValue();
      if ($price_values['price_type'] == $price_type_name) {
        return $price;
      }
    }

    return FALSE;
  }

  public function hasDiscountedPrice() {
    $this->getActualPrice();
    return !empty($this->discountedPrice);
  }

  public function getBasePrice() {
    $this->getActualPrice();
    return !empty($this->basePrice) ? $this->basePrice : FALSE;
  }

  public function getData($data_key, $default_value = NULL) {
    if ($data = $this->get('extra_data')->first()) {
      return $data->{$data_key};
    }
    return $default_value;
  }

  public function setData($key, $value) {
    if ($data = $this->get('extra_data')->first()) {
      $data->set($key, $value);
      return $this;
    }
    else {
      $data = [
        $key => $value,
      ];
    }

    $this->set('data', $data);
    return $this;
  }

  public function getDefaultVariation() {
    if (!$this->hasField('variations')) return $this;

    if ($variations_ids_by_request = $this->getVariationIdsFromRequest()) {
      $variations = $this->get('variations')->findVariations($variations_ids_by_request);

      if ($variations->count()) {
        return $variations->first()->entity;
      }
    }

    if (!$av_variations = $this->getAvailableVariations()) {
      return $this;
    }

    return reset($av_variations);

  }

  public function getAvailableVariations() {
    $av_variations = [];

    foreach ($this->getVariations() as $variation) {
      if ($variation->isAvailable()) {
        $av_variations[$variation->id()] = $variation;
      }
    }

    return $av_variations;
  }

  protected function getVariationIdsFromRequest() {

    $this->varaitionsInRequest = [];

    if ($product = $this->routeMatch()->getParameter('bs_product')) {
      $parents = $this->getParentsIds();

      if ($this->id() == $product->id() || in_array($product->id(), $parents)) {
        if ($variation = $this->request()->get('variation')) {
          $this->varaitionsInRequest = explode('/', $variation);
        }
      }
    }

    return $this->varaitionsInRequest;
  }

  public function getDiffFields() {
    return $this->getBundleEntity()->getVariationsDiffFields();
  }

  public function getDiscountedPrice() {
    $this->getActualPrice();
    return !empty($this->discountedPrice) ? $this->discountedPrice: FALSE;
  }

  /**
   *  {@inheritdoc}
   */
  public function getBundleEntity() {
    return $this->get('type')->entity;
  }

  public function setBundleEntity($bundle_entity) {
    $this->set('type', $bundle_entity);
    $this->entityKeys['bundle'] = $bundle_entity->id();
    //$this->fieldDefinitions = $this->entityManager()->getFieldDefinitions($this->entityTypeId, $this->bundle());
    $this->fieldDefinitions = $this->entityFieldManager()->getFieldDefinitions($this->entityTypeId, $this->get('type')->target_id);
    $this->entityKeys['bundle'] = $this->get('type')->target_id;
    if (!empty($this->typedData)) {
      unset($this->typedData);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {

    $contexts = ['bs_shop'];

    if ($this->isVariationsSupport()) {
      if (($current_variation = $this->getVariationIdsFromRequest()) &&
        reset($current_variation) != $this->id()) {
          $contexts[] = 'url.query_args';
        }

    }

    return Cache::mergeContexts(parent::getCacheContexts(), $contexts);
  }

  public function getCacheTagsToInvalidate() {
    $tags = parent::getCacheTagsToInvalidate();
    $tags[] = 'bs_shop:' . \Drupal::service('bs_shop.default_shop_resolver')->resolve()->id();

    if ($parent = $this->get('parent')->entity) {
      $tags[] = 'bs_product:' . $parent->id();
    }

    return $tags;
  }

  /**
   * {@inheritDoc}
   */
  public function getCategories() {

    if ($root_parent = $this->getRootParent()) {
      return $root_parent->getCategories();
    }

    $categories= $this->get('categories')->referencedEntities();
    return $this->ensureTranslations($categories);
  }


  /**
   * {@inheritDoc}
   */
  public function getMainCategory() {

    if ($this->_mainCategory) {
      return $this->_mainCategory;
    }

    if ($root_parent = $this->getRootParent()) {
      $this->_mainCategory = $root_parent->getMainCategory();
    }
    elseif ($categories = $this->getCategories()) {
      $this->_mainCategory = reset($categories);
    }

    return $this->_mainCategory;
  }

  public function setRootParent($root_parent) {
    $prev_root_parent_id = $this->getRootParentId();
    if ($prev_root_parent_id != $root_parent) {
      $this->set('root_parent', $root_parent);
      $this->needSave(TRUE);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setShops(array $shops) {
    $this->set('shops', $shops);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setShopIds(array $shop_ids) {
    $this->set('shops', $shop_ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    return $this->get('parent')->entity;
  }

  public function getParentsIds($reverse_result = TRUE) {
    $parents = [];

    if ($parent = $this->getParent()) {
      $parents[] = $parent->id();

      if ($up_level_parents = $parent->getParentsIds(FALSE)) {
        $parents = array_merge($parents, $up_level_parents);
      }
    }

    return $reverse_result ? array_reverse($parents) : $parents;
  }

  public function getTreePath($exclude_root = FALSE) {
    $parents = $this->getParentsIds();

    if ($parents && $exclude_root) {
      unset($parents[0]);
    }

    $parents[] = $this->id();

    return implode('/', $parents);
  }

  /**
   * {@inheritdoc}
   *
   * @param integer $parent_id
   */
  public function setParent($parent_id) {
    $this->set('parent', $parent_id);
    return $this;
  }

  /**
   *
   */
  public function getParentByLevel($parent_level = 0) {
    $current = $this;

    while ($current && $current->getLevel() > $parent_level) {
      $current = $current->getParent();
    }

    return $current ? $current : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId() {
    if ($this->get('parent')) {
      return $this->get('parent')->target_id ? $this->get('parent')->target_id : FALSE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootParent($by_field = TRUE) {

    $parent_field = $this->get('parent');

    if ($parent_field->isEmpty() || !$parent_field->target_id) {
      return FALSE;
    }

    if ($by_field && !$this->root_parent->isEmpty() && $this->root_parent->entity) {
      return $this->root_parent->entity;
    }

    $parent = NULL;
    $current = $this;

    while($current) {
      $parent = $current;
      $current = $current->get('parent')->entity;
    }

    $this->set('root_parent', $parent);

    return $parent;


//     if (!$this->get('parent')->isEmpty() && $this->get('parent')->target_id) {
//       if (!$this->root_parent->isEmpty() && $this->root_parent->target_id) {
//         return $this->root_parent->entity;
//       }

//       $parent = NULL;
//       $current = $this;

//       while($current) {
//         $parent = $current;
//         dpm($parent->id());
//         $current = $this->get('parent')->target_id ? $current->get('parent')->entity : FALSE;
//       }

//       if ($parent) {
//         $this->set('root_parent', $parent);
//       }

//       return $parent;
//     }
//     else {
//       return $this;
//     }

  }

  /**
   * {@inheritdoc}
   */
  public function getRootParentId() {

    if (!$this->get('parent')->isEmpty() && $this->get('parent')->target_id) {
      return $this->getRootParent()->id();
    }

    return NULL;

//     return $this->getRootParentId()->id();

    return $this->get('root_parent')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLevel() {
    return intval($this->get('level')->value);
  }

  /**
   * {@inheritdoc}
   */
  public function setLevel($value) {
    return $this->set('level', $value);
  }


  /**
   * {@inheritdoc}
   */
  public function categoriesNeedUpdate() {
    if (empty($this->original)) return TRUE;

    if ($this->original->getCategoryIds() != $this->getCategoryIds()) return TRUE;

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    $duplicate = parent::createDuplicate();

    if ($duplicate->hasField('variations') && !$duplicate->get('variations')->isEmpty()) {

      foreach ($this->variations as $index => $item) {
        $variation = $item->entity;

        if ($variation) {
          $cloned_variation = $variation->createDuplicate();

          $duplicate->variations->set($index, $cloned_variation);

        }
      }
    }

    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function canBePurchased($ignoreIfVariationsExist = FALSE) {
    if (!$this->getActualPrice()) return FALSE;

    if ($product_state = $this->getAvState()) {
      if (!$product_state->av_flag) {
        return FALSE;
      }
    }

    return TRUE;
  }

  public function hasOwnPage() {
    $level = $this->getLevel();
    $bundle = $this->getBundleEntity();
    $bundle_variation_fiels_hierarchy = $bundle->getVariationFieldsHierarchy();
    $display_level = 0;

    if ($bundle_variation_fiels_hierarchy) {
      //$display_level = -1;
      foreach ($bundle_variation_fiels_hierarchy as $field_name => $field_data) {
        if ($field_data['have_standalone_page']) {
          $display_level = $field_data['level'];
          break;
        }
      }
    }

    if ($display_level == $level) {
      return TRUE;
    }

    return FALSE;
  }

  public function updateActiveLangcode() {

    $langcode= \Drupal::languageManager()->getCurrentLanguage()->getId();

    if ($this->activeLangcode != $langcode && !empty($this->translations[$langcode])) {
      $this->activeLangcode = $langcode;
    }
  }

  /**
   * Ensures that the provided entities are in the current entity's language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The entities to process.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The processed entities.
   */
  protected function ensureTranslations(array $entities) {
    $langcode = $this->language()->getId();
    foreach ($entities as $index => $entity) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if ($entity->hasTranslation($langcode)) {
        $entities[$index] = $entity->getTranslation($langcode);
      }
    }

    return $entities;
  }


  public function setActiveLangcode($langcode) {
    $this->activeLangcode = $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    // TODO: Need check and update root parent id if parent has been changed

    if ($this->id() < 1) {
      $this->set('pid', null);
      $this->enforceIsNew();
    }

    if ($this->getParentId() < 0) {
      $this->setParent(0);
    }

    if (!$this->hasParent()) {
      $this->set('inh_price_from_parent', 0);
    }

    if (!empty($this->original) && $this->original->get('type')->target_id != $this->get('type')->target_id) {
      $this->fieldDefinitions = $this->entityManager()->getFieldDefinitions($this->entityTypeId, $this->get('type')->target_id);
      $this->entityKeys['bundle'] = $this->get('type')->target_id;
      if (!empty($this->typedData)) {
        unset($this->typedData);
      }

      $this->original->setBundleEntity($this->get('type')->entity);
    }

    if ($this->hasField('variations')) {

      $this->updateIsGroup();
      $this->inheritFieldsFromParentsForSave();
      $this->inheritFieldsFromChildsForSave();

      foreach ($this->getVariations() as $variation) {
        $variation->set('parent', $this);
      }

    }

    $this->get('product_unav_reasons')->setValue([]);

    if (!$this->isNew()) {
      if (!$this->isAvailable(FALSE, TRUE)) {

        foreach ($this->getLastGroupValidationViolations('availability') as $violation) {
          $this->get('product_unav_reasons')->setValue(['source' => $violation->getCause(), 'reason' => $violation->getCause()]);
        }
      }
      elseif ($this->getParent() && !$this->getParent()->isAvailable()) {
        $this->get('product_unav_reasons')->setValue(['source' => 'parent product', 'reason' => 'unavailable by parent']);
      }
    }

    parent::preSave($storage);



//     if ($this->isGroup() && $this->hasField('variations')) {

//       $all_variations_is_unav = TRUE;

//       foreach ($this->getVariations() as $variation) {
//         if ($variation->isAvailable()) {
//           $all_variations_is_unav = FALSE;
//         }
//       }

//       if ($all_variations_is_unav) {
//         $this->get('product_unav_reasons')->setReason('product:av_by_variations', 'All variations is unavailable');
//       }
//       else {
//         $this->get('product_unav_reasons')->removeReasonBySource('product:av_by_variations');
//       }

//     }
//     elseif (!$this->hasField('variations')) {
//       if (!$this->isAvailable()) {
//         $unav_violations = $this->getLastGroupValidationViolations();
// //         dpm($unav_violations);
//         $this->get('product_unav_reasons')->setReason('product', 'Is unavailable');
//       }
//       else {
//         $this->get('product_unav_reasons')->removeReasonBySource('product');
//       }
//     }

  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    if ($this->isNew() || $this->categoriesNeedUpdate()) {
      $categories_storage = \Drupal::service('entity_type.manager')->getStorage('bs_products_category');
      $categories_storage->updateProductCategories($this);
    }

    $this->updateLinkedVariations();

    parent::postSave($storage, $update);

    $add_tags_to_invalidate = [];

    if ($update) {
      $add_tags_to_invalidate = Cache::mergeTags($add_tags_to_invalidate, ['bs_product_values', 'entity_field_info']);
    }
    else {

      if (!$this->get('parent')->isEmpty()) {
        $add_tags_to_invalidate[] = 'bs_product:' . $this->get('parent')->target_id;
      }
      if (!$this->get('root_parent')->isEmpty()) {
        $add_tags_to_invalidate[] = 'bs_product:' . $this->get('root_parent')->target_id;
      }

    }

    if ($add_tags_to_invalidate) {
      Cache::invalidateTags($add_tags_to_invalidate);
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // Delete the product variations of a deleted product.
//     $variations = [];
//     foreach ($entities as $entity) {
//       if (empty($entity->variations)) {
//         continue;
//       }
//       foreach ($entity->variations as $item) {
//         $variations[$item->target_id] = $item->entity;
//       }
//     }
//     $variation_storage = \Drupal::service('entity_type.manager')->getStorage('commerce_product_variation');
//     $variation_storage->delete($variations);

    foreach ($entities as $entity) {

      /**
       * @var ProductInterface $entity
       */

      if ($entity->hasField('variations') && !$entity->get('variations')->isEmpty()) {
        foreach ($entity->get('variations')->referencedEntities() as $variation) {
          $variation->delete();
        }
      }

      \Drupal::database()->delete('bs_products_category_index')
      ->condition('pid', $entity->id())
      ->execute();
    }

    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    //dpm($entities);
    parent::postLoad($storage, $entities);
    foreach ($entities as $entity) {
      /**
       * @var ProductInterface $entity
       */
//       if ($actual_price = $entity->getActualPrice()) {
//         $entity->set('price_is_discounted', $actual_price->isDiscounted());
//       }
    }
//     if ($this) {

//     }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type']->setDisplayConfigurable('form', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The product author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\bs_product\Entity\Product::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The product title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'for_variation' => [
          'form' => [
            'required' => FALSE,
          ]
        ]
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sku'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SKU'))
      ->setDescription(t('The product SKU (Stock Keeping Unit).'))
      //->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
        'description' => t('The product SKU (Stock Keeping Unit).'),
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shops'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Shop'))
      ->setDescription(t('Shop.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bs_shop')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['categories'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product categories'))
      ->setDescription(t('The product categories.'))
      ->setSetting('target_type', 'bs_products_category')
      ->setSettings(['for_variation' => [
        'form' => FALSE,
      ]])
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE)
      ->setCustomStorage(TRUE);

    $fields['inh_price_from_parent'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Inherit price from parent product'))
      ->setDefaultValue(TRUE)
      ->setTranslatable(FALSE);

    $fields['price'] = BaseFieldDefinition::create('bs_product_price')
      ->setName('price')
      ->setLabel(t('Price'))
      ->setDescription(t('The product price'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'bs_price_default',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'bs_price_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['price_is_discounted'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Product actual price is discounted'))
      ->setDescription(t('Indicate that the product actual price is discounted.'))
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setClass('\Drupal\bs_product\ProductPriceIsDiscounted')
      ->setComputed(TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
//     ->setLabel(t('URL alias'))
//     ->setTranslatable(TRUE)
//     ->setDisplayOptions('form', [
//       'type' => 'path',
//       'weight' => 30,
//     ])
//     ->setDisplayConfigurable('form', TRUE)
//     ->setComputed(TRUE);
      ->setLabel(t('URL alias'))
      ->setDescription(t('The product URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['av_state'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Availability state'))
      ->setDescription(t('Availability state.'))
      ->setSetting('target_type', 'bs_product_av_state')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['product_unav_reasons'] = BaseFieldDefinition::create('product_unav_reasons')
      ->setLabel(t('Product unavailability reasons'))
      ->setDescription(t('Product unavailability reasons.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('Whether the product is published.'))
      ->setDefaultValue(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the product was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['promote'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Promoted to front page'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['sticky'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Sticky at top of lists'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the product was last edited.'))
      ->setTranslatable(TRUE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent product'))
      ->setSetting('target_type', 'bs_product')
      ->setSetting('handler', 'base:bs_product')
      ->setDescription(t('The parent product.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', TRUE)
      ->setReadOnly(TRUE);

    $fields['root_parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Root parent product'))
      ->setSetting('target_type', 'bs_product')
      ->setDescription(t('The root parent product.'))
      ->setDefaultValue(0)
      ->setReadOnly(TRUE);

    $fields['level'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Level'))
      ->setDescription(t('The product level.'))
      ->setDefaultValue(0);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The product weight'))
      ->setDefaultValue(0);

    $fields['is_group'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is group'))
      ->setDescription(t('Whether the product has childs products.'))
      ->setDefaultValue(FALSE)
      ->setReadOnly(TRUE);

    $fields['extra_data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCallback($callback) {
    if (is_string($callback) && substr($callback, 0, 2) == '::') {
      $callback = [$this, substr($callback, 2)];
    }
    return $callback;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {

    $fields = [];

    if (($bundle_entity = ProductType::load($bundle)) &&
      $bundle_entity->isVariationsSuport()) {

      $fields['inh_price_from_parent'] = clone $base_field_definitions['inh_price_from_parent'];

      $fields['inh_price_from_parent']->setDisplayConfigurable('form', TRUE);

      $fields['variations'] = BaseFieldDefinition::create('bs_product_variations')
        ->setName('variations')
        ->setLabel('Product variations')
        ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
        ->setSetting('target_type', 'bs_product')
        ->setSetting('handler', 'base:bs_product')
        ->setDisplayConfigurable('form', TRUE)
        ->setCustomStorage(TRUE);

     return $fields;

    }

    return [];
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  /**
   *
   */
  public function inheritFieldsFromChildsForSave() {

    if (!$this->isVariationsSupport()) return;

    if ($fields = $this->getBundleEntity()->getFieldsForInhFromChildsForSave($this->getLevel())) {

      $productFieldDefinitions = $this->getFieldDefinitions();

      foreach ($fields as $field_name) {
        $this->set($field_name, []);
        $setted_items[$field_name] = [];

        $field_st_def = $productFieldDefinitions[$field_name]->getFieldStorageDefinition();
        $productFieldDefinitions[$field_name]->setSetting('originalCardinality', $field_st_def->getCardinality());
//         $field_st_def->setThirdPartySetting('bs_product', 'originalCardinality', $field_st_def->getCardinality());
        $field_st_def->setCardinality(-1);
        $this->{$field_name}->setValue([]);
        $counter = 0;

        foreach ($this->getVariations() as $variation) {
          // Inherit values only if variation is published
          if (!$variation->isPublished() || $variation->bundle() != $this->bundle()) continue;

          $variation->inheritFieldsFromChildsForSave();
          $value = $variation->get($field_name);
          foreach ($value as $item) {
            $this->{$field_name}->set($counter, $item);
            $counter++;
          }
        }
      }
    }
  }

  public function getFieldsInheretedFromChildsForSave() {
    return $this->getBundleEntity()->getFieldsForInhFromChildsForSave($this->getLevel());
  }

  public function getFieldsInheretedFromParentForView() {
    return $this->getBundleEntity()->getFieldsForInhFromParentForView();
  }

  protected function alterVariationFieldsCardinality() {

    if ($this->variationFieldsCardinalityAltered) return;

    if ($fields = $this->getBundleEntity()->getFieldsForInhFromChildsForSave($this->getLevel())) {
      $productFieldDefinitions = $this->getFieldDefinitions();

      foreach ($fields as $field_name) {

        if (empty($productFieldDefinitions[$field_name])) {
          continue;
        }

        $field_st_def = $productFieldDefinitions[$field_name]->getFieldStorageDefinition();
        //$field_st_def->setThirdPartySetting('bs_product', 'originalCardinality', $field_st_def->getCardinality());
        $productFieldDefinitions[$field_name]->setSetting('originalCardinality', $field_st_def->getCardinality());
        $field_st_def->setCardinality(-1);
      }
    }

    $this->variationFieldsCardinalityAltered = TRUE;
  }

  protected function restoreVariationFieldsCardinality() {

    if (!$this->variationFieldsCardinalityAltered) return;


    foreach ($this->fieldDefinitions as $field_name => $field_definition) {
      if ($original_cardinality = $field_definition->getSetting('originalCardinality')) {
        $field_st_def = $field_definition->getFieldStorageDefinition();
        $field_st_def->setCardinality($original_cardinality);
      }
    }

//     if ($fields = $this->getBundleEntity()->getFieldsForInhFromChildsForSave($this->getLevel())) {
//       $productFieldDefinitions = $this->getFieldDefinitions();

//       foreach ($fields as $field_name) {
//         $field_st_def = $productFieldDefinitions[$field_name]->getFieldStorageDefinition();

//         if ($original_cardinality = $field_st_def->getThirdPartySetting('bs_product', 'originalCardinality')) {
//           $field_st_def->setCardinality($original_cardinality);
//         }
//       }
//     }

    $this->variationFieldsCardinalityAltered = FALSE;
  }

  /**
   *
   */
  public function inheritFieldsFromParentsForSave() {

    if (!$this->getBundleEntity()->isVariationsSuport()) return;

    if ($this->getLevel() > 1 && $parent = $this->getParent()) {
      if ($fields = $this->getBundleEntity()->getFieldsForInhFromParentForSave($this->getLevel())) {
        foreach ($fields as $field_name) {
          $value = $parent->get($field_name);
          $this->set($field_name, []);
          foreach ($value as $item) {
            $this->{$field_name}->set(count($this->{$field_name}), $item);
          }
        }
      }
    }
  }

  /**
   *
   */
  public function updateFieldsValuesByParentsForView($view_mode = 'default') {

    $fields_for_inh_from_parent = $this->getBundleEntity()->getFieldsForInhFromParentForView();

    if (!$fields_for_inh_from_parent || $this->fieldsByParentUpdated) return;

    $current_parent = $this->getParent();

    $parents = [];

    while ($current_parent) {

      $translated_entities = $this->ensureTranslations([$current_parent]);
      $current_parent = reset($translated_entities);
      $parents[$current_parent->id()] = $current_parent->id();

      foreach ($fields_for_inh_from_parent as $field_name) {
        if (!$this->hasField($field_name) || !$current_parent->hasField($field_name) /*|| !$this->{$field_name}->isEmpty()*/) continue;

        if (!$this->{$field_name}->isEmpty() &&
            $this->getFieldDefinition($field_name)->getFieldStorageDefinition()->getCardinality() >= count($this->{$field_name})) {
          continue;
        }

        foreach ($current_parent->get($field_name) as $item) {
          if (!empty($item->source) && !isset($parents[$item->source->id()])) {
            continue;
          }
          $item = $this->get($field_name)->appendItem($item->getValue());
        }

      }

      $current_parent = $current_parent->getParent();
    }

    $this->fieldsByParentUpdated = TRUE;
  }

  public function updateFieldsValuesByChildsForView($view_mode = 'default') {
    $fields_for_inh_from_parent = $this->getBundleEntity()->getFieldsForInhFromChildsForView();
    if (!$fields_for_inh_from_parent) return;
    $active_variation = $this->getActiveVariation();
    //$unshift = $active_variation != $this;

    if ($active_variation) {
      $current = $active_variation;

      $items = [];

      while ($current && $current->id() != $this->id()) {
        foreach ($fields_for_inh_from_parent as $field_name) {
          foreach ($current->get($field_name) as $fieldItem) {
            $items[$field_name][] = $fieldItem;
          }
        }

        $current = $current->getParent();
      }

      if ($items) {
        foreach ($items as $field_name => $items) {
          $exiting_items = clone $this->get($field_name);

          $this->set($field_name, []);

          if (!$this->getFieldDefinition($field_name)->getThirdPartySetting('bs_product','isVariationCharacteristicField')) {

            foreach ($items as $item) {
              /**
               * @var FieldItemInterface $item
               */
              $added_item = $this->get($field_name)->appendItem($item->getValue());
              $added_item->source = $item->getEntity();
            }

            foreach ($exiting_items as $item) {
              $this->get($field_name)->appendItem($item->getValue());
            }
          }
          else {
            foreach ($this->getVariations() as $variation) {

              /**
               * @var Product $variation
               */

              if (!$variation->isAvailable()) {
                continue;
              }

              foreach ($variation->get($field_name) as $variation_field_item) {
                $this->get($field_name)->appendItem($variation_field_item->getValue());
              }
            }
          }

        }
      }

    }

    foreach ($fields_for_inh_from_parent as $field_name) {
      //$this->addFieldItemsFromOtherProduct($field_name, $active_variation, $unshift);
    }

//     if (($default_variation = $this->getDefaultVariation()) && $default_variation->id() != $this->id()) {
//       foreach ($fields_for_inh_from_parent as $field_name) {
//         $this->addFieldItemsFromOtherProduct($field_name, $default_variation, TRUE);
//       }
//     }
//     else {
//       foreach ($this->get('variations')->referencedEntities() as $variation) {
//         foreach ($fields_for_inh_from_parent as $field_name) {
//           $this->addFieldItemsFromOtherProduct($field_name, $variation, FALSE);
//         }
//       }
//     }

  }

  protected function addFieldItemsFromOtherProduct($field_name, ProductInterface $source, $unshift = FALSE) {

    if (!$source->hasField($field_name)) return;

    if (!$source->get($field_name)->isEmpty() && ($items = $source->get($field_name))) {

      if ($unshift) {
        $exiting_items = clone $this->get($field_name);
        $this->set($field_name, $source->get($field_name)->getValue());

        foreach ($exiting_items as $item) {
          $this->get($field_name)->appendItem($item->getValue());
        }

      }
      else {
        foreach ($items as $item) {
          $item = $this->get($field_name)->appendItem($item->getValue());
          $item->source = $source;
        }
      }

    }
  }

  /**
   *
   */
  public function setDiffFieldsByVariationsValues() {
    if ($this->getLevel() > 1 && $parent = $this->getParent()) {
      if ($fields = $this->getBundleEntity()->getFieldsForInhFromParentForSave($this->getLevel())) {
        foreach ($fields as $field_name) {
          $value = $parent->get($field_name);
          $this->set($field_name, []);
          foreach ($value as $item) {
            $this->{$field_name}->set(count($this->{$field_name}), $item);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFieldsForView($view_mode = 'default') {
    if (!$this->hasField('variations')) return;


    if (!$this->variations->isEmpty()) {
      $this->updateFieldsValuesByChildsForView($view_mode);
    }

    if ($this->getParent()) {
      $this->updateFieldsValuesByParentsForView($view_mode);
    }


//     if ($this->getParent()) {
//       $this->updateFieldsValuesByParentsForView($view_mode);
//     }
//     else {
//       $this->updateFieldsValuesByChildsForView($view_mode);
//     }

  }

  /**
   * {@inheritdoc}
   */
  public function prepareForView($view_mode = 'full') {

    $this->getActiveVariation();

//     $this->updateActiveLangcode();
//     $product_type = $this->getBundleEntity();

//     if (!$product_type) {
//       throw new ProductTypeNotFoundException('Product type can\'t be loaded.');
//     }

    if (!empty($this->preparedForView[$view_mode])) return $this;

    $this->isAvailable(TRUE, TRUE, TRUE);

    $this->prepareFieldsForView($view_mode);

    $this->preparedForView[$view_mode] = TRUE;

    return $this;
  }

  public function getActiveVariation() {
    if (!$this->isVariationsSupport() || $this->variations->isEmpty()) {
      return $this;
    }

    if ($this->activeVariation) {
      return $this->activeVariation;
    }

    $this->isAvailable();

    $active_variation_resolver = $this->activeVariationResolver();

    $active_variation = $active_variation_resolver->resolve($this);

    if ($active_variation) {
      $this->activeVariation = $active_variation;
    }

    return $this->activeVariation ?: $this;
  }


  /**
   * {@inheritdoc}
   */
  public function validate() {
    $this->alterVariationFieldsCardinality();

    $violations = parent::validate();

    $this->restoreVariationFieldsCardinality();

    return $violations;
  }

  public function validateFor($group, $up_recursion = TRUE) {

    if (!empty($this->groupValidationViolations[$group])) {

      if ($up_recursion &&
        !$this->groupValidationViolations[$group]->getParentViolations()) {

          if ($parent = $this->getParent()) {
            $parent_violations = $parent->validateFor($group);
            $this->groupValidationViolations[$group]->setParentViolations($parent_violations);
          }
          else {
            $this->groupValidationViolations[$group] = $this->validateFor($group);
          }
      }
      return $this->groupValidationViolations[$group];
    }

//     $mem_cached_results = &drupal_static($group . ':' . $this->id(), NULL);

//     if ($mem_cached_results) {
//       $this->groupValidationViolations[$group] = $mem_cached_results;
//       return $this->groupValidationViolations[$group];
//     }

    $constraints_groups = $this->getEntityType()->get('constraints_by_groups');

    if (!$constraints_groups || !isset($constraints_groups[$group]))
      return new EntityConstraintViolationList($this, []);

    $constraints_by_sets = [
      'pre' => [],
      'main' => [],
      'post' => [],
    ];

    foreach ($constraints_groups[$group] as $constraint_name => $constraint_options) {

      $constraint_options += [
        'set' => 'main'
      ];

      $constraints_by_sets[$constraint_options['set']][$constraint_name] = $constraint_options;
    }

    $this->groupValidationViolations[$group] = new ProductConstraintViolationList($this);

    foreach ($constraints_by_sets as $set => $constraints_list) {
      foreach ($this->validateByConstraintsList($constraints_list, $up_recursion) as $violation) {
        $this->groupValidationViolations[$group]->add($violation);
      }
    }


    if ($up_recursion && ($parent = $this->getParent())) {
      $parent_violations = $parent->validateFor($group);
      $this->groupValidationViolations[$group]->setParentViolations($parent_violations);
    }

//     $mem_cached_results = $this->groupValidationViolations[$group];

    return $this->groupValidationViolations[$group];

  }

  protected function validateByConstraintsList(array $constraints_list, $up_recursion = TRUE) {

    $definition = DataDefinition::create('entity:bs_product');

    /**
     * @var \Drupal\Core\Validation\ConstraintManager $constraint_manager
     */
    $constraint_manager = \Drupal::service('validation.constraint');
    $constraint_plugin_definitions = [];


    foreach ($constraints_list as $constraint_name => $constraint_options) {
      $definition->addConstraint($constraint_name, $constraint_options);

      if ($constraint_plugin_definition = $constraint_manager->getDefinition($constraint_name)) {
        $constraint_plugin_definitions[$constraint_name] = $constraint_plugin_definition;
      }
    }

    $typed_data = \Drupal::typedDataManager()->create($definition, $this);

    $violations = $typed_data->validate();

    // Filter violations
    foreach ($violations as $offset => $violation) {
      /**
       * @var \Symfony\Component\Validator\ConstraintViolation $violation
       */

      $constraint_class = get_class($violation->getConstraint());
      $remove = TRUE;
      foreach ($constraint_plugin_definitions as $constraint_plugin_definition) {
        if ($constraint_plugin_definition['class'] == $constraint_class) {
          $remove = FALSE;
          break;
        }
      }

      if ($remove) {
        $violations->remove($offset);
      }
    }

    return new ProductConstraintViolationList($this, iterator_to_array($violations));
  }

  public function getLastGroupValidationViolations($group) {
    return !empty($this->groupValidationViolations[$group]) ?
           $this->groupValidationViolations[$group] :
           new EntityConstraintViolationList($this);
  }


  public function setGroupValidationVialations(array $violations) {
    $this->groupValidationViolations = $violations;
    return $this;
  }


  public function formAccessCallbackBarcode() {
    return FALSE;
  }

  public static function updateProductInCategoryWeight($pid, $cid, $direct_rel, $weight, $insert_if_not_updated = TRUE) {

    $db = \Drupal::database();

    $update_result = $db->update('bs_products_category_index')
    ->fields(['weight' => $weight])
    ->condition('pid', $pid)
    ->condition('cid', $cid)
    ->condition('direct_rel', $direct_rel)
    ->execute();

    if (!$update_result && $insert_if_not_updated) {
      $db->insert('bs_products_category_index')
      ->fields([
        'pid' => $pid,
        'cid' => $cid,
        'direct_rel' => $direct_rel,
        'weight' => $weight,
      ])
      ->execute();
    }
  }

  /**
   * @return \Drupal\bs_price\Resolver\ChainPriceResolverInterface
   */
  protected function priceResolver() {
    return \Drupal::service('price_resolver');
  }

  /**
   * @return \Drupal\bs_price\Resolver\ChainPriceResolverInterface
   */
  protected function activeVariationResolver() {
    return \Drupal::service('bs_product.chain_active_variation_resolver');
  }

//   /**
//    * @return \Drupal\Core\Cache\CacheBackendInterface
//    */
//   protected function productValidationsCache() {
//     return \Drupal::service('cache.product_validations');
//   }

  /**
   * @return EntityFieldManagerInterface
   */
  protected function entityFieldManager() {
    return \Drupal::service('entity_field.manager');
  }

  /**
   * @return \Drupal\Core\Routing\RouteMatchInterface
   */
  protected function routeMatch() {
    return \Drupal::routeMatch();
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Request
   */
  protected function request() {
    return \Drupal::request();
  }
}
