<?php

namespace Drupal\bs_product\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntity;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;


/**
 * Defines the product type entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_product_type",
 *   label = @Translation("Product type"),
 *   label_collection = @Translation("Product types"),
 *   label_singular = @Translation("product type"),
 *   label_plural = @Translation("product types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product type",
 *     plural = "@count product types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_product\ProductTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\bs_product\Form\ProductTypeForm",
 *       "add" = "Drupal\bs_product\Form\ProductTypeForm",
 *       "edit" = "Drupal\bs_product\Form\ProductTypeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_product\Routing\ProductTypeRouteProvider",
 *     },
 *   },
 *   config_prefix = "bs_product_type",
 *   admin_permission = "administer beeshop product type",
 *   bundle_of = "bs_product",
 *   hierarchy_inheritance_support = TRUE,
 *   default_hierarchy_mode = "strict",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "parent" = "parent",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *     "variationsSupport",
 *     "variationFieldsHierarchy",
 *     "weight",
 *     "parent",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/structure/products/product-types/add",
 *     "edit-form" = "/admin/beeshop/structure/products/product-types/{bs_product_type}/edit",
 *     "delete-form" = "/admin/beeshop/structure/products/product-types/{bs_product_type}/delete",
 *     "collection" = "/admin/beeshop/structure/products/product-types",
 *     "variations-structure-form" = "/admin/beeshop/structure/products/product-types/{bs_product_type}/variation-structure"
 *   }
 * )
 */
class ProductType extends BeeShopBundleWithDescriptionEntity implements ProductTypeInterface {

  /**
   * The support variations flag.
   *
   * @var boolean
   */
  protected $variationsSupport;

  protected $variationFieldsHierarchy;

  protected $variationFieldsByLevels;

  protected $variationsMaxLevel = NULL;

  protected $variationsDiffFields = NULL;

  /**
   * Local cache for field definitions.
   *
   * @var array
   */
  protected $fieldDefinitions;

  /**
   *
   */
  public function isVariationsSuport() {
    return $this->variationsSupport;
  }

  /**
   * {@inheritdoc}
   */
  public function setVariationsSupport($variations_support) {
    $this->variationsSupport = $variations_support;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationsFieldsList() {
    return self::getProductTypeVariationsFieldsList($this->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationLevelFields($level) {

    if (!$this->variationFieldsByLevels && $this->variationFieldsHierarchy) {
      foreach ($this->variationFieldsHierarchy as $field_name => $field_data) {
        if (isset($field_data['level']) && $field_data['level']) {
          $this->variationFieldsByLevels[$field_data['level']][$field_name] = $field_data;

          if ($field_def = $this->getFieldDefinition($field_name)) {
            if ($field_def instanceof ThirdPartySettingsInterface && ($related_fields = $field_def->getThirdPartySetting('bs_product', 'relatedFields', []))) {
              foreach ($related_fields as $related_field_name) {
                $this->variationFieldsByLevels[$field_data['level']][$related_field_name] = [
                  'related_by' => $field_name,
                ];
              }
            }
          }
        }
      }
    }

    return !empty($this->variationFieldsByLevels[$level]) ? $this->variationFieldsByLevels[$level] : [];

  }

  public function getVariationLevelField($level) {
    if ($level_variation_fields = $this->getVariationLevelFields($level)) {
      $keys = array_keys($level_variation_fields);
      return reset($keys);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationFieldsHierarchy() {
    return $this->variationFieldsHierarchy;
  }

  public function getMaxChildLevel() {

    if ($this->variationsMaxLevel !== NULL) {
      return $this->variationsMaxLevel;
    }

    $this->variationsMaxLevel = !$this->isVariationsSuport() ? 0 : 1;

    if (!$this->variationFieldsHierarchy) {
      return $this->variationsMaxLevel;
    }

    foreach ($this->variationFieldsHierarchy as $field_data) {
      if ($field_data['level'] > $this->variationsMaxLevel) $this->variationsMaxLevel = $field_data['level'];
    }

    return $this->variationsMaxLevel;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationFieldHierarchyData($field_name) {
    return !empty($this->variationFieldsHierarchy[$field_name]) ?
           $this->variationFieldsHierarchy[$field_name] :
           FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsForInhFromParentForView() {
    $fields = [];
    if ($not_base_fields_defs = self::getNotBaseFieldDefinitions($this->id())) {
      foreach ($not_base_fields_defs as $field_name => $field_def) {
        if (!empty($this->variationFieldsHierarchy[$field_name])) continue;
        $fields[] = $field_name;
      }
    }
    return $fields;
  }

  public function getVariationsDiffFields() {

    if ($this->variationsDiffFields !== NULL) return $this->variationsDiffFields;

    $diff_fields = [];

    if ($variations_fields = $this->getVariationFieldsHierarchy()) {
      foreach ($variations_fields as $field_name => $field_h_data) {
        $diff_fields[$field_name] = $field_name;
        if ($related_fields = $this->getFieldDefinition($field_name)->getThirdPartySetting('bs_product', 'relatedFields')) {
          foreach ($related_fields as $related_field_name) {
            $diff_fields[$related_field_name] = $related_field_name;
          }
        }
      }
    }

    $diff_fields['price'] = 'price';
    $diff_fields['sku'] = 'sku';

    $this->variationsDiffFields = $diff_fields;

    return $this->variationsDiffFields;
  }

  public function getFieldsForInhFromChildsForView() {

    $diff_fields = [];

    if ($variations_fields = $this->getVariationFieldsHierarchy()) {
      foreach ($variations_fields as $field_name => $field_h_data) {
        $diff_fields[$field_name] = $field_name;
        if ($related_fields = $this->getFieldDefinition($field_name)->getThirdPartySetting('bs_product', 'relatedFields')) {
          foreach ($related_fields as $related_field_name) {
            $diff_fields[$related_field_name] = $related_field_name;
          }
        }
      }
    }

    return $diff_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition($name) {
    if (!isset($this->fieldDefinitions)) {
      $this->getFieldDefinitions();
    }
    if (isset($this->fieldDefinitions[$name])) {
      return $this->fieldDefinitions[$name];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinitions() {

    if (!isset($this->fieldDefinitions)) {
      $this->fieldDefinitions = $this->entityFieldManager()->getFieldDefinitions($this->getEntityType()->get('bundle_of'), $this->id());
    }
    return $this->fieldDefinitions;
  }

  /**
   *
   */
  public function getFieldsForInhFromChildsForSave($product_level = NULL) {
    $fields = [];

    if ($not_base_fields_defs = self::getNotBaseFieldDefinitions($this->id())) {
      foreach ($not_base_fields_defs as $field_name => $field_def) {
        if (!empty($this->variationFieldsHierarchy[$field_name]) &&
          $this->variationFieldsHierarchy[$field_name]['level'] > $product_level) {

            $fields[] = $field_name;
          }
      }
    }

    return $fields;
  }

  public function getFieldsForInhFromParentForSave($product_level = NULL) {
    $fields = [];

    if (!$product_level) return $fields;

    if ($not_base_fields_defs = self::getNotBaseFieldDefinitions($this->id())) {
      foreach ($not_base_fields_defs as $field_name => $field_def) {
        if (!empty($this->variationFieldsHierarchy[$field_name]) &&
            $this->variationFieldsHierarchy[$field_name]['level'] < $product_level) {

              $fields[] = $field_name;
        }
      }
    }
    return $fields;
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    $variations_display_settings = [];

    if ($this->variationFieldsHierarchy) {
      foreach ($this->variationFieldsHierarchy as $field_name => $field_data) {
        if (!empty($field_data['display_on_category_page']) && isset($field_data['level'])) {
          $variations_display_settings['level_for_category'] = $field_data['level'];
        }
        if (!empty($field_data['have_standalone_page']) && isset($field_data['level'])) {
          $variations_display_settings['level_for_page'] = $field_data['level'];
        }
      }
    }

    $db = \Drupal::database();

    $db->delete('bs_product_variations_display')
    ->condition('ptype', $this->id())
    ->execute();

    if ($variations_display_settings) {
      $variations_display_settings['ptype'] = $this->id();

      $db->insert('bs_product_variations_display')
      ->fields($variations_display_settings)
      ->execute();
    }
    else {

      $variations_display_settings = [
        'ptype' => $this->id(),
        'level_for_category' => 0,
        'level_for_page' => 0
      ];

      $db->insert('bs_product_variations_display')
      ->fields($variations_display_settings)
      ->execute();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function setVariationFieldsHierarchy($v_structure) {
    $this->variationFieldsHierarchy = $v_structure;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProductTypeVariationsFieldsList($type_name) {
    $fields = [];

    if ($field_definitions = self::getNotBaseFieldDefinitions($type_name)) {
      foreach ($field_definitions as $field_name => $field_definition) {
        if ($field_definition->getThirdPartySetting('bs_product', 'isVariationCharacteristicField', FALSE)) {
          $fields[$field_name] = $field_definition;
        }
      }
    }

    return $fields;
  }

  /**
   *
   */
  public static function getFieldsAvailableForVariationRelation($type_name, $current_field_name) {
    $result = [];
    if ($fields = self::getNotBaseFieldDefinitions($type_name, [$current_field_name])) {
      foreach ($fields as $field_name => $field_definition) {
        if ($field_definition->getThirdPartySetting('bs_product', 'isVariationCharacteristicField', FALSE))
          continue;

          $result[$field_name] = $field_definition;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function getNotBaseFieldDefinitions($type_name, $excluded_fields = []) {
    $not_base_field_definitions = [];
    $entityManager = \Drupal::service('entity_field.manager');

    if ($field_definitions = $entityManager->getFieldDefinitions('bs_product', $type_name)) {
      foreach ($field_definitions as $field_name => $field_definition) {
        if (!$field_definition->getFieldStorageDefinition()->isBaseField() && !in_array($field_name, $excluded_fields)) {
          $not_base_field_definitions[$field_name] = $field_definition;
        }
      }
    }

    return $not_base_field_definitions;
  }

  /**
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected function entityFieldManager() {
    return \Drupal::service('entity_field.manager');
  }
}
