<?php
namespace Drupal\bs_product\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class ProductRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($clone_route = $this->getCloneFormRute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.clone_form", $clone_route);
    }

    return $collection;
  }

  protected function getCloneFormRute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('clone-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('clone-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'clone';
      $route
      ->setDefaults([
        '_entity_form' => "{$entity_type_id}.{$operation}",
        '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle'
          ])
          ->setRequirement('_entity_access', "{$entity_type_id}.update")
          ->setOption('parameters', [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          ])
      ->setOption('_admin_route', TRUE);

          // Entity types with serial IDs can specify this in their route
          // requirements, improving the matching process.
          if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
            $route->setRequirement($entity_type_id, '\d+');
          }
      return $route;
    }
  }

  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCanonicalRoute($entity_type);
    $route->setDefaults([
      '_controller' => 'Drupal\bs_product\Controller\ProductViewController::view',
      '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
    ]);
    return $route ? $route : NULL;
  }



}

