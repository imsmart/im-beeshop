<?php
namespace Drupal\bs_product\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityTypeInterface;

class ProductTypeRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getVariationStructureFormRoute(EntityTypeInterface $entity_type) {

    if ($entity_type->hasLinkTemplate('variations-structure-form')) {
      $route = new Route($entity_type->getLinkTemplate('variations-structure-form'));
      $route->setDefault('_form', 'Drupal\bs_product\Form\ProductTypeVariationStructructureOverviewForm');
      $route->setDefault('_title', 'Product type variation structure');
      $route->setOption('parameters', [
        'bs_product_type' => [
          'type' => 'entity:bs_product_type',
        ]
      ]);
      $route->setOption('_admin_route', TRUE);
      $route->setRequirement('_permission', 'administer beeshop product type');

      return $route;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($add_form_route = $this->getVariationStructureFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.variations_structure_form", $add_form_route);
    }

    return $collection;
  }

}

