<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\views\EntityViewsData;

class ProductsCategoryViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

//     $data['bs_products_category_field_data']['parent'] = [
//       'argument' => [
//         'title' => t('Parent category ID'),
//         'id' => 'bs_products_category_arg_parent',
//       ]
//     ];

    $data['bs_products_category']['parent'] = [
      'title' => $this->t('Category parent'),
      'field' => [
        'id' => 'field',
        "entity field" => 'parent'
      ],
//       'filter' => [
//         'id' => 'in_operator',
//         'skip base' => 'bs_products_category_field_data',
//       ]
    ];

    $data['bs_products_category']['hasSubCats'] = [
      'title' => $this->t('Category has sub categories'),
      'field' => [
        'id' => 'field',
        "entity field" => 'hasSubCats'
      ],
    ];

    //
    $data['bs_products_category_index']['table']['group']  = $this->t('Products category (index)');

    $data['bs_products_category_index']['table']['join'] = [
      'bs_products_category_field_data' => [
        // links directly to taxonomy_term_field_data via tid
        'left_field' => 'cid',
        'field' => 'cid',
      ],
      'bs_product_field_data' => [
        // links directly to node via nid
        'left_field' => 'pid',
        'field' => 'pid',
      ],
      'bs_products_category_hierarchy' => [
        'left_field' => 'cid',
        'field' => 'cid',
      ],
    ];

    $data['bs_products_category_index']['pid'] = [
      'title' => $this->t('Product, related to category'),
      'help' => $this->t('Relate all products tagged with a category.'),
      'relationship' => [
        'id' => 'standard',
        'base' => 'bs_product',
        'base field' => 'pid',
        'label' => $this->t('Product'),
        'skip base' => 'bs_product',
      ],
    ];

//     // @todo This stuff needs to move to a node field since really it's all
//     //   about nodes.
//     $data['bs_products_category_index']['cid'] = [
//       'group' => $this->t('Products'),
//       'title' => $this->t('Related to category with ID'),
//       'help' => $this->t('Display products if it has been related to category.'),
//       'argument' => [
//         'id' => 'taxonomy_index_tid',
//         'name table' => 'taxonomy_term_field_data',
//         'name field' => 'name',
//         'empty field name' => $this->t('Uncategorized'),
//         'numeric' => TRUE,
//         'skip base' => 'taxonomy_term_field_data',
//       ],
//       'filter' => [
//         'title' => $this->t('Has taxonomy term'),
//         'id' => 'taxonomy_index_tid',
//         'hierarchy table' => 'taxonomy_term_hierarchy',
//         'numeric' => TRUE,
//         'skip base' => 'taxonomy_term_field_data',
//         'allow empty' => TRUE,
//       ],
//     ];

//     $data['taxonomy_index']['status'] = [
//       'title' => $this->t('Publish status'),
//       'help' => $this->t('Whether or not the content related to a term is published.'),
//       'filter' => [
//         'id' => 'boolean',
//         'label' => $this->t('Published status'),
//         'type' => 'yes-no',
//       ],
//     ];

//     $data['taxonomy_index']['sticky'] = [
//       'title' => $this->t('Sticky status'),
//       'help' => $this->t('Whether or not the content related to a term is sticky.'),
//       'filter' => [
//         'id' => 'boolean',
//         'label' => $this->t('Sticky status'),
//         'type' => 'yes-no',
//       ],
//       'sort' => [
//         'id' => 'standard',
//         'help' => $this->t('Whether or not the content related to a term is sticky. To list sticky content first, set this to descending.'),
//       ],
//     ];

//     $data['taxonomy_index']['created'] = [
//       'title' => $this->t('Post date'),
//       'help' => $this->t('The date the content related to a term was posted.'),
//       'sort' => [
//         'id' => 'date'
//       ],
//       'filter' => [
//         'id' => 'date',
//       ],
//     ];
    //

    $data['bs_products_category_hierarchy']['table']['group']  = $this->t('Products category');
    $data['bs_products_category_hierarchy']['table']['provider']  = 'bs_product';

    $data['bs_products_category_hierarchy']['table']['join'] = [
      'bs_products_category_hierarchy' => [
        // Link to self through left.parent = right.tid (going down in depth).
        'left_field' => 'cid',
        'field' => 'parent',
      ],
      'bs_products_category_field_data' => [
        // Link directly to taxonomy_term_field_data via tid.
        'left_field' => 'cid',
        'field' => 'cid',
      ],
    ];

    $data['bs_products_category_hierarchy']['parent'] = [
      'title' => $this->t('Parent category'),
      'help' => $this->t('The parent category of the category.'),
      'relationship' => [
        'base' => 'bs_products_category_field_data',
        'field' => 'parent',
        'label' => $this->t('Parent'),
        'id' => 'standard',
      ],
      'filter' => [
        'help' => $this->t('Filter the results of "Taxonomy: Term" by the parent pid.'),
        'id' => 'numeric',
      ],
      'argument' => [
        'help' => $this->t('The parent category of the category.'),
        'id' => 'taxonomy',
      ],
    ];

    return $data;
  }

  /**
   * Processes the views data for an entity reference field.
   *
   * @param string $table
   *   The table the language field is added to.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $views_field
   *   The views field data.
   * @param string $field_column_name
   *   The field column being processed.
   */
  protected function processViewsDataForBsPrice($table, FieldDefinitionInterface $field_definition, array &$views_field, $field_column_name) {
    switch ($field_column_name) {
      case 'value':
        $views_field['field']['id'] = 'bs_price';
        break;
    }
  }

}

