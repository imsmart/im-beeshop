<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\EntityConstraintViolationList;

class ProductConstraintViolationList extends EntityConstraintViolationList {

  /**
   * @var ProductConstraintViolationList
   */
  protected $parentViolations;

  /**
   * {@inheritdoc}
   */
  public function count() {

    return isset($this->parentViolations) ?
      parent::count() + $this->parentViolations->count() :
      parent::count();
  }

  public function getParentViolations() {
    return $this->parentViolations;
  }

  public function setParentViolations(ProductConstraintViolationList $parent_violations) {
    $this->parentViolations = $parent_violations;
  }

}

