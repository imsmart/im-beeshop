<?php

namespace Drupal\bs_product\Resolver;

use Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ByRequestProductActiveVariationResolver extends ProductActiveVariationResolverBase implements ProductActiveVariationResolverInterface {

  /**
   * @var RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var Request
   */
  protected $request;

  protected $variationIdsInRequest;

  protected $resolvedVariation;

  public function __construct(RequestStack $request_stack, RouteMatchInterface $route_match) {
    $this->request = $request_stack->getCurrentRequest();
    $this->routeMatch = $route_match;
  }

  /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::applies()
  */
  public function applies(ProductInterface $entity) {

    if (($product_by_route = $this->routeMatch->getParameter('bs_product')) &&
      ($variation = $this->request->get('variation'))) {

        $parents = $entity->getParentsIds();

        if ($entity->id() == $product_by_route->id() || in_array($product_by_route->id(), $parents)) {
          $this->variationIdsInRequest = explode('/', $variation);

          return TRUE;
        }


    }

    return FALSE;
  }

  /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::resolve()
  */
  public function resolve(ProductInterface $entity) {
  // TODO: Auto-generated method stub

//     $variations = [];

//     foreach ($entity->getVariations(TRUE) as $variation) {
//       if (in_array($variation->id(), $this->variationIdsInRequest)) {
//         $variations[$variation->id()] = $variation;
//       }
//     }

//     foreach ($this->variationIdsInRequest as $id) {
//       if (isset($variations[$id])) {
//         $this->resolvedVariation = $variations[$id];
//       }
//     }


    $matches = [];

    $this->resolvedVariation = $entity->getVariationByIdsPath($this->variationIdsInRequest, $matches);

    $this->resolvedVariation = $this->resolvedVariation ?: end($matches);

    return $this->resolvedVariation;
  }


  public function breakResolving() {
    return ($this->resolvedVariation && $this->resolvedVariation->isFinalPoint()) ? TRUE : FALSE;
  }


}

