<?php

namespace Drupal\bs_product\Resolver;

use Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface;
use Drupal\bs_product\Entity\ProductInterface;

class DefaultProductActiveVariationResolver extends ProductActiveVariationResolverBase implements ProductActiveVariationResolverInterface {

 /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::applies()
  */
 public function applies(ProductInterface $entity) {
  // TODO: Auto-generated method stub
  return TRUE;
 }

 /**
  * {@inheritDoc}
  * @see \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface::resolve()
  */
 public function resolve(ProductInterface $entity) {
  // TODO: Auto-generated method stub

//    $first_processed = [];

   $min_variation_price = NULL;
   $fist_final = NULL;
   $fist_av_final = NULL;

   foreach ($entity->getVariations(TRUE) as $variation) {

     if (!$variation->isFinalPoint()) {
       continue;
     }


     $fist_final = $fist_final ?: $variation;

     if ($variation->isAvailable() && ($variation_price = $variation->getActualPrice())) {

       $fist_av_final = $fist_av_final ?: $variation;

       $min_variation_price = $min_variation_price ?
                              ($min_variation_price->getPriceValue() > $variation_price->getPriceValue() ? $variation_price : $min_variation_price) :
                              $variation_price;

     }

   }

   return  $min_variation_price ? $min_variation_price->getEntity() :
           ($fist_av_final ?: $fist_final);

 }


}

