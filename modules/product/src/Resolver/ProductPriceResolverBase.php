<?php
namespace Drupal\bs_product\Resolver;

use Drupal\bs_price\Resolver\PriceResolverInterface;
use Drupal\beeshop\PurchasableEntityInterface;
use Drupal\bs_product\Entity\ProductInterface;

abstract class ProductPriceResolverBase implements PriceResolverInterface {

  protected $priceResolvingContexts = [];

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::applies()
   */
  public function applies(PurchasableEntityInterface $entity) {
    // TODO: Auto-generated method stub
    if ($entity instanceof ProductInterface) {

      if (!$this->priceResolvingContexts) {
        return TRUE;
      }

      return in_array($entity->getPriceResolvingContext(), $this->priceResolvingContexts);
    }

    return FALSE;
  }

}

