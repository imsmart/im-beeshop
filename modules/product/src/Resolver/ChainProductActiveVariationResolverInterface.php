<?php

namespace Drupal\bs_product\Resolver;

use Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface;

interface ChainProductActiveVariationResolverInterface extends ProductActiveVariationResolverInterface {

  /**
   * Adds another active variation resolver.
   *
   * @param Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface $resolver
   *   The active variation resolver to add.
   * @param int $priority
   *   Priority of the resolver.
   */
  public function addResolver(ProductActiveVariationResolverInterface $resolver, $priority);

}

