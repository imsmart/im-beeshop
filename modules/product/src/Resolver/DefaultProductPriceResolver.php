<?php
namespace Drupal\bs_product\Resolver;

use Drupal\bs_price\Resolver\PriceResolverInterface;
use Drupal\beeshop\PurchasableEntityInterface;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_shop\Entity\Shop;

class DefaultProductPriceResolver extends ProductPriceResolverBase implements PriceResolverInterface {

 /**
  * {@inheritDoc}
  * @see \Drupal\bs_price\Resolver\PriceResolverInterface::resolve()
  */
 public function resolve(PurchasableEntityInterface $entity, $prev_resolved) {


  // TODO: Auto-generated method stub
   if ($product_price = $prev_resolved ?: $entity->getPrice()) {
    $current_shop_id = \Drupal::service('bs_shop.default_shop_resolver')->resolve()->id();

    $product_price->filterItemsByShopId($current_shop_id);

    $price_type_tree = \Drupal::entityTypeManager()->getStorage('bs_price_type')->loadTree($current_shop_id);

    // By default we take first price type as actual price type
    $base_price_type = reset($price_type_tree);

    return $product_price->getItemByType($base_price_type);
  }

 }

}

