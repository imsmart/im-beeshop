<?php

namespace Drupal\bs_product\Resolver;

use Drupal\bs_product\Resolver\ProductActiveVariationResolverInterface;
use Drupal\bs_product\Entity\ProductInterface;

class ChainProductActiveVariationResolver extends ProductActiveVariationResolverBase implements ChainProductActiveVariationResolverInterface {

  /**
   * Holds arrays of price resolvers, keyed by priority.
   *
   * @var \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface[]|null
   */
  protected $resolvers = [];

  /**
   * Holds the array of price resolvers sorted by priority.
   *
   * Set to NULL if the array needs to be re-calculated.
   *
   * @var \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface[]|null
   */
  protected $sortedResolvers;

  /**
   * {@inheritdoc}
   */
  public function addResolver(ProductActiveVariationResolverInterface $resolver, $priority) {
    $this->resolvers[$priority][] = $resolver;
    // Force the builders to be re-sorted.
    $this->sortedResolvers = NULL;
  }



  /**
   * {@inheritdoc}
   */
  public function resolve(ProductInterface $entity) {

    foreach ($this->getSortedResolvers() as $resolver) {

      if (!$resolver->applies($entity)) {
        // The resolverer does not apply, so we continue with the other resolversers.
        continue;
      }

      if ($resolved_variation = $resolver->resolve($entity)) {
        if ($resolver->breakResolving()) {
          return $resolved_variation;
        }
        else {
          $entity = $resolved_variation;
        }
      }

    }

  }

  /**
   * Returns the sorted array of resolvers.
   *
   * @return \Drupal\bs_prduct\Resolver\ProductActiveVariationResolverInterface[]
   *   An array of price resolvers objects.
   */
  protected function getSortedResolvers() {
    if (!isset($this->sortedResolvers)) {
      // Sort the resolvers according to priority.
      krsort($this->resolvers);
      // Merge nested resolvers from $this->resolvers into $this->sortedResolvers.
      $this->sortedResolvers = [];
      foreach ($this->resolvers as $resolvers) {
        $this->sortedResolvers = array_merge($this->sortedResolvers, $resolvers);
      }
    }
    return $this->sortedResolvers;
  }
}

