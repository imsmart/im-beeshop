<?php
namespace Drupal\bs_product\Resolver;

use Drupal\bs_product\Entity\ProductInterface;

abstract class ProductActiveVariationResolverBase implements ProductActiveVariationResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(ProductInterface $entity) {
    return TRUE;
  }

  public function breakResolving() {
    return TRUE;
  }

}

