<?php

namespace Drupal\bs_product;

use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\hierarchical_entity\EntityTreeProccessorTrait;

/**
 * Defines a Controller class for taxonomy terms.
 */
class ProductsCategoryStorage extends SqlContentEntityStorage implements ProductsCategoryStorageInterface {

  use EntityTreeProccessorTrait;

  protected $loadedEntities = [];

  protected $treeChildren = [];

  protected $treeParents = [];

  protected $treeCategories = [];

  protected $parentsAll = [];

  protected $children = [];

  /**
   *
   */
  const HIERARCHY_TABLE = 'bs_products_category_hierarchy';

  /**
   * {@inheritdoc}
   *
   * @param array $values
   *   An array of values to set, keyed by property name. A value for the
   *   vocabulary ID ('vid') is required.
   */
  public function create(array $values = []) {
    // Save new categories with no parents by default.
    if (empty($values['parent'])) {
      $values['parent'] = [0];
    }
    $entity = parent::create($values);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCategoryHierarchy($cids) {
    $this->database->delete('bs_products_category_hierarchy')
      ->condition('cid', $cids, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function setNewCategoryHierarchy(EntityInterface $category) {
    $query = $this->database->insert('bs_products_category_hierarchy')
    ->fields(['cid', 'parent']);

    foreach ($category->parent as $parent) {

      $query->values([
        'cid' => $category->id(),
        'parent' => (int) $parent->target_id,
      ]);
    }
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function updateCategoryHierarchy(EntityInterface $category) {
    if (!$category->isNew()) {
      $this->deleteCategoryHierarchy([$category->id()]);
    }
    $this->setNewCategoryHierarchy($category);
  }

  /**
   * {@inheritdoc}
   */
  public function loadParents($tid) {
    if (!isset($this->parents[$tid])) {
      $parents = [];
      $query = $this->database->select('taxonomy_term_field_data', 't');
      $query->join('taxonomy_term_hierarchy', 'h', 'h.parent = t.tid');
      $query->addField('t', 'tid');
      $query->condition('h.tid', $tid);
      $query->condition('t.default_langcode', 1);
      $query->addTag('taxonomy_term_access');
      $query->orderBy('t.weight');
      $query->orderBy('t.name');
      if ($ids = $query->execute()->fetchCol()) {
        $parents = $this->loadMultiple($ids);
      }
      $this->parents[$tid] = $parents;
    }
    return $this->parents[$tid];
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllParents($cid, $levels = 10, $load_entities = FALSE) {
    $cache_key = implode(':', func_get_args());

    if (!isset($this->parentsAll[$cache_key])) {
      $query = db_select(self::HIERARCHY_TABLE, 'h')
        ->fields('h', array('cid', 'parent'));

      $query->join('bs_products_category_field_data', 'cf', 'h.cid = cf.cid');
      $query->orderBy('cf.weight');

      $where = db_or();
      $last = "h";
      $range = $levels > 1 ? range(1, $levels) : array(1);

      foreach ($range as $count) {
        $query->leftJoin(self::HIERARCHY_TABLE, "h$count", "$last.cid = h$count.parent");
        $where->condition("h$count.cid", $cid);
        $last = "h$count";
      }

      $query->condition($where);
      $result = $query->execute()->fetchAllAssoc('cid');

      if ($load_entities && $result) {
        $category_entities = $this->loadMultiple(array_keys($result));

        foreach ($result as $cid => $parent_data) {
          if (isset($category_entities[$cid])) {
            $result[$cid] = $category_entities[$cid];
          }
        }
      }

      $this->parentsAll[$cache_key] = $result;
    }

    return $this->parentsAll[$cache_key];
//     if (!isset($this->parentsAll[$tid])) {
//       $parents = [];
//       if ($term = $this->load($tid)) {
//         $parents[$term->id()] = $term;
//         $terms_to_search[] = $term->id();

//         while ($tid = array_shift($terms_to_search)) {
//           if ($new_parents = $this->loadParents($tid)) {
//             foreach ($new_parents as $new_parent) {
//               if (!isset($parents[$new_parent->id()])) {
//                 $parents[$new_parent->id()] = $new_parent;
//                 $terms_to_search[] = $new_parent->id();
//               }
//             }
//           }
//         }
//       }

//       $this->parentsAll[$tid] = $parents;
//     }
  }

  /**
   * {@inheritdoc}
   */
  public function loadChildren($cid, $add_access_tag = TRUE) {

    //bs_products_category_hierarchy

    if (isset($this->children[$cid])) {
      return $this->children[$cid];
    }

    $query = $this->database->select('bs_products_category_field_data', 'c');
    $query->join('bs_products_category_hierarchy', 'h', 'h.cid = c.cid');
    $query->addField('c', 'cid');
    $query->condition('h.parent', $cid);

    if ($add_access_tag) {
      $query->addTag('products_category_access');
    }

    $query->orderBy('c.weight');
    $query->orderBy('c.name');

    $childs = [];

    if ($ids = $query->execute()->fetchCol()) {
      $childs = $this->loadMultiple($ids);
    }

    $this->children[$cid] = $childs;

    return $this->children[$cid];
//     if (!isset($this->children[$tid])) {
//       $children = [];
//
//
//
//       if ($vid) {
//         $query->condition('t.vid', $vid);
//       }
//       $query->condition('t.default_langcode', 1);
//       $query->addTag('taxonomy_term_access');


//     }
//     return $this->children[$tid];
  }

  public function loadTreeAlt($sid = 0, $parent = 0, $max_depth = NULL) {

//     $time_start = microtime(true);

    $entities = $this->loadByProperties([
      'sid' => $sid,
    ]);

    $tree = [];

    foreach ($entities as $id => $entity) {
      if ($parent = $entity->getParent()) {
        $parent->addChild($entity);
      }
      else {
        $tree[] = $entity;
      }
    }

    return $tree;
//     drupal_set_message($execution_time);

//     dpm($tree);
  }


  public function getAllChilds($id) {
    $select = $this->database->select('bs_products_category_hierarchy', 'ch');
    $select->addField('ch', 'cid');
    $or_cond = new Condition('OR');
    $where = $or_cond->condition('ch.cid', $id, '=');
    $last = "ch";

    foreach (range(1, 10) as $count) {
      $select->leftJoin('bs_products_category_hierarchy', "ch$count", "$last.parent = ch$count.cid");
      $where->condition("ch$count.cid", $id, '=');
      $last = "ch$count";
    }

    $select->condition($where);
    $select->condition('ch.cid', $id, '<>');
    return $select->execute()->fetchCol();
  }

  public function getSubCatsIds($ids) {
    $select = $this->database->select('bs_products_category_hierarchy', 'ch');

    $select->leftJoin('bs_products_category_field_data', 'cfd', 'ch.cid = cfd.cid');

    $select->addField('ch', 'cid');
    $select->addField('ch', 'parent');
    $select->condition('ch.parent', $ids, 'IN')
    ->orderBy('cfd.name');

    $result = [];

    foreach ($select->execute()->fetchAllAssoc('cid') as $row) {
      if (!isset($result[$row->parent])) {
        $result[$row->parent] = [];
      }

      $result[$row->parent][] = $row->cid;
    }

    return $result;
  }

  public function loadSubCatsInfo($ids) {
    $select = $this->database->select('bs_products_category_hierarchy', 'ch');
    $select->join('bs_products_category_field_data', 'cd', 'ch.cid = cd.cid');
    $select->addField('cd', 'weight');
    $select->fields('ch')
      ->condition('ch.parent', $ids, 'IN')
      ->orderBy('cd.weight');

    $result = [];

    foreach ($select->execute()->fetchAllAssoc('cid') as $row) {
      if (!isset($result[$row->parent])) {
        $result[$row->parent] = [];
      }

      $result[$row->parent][] = $row;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTree($sid = 0, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {

    $time_start = microtime(true);

    $cache_key = implode(':', func_get_args());

    if (!isset($this->trees[$cache_key])) {
      // We cache trees, so it's not CPU-intensive to call on a category and its
      // children, too.
      if (!isset($this->treeChildren[$sid])) {
        $query = $this->database->select('bs_products_category_field_data', 'c');
        $query->join('bs_products_category_hierarchy', 'h', 'h.cid = c.cid');
        $result = $query
        ->addTag('bs_products_category_access')
        ->fields('c')
        ->fields('h', ['parent'])
        ->condition('c.sid', $sid)
        ->condition('c.default_langcode', 1)
        ->orderBy('c.weight')
        ->orderBy('c.name')
        ->execute();

        foreach ($result as $cat_data) {
          $this->treeChildren[$sid][$cat_data->parent][] = $cat_data->cid;
          $this->treeParents[$sid][$cat_data->cid][] = $cat_data->parent;
          $this->treeCategories[$sid][$cat_data->cid] = $cat_data;
        }
      }

      $category_entities = [];
      if ($load_entities) {
        $category_entities = $this->loadMultiple(array_keys($this->treeCategories[$sid]));
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$sid]) : $max_depth;
      $tree = [];

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = [];
      $process_parents[] = $parent;

      // Loops over the parent terms and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents determines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$sid][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$sid][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $category = $load_entities ? $category_entities[$child] : $this->treeCategories[$sid][$child];
            if (isset($this->treeParents[$sid][$load_entities ? $category->id() : $category->cid])) {
              // Clone the term so that the depth attribute remains correct
              // in the event of multiple parents.
              $category = clone $category;
            }
            $category->depth = $depth;
            unset($category->parent);
            $cid = $load_entities ? $category->id() : $category->cid;
            $category->parents = $this->treeParents[$sid][$cid];
            $tree[] = $category;
            if (!empty($this->treeChildren[$sid][$cid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current term as parent for the next iteration.
              $process_parents[] = $cid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$sid][$cid]);
              // Move pointer so that we get the correct term the next time.
              next($this->treeChildren[$sid][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$sid][$parent]));

          if (!$has_children) {
            // We processed all terms in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$sid][$parent]);
          }
        }
      }
      $this->trees[$cache_key] = $tree;
    }

    $time_end = microtime(true);
    $execution_time = ($time_end - $time_start);

    drupal_set_message($execution_time);

    return $this->trees[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function productsCount($sid) {
//     $query = $this->database->select('taxonomy_index', 'ti');
//     $query->addExpression('COUNT(DISTINCT ti.nid)');
//     $query->leftJoin('taxonomy_term_data', 'td', 'ti.tid = td.tid');
//     $query->condition('td.vid', $vid);
//     $query->addTag('vocabulary_node_count');
//     return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function resetWeights($vid) {
//     $this->database->update('taxonomy_term_field_data')
//       ->fields(['weight' => 0])
//       ->condition('vid', $vid)
//       ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTerms(array $nids, array $vocabs = [], $langcode = NULL) {
    $query = db_select('taxonomy_term_field_data', 'td');
    $query->innerJoin('taxonomy_index', 'tn', 'td.tid = tn.tid');
    $query->fields('td', ['tid']);
    $query->addField('tn', 'nid', 'node_nid');
    $query->orderby('td.weight');
    $query->orderby('td.name');
    $query->condition('tn.nid', $nids, 'IN');
    $query->addTag('taxonomy_term_access');
    if (!empty($vocabs)) {
      $query->condition('td.vid', $vocabs, 'IN');
    }
    if (!empty($langcode)) {
      $query->condition('td.langcode', $langcode);
    }

    $results = [];
    $all_tids = [];
    foreach ($query->execute() as $term_record) {
      $results[$term_record->node_nid][] = $term_record->tid;
      $all_tids[] = $term_record->tid;
    }

    $all_terms = $this->loadMultiple($all_tids);
    $terms = [];
    foreach ($results as $nid => $tids) {
      foreach ($tids as $tid) {
        $terms[$nid][$tid] = $all_terms[$tid];
      }
    }
    return $terms;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoriesTreeAsOtionsForSelect($sid, $exclude = []) {
    $options = [];

    $full_tree = $this->loadTree($sid);
    foreach ($full_tree as $item) {
      if (!in_array($item->cid, $exclude)) {
        $options[$item->cid] = str_repeat('-', $item->depth) . $item->name;
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getProductsCategories($ids, $direct_only = TRUE) {
    $ids = !is_array($ids) ? [$ids] : $ids;
    $query = db_select('bs_products_category_index', 'pci');
    $query->fields('pci')
    ->condition('pci.pid', $ids, 'IN');
    $query->orderBy('delta');

    if ($direct_only) {
      $query->condition('pci.direct_rel', 1);
    }

    $categries = [];

    foreach ($query->execute() as $record) {
      $categries[$record->pid][] = $record->cid;
    }

    return $categries;
  }

  public function getCategoryProductsIds($cid, $only_direct_reference = TRUE, $depth = 10) {
    $query = db_select('bs_products_category_index', 'pci');
    $query
      ->distinct()
      ->fields('pci', ['pid']);

    if ($only_direct_reference) {
      $query->condition('pci.direct_rel', 1);
    }

    $operator = 'IN';
    $db = \Drupal::database();
    $subquery = $db->select('bs_products_category_index', 'ci');
    $subquery->addField('ci', 'pid');
    $subquery->condition('ci.direct_rel', 1);
    $or_cond = new Condition('OR');
    $where = $or_cond->condition('ci.cid', [$cid], $operator);
    $last = "ci";

    $subquery->leftJoin('bs_products_category_hierarchy', 'ch', "ch.cid = ci.cid");
    $last = "ch";
    foreach (range(1, $depth) as $count) {
      $subquery->leftJoin('bs_products_category_hierarchy', "ch$count", "$last.parent = ch$count.cid");
      $where->condition("ch$count.cid", [$cid], $operator);
      $last = "ch$count";
    }

    $subquery->condition($where);

    $query->condition('pci.pid', $subquery, 'IN');

// dpm((string) $query);

    $result = [];

    foreach ($query->execute() as $record) {
      $result[] = $record->pid;
    }

    return $result;
  }


  /**
   * {@inheritdoc}
   */
  public function updateProductCategories(ProductInterface $product) {

    debug('updateProductCategories');

    $prev_cids = !empty($product->original) ?
                 $product->original->getCategoryIds() :
                 [];

    $current_cids = $product->getCategoryIds();

    // Delete all relations to not current categories
    // Not direct links live at this step
    $query = db_delete('bs_products_category_index')
      ->condition('pid', $product->id())
      ->condition('direct_rel', 1);
    if ($current_cids) {
      $query->condition('cid', $current_cids, 'NOT IN');
    }
    $query->execute();

    $delta = 0;
    $weight = 10000;

    foreach ($current_cids as $cid) {
      if (in_array($cid, $prev_cids)) {
        db_update('bs_products_category_index')
          ->fields(['delta' => $delta])
          ->condition('pid', $product->id())
          ->condition('cid', $cid)
          ->execute();
      }
      else {
        db_insert('bs_products_category_index')
          ->fields([
            'pid' => $product->id(),
            'cid' => $cid,
            'direct_rel' => 1,
            'delta' => $delta,
            'weight' => $weight,
          ])
          ->execute();
      }
      $delta++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $vars = parent::__sleep();
    // Do not serialize static cache.
    unset($vars['parents'], $vars['parentsAll'], $vars['children'], $vars['treeChildren'], $vars['treeParents'], $vars['treeTerms'], $vars['trees']);
    return $vars;
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    parent::__wakeup();
    // Initialize static caches.
    $this->parents = [];
    $this->parentsAll = [];
    $this->children = [];
    $this->treeChildren = [];
    $this->treeParents = [];
    $this->treeTerms = [];
    $this->trees = [];
  }


  /**
   * {@inheritdoc}
   */
  protected function buildQuery($ids, $revision_id = FALSE) {
    $query = parent::buildQuery($ids, $revision_id);

    $query->leftJoin('bs_products_category_hierarchy', 'h', 'h.cid = base.cid');
    $query->fields('h', ['parent']);

    return $query;
  }

}
