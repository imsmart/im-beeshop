<?php
namespace Drupal\bs_product\Controller;

use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;

class ProductViewController extends EntityViewController {
  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $bs_product, $view_mode = 'full') {

    //$bs_product->inheritFieldsFromParentsForView();

    $build = parent::view($bs_product, $view_mode);

    return $build;
  }

}

