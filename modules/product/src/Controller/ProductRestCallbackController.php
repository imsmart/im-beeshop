<?php
namespace Drupal\bs_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\bs_product\ProductsCategoryStorage;

class ProductRestCallbackController extends ControllerBase {

  public function updateFromCatTree() {
    $request = \Drupal::request();
    $responce = new Response();

    /**
     * @var ProductsCategoryStorage $catgories_sctorage
     */
    $catgories_sctorage = \Drupal::entityTypeManager()->getStorage('bs_products_category');

    if ($request_body = $request->getContent()) {
      $request_data = json_decode($request_body);

      if ($processed_cat = $catgories_sctorage->load($request_data->changedNodeData->cid)) {
        if ($request_data->dropInsideItem) {
          $r = reset($catgories_sctorage->loadSubCatsInfo([$request_data->targetNodeData->cid]));

          $weights = [];

          foreach ($r as $row) {
            $weights[] = $row->weight;
          }

          $min_weight = min($weights);

          $processed_cat->set('weight', $min_weight-1);
          $processed_cat->set('parent', $request_data->targetNodeData->cid);
          $processed_cat->save();
        }
        else {

          $cids_in_group = [];
          $cats = [];

          $processed_cat->set('parent', $request_data->targetNodeParent->key ?: 0);

          foreach ($request_data->targetNodeParent->children as $child) {
            $cids_in_group[] = $child->data->cid;
          }

          if ($cids_in_group) {
            $cats = $catgories_sctorage->loadMultiple($cids_in_group);
          }

          if (isset($cats[$processed_cat->id()])) {
            unset($cats[$processed_cat->id()]);
          }

          $new_order = [];

          foreach ($cats as $cid => $category) {
            if ($category->id() == $request_data->targetNodeData->cid) {
              if ($request_data->fromIndex < $request_data->toIndex) {
                $new_order[] = $category;
                $new_order[] = $processed_cat;
              }
              else {
                $new_order[] = $processed_cat;
                $new_order[] = $category;
              }
            }
            else {
              $new_order[] = $category;
            }
          }

          $weight = 0;
          foreach ($new_order as $category) {
            if ($category->getWeight() != $weight) {
              $category->set('weight', $weight);
              $category->save();
            }

            $weight++;
          }

        }
      }
    }

    return $responce;
  }

}

