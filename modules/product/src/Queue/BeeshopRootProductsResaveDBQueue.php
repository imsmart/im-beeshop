<?php
namespace Drupal\bs_product\Queue;

use Drupal\Core\Queue\DatabaseQueue;

class BeeshopRootProductsResaveDBQueue extends DatabaseQueue {

  /**
   * The database table name.
   */
  const TABLE_NAME = 'bs_root_products_resave_queue';


  /**
   * Defines the schema for the queue table.
   *
   * @internal
   */
  public function schemaDefinition() {

    return [
      'description' => 'Stores items in queues.',
      'fields' => [
        'pid' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Primary Key: Unique item ID.',
        ],
        'data' => [
          'type' => 'blob',
          'not null' => FALSE,
          'size' => 'big',
          'serialize' => TRUE,
          'description' => 'The arbitrary data for the item.',
        ],
        'expire' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Timestamp when the claim lease expires on the item.',
        ],
        'created' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Timestamp when the item was created.',
        ],
      ],
      'primary key' => ['pid'],
      'indexes' => [
        'expire' => ['expire'],
      ],
    ];
  }
}

