<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultAllowed;

class PoductsCategoryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    $access = parent::checkAccess($entity, $operation, $account);

    if ($account->hasPermission('administer beeshop products categories')) {
      return AccessResult::allowed();
    }

    if (!$entity->isPublished()) {
      return AccessResult::forbiddenIf(!$account->hasPermission('view unpublished products categories'));
    }

    return $access;
  }

}

