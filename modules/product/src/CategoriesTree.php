<?php
namespace Drupal\bs_product;

use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Menu\MenuTreeParameters;

class CategoriesTree implements CategoriesTreeInterface {

  /**
   * Constructs a Drupal\bs_product\CategoriesTree object.
   *
   * @param \Drupal\Core\Menu\MenuTreeStorageInterface $tree_storage
   *   The menu link tree storage.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link plugin manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider to load routes by name.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver.
   */
  public function __construct(CategoriesTreeStorageInterface $tree_storage, RouteProviderInterface $route_provider, ControllerResolverInterface $controller_resolver) {
    $this->treeStorage = $tree_storage;
//     $this->menuLinkManager = $menu_link_manager;
    $this->routeProvider = $route_provider;
//     $this->menuActiveTrail = $menu_active_trail;
    $this->controllerResolver = $controller_resolver;
  }

  /**
   * Returns a tree containing of MenuLinkTreeElement based upon tree data.
   *
   * This method converts the tree representation as array coming from the tree
   * storage to a tree containing a list of MenuLinkTreeElement[].
   *
   * @param array $data_tree
   *   The tree data coming from the menu tree storage.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   An array containing the elements of a menu tree.
   */
  protected function createInstances(array $data_tree) {
    $tree = [];
    foreach ($data_tree as $key => $element) {
      $subtree = $this->createInstances($element['subtree']);
      // Build a MenuLinkTreeElement out of the menu tree link definition:
      // transform the tree link definition into a link definition and store
      // tree metadata.
//       $tree[$key] = new MenuLinkTreeElement(
//         $this->menuLinkManager->createInstance($element['definition']['id']),
//         (bool) $element['has_children'],
//         (int) $element['depth'],
//         (bool) $element['in_active_trail'],
//         $subtree
//         );
    }
    return $tree;
  }

  /**
   * {@inheritdoc}
   */
  public function load($shop_id = 0, MenuTreeParameters $parameters) {
    $data = $this->treeStorage->loadTreeData($shop_id, $parameters);

    dpm($data);

    // Pre-load all the route objects in the tree for access checks.
    if ($data['route_names']) {
      $this->routeProvider->getRoutesByNames($data['route_names']);
    }
    return $this->createInstances($data['tree']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentRouteCategoriesTreeParameters($shop_id) {
    //$active_trail = $this->menuActiveTrail->getActiveTrailIds($menu_name);

    $parameters = new MenuTreeParameters();
//     $parameters->setActiveTrail($active_trail)
//     // We want links in the active trail to be expanded.
//     ->addExpandedParents($active_trail)
//     // We marked the links in the active trail to be expanded, but we also
//     // want their descendants that have the "expanded" flag enabled to be
//     // expanded.
//     ->addExpandedParents($this->treeStorage->getExpanded($menu_name, $active_trail));

    return $parameters;
  }


}

