<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ProductPriceIsDiscounted extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {

    if (!$this->getEntity()->get('price')->isEmpty()) {
      if ($pice = $this->getEntity()->getActualPrice()) {
        $this->setValue($pice->isDiscounted());
      }
    }
    else {
      $this->setValue(FALSE);
    }

  }

  public function getValue() {
    return FALSE;
  }
}

