<?php

namespace Drupal\bs_product;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the products category schema handler.
 */
class ProductsCategorySchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset = FALSE);

    $schema['bs_products_category_field_data']['indexes'] += [
      'category_tree' => ['sid', 'weight', 'name'],
      'category__sid_name' => ['sid', 'name'],
    ];

    $schema['bs_products_category_hierarchy'] = [
      'description' => 'Stores the hierarchical relationship between categories.',
      'fields' => [
        'cid' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Primary Key: The {bs_products_category}.cid of the category.',
        ],
        'parent' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'description' => "Primary Key: The {bs_products_category}.cid of the categories parent. 0 indicates no parent.",
        ],
      ],
      'indexes' => [
        'parent' => ['parent'],
      ],
      'foreign keys' => [
        'bs_products_category' => [
          'table' => 'bs_products_category',
          'columns' => ['cid' => 'cid'],
        ],
      ],
      'primary key' => ['cid', 'parent'],
    ];

    $schema['bs_products_category_index'] = [
      'description' => 'Maintains denormalized information about product/category relationships.',
      'fields' => [
        'pid' => [
          'description' => 'The {bs_product}.pid this record tracks.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'cid' => [
          'description' => 'The category ID.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'direct_rel' => [
          'description' => 'Boolean indicating whether the this row hold information about direct product relation to category.',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 1,
        ],
        'delta' => array(
          'description' => 'Product category relation delta.',
          'type' => 'int',
          'not null' => TRUE,
          'unsigned' => TRUE,
          'default' => 0,
        ),
        'weight' => array(
          'description' => 'Product weight in category.',
          'type' => 'int',
          'size' => 'big',
          'not null' => TRUE,
          'default' => 0,
        ),
      ],
      'primary key' => ['pid', 'cid'],
      'indexes' => [
        'category_product' => ['cid', 'pid', 'direct_rel'],
      ],
      'foreign keys' => [
        'related_product' => [
          'table' => 'bs_product',
          'columns' => ['pid' => 'pid'],
        ],
        'category' => [
          'table' => 'bs_products_category',
          'columns' => ['cid' => 'cid'],
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

//     if ($table_name == 'taxonomy_term_field_data') {
//       // Remove unneeded indexes.
//       unset($schema['indexes']['taxonomy_term_field__vid__target_id']);
//       unset($schema['indexes']['taxonomy_term_field__description__format']);

//       switch ($field_name) {
//         case 'weight':
//           // Improves the performance of the taxonomy_term__tree index defined
//           // in getEntitySchema().
//           $schema['fields'][$field_name]['not null'] = TRUE;
//           break;

//         case 'name':
//           $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
//           break;
//       }
//     }

    return $schema;
  }

}
