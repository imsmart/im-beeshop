<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\EntityViewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;

class ProductViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\bs_product\Entity\ProductInterface [] $entities */
    if (empty($entities)) {
      return;
    }

    foreach ($entities as $entity) {

      $entity->prepareForView();
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);
  }

  /**
   * {@inheritdoc}
   */
  public function viewField(FieldItemListInterface $items, $display_options = []) {
    $entity = $items->getEntity();
//     $entity->prepareForView();

    $clone = clone $entity;
    $clone->prepareForView();
    $items = $clone->get($items->getName());

    return parent::viewField($items, $display_options);
  }

  /**
   * {@inheritdoc}
   */
  public function viewFieldItem(FieldItemInterface $item, $display = []) {
    $entity = $item->getEntity();
    $entity->prepareForView();
    return parent::viewFieldItem($item, $display);
  }


}

