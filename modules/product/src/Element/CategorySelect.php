<?php
namespace Drupal\bs_product\Element;

use Drupal\Core\Render\Element\Select;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_product\ProductsCategoryStorageInterface;
use Drupal\Core\Form\OptGroup;

/**
 *
 *
 *
 * @RenderElement("bs_product_category_select")
 */
class CategorySelect extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $info = parent::getInfo();

    $info['#shop'] = 0;
    $info['#attached']['library'][] = 'bs_product/productsCategorySelect';

    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function preRenderSelect($element) {
    $element = parent::preRenderSelect($element);

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {

    $element = parent::processSelect($element, $form_state, $complete_form);

    $element['#options'] = self::getOptions();

    $product_cats_storage = \Drupal::entityTypeManager()->getStorage('bs_products_category');

    $tree = $product_cats_storage->loadTreeAlt();

    $normalized_tree = [];

    foreach ($tree as $category) {

      $category->fullName = trim($category->label());

      $normalized_tree[$category->id()] = [
        'id' => $category->id(),
        'label' => $category->label(),
        'full_name' => $category->fullName,
        'url' => $category->toUrl()->toString(),
      ];
      /**
       * @var ProductsCategoryInterface $category
       */
      if ($childs = _bs_product_get_tree_category_childs($category)) {
        $normalized_tree += $childs;
      }
    }

    $element['#attached']['drupalSettings']['BeeShop']['categoriesSet'] = $normalized_tree;
    $element['#attributes']['data-product-categories-select'] = TRUE;

    return $element;
  }


  protected static function getOptions() {

    $options = \Drupal::service('plugin.manager.entity_reference_selection')->getInstance([
      'target_type' => 'bs_products_category'
    ])->getReferenceableEntities();

    $return = [];
    foreach ($options as $bundle => $entity_ids) {
      // The label does not need sanitizing since it is used as an optgroup
      // which is only supported by select elements and auto-escaped.
      //$bundle_label = (string) $bundles[$bundle]['label'];
      $return[$bundle] = $entity_ids;
    }

    $return = OptGroup::flattenOptions($return);

    return $return;

  }

}

