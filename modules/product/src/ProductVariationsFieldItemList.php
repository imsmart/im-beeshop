<?php

namespace Drupal\bs_product;

use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Defines a item list class for entity reference fields.
 */
class ProductVariationsFieldItemList extends EntityReferenceFieldItemList implements ProductVariationsFieldItemListInterface {

  /**
   * {@inheritDoc}
   */
  public function referencedEntities($load_original = FALSE) {
    if ($load_original) {
      return parent::referencedEntities();
    }

    $target_entities = [];

    foreach ($this->list as $delta => $item) {
      if ($item->entity) {
        $target_entities[$delta] = $item->entity;
      }
    }

    return $target_entities;
  }

  public function getVariation($id) {
    foreach ($this->referencedEntities() as $variation) {
      if ($variation->id() == $id) return $variation;
    }
  }

  public function findVariations(array $ids) {

    $cloned_list = clone $this;

    $flipped_ids = array_flip($ids);

    $cloned_list->filter(function ($item) use ($flipped_ids){
      return (($entity = $item->entity) && isset($flipped_ids[$entity->id()]));
    });

      return $cloned_list;
  }

}
