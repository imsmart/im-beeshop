<?php

namespace Drupal\bs_product;

use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for bs_products_category entity storage classes.
 */
interface ProductsCategoryStorageInterface extends ContentEntityStorageInterface {

  /**
   * Removed reference to categories from category_hierarchy.
   *
   * @param array $cids
   *   Array of categories that need to be removed from hierarchy.
   */
  public function deleteCategoryHierarchy($cids);

  /**
   * Updates category hierarchy information with the hierarchy trail of it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $category
   *   Category entity that needs to be added to category hierarchy information.
   */
  public function updateCategoryHierarchy(EntityInterface $category);

  /**
   * Finds all parents of a given category ID.
   *
   * @param int $cid
   *   Category ID to retrieve parents for.
   *
   * @return \Drupal\bs_product\Entity\ProductsCategory[]
   *   An array of term objects which are the parents of the term $tid.
   */
  public function loadParents($cid);

  /**
   * Finds all ancestors of a given category ID.
   *
   * @param int $cid
   *   Category ID to retrieve ancestors for.
   *
   * @return object[]|\Drupal\bs_product\Entity\ProductsCategoryInterface[]
   *   An array of category objects which are parents of the category $cid.
   */
  public function loadAllParents($cid, $levels, $load_entities);


  /**
   * Get categories tree as options list for select control
   *
   * @param int $sid
   *   Shop ID to retrivate categories list
   * @param array $exclude
   *   Array of categories ID that must be exluded from result list
   *
   * @return array
   */
  public function getCategoriesTreeAsOtionsForSelect($sid, $exclude);

  /**
   * Need description for getProductsCategories method
   *
   * @param array $ids
   *
   * @return array
   *   An array of products categories
   */
  public function getProductsCategories($ids);

  /**
   * updateProductCategories
   *
   *  @param \Drupal\bs_product\Entity\ProductInterface $product
   *   Product entity that needs to be update.
   */
  public function updateProductCategories(ProductInterface $product);

  /**
   * Finds all children of a category ID.
   *
   * @param int $cid
   *   Category ID to retrieve parents for.
   * @param string $vid
   *   An optional vocabulary ID to restrict the child search.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of term objects that are the children of the term $tid.
   */
  //public function loadChildren($tid, $vid = NULL);

  /**
   * Finds all terms in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree. If 0, generate the tree
   *   for the entire vocabulary.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   * @param bool $load_entities
   *   If TRUE, a full entity load will occur on the term objects. Otherwise
   *   they are partial objects queried directly from the {taxonomy_term_data}
   *   table to save execution time and memory consumption when listing large
   *   numbers of terms. Defaults to FALSE.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   An array of term objects that are the children of the vocabulary $vid.
   */
  //public function loadTree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE);

  /**
   * Count the number of nodes in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   *
   * @return int
   *   A count of the nodes in a given vocabulary ID.
   */
  //public function nodeCount($vid);

  /**
   * Reset the weights for a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   */
  //public function resetWeights($vid);

  /**
   * Returns all terms used to tag some given nodes.
   *
   * @param array $nids
   *   Node IDs to retrieve terms for.
   * @param array $vocabs
   *   (optional) A vocabularies array to restrict the term search. Defaults to
   *   empty array.
   * @param string $langcode
   *   (optional) A language code to restrict the term search. Defaults to NULL.
   *
   * @return array
   *   An array of nids and the term entities they were tagged with.
   */
  //public function getNodeTerms(array $nids, array $vocabs = [], $langcode = NULL);

}
