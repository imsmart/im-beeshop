<?php
namespace Drupal\bs_product;

use Drupal\Core\Menu\MenuActiveTrail;

class ProductMenuActiveTrail extends MenuActiveTrail {


  /**
   * {@inheritdoc}
   */
  public function getActiveLink($menu_name = NULL) {

    if ($this->routeMatch->getRouteName() == 'entity.bs_product.canonical') {
      if (($current_product = $this->routeMatch->getParameter('bs_product')) &&
        ($product_main_category = $current_product->categories->entity)) {

          $route_name = 'entity.bs_products_category.canonical';
          $route_parameters = [
            'bs_products_category' => $product_main_category->id(),
          ];
          $links = $this->menuLinkManager->loadLinksByRoute($route_name, $route_parameters, $menu_name);

//           dpm($product_main_category);
//           dpm($route_name);
//           dpm($route_parameters);

          if ($links) {
            return reset($links);
          }
        }

    }

    return parent::getActiveLink($menu_name);
  }

}

