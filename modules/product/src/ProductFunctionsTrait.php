<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 *
 * @author andrey
 *
 */
trait ProductFunctionsTrait {

  /**
   * @var ProductInterface
   */
  protected $product;

  /**
   * @var FieldDefinitionInterface
   */
  protected $productFieldDefinitions;

  /**
   * @return \Drupal\bs_product\ProductInterface
   */
  public function getProduct() {
    return $this->product;
  }

  /**
   * Returns the bundle entity of the product, or NULL if there is none.
   *
   * @return \Drupal\bs_product\Entity\ProductTypeInterface|null
   *   The bundle entity.
   */
  protected function getProductBundleEntity() {
    if (!$this->product) return NULL;
    return $this->entityTypeManager->getStorage('bs_product_type')->load($this->product->bundle());
  }

  /**
   * Gets the definitions of the product fields.
   */
  protected function getProductFieldDefinitions() {
    if (!isset($this->productFieldDefinitions)) {
      $definitions = $this->product->getFieldDefinitions();
      $this->productFieldDefinitions = $definitions;
    }

    return $this->productFieldDefinitions;
  }

  /**
   * Gets the field definition of a field.
   */
  protected function getProductFieldDefinition($field_name) {
    $definitions = $this->getProductFieldDefinitions();
    return isset($definitions[$field_name]) ? $definitions[$field_name] : NULL;
  }

}

