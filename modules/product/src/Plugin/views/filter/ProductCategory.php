<?php

namespace Drupal\bs_product\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\bs_product\ProductsCategoryStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\Entity\ProductsCategory;
use Drupal\bs_product\ProductsCategoryQueryViewsHandlerTrait;

/**
 * Filter to handle products by categories
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("product_category")
 */
class ProductCategory extends FilterPluginBase {

  use ProductsCategoryQueryViewsHandlerTrait;

//   /**
//    * The category storage.
//    *
//    * @var \Drupal\bs_product\ProductsCategoryStorageInterface
//    */
//   protected $categoryStorage;

//   /**
//    * Constructs a TaxonomyIndexTid object.
//    *
//    * @param array $configuration
//    *   A configuration array containing information about the plugin instance.
//    * @param string $plugin_id
//    *   The plugin_id for the plugin instance.
//    * @param mixed $plugin_definition
//    *   The plugin implementation definition.
//    * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
//    *   The vocabulary storage.
//    * @param \Drupal\taxonomy\TermStorageInterface $term_storage
//    *   The term storage.
//    */
//   public function __construct(array $configuration, $plugin_id, $plugin_definition, ProductsCategoryStorageInterface $product_category_storage) {
//     parent::__construct($configuration, $plugin_id, $plugin_definition);
//     $this->categoryStorage = $product_category_storage;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
//     return new static(
//       $configuration,
//       $plugin_id,
//       $plugin_definition,
//       $container->get('entity.manager')->getStorage('bs_products_category')
//       );
//   }

  public function adminSummary() {
    // set up $this->valueOptions for the parent summary
    $this->valueOptions = [];

    if ($this->value) {
      $this->value = array_filter($this->value);
      $cats = ProductsCategory::loadMultiple($this->value);
      foreach ($cats as $category) {
        $this->valueOptions[$category->id()] = $category->label();
      }
    }

    return $this->operator . ' ' . implode(', ', $this->valueOptions);
  }

  public function operators() {
    $operators = [
      'or' => [
        'title' => $this->t('Is one of'),
        'short' => $this->t('or'),
        'short_single' => $this->t('='),
        'method' => 'opHelper',
        'values' => 1,
        'ensure_my_table' => 'helper',
      ],
      'and' => [
        'title' => $this->t('Is all of'),
        'short' => $this->t('and'),
        'short_single' => $this->t('='),
        'method' => 'opHelper',
        'values' => 1,
        'ensure_my_table' => 'helper',
      ],
      'not' => [
        'title' => $this->t('Is none of'),
        'short' => $this->t('not'),
        'short_single' => $this->t('<>'),
        'method' => 'opHelper',
        'values' => 1,
        'ensure_my_table' => 'helper',
      ],
    ];
    // if the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

  /**
   * Build strings from the operators() for 'select' options
   */
  public function operatorOptions($which = 'title') {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['depth'] = ['default' => 0];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    return $this->valueOptions;
  }

  public function hasExtraOptions() {
    return TRUE;
  }

  protected function valueForm(&$form, FormStateInterface $form_state) {

    $default_value = (array) $this->value;

    $form['value'] = [
      '#type' => 'bs_product_category_select',
      '#multiple' => TRUE,
      '#default_value' => $default_value,
    ];
  }

  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildExtraOptionsForm($form, $form_state);

    $form['depth'] = [
      '#type' => 'weight',
      '#title' => $this->t('Depth'),
      '#default_value' => $this->options['depth'],
      '#description' => $this->t('The depth will match products related with categories in the hierarchy.'),
    ];
  }


}

