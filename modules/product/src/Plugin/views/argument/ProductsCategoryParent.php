<?php

namespace Drupal\bs_product\Plugin\views\argument;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Views;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for categories with depth.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("bs_products_category_arg_parent")
 */
class ProductsCategoryParent extends ArgumentPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var EntityStorageInterface
   */
  protected $categoryStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $categoryStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->categoryStorage = $categoryStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity.manager')->getStorage('bs_products_category'));
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['depth'] = ['default' => 0];
    $options['break_phrase'] = ['default' => FALSE];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['depth'] = [
      '#type' => 'weight',
      '#title' => $this->t('Depth'),
      '#default_value' => $this->options['depth'],
      '#description' => $this->t('The depth will match nodes tagged with terms in the hierarchy. For example, if you have the term "fruit" and a child term "apple", with a depth of 1 (or higher) then filtering for the term "fruit" will get nodes that are tagged with "apple" as well as "fruit". If negative, the reverse is true; searching for "apple" will also pick up nodes tagged with "fruit" if depth is -1 (or lower).'),
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Override defaultActions() to remove summary actions.
   */
  protected function defaultActions($which = NULL) {
//     if ($which) {
//       if (in_array($which, ['ignore', 'not found', 'empty', 'default'])) {
//         return parent::defaultActions($which);
//       }
//       return;
//     }
//     $actions = parent::defaultActions();
//     unset($actions['summary asc']);
//     unset($actions['summary desc']);
//     unset($actions['summary asc by count']);
//     unset($actions['summary desc by count']);
//     return $actions;
    if (!empty($this->view)) {
      return parent::defaultActions($which);
    }
  }

  public function query($group_by = FALSE) {

    $this->ensureMyTable();

    // bs_products_category_hierarchy

    $join_conf = [
      'type' => 'LEFT',
      'table' => 'bs_products_category_hierarchy',
      'field' => 'cid',
      'left_field' => 'cid',
      'left_table' => $this->table,
      'operator' => '=',
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $join_conf);

    $this->query->queueTable($this->table, NULL, $join, 'products_category_hierarchy');
  }

//   public function title() {
//     $term = $this->termStorage->load($this->argument);
//     if (!empty($term)) {
//       return $term->getName();
//     }
//     // TODO review text
//     return $this->t('No name');
//   }

}
