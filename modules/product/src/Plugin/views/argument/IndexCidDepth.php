<?php

namespace Drupal\bs_product\Plugin\views\argument;

use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Views;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\ProductsCategoryQueryViewsHandlerTrait;

/**
 * Argument handler for categories with depth.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("bs_products_category_index_cid_depth")
 */
class IndexCidDepth extends ArgumentPluginBase implements ContainerFactoryPluginInterface {

  use ProductsCategoryQueryViewsHandlerTrait;

  /**
   * @var EntityStorageInterface
   */
  protected $categoryStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $categoryStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->categoryStorage = $categoryStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity.manager')->getStorage('bs_products_category'));
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['depth'] = ['default' => 0];
    $options['break_phrase'] = ['default' => FALSE];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['depth'] = [
      '#type' => 'weight',
      '#title' => $this->t('Depth'),
      '#default_value' => $this->options['depth'],
      '#description' => $this->t('The depth will match nodes tagged with terms in the hierarchy. For example, if you have the term "fruit" and a child term "apple", with a depth of 1 (or higher) then filtering for the term "fruit" will get nodes that are tagged with "apple" as well as "fruit". If negative, the reverse is true; searching for "apple" will also pick up nodes tagged with "fruit" if depth is -1 (or lower).'),
    ];

    $form['break_phrase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple values'),
      '#description' => $this->t('If selected, users can enter multiple values in the form of 1+2+3. Due to the number of JOINs it would require, AND will be treated as OR with this filter.'),
      '#default_value' => !empty($this->options['break_phrase']),
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Override defaultActions() to remove summary actions.
   */
  protected function defaultActions($which = NULL) {
//     if ($which) {
//       if (in_array($which, ['ignore', 'not found', 'empty', 'default'])) {
//         return parent::defaultActions($which);
//       }
//       return;
//     }
//     $actions = parent::defaultActions();
//     unset($actions['summary asc']);
//     unset($actions['summary desc']);
//     unset($actions['summary asc by count']);
//     unset($actions['summary desc by count']);
//     return $actions;
    if (!empty($this->view)) {
      return parent::defaultActions($which);
    }
  }

//   public function title() {
//     $term = $this->termStorage->load($this->argument);
//     if (!empty($term)) {
//       return $term->getName();
//     }
//     // TODO review text
//     return $this->t('No name');
//   }

}
