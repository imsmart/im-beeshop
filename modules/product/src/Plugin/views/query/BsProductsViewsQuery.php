<?php

namespace Drupal\bs_product\Plugin\views\query;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\views\Plugin\views\join\JoinPluginBase;
use Drupal\views\Plugin\views\HandlerBase;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views query plugin for an SQL query.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "bs_products_views_query",
 *   title = @Translation("Beeshop products SQL Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class BsProductsViewsQuery extends Sql {

  public $expressions = [];

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['products_level_to_display'] = [
      '#type' => 'select',
      '#title' => 'Products level to display',
      '#options' => [
        'only_root' => 'Display only root products',
        'by_type_defined_level' => 'By type defined',
        'all' => 'All products (No limits)',
      ],
      '#default_value' => !empty($this->options['products_level_to_display']) ?
                          $this->options['products_level_to_display'] :
                          'by_type_defined_level',
    ];
  }

  /**
   * {@inheritDoc}
   */
  protected function compileFields($query) {

    foreach ($this->expressions as $expression) {
      $placeholders = !empty($expression['placeholders']) ? $expression['placeholders'] : [];
      $query->addExpression($expression['expression'], $expression['alias'], $placeholders);
    }

    parent::compileFields($query);
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $display_level_type = !empty($this->options['products_level_to_display']) ?
                          $this->options['products_level_to_display'] :
                          'by_type_defined_level';

    switch ($display_level_type) {
      case 'only_root':
        $or_cond = new Condition('OR');
        $where = $or_cond->isNull('parent');
        $where->condition('parent', 0);
        $this->addWhere(0, $where);
        break;
      case 'by_type_defined_level':
        $join_conf = [
        'type' => 'LEFT',
        'table' => 'bs_product_variations_display',
        'field' => 'ptype',
        'left_field' => 'type',
        'left_table' => 'bs_product_field_data',
        'operator' => '=',
        ];
        $join = Views::pluginManager('join')->createInstance('standard', $join_conf);

        $this->queueTable('bs_product_variations_display', NULL, $join, 'pvld');

        $this->addWhereExpression(0, '(pvld.level_for_category IS NULL OR pvld.level_for_category = bs_product_field_data.level)');
        break;
    }
  }

  public function addOrderByExpression($snippet, $order = 'ASC') {

  }

  public function query($get_count = FALSE) {
    $query = parent::query($get_count);

    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function loadEntities(&$results) {
    if (!empty($this->view->notLoadEntities)) {
      return;
    }

    parent::loadEntities($results);
  }

}
