<?php

namespace Drupal\bs_product\Plugin\views\display;

use Drupal\views\Plugin\views\display\Block;
use Drupal\views\Plugin\views\display\DisplayPluginBase;


/**
 *
 * @ViewsDisplay(
 *   id = "bs_view_display_block",
 *   title = @Translation("Block (ext)"),
 *   help = @Translation("Display the view as a block."),
 *   theme = "views_view",
 *   register_theme = FALSE,
 *   uses_hook_block = TRUE,
 *   contextual_links_locations = {"block"},
 *   admin = @Translation("Block (ext)")
 * )
 *
 */
class BlockExt extends Block {

  /**
   * Allows block views to put exposed filter forms in blocks.
   */
  public function usesExposedFormInBlock() {
    return TRUE;
  }

  /**
   * Block views use exposed widgets only if AJAX is set.
   */
  public function usesExposed() {
    return DisplayPluginBase::usesExposed();
  }

}

