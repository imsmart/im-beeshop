<?php
namespace Drupal\bs_product\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\bs_price\Calculator;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a view field for display discount for product.
 *
 * @ViewsField("bs_product_variations_count")
 */
class ProductVariationsCount extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['all_levels'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['all_levels'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('All levels'),
      '#description' => $this->t('Display variations count for all levels.'),
      '#default_value' => $this->options['all_levels'],
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function render(ResultRow $values) {

    /**
     * @var ProductInterface $product
     */
    $product = $values->_entity;

    $variations_count = [];

    $this->getVariationsCount($product, $variations_count);

    $output = [
      '#markup' => implode('/', $variations_count),
    ];

//     if (!empty($values->_entity)) {
//

//       if ($product->hasDiscountedPrice()) {
//         $actual_price = $product->getActualPrice()->toPrice();
//         $base_price = $product->getBasePrice()->toPrice();

//         $discount_percents = strval((1 - $actual_price->getPriceValue() / $base_price->getPriceValue()) * 100);
//         $discount_percents = Calculator::roundTo($discount_percents, 5);

//         $output['#markup'] = '-' . $discount_percents . '%';
//       }
//     }

    return $output;
  }

  protected function getVariationsCount(ProductInterface $product, &$variations_count_set) {
    if ($product->isVariationsSupport()) {

      $product_type = $product->getBundleEntity();

      if ($product->getLevel() == $product_type->getMaxChildLevel()) {
        return;
      }

      if (!isset($variations_count_set[$product->getLevel()])) {
        $variations_count_set[$product->getLevel()] = 0;
      }
      $variations_count_set[$product->getLevel()] += $product->variations->count();

      foreach ($product->getVariations() as $variation) {
        $this->getVariationsCount($variation, $variations_count_set);
      }
    }
    else {
      $variations_count_set[] = '-';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }
}

