<?php
namespace Drupal\bs_product\Plugin\views\field;

use Drupal\Core\Render\Element;
use Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\ResultRow;
use Drupal\bs_product\Entity\ProductInterface;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("bs_product_is_available")
 */
class IsAvailable extends Boolean {

  /**
   * {@inheritdoc}
   */
  public function query() {

  }

  public function getValue(ResultRow $values, $field = NULL) {
    if (!$values->_entity) {
      return FALSE;
    }

    if ($product = $values->_entity->getProduct()) {
      /**
       * @var ProductInterface $product
       */

      return $product->isAvailable();
    }

    return FALSE;
  }

}

