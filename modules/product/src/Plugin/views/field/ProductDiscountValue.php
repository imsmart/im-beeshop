<?php
namespace Drupal\bs_product\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\bs_price\Calculator;

/**
 * Defines a view field for display discount for product.
 *
 * @ViewsField("bs_product_discount_value")
 */
class ProductDiscountValue extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
  * {@inheritdoc}
  */
  public function render(ResultRow $values) {


    $output = [
      '#markup' => '',
    ];

    if (!empty($values->_entity)) {
      $product = $values->_entity;

      if ($product->hasDiscountedPrice()) {
        $actual_price = $product->getActualPrice()->toPrice();
        $base_price = $product->getBasePrice()->toPrice();

        $discount_percents = strval((1 - $actual_price->getPriceValue() / $base_price->getPriceValue()) * 100);
        $discount_percents = Calculator::roundTo($discount_percents, 5);

        $output['#markup'] = '-' . $discount_percents . '%';
      }
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }
}

