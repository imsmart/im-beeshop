<?php
namespace Drupal\bs_product\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\bs_product\Entity\Product;
use Drupal\bs_data_exchange\Exception\InvalidQueueItemDataExeption;

/**
 * A CommerceML import files processor for update linked products.
 *
 * @QueueWorker(
 *   id = "bs_product_resave",
 *   title = @Translation("BeeShop product resaver"),
 * )
 */
class ProductResaveWorker extends QueueWorkerBase {


  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (empty($data->pid)) {
      throw new InvalidQueueItemDataExeption('Queue item does not contain "pid"');
    }

    if ($product = Product::load($data->productId)) {
      $product->save();
    }
    else {

    }

  }
}

