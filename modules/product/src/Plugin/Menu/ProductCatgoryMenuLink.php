<?php

namespace Drupal\bs_product\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;

/**
 * Defines menu links provided by views.
 *
 * @see \Drupal\views\Plugin\Derivative\ViewsMenuLink
 */
class ProductCatgoryMenuLink extends MenuLinkBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $overrideAllowed = [
//     'menu_name' => 1,
//     'parent' => 1,
    'weight' => 1,
    'expanded' => 1,
    'enabled' => 1,
    'title' => 1,
    'description' => 1,
  ];

  /**
   * The static menu link service used to store updates to weight/parent etc.
   *
   * @var \Drupal\Core\Menu\StaticMenuLinkOverridesInterface
   */
  protected $staticOverride;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  protected $category;

  /**
   * Constructs a new ViewsMenuLink.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager
   * @param \Drupal\views\ViewExecutableFactory $view_executable_factory
   *   The view executable factory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, StaticMenuLinkOverridesInterface $static_override) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->staticOverride = $static_override;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('menu_link.static.overrides')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {

    $options = parent::getOptions();

    \Drupal::moduleHandler()->alter('bs_product_category_menu_link_options', $options, $this);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    // @todo Get the translated value from the config without instantiating the
    //   view. https://www.drupal.org/node/2310379
    return $this->pluginDefinition['title'];
    //return $this->loadView()->display_handler->getOption('menu')['title'];
  }

  public function getUrlObject($title_attribute = TRUE) {
    return parent::getUrlObject($title_attribute);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function isResettable() {
    return (bool) $this->staticOverride->loadOverride($this->getPluginId());
  }


  /**
   * {@inheritdoc}
   */
  public function updateLink(array $new_definition_values, $persist) {
    $overrides = array_intersect_key($new_definition_values, $this->overrideAllowed);
    $this->pluginDefinition = $overrides + $this->pluginDefinition;

    if ($persist) {
      $this->staticOverride->saveOverride($this->getPluginId(), $this->pluginDefinition);
    }
    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function isDeletable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLink() {
  }

  public function loadCategory() {
    if (empty($this->category)) {
      $metadata = $this->getMetaData();
      $category = $this->entityTypeManager->getStorage('bs_products_category')->load($metadata['category_id']);
      $this->category = $category;
    }

    return $this->category;
  }
}
