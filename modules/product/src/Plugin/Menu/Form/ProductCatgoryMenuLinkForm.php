<?php

namespace Drupal\bs_product\Plugin\Menu\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\Form\MenuLinkDefaultForm;

/**
 * Provides a form to edit Views menu links.
 *
 * This provides the feature to edit the title and description, in contrast to
 * the default menu link form.
 *
 * @see \Drupal\views\Plugin\Menu\ViewsMenuLink
 */
class ProductCatgoryMenuLinkForm extends MenuLinkDefaultForm {

  /**
   * The edited views menu link.
   *
   * @var \Drupal\views\Plugin\Menu\ViewsMenuLink
   */
  protected $menuLink;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Put the title field first.
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      // @todo Ensure that the view is not loaded with a localized title.
      //   https://www.drupal.org/node/2309507
      '#default_value' => $this->menuLink->getTitle(),
      '#weight' => -10,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Shown when hovering over the menu link.'),
      // @todo Ensure that the view is not loaded with a localized description.
      //   https://www.drupal.org/node/2309507
      '#default_value' => $this->menuLink->getDescription(),
      '#weight' => -5,
    ];

    $form += parent::buildConfigurationForm($form, $form_state);

    $form['path']['#weight'] = -7;

    $category = $this->menuLink->loadCategory();
    $label = $category->label();
    $message = $this->t('This link is provided by the ProductCategory entity. The path, parent and weight can be changed by editing the category <a href=":url">@label</a>', [':url' => $category->toUrl('edit-form')->toString(), '@label' => $label]);
    $form['info'] = [
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [$message]
      ],
//       '#status_headings' => [
//         'status' => t('Status message'),
//         'error' => t('Error message'),
//         'warning' => t('Warning message'),
//       ],
      '#weight' => - 1000,
    ];

    if (isset($form['menu_parent'])) {
      $form['menu_parent']['#access'] = FALSE;
    }

    if (isset($form['weight'])) {
      $form['weight']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(array &$form, FormStateInterface $form_state) {
    $definition = parent::extractFormValues($form, $form_state);
    $definition['title'] = $form_state->getValue('title');
    $definition['description'] = $form_state->getValue('description');

    return $definition;
  }

}
