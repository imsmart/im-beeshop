<?php
namespace Drupal\bs_product\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\CategoriesTreeInterface;
use Drupal\bs_product\ProductsCategoryStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Template\Attribute;
use Drupal\bs_product\ProductCategoryListBuilder;
use Drupal\bs_product\Entity\ProductsCategoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a Categories Menu block.
 *
 * @Block(
 *   id = "bs_product_cayegories_menu_block",
 *   admin_label = @Translation("Categories menu"),
 *   category = @Translation("Menus"),
 * )
 */
class CategoriesMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The category tree storage.
   *
   * @var \Drupal\bs_product\CategoriesTreeInterface
   */
  protected $categoriesTree;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var ProductsCategoryStorageInterface
   */
  protected $categoryStorage;

  /**
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
      parent::__construct($configuration, $plugin_id, $plugin_definition);
      $this->entityTypeManager = $entity_type_manager;
      $this->categoryStorage = $this->entityTypeManager->getStorage('bs_products_category');
      $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler')
      );

  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $categories = $this->categoryStorage->loadMultiple();

    $categories_tree = $this->categoryStorage->getEntitiesTree($categories, 0);

    $build['#theme'] = 'menu__shop_categories';
    //$build['#menu_name'] = $menu_name;
    $build['#items'] = $this->buildItems($categories_tree);

//     $parameters = $this->categoriesTree->getCurrentRouteCategoriesTreeParameters($shop_id);
//     $tree = $this->categoriesTree->load($shop_id, $parameters);

    return $build;
  }

  protected function buildItems(array $categories) {
    $items = [];

    foreach ($categories as $category) {

      if (!$category->isPublished()) continue;

      $element = [
        'category' => $category,
        'is_expanded' => FALSE,
        'is_collapsed' => FALSE,
        'in_active_trail' => FALSE,
        'attributes' => new Attribute(),
        'title' => $category->label(),
        'url' => $category->toUrl(),
        'below' => $category->childs ? $this->buildItems($category->childs) : []
      ];

      $this->moduleHandler->alter('bs_product_categories_menu_item', $element);

      $items[$category->id()] = $element;
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // The messages are session-specific and hence aren't cacheable, but the
    // block itself *is* cacheable because it uses a #lazy_builder callback and
    // hence the block has a globally cacheable render array.
    return -1;
  }

}

