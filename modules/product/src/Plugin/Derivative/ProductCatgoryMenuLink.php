<?php

namespace Drupal\bs_product\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides menu links for Views.
 *
 * @see \Drupal\views\Plugin\Menu\ViewsMenuLink
 */
class ProductCatgoryMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $categoryStorage;

  /**
   * Constructs a \Drupal\views\Plugin\Derivative\ViewsLocalTask instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $category_storage
   *   The category storage.
   */
  public function __construct(EntityStorageInterface $category_storage) {
    $this->categoryStorage = $category_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity.manager')->getStorage('bs_products_category')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $categories = $this->categoryStorage->loadByProperties([
      'sid' => 0
    ]);

    foreach ($categories as $category) {
      $link = $category->toMenuLink();
      $link += $base_plugin_definition;
      $links[$category->id()] = $link;
    }

    return $links;
  }



}
