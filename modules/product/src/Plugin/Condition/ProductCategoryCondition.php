<?php
namespace Drupal\bs_product\Plugin\Condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_product\Entity\ProductsCategoryInterface;

/**
 * Provides a 'Node Type' condition.
 *
 * @Condition(
 *   id = "beeshop_products_category",
 *   label = @Translation("Products category"),
 *   context_definitions = {
 *     "bs_products_category" = @ContextDefinition("entity:bs_products_category", label = @Translation("Products category"), required = false)
 *   }
 * )
 */
class ProductCategoryCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Creates a new NodeType instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityStorageInterface $entity_storage, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')->getStorage('bs_products_category'),
      $configuration,
      $plugin_id,
      $plugin_definition
      );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['only_on_category_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only on category page'),
      '#default_value' => $this->configuration['only_on_category_page'],
    ];

    $form['rule'] = [
      '#title' => $this->t('Rule'),
      '#type' => 'select',
      '#options' => [
        'true_if_has_subcats' => 'Visible if has subcats',
        'true_if_has_no_subcats' => 'Visible if has no subcats',
      ],
      '#empty_value' => 'Not applicable',
      '#default_value' => $this->configuration['rule'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['rule'] = $form_state->getValue('rule');
    $this->configuration['only_on_category_page'] = $form_state->getValue('only_on_category_page');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function evaluate() {

    $result = TRUE;

    if (!$this->configuration['rule']) {
      return TRUE;
    }

    /**
     * @var ProductsCategoryInterface $category
     */
    $category = $this->getContextValue('bs_products_category');

    if (!$category) {
      return !$this->configuration['only_on_category_page'];
    }

    switch ($this->configuration['rule']) {
      case 'true_if_has_subcats':
        $result = !$category->get('childCats')->isEmpty();
        break;
      case 'true_if_has_no_subcats':
        $result = $category->get('childCats')->isEmpty();
        break;
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function summary() {
      // TODO Auto-generated method stub

  }

}

