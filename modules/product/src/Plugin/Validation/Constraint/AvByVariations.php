<?php
namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\bs_product\ProductValidationConstraintBase;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "bs_product_by_variations",
 *   label = @Translation("Product availability by variations", context = "Validation"),
 *   type = {"entity:bs_product"}
 * )
 */
class AvByVariations extends ProductValidationConstraintBase {

  public $message = 'All product "%product_title" variations is unavailable';

}

