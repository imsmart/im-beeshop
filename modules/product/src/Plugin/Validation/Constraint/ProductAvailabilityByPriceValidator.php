<?php
namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\bs_cart\CartItemInterface;
use Drupal\bs_product\Entity\ProductInterface;


class ProductAvailabilityByPriceValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($product, Constraint $constraint) {

    /**
     * @var ProductInterface $product
     */

    /**
     * @var ProductAvailabilityByPrice $constraint
     */

    if ($constraint->skipForParents && $product->isGroup()) {
      return;
    }

    if (!$product->getActualPrice()) {
      $this->context->buildViolation('Product price can\'t be resolved', ['%product_title' => $product->getTitle()])
        ->atPath('price')
        ->setInvalidValue($product)
        ->setCause('bs_product:price not defined')
        ->setCode('bs_product_av_by_price:')
        ->addViolation();
    }

//     foreach ($items as $delta => $item) {
//       if ($cart_item_entity = $item->entity) {
//         if ($product = $cart_item_entity->getProduct()) {
//           if ($product->canBePurchased()) {

//             if (!$cart_item_entity->isAvailable()) {
//               $this->context->buildViolation($constraint->notAvailableIsAvailableNow, ['%product_title' => $product->getTitle()])
//               ->atPath($delta)
//               ->setInvalidValue($item)
//               ->setCause('bs_cart:unavailable_product_is_available')
//               ->addViolation();
//             }

//             if ($actual_price = $product->getActualPrice()) {

//               try {
//                 if ($actual_price->toPrice() != $cart_item_entity->get('price')->first()->toPrice()) {

//                   $this->context->buildViolation($constraint->priceChanged, ['%product_title' => $product->getTitle()])
//                   ->atPath($delta)
//                   ->setInvalidValue($item)
//                   ->setCause('bs_cart:product_actual_price_changed')
//                   ->addViolation();
//                 }
//               }
//               catch (\Exception $e) {
//                 $this->context->buildViolation($constraint->priceChanged, ['%product_title' => $product->getTitle()])
//                 ->atPath($delta)
//                 ->setInvalidValue($item)
//                 ->setCause('bs_cart:product_actual_price_changed')
//                 ->addViolation();
//               }
//             }
//             else {
//               $this->context->buildViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()])
//               ->atPath($delta)
//               ->setInvalidValue($item)
//               ->setCause('bs_cart:product_not_available_for_order')
//               ->addViolation();
//             }

//           }
//           else {
//             $this->context->buildViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()])
//             ->atPath($delta)
//             ->setInvalidValue($item)
//             ->setCause('bs_cart:product_not_available_for_order')
//             ->addViolation();
//           }
//         }
//         else {
//           $this->context->buildViolation($constraint->productNotFound)
//           ->atPath($delta)
//           ->setInvalidValue($item)
//           ->setCause('bs_cart:product_not_found')
//           ->addViolation();
//         }
//       }
//     }
  }

//   protected function setItemAsAnavailable(CartItemInterface $cart_item_entity, Constraint $constraint) {

//     $product = $cart_item_entity->getProduct();

//     $cart_item_entity->setUnavailableSatatus(TRUE);
//     $cart_item_entity->set('price', [
//       'value' => NULL,
//       'currency_code' => NULL,
//     ]);

//     //$cart_item_entity->save();

//     $this->context->addViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()]);
//   }

}

