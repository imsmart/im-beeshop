<?php

namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\bs_product\ProductValidationConstraintBase;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "bs_product_av_by_price",
 *   label = @Translation("Product availability by price", context = "Validation"),
 *   type = {"entity:bs_product"}
 * )
 */
class ProductAvailabilityByPrice extends ProductValidationConstraintBase {

  public $skipForParents = FALSE;

  public $priceNotDefined = 'Product "%product_title" price not defined';

}

