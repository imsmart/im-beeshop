<?php

namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\bs_product\ProductValidationConstraintBase;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "bs_product_access",
 *   label = @Translation("Product availability by access", context = "Validation"),
 *   type = {"entity:bs_product"}
 * )
 */
class ProductAccess extends ProductValidationConstraintBase {

  public $operation = 'view';

  public $message = 'You do not have permission to access the product "%product_title"';

}

