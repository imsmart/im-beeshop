<?php
namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\bs_product\ProductValidationConstraintBase;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "bs_product_state",
 *   label = @Translation("Product availability by state", context = "Validation"),
 *   type = {"entity:bs_product"}
 * )
 */
class ProductState extends ProductValidationConstraintBase {

  public $message = 'Product "%product_title" not published';

}

