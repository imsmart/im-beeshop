<?php
namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\bs_product\Entity\ProductInterface;


class ProductStateValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($product, Constraint $constraint) {

    /**
     * @var ProductInterface $product
     */

    if (!$product->isPublished()) {
      $this->context->buildViolation($constraint->message, ['%product_title' => $product->getTitle()])
        ->setCause('bs_product:product not published')
        ->setCode(403)
        ->addViolation();
    }

    if ($product->get('av_state')->entity) {
      if (!$product->get('av_state')->entity->av_flag) {
        $this->context->buildViolation($constraint->message, ['%product_title' => $product->getTitle()])
        ->setCause('bs_product:product not published')
        ->addViolation();
      }
    }


  }

//   protected function setItemAsAnavailable(CartItemInterface $cart_item_entity, Constraint $constraint) {

//     $product = $cart_item_entity->getProduct();

//     $cart_item_entity->setUnavailableSatatus(TRUE);
//     $cart_item_entity->set('price', [
//       'value' => NULL,
//       'currency_code' => NULL,
//     ]);

//     //$cart_item_entity->save();

//     $this->context->addViolation($constraint->notAvailableNow, ['%product_title' => $product->getTitle()]);
//   }

}

