<?php
namespace Drupal\bs_product\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\bs_product\Entity\ProductInterface;


class AvByVariationsValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($product, Constraint $constraint) {

    /**
     * @var ProductInterface $product
     */

    if (!$product->hasField('variations') || $product->get('variations')->isEmpty()) {
      return;
    }

    $context = $this->context;

//     $all_new = 0;

    foreach ($product->get('variations')->referencedEntities() as $variation) {

//       if (!$variation->isNew()) {
//         $all_new++;
//         continue;
//       }

     /**
      * @var ProductInterface $variation
      */
     if ($variation->isAvailable(FALSE, TRUE)) {
       return;
     }
   }

//    if (!$all_new) {
//      return;
//    }

   $this->context = $context;

   $this->context->buildViolation($constraint->message, ['%product_title' => $product->getTitle()])
   ->setCause('bs_product:all variations is unavailable')
   ->setTranslationDomain('variations_av_validator')
   ->addViolation();

  }

}

