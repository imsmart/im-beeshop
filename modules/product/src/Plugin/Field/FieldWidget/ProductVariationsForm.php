<?php

namespace Drupal\bs_product\Plugin\Field\FieldWidget;

use Drupal\beeshop\TranslationHelper;
use Drupal\Component\Utility\SortArray;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Field\PluginSettingsBase;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Component\Serialization\Json;


/**
 * Plugin implementation of the 'bs_product_variations_form' widget.
 *
 * @FieldWidget(
 *   id = "bs_product_variations_form",
 *   label = @Translation("Product variations form"),
 *   field_types = {
 *     "bs_product_variations"
 *   },
 *   multiple_values = true
 * )
 */
class ProductVariationsForm extends WidgetBase {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {

    $parent_product = $items->getEntity();
    $variations = $form_state->get(['variations', 'variation_entities']);

    $variations_updated = [];

    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);


    /**
     * @var EntityFormDisplay $display
     */
    $display = $form['variations']['widget']['#vtable_display'];

    $variations_by_id = [];

    foreach ($variations as $variation) {
      $variations_by_id[$variation->id()] = $variation;
    }

    if ($values['variations_table']) {
      $delta = 0;

      foreach ($values['variations_table'] as $key => $value) {

        if (empty($variations_by_id[$value['weight_and_relations']['id']])) {
          $message = new TranslatableMarkup('Can\'t find variation with ID:%id in variations list', ['%id' => $value['weight_and_relations']['id']]);
          \Drupal::messenger()->addWarning($message);
          continue;
        }

        $variation_entity = $variations_by_id[$value['weight_and_relations']['id']];

        $variation_entity->setWeight($value['weight_and_relations']['weight']);
        $variation_entity->setParent($value['weight_and_relations']['parent']);

        $parent_id = $value['weight_and_relations']['parent'] ?
                     $value['weight_and_relations']['parent'] :
                     $parent_product->id();

        $variation_entity->setLevel($value['weight_and_relations']['depth'] + 1);

        $variation_entity->set('variations', []);

        //$variations_by_id[$variation_entity->id()] = $variation_entity;

        if ($parent_product->id() != $variation_entity->getParentId() && !empty($variations_by_id[$variation_entity->getParentId()])) {
          $variations_by_id[$variation_entity->getParentId()]->get('variations')->appendItem($variation_entity);
        }

        foreach ($value as $field_name => $field_value) {

          if (!$variation_entity->hasField($field_name)) {
            continue;
          }

//           //if ($v_field_name == 'weight') continue;

//           $variation_field_path = $variation_field_path_base;
//           $variation_field_path[] = $v_field_name;
//           $variation_field_values = NestedArray::getValue($form_state->getValues(), $variation_field_path, $key_exists);

          if ($widget = $display->getRenderer($field_name)) {
            $w_values = $widget->massageFormValues($field_value, $form, $form_state);
            $variation_entity->set($field_name, $w_values);
          }

        }

        $variations_updated[] = $variation_entity;

        $delta++;
      }
    }

    $current_parent_variations = [];

    foreach ($variations_by_id as $id => $variation) {
      if (!$variation->getParentId() || $parent_product->id() == $variation->getParentId()) {
        $current_parent_variations[] = $variation;
      }
    }

    $form_state->set(['variations', 'variation_entities'], $variations_updated ? $variations_updated : $variations);

    $items->setValue($current_parent_variations);

  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $title = $this->fieldDefinition->getLabel();

    $parent_product = $items->getEntity();
    $parent_langcode = $items->getEntity()->language()->getId();

    $element = [
      '#type' => 'fieldset',
      '#title' => $title,
      '#prefix' => '<div id="variations-container">',
      '#suffix' => '</div>',
      '#root' => TRUE,
    ];

    $this->prepareFormState($form, $form_state, $items);
    $variation_entities_data = $form_state->get(['variations', 'variation_entities']);
    $variation_data = $form_state->get(['variations']);

    if (!empty($variation_data['action']) && $variation_data['action'] == 'add_new_row') {

      for($new_variation_count = 1; $new_variation_count <= $form_state->getValue(['variations', 'actions', 'new_variations_count'], 1); $new_variation_count++) {

        $variation_data['last_new_id'] -= 1;

        $new_variation = \Drupal::entityTypeManager()->getStorage('bs_product')->create([
          'pid' => $variation_data['last_new_id'],
          'entity_type' => 'bs_product',
          'type' => $parent_product->bundle(),
          'level' => $parent_product->getLevel() + 1,
          'title' => $parent_product->label(),
          'sku' => $parent_product->getSKU() . '--',
        ]);

        $new_variation->setWeight($variation_data['last_root_weight'] + 1);
        $variation_entities_data[] = $new_variation;

      }

      $form_state->set(['variations'], $variation_data);
      $form_state->set(['variations', 'variation_entities'], $variation_entities_data);
      $form_state->set(['variations', 'action'], null);
    }

    $field_definitions = $parent_product->getFieldDefinitions('bs_product', $parent_product->bundle());
    $variations_fields_hr = $parent_product->getBundleEntity()->getVariationFieldsHierarchy();
    $variations_level = $parent_product->get('level')->value + 1;
    $form_display_storage = \Drupal::entityTypeManager()->getStorage('entity_form_display');

    $header = [$this->t('#ID')];
    $element['#v_field_definitions'] = $field_definitions;

    if ($display = $form_display_storage->load('bs_product.' . $parent_product->bundle(). '.variation_in_table_edit')) {

      foreach ($variations_fields_hr as $field_name => $field_data) {
        if ($variations_level > $field_data['level']) {
          $display->removeComponent($field_name);
        }

        $field_definitions[$field_name]->setSetting('variation_level', $field_data['level']);

        if ($variations_level == $field_data['level'] || $variations_level < $field_data['level']) {
          $field_st_def = $field_definitions[$field_name]->getFieldStorageDefinition();
//           $field_st_def->
          if ($or_cardinality = $field_st_def->getThirdPartySetting('bs_product', 'originalCardinality', null)) {
            if ($or_cardinality != $field_st_def->getCardinality()) {
              $field_st_def->setCardinality($or_cardinality);
            }
          }
        }
      }

      $element['#vtable_display'] = $display;

      $elemets_list = $display->getComponents();
      uasort($elemets_list, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
      $element['#variation_fields'] = $elemets_list;

      foreach ($elemets_list as $name => $options) {
        if (!empty($field_definitions[$name])) {
          $header[] = $field_definitions[$name]->getLabel();
        }
      }

    }

    $header[] = $this->t('Weight');
    $header[] = $this->t('Operations');

    $element['variations_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#sticky' => TRUE,
      '#attributes' => [
        'id' => 'product-variations',
      ],
    ];

    $element['variations_table']['#tabledrag'] = [
      [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'variation-delta',
      ],
      [
        'action' => 'match',
        'relationship' => 'parent',
        'group' => 'parent',
        'subgroup' => 'parent',
        'source' => 'id',
        'hidden' => FALSE,
        'limit' => $parent_product->getBundleEntity()->getMaxChildLevel() - 1,
      ],
      [
        'action' => 'depth',
        'relationship' => 'group',
        'group' => 'depth',
        'hidden' => FALSE,
      ],
    ];

    foreach ($variation_entities_data as $delta => $variation) {

      if ($variation->mustBeDeletedOnParentSave()) continue;

      $variation_row = $this->getVariationRow($variation, $delta, $elemets_list, $display, $form_state);
      $variation_row['weight_and_relations']['weight']['#default_value'] = $delta;
      $element['variations_table'][] = $variation_row;

    }

    foreach (Element::children($element['variations_table']) as $row_index) {
      foreach (Element::children($element['variations_table'][$row_index]) as $field_name) {
        if (!empty($element['variations_table'][$row_index][$field_name]['#visible_for_level'])) {

          $states  = [
            'visible' => [
              'input[name="variations[variations_table][' . $row_index . '][weight_and_relations][depth]"]' => ['value' => $element['variations_table'][$row_index][$field_name]['#visible_for_level'] - 1]
            ],
            'required' => [
              'input[name="variations[variations_table][' . $row_index . '][weight_and_relations][depth]"]' => ['value' => $element['variations_table'][$row_index][$field_name]['#visible_for_level'] - 1]
            ],
            'disabled' => [
              'input[name="variations[variations_table][' . $row_index . '][weight_and_relations][depth]"]' => ['!value' => $element['variations_table'][$row_index][$field_name]['#visible_for_level'] - 1]
            ],
          ];

          if(!isset($element['variations_table'][$row_index][$field_name]['value'])) {
            //$element['variations_table'][$row_index][$field_name]['#attributes']['data-states'] = Json::encode($states);
            $element['variations_table'][$row_index][$field_name]['#states'] = $states;
          }
          else {
//             $element['variations_table'][$row_index][$field_name]['value']['#attributes']['data-states'] = Json::encode($states);
            $element['variations_table'][$row_index][$field_name]['value']['#states'] = $states;
          }

        }
      }
    }

    if (empty($variation_data['form']) || $variation_data['form'] == 'add_new_row') {
      $element['actions'] = [
        '#type' => 'actions',
      ];

      $element['actions']['new_variations_count'] = [
        '#type' => 'number',
        '#title' => $this->t('New varioations count'),
        '#min' => 1,
        '#default_value' => 1,
      ];

      $element['actions']['add_new_row'] = [
        '#type' => 'submit',
        '#value' => t('Add new variation row'),
        '#action' => 'add_new_row',
        '#ajax' => [
          'callback' => 'Drupal\bs_product\Plugin\Field\FieldWidget\ProductVariationsForm::ajaxGetElement',
          'wrapper' => 'variations-container',
        ],
        //'#submit' => ['bs_product_open_form_add_new_variation_form'],
        '#submit' => ['\Drupal\bs_product\Plugin\Field\FieldWidget\ProductVariationsForm::addNewVariationRowSubmit'],
      ];


    }
    else {

      $new_variation = \Drupal::entityTypeManager()->getStorage('bs_product')->create([
        'entity_type' => 'bs_product',
        'type' => $parent_product->bundle(),
        'level' => $parent_product->getLevel() + 1,
      ]);

      $element['variation_form_container'] = [
        '#type' => 'fieldset',
        '#title' => t('Variation add/edit form'),
        'variation_form' => [
          '#type' => 'inline_product_variation_form',
          '#entity_type' => 'bs_product',
          '#op' => 'edit',
          '#default_value' => $new_variation,
          '#bundle' => $new_variation->bundle(),
          '#langcode' => $parent_langcode,
          '#form_mode' => 'product_variation',
          '#save_entity' => FALSE,
        ]
      ];


    }

    $element['#attached']['library'][] = 'bs_product/variationsWidget';

//     dpm($element['variations_table']);

    return $element;
  }

  public static function addNewVariationRowSubmit($form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $triggering_element = $form_state->getTriggeringElement();
    $form_state->set(['variations', 'action'], $triggering_element['#action']);
  }

  public static function ajaxGetElement($form, FormStateInterface $form_state) {

    $element = [];
    $triggering_element = $form_state->getTriggeringElement();
    // Remove the action and the actions container.
    $array_parents = array_slice($triggering_element['#array_parents'], 0, -2);

    if (!isset($form['variations'])) {
      throw new \InvalidArgumentException('ERROR.');
    }

    while (!isset($element['#root'])) {
      $element = NestedArray::getValue($form, $array_parents);
      array_pop($array_parents);
    }

    return $element;

  }

  protected function getVariationRow(ProductInterface $variation, $delta, $elemets_list, $display, $form_state) {

    $variation_row = [];
    $destination = $this->getDestinationArray();
    $operations = [];

    $field_definitions = $variation->getFieldDefinitions();

    if (!$variation->isNew()) {
      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          'query' => $destination,
          'url' => $variation->toUrl('edit-form'),
        ],
      ];
    }

    $depth = $variation->getLevel() - 1;

    $indentation = $indentation = [
      '#theme' => 'indentation',
      '#size' => $depth,
    ];

    $id_prefix = '';

//     if (!$variation->get('field_product_photo')->isEmpty()) {
//       $product_image = $variation->get('field_product_photo');

//       $id_prefix_rendable = $product_image->view([
//         'settings' => [
//           'image_style' => 'thumbnail_retina'
//         ]
//       ]);

//       foreach (Element::children($id_prefix_rendable) as $key) {
//         if ($key != 0) {
//           unset($id_prefix_rendable[$key]);
//         }
//       }

//       $id_prefix_rendable['#label_display'] = 'hidden';

//       $id_prefix = \Drupal::service('renderer')->render($id_prefix_rendable);

//     }

    $variation_row = [
      '#entity' => $variation,
      'id' => [
        'data' => [
          '#markup' => $id_prefix . ' #' . (!$variation->isNew() ? $variation->id() : ' <span class="marker">' . $this->t('New') . '</span>') ,
          'prefix' => [
            '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
          ]
        ],
        '#wrapper_attributes' => [
          'class' => [
            'cell--id'
          ],
        ]
      ],
    ];

    if ($elemets_list) {
      foreach ($elemets_list as $name => $options) {
        if ($name == 'translation' || !$variation->hasField($name)) continue;

        $f = [
          '#parents' => [],
        ];
        $widget = $display->getRenderer($name);
        $items = $variation->get($name);
        $items->filterEmptyItems();
        $field_element = [
          '#required' => (!empty($field_definitions[$name]) && $field_definitions[$name]->isRequired()) ? TRUE : FALSE,
        ];

        $variation_row[$name] = $widget->formElement($items, 0, $field_element, $f, $form_state);
//         $variation_row[$name] = $widget->form($items, $f, $form_state);
//         $variation_row[$name]['#title_display'] = 'hidden';

//         dpm($variation_row[$name]);

        if ($field_definitions[$name]->getSetting('variation_level') !== NULL) {
          $variation_row[$name]['#visible_for_level'] = $field_definitions[$name]->getSetting('variation_level');
        }

        //            $element['variations_table'][$delta][$name] = $widget->form($items, $form, $form_state);
        if (!empty($variation_row[$name]['value'])) {
          $variation_row[$name]['value']['#title_display'] = 'attribute';
        }
      }
    }

    $variation_row['weight_and_relations']['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight for added term'),
      '#title_display' => 'invisible',
      '#default_value' => $variation->getWeight(),
      '#attributes' => [
        'class' => ['variation-delta'],
      ],
    ];
    $variation_row['weight_and_relations']['id'] = [
      '#type' => 'hidden',
      '#value' => $variation->id(),
      '#attributes' => [
        'class' => ['id'],
      ],
    ];
    $variation_row['weight_and_relations']['parent'] = [
      '#type' => 'hidden',
      // Yes, default_value on a hidden. It needs to be changeable by the
      // javascript.
      '#default_value' => $variation->getParentId(),
      '#attributes' => [
        'class' => ['parent'],
      ],
    ];

    $variation_row['weight_and_relations']['depth'] = [
      '#type' => 'hidden',
      // Same as above, the depth is modified by javascript, so it's a
      // default_value.
      '#default_value' => $variation->getLevel() - 1,
      '#attributes' => [
        'class' => ['depth'],
      ],
    ];

    $variation_row['operations']['links'] = [
      '#type' => 'operations',
      '#links' => $operations,
    ];

//     $variation_row['operations']['remove'] = [
//       '#type' => 'submit',
//       '#name' => 'remove_row_' . $delta,
//       '#value' => t('Remove'),
//       '#action' => 'remove_row',
//       '#row_delta' => $delta,
//       '#ajax' => [
//         'callback' => 'bs_product_inline_variation_entity_form_get_element',
//         'wrapper' => 'variations-container',
//       ],
//       '#submit' => ['bs_product_remove_variation_from_variations_list'],
//       '#attributes' => [
//         'class' => [
//           'button--remove-variation'
//         ]
//       ]
//     ];

    $variation_row['#attributes']['class'][] = 'draggable';

    return $variation_row;

  }

//   /**
//    * Gets the target bundles for the current field.
//    *
//    * @return string[]
//    *   A list of bundles.
//    */
//   protected function getTargetBundles() {
//     $settings = $this->getFieldSettings();
//     if (!empty($settings['handler_settings']['target_bundles'])) {
//       $target_bundles = array_values($settings['handler_settings']['target_bundles']);
//       // Filter out target bundles which no longer exist.
//       $existing_bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo($settings['target_type']));
//       $target_bundles = array_intersect($target_bundles, $existing_bundles);
//     }
//     else {
//       // If no target bundles have been specified then all are available.
//       $target_bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo($settings['target_type']));
//     }

//     return $target_bundles;
//   }

  /**
   * Prepares the form state for the current widget.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values.
   * @param bool $translating
   *   Whether there's a translation in progress.
   */
  protected function prepareFormState($form, FormStateInterface $form_state, FieldItemListInterface $items) {
    $widget_state = $form_state->get(['variations']);
    if (empty($widget_state)) {
      $widget_state = [
        'last_new_id' => 0,
        'last_root_weight' => 0,
        'delete' => [],
        'variation_entities' => [],
      ];

      // Store the $items entities in the widget state, for further manipulation.
      foreach ($items->getEntity()->getVariations(TRUE) as $delta => $variation) {
        $widget_state['variation_entities'][$delta] = $variation;
      }
      $form_state->set(['variations'], $widget_state);
    }

  }

}

