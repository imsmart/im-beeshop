<?php
namespace Drupal\bs_product\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "bs_referenced_product_ac",
 *   label = @Translation("Product AC"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "bs_referenced_product"
 *   }
 * )
 */
class ProductReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {


}

