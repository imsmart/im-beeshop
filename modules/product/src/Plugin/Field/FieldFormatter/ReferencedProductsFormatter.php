<?php
namespace Drupal\bs_product\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "referenced_products_by_view",
 *   label = @Translation("Referenced products (by view)"),
 *   field_types = {
 *     "bs_referenced_product"
 *   }
 * )
 */
class ReferencedProductsFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
      );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $pids = [];

    foreach ($items->getValue() as $value) {
      $pids[] = $value['target_id'];
    }

    if (!$pids) return [];

    $element = [
      '#type' => 'view',
      '#prefix' => '<div class="referenced-products">',
      '#suffix' => '</div>',
      '#name' => 'products',
      '#display_id' => 'block_referenced_products',
      '#arguments' => [implode('+', $pids)],
      '#embed' => TRUE,
      '#parent_entity' => $items->getEntity(),
      '#field_items' => $items,
    ];

    return $element;
  }

}

