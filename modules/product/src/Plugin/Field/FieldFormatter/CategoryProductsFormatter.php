<?php

namespace Drupal\bs_product\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "category_products",
 *   label = @Translation("Category products"),
 *   field_types = {
 *     "category_products"
 *   }
 * )
 */
class CategoryProductsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['view_display'] = NULL;
    return $settings;
  }

  /**
   * Helper function to get all display ids.
   */
  protected function getAllViewsDisplayOptions() {
    $views = Views::getAllViews();
    $options = [];
    foreach ($views as $view) {

      if ($view->get('base_table') != 'bs_product_field_data')
        continue;

      foreach ($view->get('display') as $display) {
        $options[$view->label()][$view->id() . ':' . $display['id']] = $display['display_title'];
      }
    }
    return $options;
  }

  /**
  * {@inheritdoc}
  */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['view_display'] = [
      '#title' => 'Views display',
      '#type' => 'select',
      '#options' => $this->getAllViewsDisplayOptions(),
      '#default_value' => $this->getSetting('view_display')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    list($view_id, $display_id) = explode(':', $this->getSetting('view_display') ?: 'products:block_category_products');

    $element = [];

    $entity = $items->getEntity();
    $element = [];

    $cacheable_metadata = new CacheableMetadata();
//     $cacheable_metadata->addCacheContexts(['user', 'session']);

    //$element[0] = views_embed_view($view_name, $views_display, $entity->id());

    $category_products_element = [
      '#type' => 'view',
      '#name' => $view_id,
      '#display_id' => $display_id,
      '#arguments' => [$entity->id()],
      '#embed' => TRUE,
      '#cache' => [
        'contexts' => $cacheable_metadata->getCacheContexts(),
        'tags' => $cacheable_metadata->getCacheTags(),
        'max-age' => $cacheable_metadata->getCacheMaxAge(),
      ]
//       '#lazy_builder' => [static::class . '::lazyBuild', [$entity->id(), $view_id, $display_id]],
//       '#create_placeholder' => TRUE,
    ];

    $element[0] = $category_products_element;

    //cache_context.shop
    $element[0]['#cache']['contexts'][] = 'bs_shop';//\Drupal::service('cache_context.shop')->getContext();
//     dpm($element);

    $element['#attributes'] = [
      'id' => 'category-products'
    ];

    return $element;
  }

  public static function lazyBuild($cid, $view_id, $display_id) {
    $element = [
      '#type' => 'view',
      '#name' => $view_id,
      '#display_id' => $display_id,
      '#arguments' => [$cid],
      '#embed' => TRUE,
//       '#cache' => [
//           'contexts' => $cacheable_metadata->getCacheContexts(),
//         'tags' => $cacheable_metadata->getCacheTags(),
//         'max-age' => $cacheable_metadata->getCacheMaxAge(),
//       ]
    ];

//     $r = render($element);
$r = '*******';
    return [
      '#markup' => $r,
    ];
  }

}
