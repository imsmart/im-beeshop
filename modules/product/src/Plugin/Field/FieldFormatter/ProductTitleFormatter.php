<?php

namespace Drupal\bs_product\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "product_title",
 *   label = @Translation("Product title"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class ProductTitleFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
    );
  }

//   /**
//    * Constructs a new LinkFormatter.
//    *
//    * @param string $plugin_id
//    *   The plugin_id for the formatter.
//    * @param mixed $plugin_definition
//    *   The plugin implementation definition.
//    * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
//    *   The definition of the field to which the formatter is associated.
//    * @param array $settings
//    *   The formatter settings.
//    * @param string $label
//    *   The formatter label display setting.
//    * @param string $view_mode
//    *   The view mode.
//    * @param array $third_party_settings
//    *   Third party settings.
//    * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
//    *   The path validator service.
//    */
//   public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PathValidatorInterface $path_validator) {
//     parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
//     $this->pathValidator = $path_validator;
//   }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'wrapper_tag' => 'h2',
      'classes' => '',
      'id_attribute' => '',
      'display_as_link' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['wrapper_tag'] = [
      '#type' => 'select',
      '#title' => t('Wrapper tag'),
      '#default_value' => $this->getSetting('wrapper_tag'),
      '#options' => array(
        'h1' => 'h1',
        'h2' => 'h2',
        'h3' => 'h3',
        'h4' => 'h4',
        'h5' => 'h5',
        'h6' => 'h6',
        'span' => 'span',
        'div' => 'div'
      ),
      '#description' => t('Choose the HTML element to wrap around this field, e.g. H1, H2, etc..'),
    ];
    $elements['classes'] = [
      '#type' => 'textfield',
      '#title' => t('CSS classes'),
      '#default_value' => $this->getSetting('classes'),
      '#description' => t('Define custom CSS classes for this field'),
    ];
    $elements['id_attribute'] = [
      '#type' => 'textfield',
      '#title' => t('ID'),
      '#default_value' => $this->getSetting('id_attribute'),
      '#description' => t('Define ID attrbute for this field'),
    ];

    $elements['display_as_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as link to product page'),
      '#default_value' => $this->getSetting('display_as_link'),
    ];

    return $elements;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function settingsSummary() {
//     $summary = [];

//     $settings = $this->getSettings();

//     if (!empty($settings['trim_length'])) {
//       $summary[] = t('Link text trimmed to @limit characters', ['@limit' => $settings['trim_length']]);
//     }
//     else {
//       $summary[] = t('Link text not trimmed');
//     }
//     if ($this->getPluginId() == 'link' && !empty($settings['url_only'])) {
//       if (!empty($settings['url_plain'])) {
//         $summary[] = t('Show URL only as plain-text');
//       }
//       else {
//         $summary[] = t('Show URL only');
//       }
//     }
//     if (!empty($settings['rel'])) {
//       $summary[] = t('Add rel="@rel"', ['@rel' => $settings['rel']]);
//     }
//     if (!empty($settings['target'])) {
//       $summary[] = t('Open link in new window');
//     }

//     return $summary;
//   }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $entity = $items->getEntity();
    $settings = $this->getSettings();

    $element = [];

    $element[0] = [
      '#theme' => 'bs_product_title',
      '#title' => $entity->getTitle(),
      '#settings' => $settings,
    ];

    if (!empty($settings['display_as_link'])) {
      $element[0]['#url'] = $entity->toUrl();
    }

    return $element;
  }

}
