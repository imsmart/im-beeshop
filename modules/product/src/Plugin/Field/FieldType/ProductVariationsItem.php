<?php
namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\bs_product\ProductVariationsItemInreface;
use Drupal\beeshop\BeeshopComptutedFieldItemBase;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'product_variations' entity field type.
 *
 * @FieldType(
 *   id = "bs_product_variations",
 *   label = @Translation("Product variations"),
 *   description = @Translation("An bs_product entity field containing an variations reference."),
 *   category = @Translation("Product variations"),
 *   default_widget = "bs_product_variations_form",
 *   list_class = "\Drupal\bs_product\ProductVariationsFieldItemList",
 *   no_ui = TRUE,
 * )
 */
class ProductVariationsItem extends EntityReferenceItem implements ProductVariationsItemInreface {

}

