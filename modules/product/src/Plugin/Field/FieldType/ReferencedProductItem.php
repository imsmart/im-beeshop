<?php
namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'product_variations' entity field type.
 *
 * @FieldType(
 *   id = "bs_referenced_product",
 *   label = @Translation("Referenced product"),
 *   description = @Translation("Field containing an reference to product(s)."),
 *   category = @Translation("Product"),
 *   default_widget = "bs_referenced_product_ac",
 *   list_class = "\Drupal\bs_product\ProductReferenceFieldItemList",
 * )
 */
class ReferencedProductItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'bs_product',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {

    $entity_types = \Drupal::entityTypeManager()->getDefinitions();

    $options['bs_product'] = [
      'label' => $entity_types['bs_product']->getLabel(),
      'field_storage_config' => [
        'settings' => [
          'target_type' => $entity_types['bs_product']->id(),
        ]
      ]
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form =  [];
    return $form;
  }

}

