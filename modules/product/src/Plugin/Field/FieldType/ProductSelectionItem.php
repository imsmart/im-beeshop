<?php

namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'product_selection' field type.
 *
 * @FieldType(
 *   id = "product_selection",
 *   label = @Translation("Product selections"),
 *   no_ui = TRUE
 * )
 */
class ProductSelectionItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['plugin_id'] = DataDefinition::create('string')
      ->setLabel(t('Selection plugin'))
      ->setRequired(TRUE);

    $properties['plugin_settings'] = MapDataDefinition::create()
      ->setLabel(t('Plugin settings'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function mainPropertyName() {
    return 'plugin_id';
  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::schema()
   */
  public static function schema(\Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::generateSampleValue()
   */
  public static function generateSampleValue(\Drupal\Core\Field\FieldDefinitionInterface $field_definition) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::defaultStorageSettings()
   */
  public static function defaultStorageSettings() {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::defaultFieldSettings()
   */
  public static function defaultFieldSettings() {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::storageSettingsToConfigData()
   */
  public static function storageSettingsToConfigData(array $settings) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::storageSettingsFromConfigData()
   */
  public static function storageSettingsFromConfigData(array $settings) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::fieldSettingsToConfigData()
   */
  public static function fieldSettingsToConfigData(array $settings) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::fieldSettingsFromConfigData()
   */
  public static function fieldSettingsFromConfigData(array $settings) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::calculateDependencies()
   */
  public static function calculateDependencies(\Drupal\Core\Field\FieldDefinitionInterface $field_definition) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::calculateStorageDependencies()
   */
  public static function calculateStorageDependencies(\Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\FieldItemInterface::onDependencyRemoval()
   */
  public static function onDependencyRemoval(\Drupal\Core\Field\FieldDefinitionInterface $field_definition, array $dependencies) {
      // TODO Auto-generated method stub

  }

    /**
   * {@inheritDoc}
   * @see \Drupal\Core\TypedData\TypedDataInterface::createInstance()
   */
  public static function createInstance($definition, $name = NULL, \Drupal\Core\TypedData\TraversableTypedDataInterface $parent = NULL) {
      // TODO Auto-generated method stub

  }

}

