<?php
namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\bs_product\ProductTitleItemIterface;
use Drupal\beeshop\BeeshopComptutedFieldItemBase;

/**
 * Plugin implementation of the 'product title' field type.
 *
 * @FieldType(
 *   id = "product_titile",
 *   label = @Translation("Product title"),
 *   description = @Translation("Display product title"),
 *   default_formatter = "product_title",
 *   no_ui = TRUE,
 * )
 */
class ProductTitleItem extends BeeshopComptutedFieldItemBase implements ProductTitleItemIterface {
}

