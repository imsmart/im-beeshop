<?php
namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;


/**
 * Plugin implementation of the 'list_string' field type.
 *
 * @FieldType(
 *   id = "product_unav_reasons",
 *   label = @Translation("Product unavailability reasons"),
 *   no_ui = TRUE,
 *   list_class = "Drupal\bs_product\ProductUnavailabilityReasonsItemList",
 * )
 */
class ProductUnavailabilityReasonsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['source'] = DataDefinition::create('string')
      ->setLabel(t('Source'))
      ->setRequired(FALSE);

    $properties['reason'] = DataDefinition::create('string')
      ->setLabel(t('Reason description'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'source' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'reason' => [
          'type' => 'text',
          'size' => 'big',
        ],
      ],
    ];
  }

}

