<?php
namespace Drupal\bs_product\Plugin\Field\FieldType;

use Drupal\bs_product\CategoryProductsItemInterface;
use Drupal\beeshop\BeeshopComptutedFieldItemBase;

/**
 * Plugin implementation of the 'link' field type.
 *
 * @FieldType(
 *   id = "category_products",
 *   label = @Translation("Category products"),
 *   description = @Translation("Display category products"),
 *   default_formatter = "category_products",
 *   no_ui = TRUE,
 * )
 */
class CategoryProductsItem extends BeeshopComptutedFieldItemBase implements CategoryProductsItemInterface {

}

