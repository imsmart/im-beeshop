<?php
namespace Drupal\bs_product\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity Reference Selection with distinguishing field.
 *
 * @EntityReferenceSelection(
 *   id = "real_items:bs_product",
 *   label = @Translation("Example"),
 *   group = "bs_products",
 * )
 */
class ProductRealItemsSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $target_type = $this->configuration['target_type'];

    $query = $this->buildEntityQuery(NULL, $match_operator);

    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $title_or_sku_cond = $query->orConditionGroup()
    ->condition('title', $match, $match_operator)
    ->condition('sku', $match, $match_operator);

    $query->condition($title_or_sku_cond);

    $or_cond = $query->orConditionGroup()
    ->condition('is_group', 0)
    ->condition('is_group', NULL, 'IS NULL');

    $query->condition($or_cond);

    if (!empty($this->configuration['handler_settings']['exclude_pids'])) {
      $query->condition('pid', $this->configuration['handler_settings']['exclude_pids'], 'NOT IN');
    }

    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    $options = [];
    $entities = $this->entityManager->getStorage($target_type)->loadMultiple($result);

    foreach ($entities as $entity_id => $entity) {

      if (!empty($this->configuration['handler_settings']['only_available']) && !$entity->isAvailable())
        continue;

      $bundle = $entity->bundle();
      $entity_translation = $this->entityManager->getTranslationFromContext($entity);
      $options[$bundle][$entity_id] = Html::escape($entity_translation->getTitleForAC());
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

}

