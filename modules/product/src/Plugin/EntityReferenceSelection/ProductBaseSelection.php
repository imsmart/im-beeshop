<?php
namespace Drupal\bs_product\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Default Products Entity Reference Selection.
 *
 * @EntityReferenceSelection(
 *   id = "base:bs_product",
 *   label = @Translation("Products default entity reference selection"),
 *   group = "bs_products",
 * )
 */
class ProductBaseSelection extends DefaultSelection {
}

