<?php
namespace Drupal\bs_product\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity Reference Selection with distinguishing field.
 *
 * @EntityReferenceSelection(
 *   id = "default:bs_product",
 *   label = @Translation("Example"),
 *   group = "default",
 * )
 */
class ProductDefaultSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $target_type = $this->configuration['target_type'];

    $query = $this->buildEntityQuery($match, $match_operator, $limit);

//     $query = $this->entityManager->getStorage($target_type)->getQuery();

    $title_or_sku_cond = $query->orConditionGroup()
    ->condition('title', $match, $match_operator)
    ->condition('sku', $match, $match_operator);

    $query->condition($title_or_sku_cond);

//     $or_cond = $query->orConditionGroup()
//     ->condition('is_group', 0)
//     ->condition('is_group', NULL, 'IS NULL');

//     $query->condition($or_cond);

    $query->addTag('product_default_selection');

    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    $options = [];
    $entities = $this->entityManager->getStorage($target_type)->loadMultiple($result);
    foreach ($entities as $entity_id => $entity) {
      $bundle = $entity->bundle();
      $entity_translation = $this->entityManager->getTranslationFromContext($entity);
      $options[$bundle][$entity_id] = Html::escape($entity_translation->label() . ' (#' . $entity_translation->getSKU() . ')');
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

}

