<?php

namespace Drupal\bs_product;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\bs_product\Entity\ProductsCategoryInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a custom taxonomy breadcrumb builder that uses the term hierarchy.
 */
class ProductAndCategoryBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The category storage.
   *
   * @var \Drupal\bs_product\ProductsCategoryStorageInterface
   */
  protected $categoryStorage;

  /**
   * The entity which page is triggered by the builder
   *
   * @var EntityInterface
   */
  protected $appliedFor;

  /**
   * Constructs the TermBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
    $this->categoryStorage = $entityManager->getStorage('bs_products_category');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $result = FALSE;
    $entity = FALSE;

    switch ($route_match->getRouteName()) {
      case 'entity.bs_products_category.canonical':
        $entity = $route_match->getParameter('bs_products_category');
        if ($entity instanceof ProductsCategoryInterface) {
          $result = TRUE;
        }
        break;
      case 'entity.bs_product.canonical':
        $entity = $route_match->getParameter('bs_product');
        if ($entity instanceof ProductInterface && !$entity->get('categories')->isEmpty()) {
          $result = TRUE;
        }
        break;
    }

    if ($result) {
      $this->appliedFor = $entity;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();

    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    $current_category = $this->appliedFor instanceof ProductsCategoryInterface ?
                        $this->appliedFor :
                        $this->appliedFor->getMainCategory();

    $breadcrumb->addCacheContexts(['route']);

    if (!$current_category) return $breadcrumb;

    $current_category = $this->entityManager->getTranslationFromContext($current_category);
    $parent = $current_category->getParent();

    $links = [];

    while ($parent) {
      $breadcrumb->addCacheableDependency($parent);
      array_unshift($links, Link::createFromRoute($parent->getName(), 'entity.bs_products_category.canonical', ['bs_products_category' => $parent->id()]));
      $parent = $parent->getParent();
    }

    foreach ($links as $link) {
      $breadcrumb->addLink($link);
    }

    if ($this->appliedFor instanceof ProductInterface) {
      $breadcrumb->addLink(Link::createFromRoute($current_category->getName(), 'entity.bs_products_category.canonical', ['bs_products_category' => $current_category->id()]));
    }

    return $breadcrumb;
  }

}
