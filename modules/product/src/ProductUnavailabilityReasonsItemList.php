<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\FieldItemList;

class ProductUnavailabilityReasonsItemList extends FieldItemList {

  public function setReason($source, $reason) {
    foreach ($this->list as $item) {
      if ($item->source == $source) {
        $item->reason = $reason;
        return;
      }
    }

    $this->appendItem([
      'source' => $source,
      'reason' => $reason,
    ]);
  }

  public function removeReasonBySource($source) {
    foreach ($this->list as $index => $item) {
      if ($item->source == $source) {
        $this->removeItem($index);
        return;
      }
    }
  }

}

