<?php

namespace Drupal\bs_product;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\Entity\ProductInterface;

/**
 * Defines the access control handler for the node entity type.
 *
 * @see \Drupal\node\Entity\Node
 * @ingroup node_access
 */
class ProductAccessControlHandler extends EntityAccessControlHandler implements ProductAccessControlHandlerInterface, EntityHandlerInterface {

//   /**
//    * The node grant storage.
//    *
//    * @var \Drupal\node\NodeGrantDatabaseStorageInterface
//    */
//   protected $grantStorage;

//   /**
//    * Constructs a NodeAccessControlHandler object.
//    *
//    * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
//    *   The entity type definition.
//    * @param \Drupal\node\NodeGrantDatabaseStorageInterface $grant_storage
//    *   The node grant storage.
//    */
//   public function __construct(EntityTypeInterface $entity_type, ProductGrantDatabaseStorageInterface $grant_storage) {
//     parent::__construct($entity_type);
//     $this->grantStorage = $grant_storage;
//   }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type
//       $container->get('product.grant_storage')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    /**
     * @var \Drupal\bs_product\Entity\ProductInterface $entity
     */

    $account = $this->prepareUser($account);

    if ($account->hasPermission('bypass product access')) {
      $result = AccessResult::allowed()->cachePerPermissions();
      return $return_as_object ? $result : $result->isAllowed();
    }

    // Check acces for product categories first
    $categories = $entity->get('categories')->referencedEntities();
    $acces_by_category = FALSE;

    foreach ($categories as $category) {
      /**
       * @var ProductsCategoryInterface $category
       */
      if ($cat_access = $category->access($operation, $account, $return_as_object)) {
        $acces_by_category = $return_as_object ? $cat_access->isAllowed() : $cat_access;
        if ($acces_by_category) {
          break;
        }
      }

      if (!$acces_by_category) {
        $result = AccessResult::forbidden();
        return $return_as_object ? $result : FALSE;
      }
    }
    //

    if (!$account->hasPermission('access content')) {
      $result = AccessResult::forbidden("The 'access products' permission is required.")->cachePerPermissions();

      if ($result->isAllowed() && ($parent = $entity->getParent())) {
        $result = $parent->access($operation, $account, TRUE);
      }

      return $return_as_object ? $result : $result->isAllowed();
    }

    $result = parent::access($entity, $operation, $account, TRUE)->cachePerPermissions();

    if ($result->isAllowed() && ($parent = $entity->getParent())) {
      $result = $parent->access($operation, $account, TRUE);
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function createAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = [], $return_as_object = FALSE) {
//     $account = $this->prepareUser($account);

//     if ($account->hasPermission('bypass node access')) {
//       $result = AccessResult::allowed()->cachePerPermissions();
//       return $return_as_object ? $result : $result->isAllowed();
//     }
//     if (!$account->hasPermission('access content')) {
//       $result = AccessResult::forbidden()->cachePerPermissions();
//       return $return_as_object ? $result : $result->isAllowed();
//     }

//     $result = parent::createAccess($entity_bundle, $account, $context, TRUE)->cachePerPermissions();
//     return $return_as_object ? $result : $result->isAllowed();
//   }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $product, $operation, AccountInterface $account) {
    /** @var \Drupal\bs_product\Entity\ProductInterface $product */
    // Fetch information from the node object if possible.
    $status = $product->isPublished();
    $uid = $product->getOwnerId();

    // Check if authors can view their own unpublished nodes.
    if ($operation === 'view' && !$status && $account->hasPermission('view own unpublished products') && $account->isAuthenticated() && $account->id() == $uid) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($product);
    }

    // Evaluate node grants.
    return AccessResult::neutral();
  }

//   /**
//    * {@inheritdoc}
//    */
//   protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
//     return AccessResult::allowedIf($account->hasPermission('create ' . $entity_bundle . ' content'))->cachePerPermissions();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
//     // Only users with the administer nodes permission can edit administrative
//     // fields.
//     $administrative_fields = ['uid', 'status', 'created', 'promote', 'sticky'];
//     if ($operation == 'edit' && in_array($field_definition->getName(), $administrative_fields, TRUE)) {
//       return AccessResult::allowedIfHasPermission($account, 'administer nodes');
//     }

//     // No user can change read only fields.
//     $read_only_fields = ['revision_timestamp', 'revision_uid'];
//     if ($operation == 'edit' && in_array($field_definition->getName(), $read_only_fields, TRUE)) {
//       return AccessResult::forbidden();
//     }

//     // Users have access to the revision_log field either if they have
//     // administrative permissions or if the new revision option is enabled.
//     if ($operation == 'edit' && $field_definition->getName() == 'revision_log') {
//       if ($account->hasPermission('administer nodes')) {
//         return AccessResult::allowed()->cachePerPermissions();
//       }
//       return AccessResult::allowedIf($items->getEntity()->type->entity->isNewRevision())->cachePerPermissions();
//     }
//     return parent::checkFieldAccess($operation, $field_definition, $account, $items);
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function acquireGrants(ProductInterface $product) {
//     $grants = $this->moduleHandler->invokeAll('node_access_records', [$product]);
//     // Let modules alter the grants.
//     $this->moduleHandler->alter('node_access_records', $grants, $product);
//     // If no grants are set and the node is published, then use the default grant.
//     if (empty($grants) && $product->isPublished()) {
//       $grants[] = ['realm' => 'all', 'gid' => 0, 'grant_view' => 1, 'grant_update' => 0, 'grant_delete' => 0];
//     }
//     return $grants;
//   }


//   /**
//    * {@inheritdoc}
//    */
//   public function writeDefaultGrant() {
//     $this->grantStorage->writeDefault();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function deleteGrants() {
//     $this->grantStorage->delete();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function countGrants() {
//     return $this->grantStorage->count();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function checkAllGrants(AccountInterface $account) {
//     return $this->grantStorage->checkAll($account);
//   }

}
