<?php
namespace Drupal\bs_product;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\views\EntityViewsData;

class ProductViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bs_product_field_data']['bs_products_category_index_cid_depth'] = [
      'help' => t('Display products if it refers to category, or to children of the selected category.'),
      'real field' => 'root_parent',
      'argument' => [
        'title' => t('Refers to category (with depth)'),
        'id' => 'bs_products_category_index_cid_depth',
        'accept depth modifier' => TRUE,
      ],
    ];

    $data['bs_product_field_data']['product_category'] = [
      'filter' => [
        'title' => t('Category'),
        'id' => 'product_category',
      ],
    ];

    $data['bs_product_field_data']['product_in_category_weight'] = [
      'help' => t('Product in category weight.'),
      'real field' => 'root_parent',
      'sort' => [
        'title' => t('Product weight in category'),
        'id' => 'bs_product_in_category_weight_sort',
      ],
    ];

    $data['bs_product_field_data']['table']['base']['query_id'] = 'bs_products_views_query';

    $data['bs_product_field_data']['discount_value']['field'] = [
      'title' => t('Discount value'),
      'help' => t('Display product discount value.'),
      'id' => 'bs_product_discount_value',
    ];

    $data['bs_product_field_data']['bs_product_variations_count']['field'] = [
      'title' => t('Variations count'),
      'help' => t('Display product variations count.'),
      'id' => 'bs_product_variations_count',
    ];

    //
    $data['bs_product_field_data']['bs_product_is_available'] = [
      'title' => $this->t('Is available'),
      'help' => $this->t('Indicate product availability.'),
      'field' => [
        'id' => 'bs_product_is_available',
      ],
    ];

    //price_is_discounted
    $data['bs_product_field_data']['price_is_discounted'] = [
      'title' => $this->t('Product price is discounted'),
      'help' => $this->t('Indicate that product price is discounted'),
      'field' => [
        'id' => 'field',
        'default_formatter' => 'boolean',
        'field_name' => 'price_is_discounted',
      ],
    ];

    $data['bs_product__price']['price_value']['sort']['id'] = 'product_price_sort';
    $data['bs_product__price']['price_value']['filter']['id'] = 'product_price_filter';
    $data['bs_product__price']['price__price_type']['filter']['id'] = 'price_type';

//     dpm($data);

    return $data;
  }

  /**
   * Processes the views data for an entity reference field.
   *
   * @param string $table
   *   The table the language field is added to.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $views_field
   *   The views field data.
   * @param string $field_column_name
   *   The field column being processed.
   */
  protected function processViewsDataForBsPrice($table, FieldDefinitionInterface $field_definition, array &$views_field, $field_column_name) {
    switch ($field_column_name) {
      case 'value':
        $views_field['field']['id'] = 'bs_price';
        break;
    }
  }

}

