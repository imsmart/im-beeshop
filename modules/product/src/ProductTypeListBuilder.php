<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\EntityInterface;
use Drupal\beeshop\HierarchicalDraggableListBuilder;
use Drupal\Core\Routing\RedirectDestinationTrait;

class ProductTypeListBuilder extends HierarchicalDraggableListBuilder {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => $this->t('Name'),
      'variationsSupport' => $this->t('Support variations'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_product_type_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['name']['data']['label_markup']['#markup'] = $entity->label();
    $row['variationsSupport']['data']['label_markup']['#markup'] = $entity->isVariationsSuport() ? '+' : '-';

//     $row = [
//       'name' => $entity->label(),
//       'variationsSupport' => $entity->isVariationsSuport() ? '+' : '-',
//     ];

//     if ($entity->isVariationsSuport()) {
//       $row['operations']['data']['#links']['manage-variation-structure'] = [
//         'title' => $this->t('Manage variations structure'),
//         'url' => $entity->toUrl('variations-structure'),
//         'weight' => 30,
//       ];
//     }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritDoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->hasLinkTemplate('variations-structure-form') && $entity->isVariationsSuport()) {
      $operations['variations_structure'] = [
        'title' => $this->t('Manage variations structure'),
        'weight' => 20,
        'url' => $this->ensureDestination($entity->toUrl('variations-structure-form')),
      ];
    }

    return $operations;
  }

}

