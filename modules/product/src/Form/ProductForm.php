<?php
namespace Drupal\bs_product\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\bs_product\Entity\ProductInterface;

class ProductForm extends ContentEntityForm {

  /**
   * @var unknown
   */
  protected $categoriesStorage;

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $productFieldDefinitions;

  protected $productType;

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, AccountInterface $current_user) {
    parent::__construct($entity_manager, $entity_type_bundle_info, $time);
    $this->categoriesStorage = $this->entityManager->getStorage('bs_products_category');
    $this->entityTypeManager = $this->entityTypeManager ? $this->entityTypeManager : \Drupal::service('entity_type.manager');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    $actions = parent::actions($form, $form_state);

    $actions['submit']['#dropbutton'] = 'save';

    $actions['submit_and_reload_form'] = $actions['submit'];

    $actions['submit']['#submit'][] = '::setRedirectToProductPage';

    $actions['submit_and_reload_form']['#dropbutton'] = 'save';
    $actions['submit_and_reload_form']['#value'] = $this->t('Save and reload form');
    $actions['submit_and_reload_form']['#submit'][] = '::setRedirectToEditForm';

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user')
      );
  }



  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    /**
     * @var ProductInterface $product
     */
    $product = $this->entity;

    if ($product->hasField('variations') && $product->isNew()) {
      $product->set('pid', 0);
      $product->enforceIsNew();
    }

    if ($parent = $product->getParent()) {
      $link = \Drupal\Core\Link::fromTextAndUrl($parent->id(), $parent->toUrl('edit-form'));
      $form['warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [$this->t('This product is variation of other product "@pid"', ['@pid' => $link->toString()])]
        ],
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
        '#weight' => -100,
      ];
    }

    $rm = \Drupal::service('current_route_match');
    if ($level = $rm->getParameter('level')) {
      $product->set('level', $level);
    }

    $this->setActiveFormDisplay($form_state);

    $current_form_display = $this->getFormDisplay($form_state);

    $form = parent::form($form, $form_state);
    $form['#theme'] = ['bs_product_edit_form'];
    $form['#attached']['library'][] = 'bs_product/product_edit_form';

    $form['advanced'] = [
      '#type' => 'container',
      '#weight' => 99,
      '#attributes' => [
        'class' => [
          'entity-meta',
        ]
      ]
    ];

    $form['meta'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => $this->currentUser->hasPermission('administer beeshop products'),
    ];

    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $product->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$product->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];

    $form['meta']['result_av_state'] = [
      '#type' => 'item',
      '#title' => $this->t('Availability'),
      '#markup' => '',
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__av_state--result',
          'container-inline'
        ]
      ],
    ];

    $form['meta']['av_state'] = [
      '#type' => 'item',
      '#title' => $this->t('Availability state (by field)'),
      '#markup' => $this->entity->getAvailabilitySateLabel(),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__av_state',
          'container-inline'
        ]
      ],
    ];

    $form['meta']['created'] = [
      '#type' => 'item',
      '#title' => $this->t('Created'),
      '#markup' => !$product->isNew() ? format_date($product->getCreatedTime(), 'bs_date_time_output') : $this->t('Not saved yet'),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__created',
          'container-inline'
        ]
      ],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$product->isNew() ? format_date($product->getChangedTime(), 'bs_date_time_output') : $this->t('Not saved yet'),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__last-saved',
          'container-inline'
        ]
      ],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $product->getOwner()->getUsername(),
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__author',
          'container-inline'
        ]
      ],
    ];

    $form['catalog_settings'] = [
      '#type' => 'details',
      '#title' => t('Catalog settings'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['bs-product--product-form--catalog-settings'],
      ],
      '#weight' => 10,
      '#optional' => TRUE,
    ];

    $form['accounting'] = [
      '#type' => 'details',
      '#title' => t('Accounting'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['bs-product--product-form--accounting'],
      ],
      '#weight' => 10,
      '#optional' => TRUE,
    ];



//     if (!empty($form['categories'])) {
//       $form['categories']['#group'] = 'nomenclature_data';
//     }

//     if (!empty($form['sku'])) {
//       $form['sku']['#group'] = 'nomenclature_data';
//     }

//     dpm($form);

    $form['price_group'] = [
      '#type' => 'details',
      '#title' => t('Product price'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['bs-product--product-form--price'],
      ],
      '#attached' => [
        //'library' => ['node/drupal.node'],
      ],
      '#weight' => 10,
      '#optional' => TRUE,
    ];

    if (!empty($form['price'])) {
      $form['price']['#group'] = 'price_group';
    }

    if (!empty($form['inh_price_from_parent'])) {
      $form['inh_price_from_parent']['#group'] = 'price_group';
      if (!$product->hasParent()) {
        $form['inh_price_from_parent']['#access'] = FALSE;
      }
    }

//     $form['catlog__settings'] = [
//       '#type' => 'details',
//       '#title' => t('Visibility settings'),
//       '#open' => TRUE,
//       '#group' => 'advanced',
//       '#access' => !empty($form['shops']['#access']),
//       '#attributes' => [
//         'class' => ['product-visibility-settings'],
//       ],
//       '#weight' => 30,
//     ];

    $form['author'] = [
      '#type' => 'details',
      '#title' => t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['bs-product--product-form--author'],
      ],
      '#attached' => [
        //'library' => ['node/drupal.node'],
      ],
      '#weight' => 150,
      '#optional' => TRUE,
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    if (isset($form['shops'])) {
      $form['shops']['#group'] = 'accounting';
    }

    if (isset($form['categories'])) {
      $form['categories']['#group'] = 'catalog_settings';
    }


    $form['variations_container'] = [
      '#type' => 'container',
      '#weight' => 99,
      '#attributes' => [
        'class' => ['bs-product--product-variations-form-container']
      ]
    ];

    $form['footer'] = [
      '#type' => 'container',
      '#weight' => 99,
      '#attributes' => [
        'class' => ['bs-product-form-footer']
      ]
    ];

    $form['group_options'] = [
      '#type' => 'details',
      '#title' => t('Publication and availability options'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['bs-product-product-form-options'],
      ],
      '#attached' => [
        //'library' => ['node/drupal.node'],
      ],
      '#weight' => 300,
      '#optional' => TRUE,
    ];

    if (isset($form['status'])) {
      $form['status']['#group'] = 'group_options';
    }

    if (isset($form['promote'])) {
      $form['promote']['#group'] = 'group_options';
    }

    if (isset($form['sticky'])) {
      $form['sticky']['#group'] = 'group_options';
    }

    if (isset($form['av_state'])) {
      $form['av_state']['#group'] = 'group_options';
    }

    if (isset($form['path'])) {

      $form['path_settings'] = [
        '#type' => 'details',
        '#title' => t('URL path settings'),
        '#open' => !empty($form['path']['widget'][0]['alias']['#value']),
        '#group' => 'advanced',
        '#access' => !empty($form['path']['#access']) && $product->hasField('path') && $product->get('path')->access('edit'),
        '#attributes' => [
          'class' => ['path-form'],
        ],
        '#attached' => [
          'library' => ['path/drupal.path'],
        ],
        '#weight' => 90,
      ];
      $form['path']['#group'] = 'path_settings';

      $form['path']['#group'] = 'path_settings';
    }

    if (!empty($form['variations'])) {
      $form['variations']['#group'] = 'variations_container';
    }

//     dpm($this->getBaseFormId());

    if (!empty($form['type']['widget'])) {
      $form['type']['widget']['#element_validate'][] = [get_class($this), 'validateProductType'];
    }

    return $form;
  }

  public function variationForm(array $form, FormStateInterface $form_state) {

    $entity = $form['#variation'];
    $this->setEntity($entity);

    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'product_variation');

    if ($this->productType->isVariationsSuport()) {
      $variations_fields_hr = $this->productType->getVariationFieldsHierarchy();
      $product_level = $this->entity->get('level')->value;

      //
      $form_display->removeComponent('variations');

      foreach ($variations_fields_hr as $field_name => $field_data) {
        if ($product_level != $field_data['level']) {
          $form_display->removeComponent($field_name);
        }
        if ($product_level < $field_data['level']) {
          $field_st_def = $this->productFieldDefinitions[$field_name]->getFieldStorageDefinition();
          $field_st_def->setThirdPartySetting('originalCardinality', $field_st_def->getCardinality());
          $field_st_def->setCardinality(-1);
        }
      }

    }

    $form_display->buildForm($entity, $form, $form_state);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#name' => 'inline-variation-submit-save',
      '#ajax' => [
        'callback' => 'bs_product_inline_variation_entity_form_get_element',
        'wrapper' => 'variations-container',
      ],
      '#submit' => ['bs_product_inline_variation_entity_form_save_submit']
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  public static function validateProductType($element, FormStateInterface $form_state, array $form) {
//     dpm('validateProductType');
//     dpm($element);
//     dpm($form);
//     dpm($form_state->getFormObject()->getEntity()->get('type'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $product = $this->entity;
    $insert = $product->isNew();

    parent::save($form, $form_state);

    $product_link = $product->link($this->t('View'));
    $context = ['@type' => $product->bundle(), '%title' => $product->label(), 'link' => $product_link];
    $t_args = ['@type' => $product->getBundleEntity()->label(), '%title' => $product->link($product->label())];

    if ($insert) {
      $this->logger('content')->notice('@type: added %title.', $context);
      drupal_set_message(t('Product "%title" (@type) has been created.', $t_args));
    }
    else {
      $this->logger('content')->notice('@type: updated %title.', $context);
      drupal_set_message(t('Product "%title" (@type) has been updated.', $t_args));
    }

  }

  function setRedirectToProductPage(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect(
      'entity.bs_product.canonical',
      ['bs_product' => $this->entity->id()]
      );
  }

  function setRedirectToEditForm(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect(
      'entity.bs_product.edit_form',
      ['bs_product' => $this->entity->id()]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    parent::setEntity($entity);
    $this->productFieldDefinitions = $this->entityFieldManager->getFieldDefinitions('bs_product', $entity->bundle());
    $this->productType = $this->getBundleEntity();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {

    if ($this->operation == 'clone') {
      $new_entity = $this->entity->createDuplicate();
      $new_entity->setTitle($this->t('Clone of ') . $new_entity->getTitle());
      $this->entity = $new_entity;
    }

    parent::prepareEntity();

  }

  protected function setActiveFormDisplay(FormStateInterface $form_state) {

    /**
     * @var ProductInterface $product
     */
    $product = $this->entity;

    $change_display = FALSE;
    $current_form_display = $this->getFormDisplay($form_state);

    if ($product->hasField('variations')) {
      if ($product->getParentId() || $product->getLevel() > 0) {
        if ($current_form_display = EntityFormDisplay::collectRenderDisplay($this->entity, 'product_variation')) {
          $change_display = TRUE;
        }
      }

      $variations_fields_hr = $this->productType->getVariationFieldsHierarchy();
      $product_level = $this->entity->get('level')->value;
      foreach ($variations_fields_hr as $field_name => $field_data) {
        if ($product_level != $field_data['level']) {
          $current_form_display->removeComponent($field_name);
        }
        if ($product_level < $field_data['level']) {
          $field_st_def = $this->productFieldDefinitions[$field_name]->getFieldStorageDefinition();
          $field_st_def->setThirdPartySetting('bs_product', 'originalCardinality', $field_st_def->getCardinality());
          $field_st_def->setCardinality(-1);
        }
      }

    }

    if ($change_display) {
      $this->setFormDisplay($current_form_display, $form_state);
    }

  }
}

