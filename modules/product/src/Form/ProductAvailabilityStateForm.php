<?php
namespace Drupal\bs_product\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;

class ProductAvailabilityStateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\bs_product\Entity\ProductType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['av_flag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Indicate that product with this state is available for ordering'),
      '#default_value' => $this->entity->av_flag,
    ];

//     $form['label']['#weight'] = -10;
//     $form['machine_name']['#weight'] = -9;

//     $form['machine_name']['widget'][0]['value']['#type'] = 'machine_name';
// //     $form['machine_name']['widget'][0]['value']['#machine_name'] = [
// //       'exists' => '\Drupal\token_custom\Entity\TokenCustom::load',
// //     ];

//     $form['description'] = [
//       '#type' => 'textarea',
//       '#title' => $this->t('Product state description'),
//       '#default_value' => $this->entity->get('description')->value,
//     ];

    $form['add_to_cart_form_replacement_text_field'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Add to cart form replacement text'),
      '#default_value' => $this->entity->get('add_to_cart_form_replacement_text')->value,
      '#format' => $this->entity->get('add_to_cart_form_replacement_text_format')->value
    ];

//     $form['add_to_cart_form_description_text_field'] = [
//       '#type' => 'text_format',
//       '#title' => $this->t('Add to cart form replacement text'),
//       '#default_value' => $this->entity->get('add_to_cart_form_description_text')->value,
//       '#format' => $this->entity->get('add_to_cart_form_description_text_format')->value
//     ];

    return $this->protectIdElement($form);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function save(array $form, FormStateInterface $form_state) {

//   }

  protected function protectIdElement(array $form) {
    $entity = $this->getEntity();
    $id_key = $entity->getEntityType()->getKey('id');
    assert(isset($form[$id_key]));
    $element = &$form[$id_key];

    // Make sure the element is not accidentally re-enabled if it has already
    // been disabled.
    if (empty($element['#disabled'])) {
      $element['#disabled'] = !$entity->isNew();
    }
    return $form;
  }

}

