<?php
namespace Drupal\bs_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ProductCollectionOverviewForm extends FormBase {

  protected $store;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_products_collection_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $catgories_storage = \Drupal::entityTypeManager()->getStorage('bs_products_category');
    $products_storage = \Drupal::entityTypeManager()->getStorage('bs_product');

    $this->store = \Drupal::service('user.private_tempstore')->get('products_manage_form');

    $current_category = $this->store->get('category');

    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter'),
    ];

    $form['filters']['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => $catgories_storage->getCategoriesTreeAsOtionsForSelect(0),
      '#empty_option' => $this->t('Select category'),
      '#default_value' => $current_category ? $current_category : NULL,
    ];

    $form['filters']['actions'] = ['#type' => 'actions'];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Filter'),
      '#op' => 'filter',
      '#submit' => ['::setCategoryFilter'],
    ];


    if (!$current_category) {
      $form['products_lis']['#markup'] = 'Please select category';
    }
    else {

      $pids_select = \Drupal::database()->select('bs_product_field_data', 'p');

      $parent_group_conditions = $pids_select->conditionGroupFactory('OR')->condition('parent', 0)->isNull('parent');
      $pids_select->condition($parent_group_conditions);

      $pids_select->fields('p', ['pid', 'title', 'sku']);
      $pids_select->join('bs_products_category_index', 'ci', 'p.pid = ci.pid');
      $pids_select->fields('ci', ['weight', 'direct_rel'])
      ->condition('ci.cid', $current_category)
      ->orderBy('ci.weight');
//       ->range(0, 100);

      $pids = $pids_select->execute()->fetchAllAssoc('pid');

//       dpm(count($pids));

      if ($pids) {

        $form['products_lis']['products_table'] = [
          '#type' => 'bs_table_width_regions',
          '#header' => [$this->t('Name'), $this->t('Weight'), $this->t('Region')],
          '#regions' => $this->getRegions(),
          '#attributes' => [
            'id' => 'products',
          ],
        ];

        $form['products_lis']['products_table']['#tabledrag'] = [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'product-weight',
          ],
          [
            'action' => 'match',
            'relationship' => 'parent',
            'group' => 'region',
            'subgroup' => 'region',
            'source' => 'field-name',
          ],
        ];

        $delta = count($pids);

//         $products_sorted = [];

//         foreach ($products as $key => $product) {
//           $products_sorted[$key]['product'] = $product;
//           $products_sorted[$key]['weight'] = $pids[$key]->weight;
//         }

//         uasort($products_sorted, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

        foreach ($pids as $key => $product_data) {

            /** @var $term \Drupal\Core\Entity\EntityInterface */
          //$product = $product_data['product'];
          //$form['products_lis']['products_table'][$key]['#product'] = $product;

          $form['products_lis']['products_table'][$key]['#direct_rel'] = $product_data->direct_rel;
          $form['products_lis']['products_table'][$key]['#op'] = 'update';

          $form['products_lis']['products_table'][$key]['product'] = [
            '#type' => 'link',
            '#title' => $product_data->title . ' (#' . $product_data->sku . ')',
            '#url' => Url::fromRoute('entity.bs_product.canonical', ['bs_product' => $product_data->pid]),
          ];

          $form['products_lis']['products_table'][$key]['weight'] = [
              '#type' => 'weight',
              '#delta' => $delta,
              '#title' => $this->t('Weight for added category'),
              '#title_display' => 'invisible',
            '#default_value' => $product_data->weight,
              '#attributes' => [
                'class' => ['product-weight'],
              ],
            ];

          $form['products_lis']['products_table'][$key]['#region_callback'] = [$this, 'getRowRegion'];

          $form['products_lis']['products_table'][$key]['#attributes']['class'] = [];
          $form['products_lis']['products_table'][$key]['#attributes']['class'][] = 'draggable';
          $form['products_lis']['products_table'][$key]['region'] = [
            '#type' => 'select',
            //'#title' => $this->t('Region for @title', ['@title' => $label]),
            '#title_display' => 'invisible',
            '#options' => $this->getRegionOptions(),
            '#default_value' => 'active',
            '#attributes' => ['class' => ['region']],
          ];

        }


        if ($all_pids = $catgories_storage->getCategoryProductsIds($current_category, FALSE)) {
          //       dpm($all_pids);

          $in_childs_cats_pids_select = \Drupal::database()->select('bs_product_field_data', 'p');
          $in_childs_cats_pids_select->fields('p', ['pid', 'title', 'sku']);
          $parent_group_conditions = $in_childs_cats_pids_select->conditionGroupFactory('OR')->condition('parent', 0)->isNull('parent');
          $in_childs_cats_pids_select->condition($parent_group_conditions);
          $in_childs_cats_pids_select->condition('p.pid', $all_pids, 'IN');

          $in_childs_cats_pids = $in_childs_cats_pids_select->execute()->fetchAllAssoc('pid');

//           dpm($in_childs_cats_pids);

          foreach ($in_childs_cats_pids as $key => $product_data) {

            if (isset($pids[$key])) {
              continue;
            }

            /** @var $term \Drupal\Core\Entity\EntityInterface */
            //$product = $product_data['product'];
            //$form['products_lis']['products_table'][$key]['#product'] = $product;

            $form['products_lis']['products_table'][$key]['#direct_rel'] = 0;
            $form['products_lis']['products_table'][$key]['#op'] = 'insert';

            $form['products_lis']['products_table'][$key]['product'] = [
              '#type' => 'link',
              '#title' => $product_data->title . ' (#' . $product_data->sku . ')',
              '#url' => Url::fromRoute('entity.bs_product.canonical', ['bs_product' => $product_data->pid]),
            ];

            $form['products_lis']['products_table'][$key]['weight'] = [
              '#type' => 'weight',
              '#delta' => $delta,
              '#title' => $this->t('Weight for added category'),
              '#title_display' => 'invisible',
              '#default_value' => 1000000000000,
              '#attributes' => [
                'class' => ['product-weight'],
              ],
            ];

            $form['products_lis']['products_table'][$key]['#region_callback'] = [$this, 'getRowRegion'];

            $form['products_lis']['products_table'][$key]['#attributes']['class'] = [];
            $form['products_lis']['products_table'][$key]['#attributes']['class'][] = 'draggable';
            $form['products_lis']['products_table'][$key]['region'] = [
              '#type' => 'select',
              //'#title' => $this->t('Region for @title', ['@title' => $label]),
              '#title_display' => 'invisible',
              '#options' => $this->getRegionOptions(),
              '#default_value' => 'active',
              '#attributes' => ['class' => ['region']],
            ];

          }

        }


        $form['products_lis']['products_table']['#attributes']['data-total-items-count'] = $delta;

        $form['products_lis']['actions'] = ['#type' => 'actions'];
        $form['products_lis']['actions']['submit'] = [
          '#type' => 'submit',
          '#button_type' => 'primary',
          '#value' => $this->t('Save'),
          '#op' => 'save',
          '#submit' => ['::saveWeights'],
        ];
      }
      else {
        $form['products_lis']['#markup'] = 'Категория не содержит товаров, привязанных непосредственно к ней';
      }

    }

//     dpm($form);

    return $form;
  }

  /**
   * Returns the region to which a row in the display overview belongs.
   *
   * @param array $row
   *   The row element.
   *
   * @return string|null
   *   The region name this row belongs to.
   */
  public function getRowRegion(&$row) {
    $regions = $this->getRegions();
    if (!isset($regions[$row['region']['#value']])) {
      $row['region']['#value'] = 'hidden';
    }
    return $row['region']['#value'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
//     $trigerred_element = $form_state->getTriggeringElement();

//     if ($trigerred_element['#op'] == 'filter') {

//       return;
//     }

//     if ($trigerred_element['#op'] == 'save' && $form_state->getValue('category', NULL)) {

//       dpm($form_state->getValues());

// //       foreach ($form_state->getValue('products_table', []) as $pid => $data) {
// //         \Drupal::database()->update('bs_products_category_index')
// //         ->fields(['weight' => $data['weight']])
// //         ->condition('pid', $pid)
// //         ->condition('cid', $form_state->getValue('category', NULL))
// //         ->execute();
// //       }
//     }
  }

  public function setCategoryFilter(array &$form, FormStateInterface $form_state) {
//     dpm('::setCategoryFilter');

    $this->store = \Drupal::service('user.private_tempstore')->get('products_manage_form');
    $this->store->set('category', $form_state->getValue('category'));
  }

  public function saveWeights(array &$form, FormStateInterface $form_state) {
//     dpm('::saveWeights');
//     dpm($form_state->getValues());

    $cid = $form_state->getValue('category', NULL);

    if (!$cid) {
      \Drupal::messenger()->addWarning('Category ID not defined!');
      return;
    }

    $batch = array(
      'title' => t('Updating product weights'),
      'operations' => [],
      'init_message'     => t('Init'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      //'finished' => '\Drupal\batch_example\DeleteNode::ExampleFinishedCallback',
    );



    foreach ($form_state->getValue('products_table', []) as $pid => $data) {
      $batch['operations'][] = [
        ['\Drupal\bs_product\Entity\Product', 'updateProductInCategoryWeight'],
        [
          $pid,
          $cid,
          $form['products_lis']['products_table'][$pid]['#direct_rel'],
          $data['weight']
        ]
      ];
    }

    batch_set($batch);
  }

  public function getRegions() {
    return [
//       'sticked_at_top' => [
//         'title' => $this->t('Sticked at top', [], ['context' => 'Plural']),
//         'message' => $this->t('No products with default sorting')
//       ],
      'active' => [
        'title' => $this->t('Active'),
        'invisible' => TRUE,
        'message' => $this->t('No products related with this category.')
      ],
//       'default' => [
//         'title' => $this->t('Default sorting', [], ['context' => 'Plural']),
//         'message' => $this->t('No products with default sorting')
//       ],
    ];

    // К товарам ниже будет применена сортировка по умолчанию (на основании веса его категории и веса товара в его категории):
  }

  /**
   * Returns an associative array of all regions.
   *
   * @return array
   *   An array containing the region options.
   */
  public function getRegionOptions() {
    $options = [];
    foreach ($this->getRegions() as $region => $data) {
      $options[$region] = $data['title'];
    }
    return $options;
  }
}

