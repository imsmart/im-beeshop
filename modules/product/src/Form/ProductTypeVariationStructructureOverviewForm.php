<?php
namespace Drupal\bs_product\Form;

use Drupal\bs_product\Entity\ProductTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ProductTypeVariationStructructureOverviewForm extends FormBase {

  private $fields_parents_options = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_product_type_variotin_structure_overview';
  }

  public function buildForm(array $form, FormStateInterface $form_state, ProductTypeInterface $bs_product_type = NULL) {

    if (!$bs_product_type) return $form;

    if (!$bs_product_type->isVariationsSuport()) {
      \Drupal::messenger()->addWarning('This product type does not support variations for products!', 'warning');
      return $form;
    }

    $fields = $bs_product_type->getVariationsFieldsList();

    if (!$fields) {
      drupal_set_message('There are no variation fields in this type!', 'warning');
    }

    foreach ($fields as $field_name => $field_definition) {
      $this->fields_parents_options[$field_name] = $field_definition->getLabel();
    }

    $table = [
      '#type' => 'bs_ptvs_table',
      '#header' => $this->getTableHeader(),
      '#regions' => $this->getRegions(),
      '#attributes' => [
        'class' => ['field-ui-overview'],
        'id' => 'field-display-overview',
      ],
      '#tabledrag' => [
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'field-parent',
          'subgroup' => 'field-parent',
          'source' => 'field-name',
          'hidden' => FALSE,
        ],
        [
          'action' => 'depth',
          'relationship' => 'group',
          'group' => 'field-depth',
          'hidden' => FALSE,
        ],
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'field-weight',
        ],
      ],
    ];

    foreach ($fields as $field_name => $field_definition) {
      $table[$field_name] = $this->buildFieldRow($field_definition, $bs_product_type->getVariationFieldHierarchyData($field_name));
    }

    $form['fields'] = $table;

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['#product_type'] = $bs_product_type;
    //$form['#attached']['library'][] = 'field_ui/drupal.field_ui';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $product_type = $form['#product_type'];

    $variationFieldsHierarchy = [];

    foreach ($form_values['fields'] as $field_name => $field_data) {
      $field_info = [
        'field_name' => $field_name,
        'level' => $field_data['field_label_wrapper']['depth'] + 1,
        'weight' => $field_data['weight'],
        'parent_name' => $field_data['field_label_wrapper']['parent'],
        'have_standalone_page' => $field_data['have_standalone_page'],
        'display_on_category_page' => $field_data['display_on_category_page']
      ];
      $variationFieldsHierarchy[$field_name] = $field_info;
    }

    $product_type->setVariationFieldsHierarchy($variationFieldsHierarchy);
    $product_type->save();

  }

  /**
   * Builds the table row structure for a single product variation field.
   *
   * @param string $field_id
   *   The field ID.
   * @param array $extra_field
   *   The pseudo-field element.
   *
   * @return array
   *   A table row array.
   */
  protected function buildFieldRow(FieldDefinitionInterface $field_definition, $variation_field_h_data) {

    $regions = array_keys($this->getRegions());
    $field_name = $field_definition->getName();
    $label = $field_definition->getLabel();

    $variation_field_h_data = $variation_field_h_data ?
      $variation_field_h_data :
      [
        'level' => 0,
        'weight' => 0,
        'display_on_category_page' => FALSE,
        'have_standalone_page' => FALSE,
      ];

    $depth = !empty($variation_field_h_data['level']) ? $variation_field_h_data['level'] : 0;

    $indentation = [
      '#theme' => 'indentation',
      '#size' => $depth - 1,
    ];

    $extra_field_row = [
      '#attributes' => ['class' => ['draggable']],
      '#row_type' => 'extra_field',
      '#region_callback' => [$this, 'getRowRegion'],
      '#js_settings' => ['rowHandler' => 'field'],
      'field_label_wrapper' => [
        'depth' => [
          '#type' => 'hidden',
          '#default_value' => ($variation_field_h_data && isset($variation_field_h_data['level'])) ?
                              $variation_field_h_data['level'] - 1:
                              0,
          '#attributes' => [
            'class' => ['field-depth'],
          ],
        ],


        'field_label' => [
          '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
          '#markup' => $label,
        ],
        'field_name' => [
          '#type' => 'hidden',
          '#default_value' => $field_name,
          '#attributes' => ['class' => ['field-name']],
        ],
        'parent' => [
          '#type' => 'hidden',
          '#attributes' => ['class' => ['js-field-parent', 'field-parent']],
        ],
      ],
      'weight' => [
        '#type' => 'textfield',
        '#title' => $this->t('Weight for @title', ['@title' => $label]),
        '#title_display' => 'invisible',
        '#default_value' => ($variation_field_h_data && isset($variation_field_h_data['weight'])) ?
                            $variation_field_h_data['weight']: 0,
        '#size' => 3,
        '#attributes' => ['class' => ['field-weight']],
      ],
      'display_on_category_page' => [
        '#type' => 'checkbox',
        '#default_value' => ($variation_field_h_data && isset($variation_field_h_data['display_on_category_page'])) ?
                            $variation_field_h_data['display_on_category_page'] :
                            FALSE,
      ],
      'have_standalone_page' => [
        '#type' => 'checkbox',
        '#default_value' => ($variation_field_h_data && isset($variation_field_h_data['have_standalone_page'])) ?
                            $variation_field_h_data['have_standalone_page'] :
                            FALSE,
      ],
      'region' => [
        '#type' => 'hidden',
        '#default_value' => !empty($variation_field_h_data['level']) ? 'structure_tree' : 'not_defined',
        '#attributes' => ['class' => ['field-region']],
      ],
      'settings_summary' => [],
      'settings_edit' => [],
    ];

    return $extra_field_row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableHeader() {
    return [
      $this->t('Field'),
      $this->t('Weight'),
      $this->t('Display on category page'),
      $this->t('Have standalone page'),
      $this->t('Region'),
    ];
  }

  /**
   * Returns the region to which a row in the display overview belongs.
   *
   * @param array $row
   *   The row element.
   *
   * @return string|null
   *   The region name this row belongs to.
   */
  public function getRowRegion(&$row) {
    $regions = $this->getRegions();
    if (!isset($regions[$row['region']['#value']])) {
      $row['region']['#value'] = 'hidden';
    }
    return $row['region']['#value'];
  }

  /**
   * Returns an associative array of all regions.
   *
   * @return array
   *   An array containing the region options.
   */
  public function getRegionOptions() {
    $options = [];
    foreach ($this->getRegions() as $region => $data) {
      $options[$region] = $data['title'];
    }
    return $options;
  }

  /**
   * Get the regions needed to create the overview form.
   *
   * @return array
   *   Example usage:
   *   @code
   *     return array(
   *       'content' => array(
   *         // label for the region.
   *         'title' => $this->t('Content'),
   *         // Indicates if the region is visible in the UI.
   *         'invisible' => TRUE,
   *         // A message to indicate that there is nothing to be displayed in
   *         // the region.
   *         'message' => $this->t('No field is displayed.'),
   *       ),
   *     );
   *   @endcode
   */
  public function getRegions() {
    return [
      'structure_tree' => [
        'title' => $this->t('Structure tree'),
        'invisible' => TRUE,
        'message' => $this->t('No field is defined for product structure tree.')
      ],
      'not_defined' => [
        'title' => $this->t('Not defined', [], ['context' => 'Plural']),
        'message' => $this->t('No field is not defined.')
      ],
    ];
  }

}

