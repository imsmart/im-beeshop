<?php

namespace Drupal\bs_product\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_shop\Entity\ShopInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides categories overview form for a shop or global categories.
 */
class OverviewCategories extends FormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The term storage handler.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $storageController;

  /**
   * Constructs an OverviewCategories object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityManagerInterface $entity_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityManager = $entity_manager;
    $this->storageController = $entity_manager->getStorage('bs_products_category');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_products_categories_tree_overview';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the terms in a vocabulary, with options to edit
   * each one. The form is made drag and drop by the theme function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\taxonomy\VocabularyInterface $taxonomy_vocabulary
   *   The vocabulary to display the overview form for.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ShopInterface $shop = NULL) {

    $form_state->set(['bs_products_category', 'shop'], $shop);

    $sid = $shop ? $shop->id() : 0;

//     $tree = $this->storageController->loadTree($sid, 0, NULL, TRUE);
//     $this->storageController->loadTreeAlt();
//     // Need to check for categories types defined or not for #empty text

//     $form['categories'] = [
//       '#type' => 'table',
//       '#header' => [$this->t('Name'), $this->t('Weight'), $this->t('Operations')],
//       '#empty' => $this->t('No categories available. <a href=":link">Add category</a>.', [':link' => $this->url('entity.bs_products_category.add_page')]),
//       '#attributes' => [
//         'id' => 'categories',
//       ],
//     ];

//     $form['categories']['#tabledrag'][] = [
//       'action' => 'order',
//       'relationship' => 'sibling',
//       'group' => 'category-weight',
//     ];
//     $form['categories']['#tabledrag'][] = [
//       'action' => 'match',
//       'relationship' => 'parent',
//       'group' => 'category-parent',
//       'subgroup' => 'category-parent',
//       'source' => 'category-id',
//       'hidden' => FALSE,
//     ];
//     $form['categories']['#tabledrag'][] = [
//       'action' => 'depth',
//       'relationship' => 'group',
//       'group' => 'category-depth',
//       'hidden' => FALSE,
//     ];

//     $delta = count($tree);
//     $row_position = 0;
//     $errors = $form_state->getErrors();
//     $destination = $this->getDestinationArray();

//     foreach ($tree as $key => $category) {
//       /** @var $term \Drupal\Core\Entity\EntityInterface */
//       $category = $this->entityManager->getTranslationFromContext($category);
//       $form['categories'][$key]['#category'] = $category;
//       $indentation = [];
//       if (isset($category->depth) && $category->depth > 0) {
//         $indentation = [
//           '#theme' => 'indentation',
//           '#size' => $category->depth,
//         ];
//       }
//       $form['categories'][$key]['category'] = [
//         '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
//         '#type' => 'link',
//         '#title' => $category->getName(),
//         '#url' => $category->toUrl(),
//       ];

//       $parent_fields = TRUE;
//       $form['categories'][$key]['category']['cid'] = [
//         '#type' => 'hidden',
//         '#value' => $category->id(),
//         '#attributes' => [
//           'class' => ['category-id'],
//         ],
//       ];
//       $form['categories'][$key]['category']['parent'] = [
//         '#type' => 'hidden',
//         // Yes, default_value on a hidden. It needs to be changeable by the
//         // javascript.
//         '#default_value' => $category->parents[0],
//         '#attributes' => [
//           'class' => ['category-parent'],
//         ],
//       ];
//       $form['categories'][$key]['category']['depth'] = [
//         '#type' => 'hidden',
//         // Same as above, the depth is modified by javascript, so it's a
//         // default_value.
//         '#default_value' => $category->depth,
//         '#attributes' => [
//           'class' => ['category-depth'],
//         ],
//       ];

//       $form['categories'][$key]['weight'] = [
//         '#type' => 'weight',
//         '#delta' => $delta,
//         '#title' => $this->t('Weight for added category'),
//         '#title_display' => 'invisible',
//         '#default_value' => $category->getWeight(),
//         '#attributes' => [
//           'class' => ['category-weight'],
//         ],
//       ];
//       $operations = [
//         'edit' => [
//           'title' => $this->t('Edit'),
//           'query' => $destination,
//           'url' => $category->toUrl('edit-form'),
//         ],
//         'delete' => [
//           'title' => $this->t('Delete'),
//           'query' => $destination,
//           'url' => $category->toUrl('delete-form'),
//         ],
//       ];
//       if ($this->moduleHandler->moduleExists('content_translation') && content_translation_translate_access($category)->isAllowed()) {
//         $operations['translate'] = [
//           'title' => $this->t('Translate'),
//           'query' => $destination,
//           'url' => $category->urlInfo('drupal:content-translation-overview'),
//         ];
//       }
//       $form['categories'][$key]['operations'] = [
//         '#type' => 'operations',
//         '#links' => $operations,
//       ];

//       $form['categories'][$key]['#attributes']['class'] = [];
//       if ($parent_fields) {
//         $form['categories'][$key]['#attributes']['class'][] = 'draggable';
//       }

//       $row_position++;
//     }

//     $min_required_max_input_vars = 4 * $delta * 4 + 10;

//     if (ini_get('max_input_vars') < $min_required_max_input_vars) {
//       $message = $this->t('"max_input_vars" is set to @max_input_vars, but this form required that this value must be more than @min_required. You can\'t save changes on this form until increase "max_input_vars" to min required value (@min_required)', [
//         '@max_input_vars' => ini_get('max_input_vars'),
//         '@min_required' => $min_required_max_input_vars,
//       ]);
//       \Drupal::messenger()->addWarning($message);
//     }
//     else {
//       $form['actions'] = ['#type' => 'actions'];
//       $form['actions']['submit'] = [
//         '#type' => 'submit',
//         '#button_type' => 'primary',
//         '#value' => $this->t('Save'),
//       ];
//     }

    $form['app-cat-tree'] = [
      '#type' => 'html_tag',
      '#tag' => 'app-categories-tree',
    ];

    $form['#attached']['library'][] = 'bs_product/catTree';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $changed_cats = [];

    $weight = -1;

    foreach ($form_state->getValue('categories') as $index => $cat_data) {
      $weight++;
      if (!isset($form['categories'][$index]['#category'])) continue;

      $category = $form['categories'][$index]['#category'];

      if ($category->getWeight() != $weight){
        $category->setWeight($weight);
        $changed_cats[$category->id()] = $category;
      }

      if ($category->getParentCid() != $cat_data['category']['parent']){
        $category->setParentCid($cat_data['category']['parent']);
        $changed_cats[$category->id()] = $category;
      }

    }

    foreach ($changed_cats as $category) {
      $category->save();
    }
    drupal_set_message($this->t('Categories weight has been saved.'));

  }

}

