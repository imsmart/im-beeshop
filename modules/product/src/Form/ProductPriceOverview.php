<?php
namespace Drupal\bs_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class ProductPriceOverview extends FormBase {

  /**
   * The plugin manager used by this entity type.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * @var ProductInterface
   */
  protected $product;

  protected $fieldWidgets = [];

  /**
   * Constructs an PriceTypesOverviewForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(PluginManagerInterface $field_widget_plugin_mager) {
    $this->pluginManager = $field_widget_plugin_mager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.field.widget')
      );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
      return 'product_price_overview';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, ProductInterface $bs_product = NULL) {

    if ($bs_product) {
      $this->product = $bs_product;
    }

    $header = [
      $this->t('#ID'),
      $this->t('Title (#SKU)'),
      $this->t('Characteristics'),
      $this->t('Inherit price from parent'),
      $this->t('Price(s)'),
    ];

    $product_variations = $bs_product->getVariations(TRUE);

    $form['variations_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#sticky' => TRUE,
      '#attributes' => [
        'id' => 'product-variation-prices',
      ],
    ];

    $form['variations_table'][$this->product->id()] = $this->getVariationRow($this->product, $form_state);
    $form['variations_table'][$this->product->id()]['#attributes']['class'][] = 'variaton-row--root-product';

    foreach ($product_variations as $variation) {
      $form['variations_table'][$variation->id()] = $this->getVariationRow($variation, $form_state);
    }

    if (Element::children($form['variations_table'])) {
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Save'),
      ];
    }

    $form['#attached']['library'][] = 'bs_price/productVariationsPriceOverview';

    return $form;
  }

  protected function getVariationRow(ProductInterface $variation, FormStateInterface $form_state) {

    $parent = $variation->getParent();

    $variation_row = [
//       '#entity' => $variation
    ];

    $depth = $variation->getLevel();

    $indentation = $indentation = [
      '#theme' => 'indentation',
      '#size' => $depth,
    ];

    $variation_row['id'] = [
      'data' => [
        '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
        '#markup' => '#' . $variation->id(),
      ],
      '#wrapper_attributes' => [
        'class' => [
          'cell--id'
        ]
      ],
    ];

    $variation_row['title'] = [
      '#markup' => $variation->getParent() ? ' (#' . $variation->getSKU() . ')' : $variation->label() . ' (#' . $variation->getSKU() . ')',
    ];

    $variation_row['characteristics'] = [
      '#markup' => $variation->characteristicsLabel(FALSE, '/'),
    ];

    /**
     * @var WidgetInterface $inh_price_from_parent_widget
     */
    $inh_price_from_parent_widget = $this->getFieldWidget($variation, 'inh_price_from_parent', $form_state);
    $parents = [
      'variations_table',
      $variation->id(),
    ];
    $inh_price_from_parent_widget->setSetting('display_label', FALSE);

    $variation_row['inh_price_from_parent'] = $variation->getParent() ? $this->getFieldWidgetFormElement($variation, 'inh_price_from_parent', $form_state) : [];

    $price_field_widget = $this->getFieldWidget($variation, 'price', $form_state);
    $field_state = $price_field_widget->getWidgetState($parents, 'price', $form_state);
    $field_state['by_shops']['states']['invisible'] = [
      'input[name="variations_table[' . $variation->id() . '][inh_price_from_parent][value]"]' => ['checked' => TRUE],
    ];
    $inh_price_from_parent_widget->setWidgetState($parents, 'price', $form_state, $field_state);
    $variation_row['price'] = $this->getFieldWidgetFormElement($variation, 'price', $form_state);
    $variation_row['price']['#wrapper_attributes'] = [
      'class' => [
        'cell--prices'
      ]
    ];


    $variation_row['#attributes']['class'][] = 'variaton-row';

    if ($parent) {
      $variation_row['#attributes']['data-parent-id'] = $parent->id();
    }

    if ($variation->isGroup()) {
      $variation_row['#attributes']['class'][] = 'variaton-row--group';
    }
//     dpm($variation_row);
    return $variation_row;
  }

  protected function getFieldWidgetFormElement(ProductInterface $variation, $field_name, FormStateInterface $form_state) {

    $widget = $this->getFieldWidget($variation, $field_name, $form_state);

    $f = [
      '#parents' => [
        'variations_table',
        $variation->id(),
      ],
    ];

    return $widget->formElement($variation->get($field_name), 0, [], $f, $form_state);
  }

  protected function getFieldWidget(ProductInterface $variation, $field_name, FormStateInterface $form_state) {

    if (!empty($this->fieldWidgets[$field_name])) {
      return $this->fieldWidgets[$field_name];
    }

    $price_field_definition = $variation->getFieldDefinition($field_name);
    $options = $this->pluginManager->prepareConfiguration($price_field_definition->getType(), []);
    $options += ['settings' => [], 'third_party_settings' => []];

    $widget = $this->pluginManager->getInstance([
      'field_definition' => $price_field_definition,
      'form_mode' => 'default',
      // No need to prepare, defaults have been merged in setComponent().
      'prepare' => FALSE,
      'configuration' => $options
    ]);

    return $widget;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    $variations_table = $form_state->getValues()['variations_table'];
    $variations = Product::loadMultiple(array_keys($variations_table));

    foreach ($variations_table as $id => $values) {

      if (!isset($variations[$id])) {
        $message = new TranslatableMarkup('Product with ID !pid not loaded and will be skipped for price update', ['!pid' => $id]);
        \Drupal::messenger()->addWarning($message);
        continue;
      }

      /**
       * @var ProductInterface $variation
       */
      $variation = $variations[$id];

      $inh_price_from_parent_widget = $this->getFieldWidget($variation, 'inh_price_from_parent', $form_state);
      $price_widget = $this->getFieldWidget($variation, 'price', $form_state);

      $inh_price_from_parent_widget->extractFormValues($variation->get('inh_price_from_parent'), $form['variations_table'][$id], $form_state);
      $price_widget->extractFormValues($variation->get('price'), $form['variations_table'][$id], $form_state);

      $variation->save();

    }

  }



}

