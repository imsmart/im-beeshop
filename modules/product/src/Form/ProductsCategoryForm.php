<?php
namespace Drupal\bs_product\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class ProductsCategoryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $category = $this->entity;

    $categories_storage = $this->entityManager->getStorage('bs_products_category');
    $sid = 0;

    $form = parent::form($form, $form_state);

    $form['advanced']['#attributes']['class'][] = 'entity-meta';

    $form['relations'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Relations'),
//       '#open' => $vocabulary->getHierarchy() == VocabularyInterface::HIERARCHY_MULTIPLE,
      '#weight' => 10,
    ];

    // All current childs must be excluded from parent options list
    $full_tree = $categories_storage->loadTree($sid);
    $child_cats = $categories_storage->loadTree($sid, $category->id());
    $options = [0 => '<' . $this->t('Categories root') . '>'];

    foreach ($child_cats as $child) {
      $exclude[] = $child->cid;
    }
    $exclude[] = $category->id();

    foreach ($full_tree as $item) {
      if (!in_array($item->cid, $exclude)) {
        $options[$item->cid] = str_repeat('-', $item->depth) . $item->name;
      }
    }

    $form['relations']['parent'] = [
      '#type' => 'select',
      '#title' => $this->t('Parent category'),
      '#options' => $options,
      '#default_value' => $category->getParentCid(),
    ];

    $form['relations']['weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Weight'),
      '#size' => 6,
      '#default_value' => $category->getWeight(),
      '#description' => $this->t('Categories are displayed in ascending order by weight.'),
      '#required' => TRUE,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    parent::save($form, $form_state);

    $form_state->setRedirect(
      'entity.bs_products_category.canonical',
      ['bs_products_category' => $this->entity->id()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Ensure numeric values.
    if ($form_state->hasValue('weight') && !is_numeric($form_state->getValue('weight'))) {
      $form_state->setErrorByName('weight', $this->t('Weight value must be numeric.'));
    }
  }

}

