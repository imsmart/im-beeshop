<?php
namespace Drupal\bs_product\Form;

use Drupal\Core\Form\ConfigFormBase;

class CategoriesGlobalSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bs_product.categories'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'beeshop_categories_settings';
  }

}

