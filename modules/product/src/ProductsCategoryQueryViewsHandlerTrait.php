<?php
namespace Drupal\bs_product;

use Drupal\Core\Database\Query\Condition;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

trait ProductsCategoryQueryViewsHandlerTrait {

  public function query($group_by = FALSE) {

    $this->ensureMyTable();

    $categories_ids = [];

    $operator = '';

    if ($this instanceof ArgumentPluginBase) {

      if (!empty($this->options['break_phrase'])) {
        $break = static::breakString($this->argument);
        if ($break->value === [-1]) {
          return FALSE;
        }

        $operator = (count($break->value) > 1) ? 'IN' : '=';
        $categories_ids = $break->value;
      }
      else {
        $operator = "=";
        $categories_ids = $this->argument;
      }

    }
    elseif ($this instanceof FilterPluginBase && $this->value) {
      $operator = 'IN';
      $categories_ids = array_keys($this->value);
    }

    $key = is_array($categories_ids) ? implode('_', $categories_ids) : $categories_ids;

    // Now build the subqueries.
    $db = \Drupal::database();
    $subquery = $db->select('bs_products_category_index', 'ci');
    $subquery->addField('ci', 'pid');
    $or_cond = new Condition('OR');
    $where = $or_cond->condition('ci.cid', $categories_ids, $operator);
    $last = "ci";

    if (!empty($this->options['depth'])) {
      if ($this->options['depth'] > 0) {
        $subquery->leftJoin('bs_products_category_hierarchy', 'ch', "ch.cid = ci.cid AND ci.direct_rel = 1");
        $last = "ch";
        foreach (range(1, abs($this->options['depth'])) as $count) {
          $subquery->leftJoin('bs_products_category_hierarchy', "ch$count", "$last.parent = ch$count.cid");
          $where->condition("ch$count.cid", $categories_ids, $operator);
          $last = "ch$count";
        }
      }
      elseif ($this->options['depth'] < 0) {
        foreach (range(1, abs($this->options['depth'])) as $count) {
          $subquery->leftJoin('bs_products_category_hierarchy', "ch$count", "$last.cid = ch$count.parent");
          $where->condition("ch$count.cid", $categories_ids, $operator);
          $last = "ch$count";
        }
      }
    }

    $subquery->condition($where);

    $pid_operator = 'IN';

    if (!empty($this->options['operator'])) {
      switch ($this->options['operator']) {
        case 'not':
          $pid_operator = 'NOT IN';
          break;
      }
    }

    $snippet = '(CASE
   WHEN root_parent is null THEN bs_product_field_data.pid
   WHEN root_parent = 0 THEN bs_product_field_data.pid
   ELSE root_parent
END) ' . $pid_operator . ' (' . $subquery . ')';

    $snippet = preg_replace('#db_condition_placeholder#', 'db_ciddepth_condition_placeholder_' . $key, $snippet);

    $args = [];


    foreach ($subquery->getArguments() as $placeholder_name => $plh_value) {
      $altered_placeholder_name = preg_replace('#db_condition_placeholder#', 'db_ciddepth_condition_placeholder_' . $key, $placeholder_name);
      $args[$altered_placeholder_name] = $plh_value;
    }

    $group = !empty($this->options['group']) ? $this->options['group'] : 0;

    $this->query->addWhereExpression($group, $snippet, $args);

  }

}

