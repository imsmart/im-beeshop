<?php
namespace Drupal\bs_product;

use Drupal\Core\Entity\ContentEntityStorageInterface;

interface ProductsStorageInterface extends ContentEntityStorageInterface {


  /**
   *
   */
  public function getProductsVariationsIdsMultily(array $pids);
}

