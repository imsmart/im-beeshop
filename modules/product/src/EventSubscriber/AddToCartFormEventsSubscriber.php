<?php
namespace Drupal\bs_product\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\bs_cart\Event\AddToCartFormEvents;
use Drupal\bs_cart\Event\AddToCartFormAjaxVariationChanged;
use Drupal\bs_cart\Event\AddToCartFormAjaxVariationChangedEvent;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\Entity\EntityViewDisplay;

class AddToCartFormEventsSubscriber implements EventSubscriberInterface {


  public function onAddToCartFormAjaxVariationChanged(AddToCartFormAjaxVariationChangedEvent $event) {

    $form = $event->getForm();
    $variation_id = $event->getVariationId();
    $response = $event->getResponse();
    $view_mode = $form['#mode'];

    if ($variation = Product::load($variation_id)) {

      $display = EntityViewDisplay::collectRenderDisplay($variation, $view_mode);

      $components = $display->getComponents();

      if ($diff_fields = $variation->getDiffFields()) {

        foreach ($diff_fields as $field_name) {
          if (isset($components[$field_name])) {
            $rendable_field = $variation->get($field_name)->view($view_mode);
            $field_selector = '[data-product-tree-path^="' . $form['#pid'] . '"][data-product-pid-field-mode$="' . $field_name . '/' . $form['#mode'] . '"]';
            $response->addCommand(new ReplaceCommand($field_selector, $rendable_field));
          }
        }
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events = [];

    if (class_exists('AddToCartFormEvents')) {
      $events[AddToCartFormEvents::ADD_TO_CART_FORM_AJAX_VARIATION_CHANGED][] = ['onAddToCartFormAjaxVariationChanged'];
    }

    return $events;
  }

}

