/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {
	
  'use strict';
  
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

  var trackChange = function(element) {
    var observer = new MutationObserver(function(mutations, observer) {
    	$.each(mutations, function(index, mutation) {
    		if(mutation.attributeName == "value") {
          $(element).trigger("change");
        }
    	});
    });
    observer.observe(element, {
      attributes: true
    });
  }

  // Just pass an element to the function to start tracking
  
  $(document).ready(function() {
  	
  });
  
  
  Drupal.behaviors.beeshopProductVariationsWidget = {
  		attach: function attach(context, settings) {
  			
//  			$.each($('[data-states]', context), function(index, dependent_element) {
//  				let states = JSON.parse(dependent_element.dataset.states);
//  				
//  				Object.keys(states).forEach(function (state) {
//  					let state_config = states[state];
//  					Object.keys(state_config).forEach((selector) => {
//  						let condition = state_config[selector];
//  						
//  						Object.keys(condition).forEach((propName) => {
//    						let prop_value = condition[propName];
//    						
////    						let leads = document.querySelector(selector);
//    						
//    						$(selector).each( (index, element) => {
//    							element.data = element.data || {}; 
//    							element.data.dependentElements = element.data.dependentElements || {};
//    							element.data.dependentElements[propName] = element.data.dependentElements[propName] || [];
//    							element.data.dependentElements[propName].push({
//    								element: dependent_element,
//    								value: prop_value,
//    							});
//    						});
//    					});
//  					});
//  				});
//  			})
  			
  			$('input.depth', context).each(function(index, element) {
  	  		if (!$(element).hasClass('change-tracked')) {
  	  			trackChange(element);
  	  			$(element).addClass('change-tracked');
  	  		}
  	  	});
  			
  		},
  };
	
})(jQuery, window, Drupal, drupalSettings);