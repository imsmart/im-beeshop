/**
 * 
 */
(function ($, Drupal, drupalSettings) {
	
	
	formatSelectedCategory = function(item) {
		
		let output = document.createElement('span');
		
		if (drupalSettings.BeeShop && drupalSettings.BeeShop.categoriesSet &&
				drupalSettings.BeeShop.categoriesSet[item.id]) {
			output.innerText = drupalSettings.BeeShop.categoriesSet[item.id].full_name + ' ';
		}
		else {
			output.innerText = `${item.text} `;
		}
		
		let categoryLink = document.createElement('a');
		categoryLink.setAttribute('target', '_blank');
		categoryLink.setAttribute('href', `/catalog/category/${item.id}`);
		categoryLink.innerText = `#${item.id}`;
		categoryLink.onclick = function(event) {
			event.stopPropagation();
		};
		
		output.appendChild(categoryLink);
		
		return output;
	}
	
	formatResultCategory = function(item) {
    let output = document.createElement('span');
		
		if (drupalSettings.BeeShop && drupalSettings.BeeShop.categoriesSet &&
				drupalSettings.BeeShop.categoriesSet[item.id]) {
			output.innerText = drupalSettings.BeeShop.categoriesSet[item.id].full_name;
		}
		else {
			output.innerText = `${item.text} `;
		}
		
		return output;
	}
	
	Drupal.behaviors.beeshopProductVariationsWidget = {
  		attach: function attach(context, settings) {
  			
  			if ($.fn.select2) {
  				$('select[data-product-categories-select]', context).once().select2({
  					width: 'element',
  					templateSelection: formatSelectedCategory,
  					templateResult: formatResultCategory,
  				});
  			}
  			
  		},
  };
	
	
})(jQuery, Drupal, drupalSettings);