<?php
namespace Drupal\bs_product_search;

use Drupal\search\SearchQuery;
use Drupal\Core\Database\Query\Condition;

class ProductSearchQuery extends SearchQuery {

  /**
   * {@inheritDoc}
   */
  public function prepareAndNormalize() {

    $search_expr = preg_replace('#[^A-Za-z0-9]#ui', '', $this->searchExpression);
    $this->query->addMetaData('words', $search_expr);

    return parent::prepareAndNormalize();
  }


}

