<?php
namespace Drupal\bs_shop_warehouse;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Access\AccessResult;

class ShopWarehouseAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    $account = $this->prepareUser($account);

    if ($admin_permission = $this->entityType->getAdminPermission()) {
      $result = AccessResult::allowedIfHasPermission($account, $admin_permission);

      if ($result->isAllowed()) {
        return $result;
      }

    }

    if (!$entity->get('type')->entity->canonycal_link_enabled) {
      return new AccessResultForbidden();
    }

    $result = parent::access($entity, $operation, $account, $return_as_object);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\node\NodeInterface $node */

    // Fetch information from the node object if possible.
    $is_enabled = $entity->isEnabled();

    // Check if authors can view their own unpublished nodes.
    if ($operation === 'view' && !$is_enabled && $account->hasPermission('view disabled warehouse')) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    if ($operation === 'view' && $is_enabled/* && $account->hasPermission('view warehouse')*/) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    // Evaluate node grants.
    return parent::checkAccess($entity, $operation, $account);
  }

}

