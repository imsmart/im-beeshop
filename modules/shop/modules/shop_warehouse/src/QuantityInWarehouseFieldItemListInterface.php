<?php

namespace Drupal\bs_shop_warehouse;

use Drupal\Core\Field\FieldItemListInterface;

interface QuantityInWarehouseFieldItemListInterface extends FieldItemListInterface {

  /**
   *
   * @param int $warehouse_id
   */
  public function getQuantityByWarehouse($warehouse_id);

  /**
   *
   */
  public function refillByWarehouseIndex();

  /**
   * @return float
   */
  public function getTotalQuantity();

}

