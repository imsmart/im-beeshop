<?php

namespace Drupal\bs_shop_warehouse\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_shop_store\Entity\ShopStoreInreface;
use Drupal\beeshop\Entity\BeeshopEntityEnabledTrait;
use Drupal\beeshop\Entity\BeeshopContentEntityBase;
use Drupal\bs_shop_warehouse\Entity\ShopWarehouseInreface;
use Drupal\bs_shop\Entity\Shop;
use Drupal\Core\Session\AccountInterface;


/**
 * Defines the shop entity class.
 *
 * @ContentEntityType(
 *   id = "bs_shop_warehouse",
 *   label = @Translation("Shop warehouse"),
 *   label_singular = @Translation("shop warehouse"),
 *   label_plural = @Translation("shop warehouse"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shop warehouse",
 *     plural = "@count shop warehouses",
 *   ),
 *   bundle_label = @Translation("Shop warehouse type"),
 *   handlers = {
 *     "access" = "Drupal\bs_shop_warehouse\ShopWarehouseAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\bs_shop_warehouse\Form\ShopWarehouseForm",
 *       "add" = "Drupal\bs_shop_warehouse\Form\ShopWarehouseForm",
 *       "edit" = "Drupal\bs_shop_warehouse\Form\ShopWarehouseForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_shop_warehouse\ShopWarehouseListBuilder"
 *   },
 *   base_table = "bs_shop_warehouse",
 *   data_table = "bs_shop_warehouse__field_data",
 *   admin_permission = "administer bs_shop_warehouse",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "sw_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "enabled" = "enabled",
 *   },
 *   links = {
 *     "canonical" = "/shop/warehouse/{bs_shop_warehouse}",
 *     "add-page" = "/admin/beeshop/config/shops/warehouse/add",
 *     "add-form" = "/admin/beeshop/config/shops/warehouse/add/{bs_shop_warehouse_type}",
 *     "edit-form" = "/admin/beeshop/config/shops/warehouse/{bs_shop_warehouse}/edit",
 *     "delete-form" = "/admin/beeshop/config/shops/warehouse/{bs_shop_warehouse}/delete",
 *     "collection" = "/admin/beeshop/config/shops/warehouse/collection",
 *     "by-shop-collection" = "/admin/beeshop/config/shops/{bs_shop}/warehouse/collection",
 *   },
 *   bundle_entity_type = "bs_shop_warehouse_type",
 *   field_ui_base_route = "entity.bs_shop_warehouse_type.edit_form",
 * )
 */
class ShopWarehouse extends BeeshopContentEntityBase implements ShopWarehouseInreface {

  use BeeshopEntityEnabledTrait;


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the enable field.
    $fields += static::enabledBaseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Type'))
    ->setDescription(t('The shop type.'))
    ->setSetting('target_type', 'bs_shop_warehouse_type')
    ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The shop name.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Description'))
    ->setTranslatable(TRUE)
    ->setDisplayConfigurable('view', FALSE)
    ->setDisplayOptions('form', [
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE);

    $fields['shops'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shop'))
    ->setDescription(t('Shop'))
    ->setRequired(TRUE)
    ->setDefaultValueCallback('Drupal\bs_shop_warehouse\Entity\ShopWarehouse::getDefaultShopId')
    ->setSetting('target_type', 'bs_shop')
    ->setSetting('handler', 'default')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => 2,
    ])
    ->setDisplayConfigurable('view', FALSE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['products_quantity_enabled'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Store products quantity for this warehouse'))
    ->setRevisionable(TRUE)
    ->setTranslatable(TRUE)
    ->setDefaultValue(TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Weight'))
    ->setDescription(t('The weight of this warehouse in relation to other warehouses.'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }


  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {

    $fields = [];



    if (($bundle_entity = ShopWarehouseType::load($bundle)) && !empty($bundle_entity->canonycal_link_enabled)) {
      $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The product URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);
    }


    return $fields;
  }


 /**
  * {@inheritDoc}
  */
 public function getShop() {
   return $this->getReferencedEntity('shop');
 }



 /**
  * {@inheritdoc}
  */
 public function getDefaultShopId() {
   $entity_query = \Drupal::entityQuery('bs_shop');

   $ids = $entity_query->execute();

   return [reset($ids)];
 }
}