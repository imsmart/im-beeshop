<?php
namespace Drupal\bs_shop_warehouse\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntity;

/**
 * Defines the Node type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "bs_shop_warehouse_type",
 *   label = @Translation("Shop warehouse type"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\bs_shop_warehouse\Form\ShopWarehouseTypeForm",
 *       "add" = "Drupal\bs_shop_warehouse\ShopWarehouseTypeForm",
 *       "edit" = "Drupal\bs_shop_warehouse\ShopWarehouseTypeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_shop_warehouse\ShopWarehouseTypeListBuilder",
 *   },
 *   admin_permission = "administer shop_warehouse types",
 *   config_prefix = "type",
 *   bundle_of = "bs_shop_warehouse",
 *   hierarchy_inheritance_support = TRUE,
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name",
 *     "parent" = "parent",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/shops/warehouse/types/add",
 *     "edit-form" = "/admin/beeshop/config/shops/warehouse/types/{bs_shop_warehouse_type}/edit",
 *     "delete-form" = "/admin/beeshop/config/shops/warehouse/types/{bs_shop_warehouse_type}/delete",
 *     "collection" = "/admin/beeshop/config/shops/warehouse/types",
 *   },
 *   config_export = {
 *     "name",
 *     "type",
 *     "description",
 *     "weight",
 *     "parent",
 *     "canonycal_link_enabled",
 *   }
 * )
 */
class ShopWarehouseType extends BeeShopBundleWithDescriptionEntity implements ShopWarehouseTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

}

