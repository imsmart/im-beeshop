<?php
namespace Drupal\bs_shop_warehouse\Entity;

use Drupal\beeshop\Entity\BeeShopBundleWithDescriptionEntityInterface;

interface ShopWarehouseTypeInterface extends BeeShopBundleWithDescriptionEntityInterface {

}

