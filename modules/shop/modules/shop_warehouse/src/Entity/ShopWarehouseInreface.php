<?php
namespace Drupal\bs_shop_warehouse\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\beeshop\Entity\BeeshopEntityEnabledInterface;

interface ShopWarehouseInreface extends ContentEntityInterface, BeeshopEntityEnabledInterface {

  /**
   *  Get store shop
   *
   *  @return \Drupal\bs_shop\Entity\ShopInterface
   */
  public function getShop();

  /**
   * Default value callback for 'shop' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   *
   */
  public function getDefaultShopId();

}

