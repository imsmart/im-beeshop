<?php
namespace Drupal\bs_shop_warehouse;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class ShopWarehouseListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Shop');
    $header['type'] = $this->t('Shop type');
    $header['qc'] = $this->t('Quantity control');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['type'] = $entity->bundle();
    $row['qc'] = $entity->get('products_quantity_enabled')->value ? '+' : '-';
    return $row + parent::buildRow($entity);
  }

}

