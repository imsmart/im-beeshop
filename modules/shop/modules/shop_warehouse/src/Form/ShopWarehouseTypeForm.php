<?php
namespace Drupal\bs_shop_warehouse\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeInterface;

class ShopWarehouseTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $warehouse_type = $this->entity;

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $warehouse_type->label(),
      '#required' => TRUE,
    ];
    $form['type'] = [
      '#type' => 'machine_name',
      '#default_value' => $warehouse_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
//       '#disabled' => $warehouse_type->isLocked(),
      '#machine_name' => [
        'exists' => ['\Drupal\bs_shop_warehouse\Entity\ShopWarehouseType', 'load'],
        'source' => ['name'],
      ],
      '#description' => t('A unique machine-readable name for this content type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %node-add page, in which underscores will be converted into hyphens.', [
        '%node-add' => t('Add content'),
      ]),
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $warehouse_type->getDescription(),
    ];

    $form['canonycal_link_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Canonycal link enabled'),
      '#default_value' => $warehouse_type->canonycal_link_enabled,
    ];

    return $this->protectBundleIdElement($form);
  }

}

