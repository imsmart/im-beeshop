<?php
namespace Drupal\bs_shop_warehouse;

use Drupal\Core\Field\FieldItemList;
use Drupal\bs_shop_warehouse\Entity\ShopWarehouse;

class QuantityInWarehouseFieldItemList extends FieldItemList implements QuantityInWarehouseFieldItemListInterface {

  protected $by_wh_quantity = [];

 /**
  * {@inheritDoc}
  */
  public function getQuantityByWarehouse($warehouse_id) {

    if ($this->by_wh_quantity && isset($this->by_wh_quantity[$warehouse_id])) {
     return $this->by_wh_quantity[$warehouse_id];
    }

    $this->refillByWarehouseIndex();

    if (isset($this->by_wh_quantity[$warehouse_id])) {
     return $this->by_wh_quantity[$warehouse_id];
    }

    return NULL;
  }

  public function refillByWarehouseIndex() {
    foreach ($this->list as $item) {
      $value = $item->getValue();
      $this->by_wh_quantity[$value['wid']] = $value['quantity'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);
    $this->refillByWarehouseIndex();
  }

  public function setInWarehoseQuantity($warehouse_id, $quatity) {
    foreach ($this->list as $item) {
      if ($item->wid == $warehouse_id) {
        $item->quantity = $quatity;
        return $this;
      }
    }

    $result = $this->appendItem([
      'wid' => $warehouse_id,  'quantity' => $quatity,
    ]);

    return $this;
  }

  public function getTotalQuantity() {
    $result = 0;

    foreach ($this->list as $item) {
      $result += $item->quantity;
    }

    return $result;
  }

  public function getAllWhCountSum($include_disabled = FALSE) {
    $result = 0;

    foreach ($this->list as $item) {
      $sw = ShopWarehouse::load($item->wid);
      if (!$sw->isEnabled() && !$include_disabled) {
        continue;
      }
      $result += $item->quantity;
    }

    return $result;
  }

}

