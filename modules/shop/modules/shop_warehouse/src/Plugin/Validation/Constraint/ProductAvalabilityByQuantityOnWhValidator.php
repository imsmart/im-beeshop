<?php
namespace Drupal\bs_shop_warehouse\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\bs_cart\CartItemInterface;
use Drupal\bs_product\Entity\ProductInterface;


class ProductAvalabilityByQuantityOnWhValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($product, Constraint $constraint) {
    /**
     * @var ProductInterface $product
     */
    if ($product->hasField('variations') && !$product->get('variations')->isEmpty()) {
      return;
    }

    if ($product->hasField('ctrl_quantity_in_wh') && $this->isControlQuantity($product) &&
      $product->hasField('quantity_in_wh')) {

        if (!$product->get('quantity_in_wh')->getAllWhCountSum()) {
          $this->context->buildViolation($constraint->message, ['%product_title' => $product->getTitle()])
          ->setCause('bs_shop_warehouse:not rests')
          ->addViolation();
        }

      }


  }

  protected function isControlQuantity(ProductInterface $product) {
    $result = $product->get('ctrl_quantity_in_wh')->value ? 1 : 0;


    if (!$result) {
      return FALSE;
    }

    if ($parent = $product->getParent()) {
      $result = $result * $this->isControlQuantity($parent);
    }

    return boolval($result);
  }

}

