<?php
namespace Drupal\bs_shop_warehouse\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\bs_product\ProductValidationConstraintBase;

/**
 * Checks that the cart item for actual availability state and price.
 *
 * @Constraint(
 *   id = "bs_swh_by_quantity_on_wh",
 *   label = @Translation("Product availability by quantity on warehouse", context = "Validation"),
 *   type = {"entity:bs_product"}
 * )
 */
class ProductAvalabilityByQuantityOnWh extends ProductValidationConstraintBase {

  public $message = 'Product "%product_title" has no rests on warehouses';

}

