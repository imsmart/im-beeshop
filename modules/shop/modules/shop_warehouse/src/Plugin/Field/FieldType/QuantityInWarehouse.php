<?php

namespace Drupal\bs_shop_warehouse\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 * default_formatter = "commerce_price_default",
 *
 * @FieldType(
 *   id = "bs_quantity_in_wh",
 *   label = @Translation("Control quantity in warehouse"),
 *   description = @Translation("Control quantity in warehouse."),
 *   category = @Translation("BeeShop"),
 *   no_ui = TRUE,
 *   default_widget = "bs_quantity_in_wh",
 *   list_class = "Drupal\bs_shop_warehouse\QuantityInWarehouseFieldItemList",
 * )
 */
class QuantityInWarehouse extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['wid'] = DataDefinition::create('integer')
    ->setLabel(t('Shop ID'))
    ->setRequired(FALSE);

    $properties['quantity'] = DataDefinition::create('float')
      ->setLabel(t('Quantity'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->quantity) && (string) $this->value !== '0') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // We don't need storage but as computed fields are not properly implemented
    // We will use a dummy column that should be ignored.
    // @see https://www.drupal.org/node/2392845.
    return [
      'columns' => [
        'wid' => [
          'type' => 'int',
          'size' => 'normal',
          'not null' => FALSE,
        ],
        'quantity' => [
          'type' => 'numeric',
          'size' => 'normal',
          'precision' => 10,
          'scale' => 2,
          'not null' => FALSE,
        ],
      ],
    ];
  }

//   /**
//    * {@inheritdoc}
//    */
//   public static function defaultFieldSettings() {
//     return [
//       'available_currencies' => [],
//     ] + parent::defaultFieldSettings();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
//     $currencies = \Drupal::entityTypeManager()->getStorage('commerce_currency')->loadMultiple();
//     $currency_codes = array_keys($currencies);

//     $element = [];
//     $element['available_currencies'] = [
//       '#type' => count($currency_codes) < 10 ? 'checkboxes' : 'select',
//       '#title' => $this->t('Available currencies'),
//       '#description' => $this->t('If no currencies are selected, all currencies will be available.'),
//       '#options' => array_combine($currency_codes, $currency_codes),
//       '#default_value' => $this->getSetting('available_currencies'),
//       '#multiple' => TRUE,
//       '#size' => 5,
//     ];

//     return $element;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function getConstraints() {
//     $manager = \Drupal::typedDataManager()->getValidationConstraintManager();
//     $constraints = parent::getConstraints();
//     $constraints[] = $manager->create('ComplexData', [
//       'value' => [
//         'Regex' => [
//           'pattern' => '/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/i',
//         ],
//       ],
//     ]);
//     $available_currencies = $this->getSetting('available_currencies');
//     //$constraints[] = $manager->create('Currency', ['availableCurrencies' => $available_currencies]);

//     return $constraints;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function isEmpty() {
//     return $this->value === NULL || $this->value === '' || empty($this->currency_code);
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function setValue($values, $notify = TRUE) {
//     // Allow callers to pass a Price value object as the field item value.
//     if ($values instanceof Price) {
//       $price = $values;
//       $values = [
//         'value' => $price->getPriceValue(),
//         'currency_code' => $price->getCurrencyCode(),
//       ];
//     }
//     parent::setValue($values, $notify);
//   }

//   /**
//    * Gets the Price value object for the current field item.
//    *
//    * @return \Drupal\commerce_price\Price
//    *   The Price value object.
//    */
//   public function toPrice() {
//     if ($this->value && $this->currency_code) {
//       return new Price($this->value, $this->currency_code);
//     }
//     return FALSE;
//   }

//   public function getCurrencyCode() {
//     return $this->currency_code;
//   }

//   public function getPriceValue() {
//     return $this->value;
//   }

//   public function getType() {
//     return $this->price_type;
//   }
}
