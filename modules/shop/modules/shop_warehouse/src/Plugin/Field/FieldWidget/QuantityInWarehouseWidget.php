<?php

namespace Drupal\bs_shop_warehouse\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use function Drupal\Core\Form\drupal_set_message;
use Drupal\bs_shop_warehouse\QuantityInWarehouseFieldItemListInterface;

/**
 * Plugin implementation of the 'bs_price_default' widget.
 *
 * @FieldWidget(
 *   id = "bs_quantity_in_wh",
 *   label = @Translation("Quantity in warehouse"),
 *   multiple_values = true,
 *   field_types = {
 *     "bs_quantity_in_wh"
 *   }
 * )
 */
class QuantityInWarehouseWidget extends WidgetBase {


  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /**
     * @var QuantityInWarehouseFieldItemListInterface $items
     */

    foreach ($items->getEntity()->getShops() as $shop) {
      $element[$shop->id()] = [
        '#type' => 'details',
        '#title' => $this->t('@shop_name warehouses:', ['@shop_name' => $shop->label()]),
        '#states' => [
          'invisible' => array(
            ':input[name="ctrl_quantity_in_wh[value]"]' => array('checked' => FALSE),
          ),
        ],
      ];

      if ($shop_whs = \Drupal::entityTypeManager()
            ->getStorage('bs_shop_warehouse')
            ->loadByProperties([
              'shops.target_id' => $shop->id(),
              'products_quantity_enabled' => 1,
            ])) {

              foreach ($shop_whs as $shop_warehouse) {

                $element[$shop->id()][$shop_warehouse->id()] = [
                  '#type' => 'number',
                  '#title' => $shop_warehouse->label(),
                  '#step' => 'any',
                  '#default_value' => $items->getQuantityByWarehouse($shop_warehouse->id()),
                ];

              }

      }

    }

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    $massaged_values = [];

    foreach ($values as $sid => $shop_by_wh_quantity) {
      foreach ($shop_by_wh_quantity as $wid => $quantity) {
        $massaged_values[] = [
          'wid' => $wid,
          'quantity' => $quantity,
        ];
      }
    }

    return $massaged_values;
  }

}
