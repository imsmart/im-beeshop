<?php
namespace Drupal\bs_shop_warehouse;

use Drupal\Core\Entity\EntityInterface;
use Drupal\beeshop\HierarchicalDraggableListBuilder;

class ShopWarehouseTypeListBuilder extends HierarchicalDraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_warehouse_types_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Shop type');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name']['data']['label_markup']['#markup'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

}

