<?php
namespace Drupal\bs_shop\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Language switcher' block.
 *
 * @Block(
 *   id = "shop_switcher_block",
 *   admin_label = @Translation("Shop switcher"),
 *   category = @Translation("BeeShop"),
 * )
 */
class ShopSwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The shop storage
   *
   * @var \Drupal\bs_shop\ShopStorageInterface
   */
  protected $shopStorage;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, PathMatcherInterface $path_matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathMatcher = $path_matcher;
    $this->shopStorage = \Drupal::entityTypeManager()->getStorage('bs_shop');

  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.matcher')
      );
  }

  public function build() {
    $build = [];
    $def_shop = \Drupal::service('bs_shop.default_shop_resolver')->resolve();

    foreach ($this->shopStorage->loadMultiple() as $shop) {
      /**
       * @var \Drupal\bs_shop\Entity\ShopInterface $shop
       */

      $build[$shop->id()] = [
        '#type' => 'link',
        '#title' => $shop->getName(),
        '#url' => $shop->getSwitchToShopUrl(),
        '#attributes' => [
          'data-shop-id' => $shop->id(),
        ]
      ];

      if ($def_shop && $def_shop->id() == $shop->id()) {
        $build[$shop->id()]['#attributes']['class'][] = 'active';
      }
    }

//     $type = $this->getDerivativeId();
//     $links = $this->languageManager->getLanguageSwitchLinks($type, Url::fromRoute($route_name));

//     if (isset($links->links)) {
//       $build = [
//         '#theme' => 'links__language_block',
//         '#links' => $links->links,
//         '#attributes' => [
//           'class' => [
//             "language-switcher-{$links->method_id}",
//             ],
//             ],
//             '#set_active_class' => TRUE,
//             ];
//     }

    $build['#cache'] = [
      'contexts' => [
        'bs_shop',
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('bs_shop'));
  }

  /**
   * {@inheritdoc}
   *
   * @todo Make cacheable in https://www.drupal.org/node/2232375.
   */
  public function getCacheMaxAge() {
    return 0;
  }
}

