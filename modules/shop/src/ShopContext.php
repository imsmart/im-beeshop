<?php

namespace Drupal\bs_shop;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\bs_shop\Resolver\ShopResolverInterface;
use Drupal\bs_shop\ShopContextInterface;

/**
 * Holds a reference to the active store, resolved on demand.
 *
 * The ChainStoreResolver runs the registered store resolvers one by one until
 * one of them returns the store.
 * The DefaultStoreResolver runs last, and will select the default store.
 * Custom resolvers can choose based on the url, the user's country, etc.
 *
 * Note that this functionality is optional, since not every site will be
 * limited to having only one active store at the time.
 *
 * @see \Drupal\commerce_store\Resolver\ChainStoreResolver
 * @see \Drupal\commerce_store\Resolver\DefaultStoreResolver
 */
class ShopContext implements ShopContextInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The chain resolver.
   *
   * @var \Drupal\bs_shop\Resolver\ShopResolverInterface
   */
  protected $shopResolver;

  /**
   * Static cache of resolved stores. One per request.
   *
   * @var \SplObjectStorage
   */
  protected $shops;

  /**
   * Constructs a new StoreContext object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\commerce_store\Resolver\ChainStoreResolverInterface $chain_resolver
   *   The chain resolver.
   */
  public function __construct(RequestStack $request_stack, ShopResolverInterface $shop_resolver) {
    $this->requestStack = $request_stack;
    $this->shopResolver = $shop_resolver;
    $this->shops = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function getShop() {
    $request = $this->requestStack->getCurrentRequest();
    if (!$this->shops->contains($request)) {
      $this->shops[$request] = $this->shopResolver->resolve();
    }

    return $this->shops[$request];
  }

}
