<?php

namespace Drupal\bs_shop\Cache\Context;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\bs_shop\ShopContextInterface;

/**
 * Defines the ShopCacheContext service, for "per shop" caching.
 *
 * Cache context ID: 'bs_shop'.
 */
class ShopCacheContext implements CacheContextInterface {

  /**
   * The store context.
   *
   * @var \Drupal\commerce_store\StoreContextInterface
   */
  protected $storeContext;

  /**
   * Constructs a new StoreCacheContext class.
   *
   * @param \Drupal\commerce_store\StoreContextInterface $context
   *   The store context.
   */
  public function __construct(ShopContextInterface $context) {
//     debug('ShopCacheContext::__construct()');
    $this->storeContext = $context;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Shop');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->storeContext->getShop()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
