<?php
namespace Drupal\bs_shop\Language;

use Drupal\language\LanguageNegotiator;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

class ByShopLanguageNegotiator extends LanguageNegotiator {
  protected $languageCode = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigurableLanguageManagerInterface $language_manager, PluginManagerInterface $negotiator_manager, ConfigFactoryInterface $config_factory, Settings $settings, RequestStack $requestStack) {
    $this->currentUser = \Drupal::currentUser();
    parent::__construct($language_manager, $negotiator_manager, $config_factory, $settings, $requestStack);
  }

  /**
   * {@inheritdoc}
   */
  public function initializeType($type) {
    $language = NULL;
    $method_id = static::METHOD_ID;
    $availableLanguages = $this->languageManager->getLanguages();

    if ($this->languageCode && isset($availableLanguages[$this->languageCode])) {
      $language = $availableLanguages[$this->languageCode];
    }
    else {
      // If no other language was found use the default one.
      $language = $this->languageManager->getDefaultLanguage();
    }
    return array($method_id => $language);
  }

  /**
   * @param string $languageCode
   */
  public function setLanguageCode($languageCode) {
    $this->languageCode = $languageCode;
  }
}

