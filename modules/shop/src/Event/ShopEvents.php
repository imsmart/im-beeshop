<?php

namespace Drupal\bs_shop\Event;

final class ShopEvents {

  /**
   * Name of the event fired after switching current shop.
   *
   * Fired after current shop is switched.
   *
   * @Event
   *
   * @see \Drupal\bs_shop\Event\CurrentShopEvent
   */
  const CURRENT_SHOP_SWITCHED = 'bs_shop.current_shop.switched';


}
