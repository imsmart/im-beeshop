<?php

namespace Drupal\bs_shop\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\bs_shop\Entity\ShopInterface;

/**
 * Defines the order event.
 *
 * @see \Drupal\commerce_order\Event\OrderEvents
 */
class CurrentShopEvent extends Event {

  /**
   * The current active shop.
   *
   * @var \Drupal\bs_shop\Entity\ShopInterface
   */
  protected $current_shop;

  /**
   * The current active shop.
   *
   * @var \Drupal\bs_shop\Entity\ShopInterface
   */
  protected $prev_current_shop;

  /**
   * Constructs a new CurrentShopEvent.
   */
  public function __construct() {
    $this->current_shop = \Drupal::service('bs_shop.default_shop_resolver')->resolve();
  }

  public function setCurrentShop(ShopInterface $shop) {
    $this->current_shop = $shop;
  }

  public function setPrevShop(ShopInterface $shop) {
    $this->prev_current_shop = $shop;

    return $this;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\bs_shop\Entity\ShopInterface
   *   Gets the current active shop.
   */
  public function getCurrentActiveShop() {
    return $this->current_shop;
  }

}
