<?php

namespace Drupal\bs_shop;

use Drupal\bs_shop\Entity\ShopInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the interface for store storage.
 */
interface ShopStorageInterface extends EntityStorageInterface {

  /**
   * Loads the default store.
   *
   * @return \Drupal\commerce_store\Entity\StoreInterface|null
   *   The default store, if known.
   */
  public function loadDefault();

  /**
   * Marks the provided store as the default.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   The new default store.
   */
  public function markAsDefault(ShopInterface $store);

}
