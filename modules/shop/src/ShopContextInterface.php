<?php

namespace Drupal\bs_shop;

/**
 * Holds a reference to the active store, resolved on demand.
 *
 * @see \Drupal\commerce_store\StoreContext
 */
interface ShopContextInterface {

  /**
   * Gets the active store for the current request.
   *
   * @return \Drupal\bs_shop\Entity\StoreInterface
   *   The active store.
   */
  public function getShop();

}
