<?php

namespace Drupal\bs_shop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the store type entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_shop_type",
 *   label = @Translation("Shop type"),
 *   label_singular = @Translation("shop type"),
 *   label_plural = @Translation("shop types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shop type",
 *     plural = "@count shop types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_shop\ShopTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\bs_shop\Form\ShopTypeForm",
 *       "edit" = "Drupal\bs_shop\Form\ShopTypeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer beeshop shop type",
 *   config_prefix = "bs_shop_type",
 *   bundle_of = "bs_shop",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "type",
 *     "label",
 *     "uuid",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/shops/shop-types/add",
 *     "edit-form" = "/admin/beeshop/config/shops/shop-types/{bs_shop_type}/edit",
 *     "delete-form" = "/admin/beeshop/config/shops/shop-types/{bs_shop_type}/delete",
 *     "collection" = "/admin/beeshop/config/shops/shop-types",
 *   }
 * )
 */
class ShopType extends ConfigEntityBundleBase implements ShopTypeInterface {

  /**
   * A brief description of this store type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
