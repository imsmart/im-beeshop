<?php

namespace Drupal\bs_shop\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for shop.
 */
interface ShopInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the shop name.
   *
   * @return string
   *   The shop name.
   */
  public function getName();

  /**
   * Sets the shop name.
   *
   * @param string $name
   *   The shop name.
   *
   * @return $this
   */
  public function setName($name);


  /**
   *
   */
  public function getShopCurrenciesSelectOptions();

}