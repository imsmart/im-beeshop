<?php

namespace Drupal\bs_shop\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;
use Drupal\Core\Url;


/**
 * Defines the shop entity class.
 *
 * @ContentEntityType(
 *   id = "bs_shop",
 *   label = @Translation("Shop"),
 *   label_singular = @Translation("shop"),
 *   label_plural = @Translation("shop"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shop",
 *     plural = "@count shops",
 *   ),
 *   bundle_label = @Translation("Shop type"),
 *   handlers = {
 *     "storage" = "Drupal\bs_shop\ShopStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\bs_shop\Form\ShopForm",
 *       "add" = "Drupal\bs_shop\Form\ShopForm",
 *       "edit" = "Drupal\bs_shop\Form\ShopForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "list_builder" = "Drupal\bs_shop\ShopListBuilder"
 *   },
 *   base_table = "bs_shop",
 *   data_table = "bs_shop_field_data",
 *   admin_permission = "administer bs_shop",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "shop_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/shop/{bs_shop}",
 *     "add-page" = "/admin/beeshop/config/shops/add",
 *     "add-form" = "/store/add/{bs_shop_type}",
 *     "edit-form" = "/shop/{bs_shop}/edit",
 *     "delete-form" = "/shop/{bs_shop}/delete",
 *     "delete-multiple-form" = "/admin/beeshop/config/shops/delete",
 *     "collection" = "/admin/beeshop/config/shops/collection",
 *   },
 *   bundle_entity_type = "bs_shop_type",
 *   field_ui_base_route = "entity.bs_shop_type.edit_form",
 * )
 */
class Shop extends ContentEntityBase implements ShopInterface {


  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations() {
    $operations = [];
    if ($this->access('update') && $this->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => t('Edit'),
        'weight' => 10,
        'url' => $this->urlInfo('edit-form'),
      ];
    }
    if ($this->access('delete') && $this->hasLinkTemplate('delete-form')) {
      $operations['delete'] = [
        'title' => t('Delete'),
        'weight' => 100,
        'url' => $this->urlInfo('delete-form'),
      ];
    }

    $operations += \Drupal::moduleHandler()->invokeAll('entity_operation', [$this]);
    \Drupal::moduleHandler()->alter('entity_operation', $operations, $this);
    uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getEntityKey('uid');
  }

  public function getShopPriceTypesTree() {
    return \Drupal::entityTypeManager()
    ->getStorage('bs_price_type')
    ->loadTree($this->id());
  }

  public function getSwitchToShopUrl() {
    $path_matcher = \Drupal::service('path.matcher');
    $route_name = $path_matcher->isFrontPage() ? '<front>' : '<current>';
    $url = Url::fromRoute($route_name);
    $url->setOption('query', ['shop' => $this->id(),]);
    return $url;
  }


  public function getShopCurrenciesCodes() {
    $c_codes = [];

    foreach ($this->get('currencies') as $shop_currency) {
      $c_codes[] = $shop_currency->entity->getCurrencyCode();
    }

    return $c_codes;
  }

  /**
   * {@inheritdoc}
   */
  public function getShopCurrenciesSelectOptions() {
    $shop_currencies_options = [];
    if ($shop_currencies = $this->get('currencies')->getValue()) {
      foreach ($shop_currencies as $shop_currency) {
        $shop_currencies_options[$shop_currency['target_id']] = $shop_currency['target_id'];
      }
    }

    return $shop_currencies_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultInterfaceLangcode() {
    return $this->get('def_interface_lang')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isDefault() {
    return $this->get('is_default')->value;
  }

  public function setAsDefault() {

    $this->setIsDefault(TRUE);

    return $this;
  }

  public function setIsDefault($value = FALSE) {

    $this->set('is_default', $value);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Type'))
    ->setDescription(t('The shop type.'))
    ->setSetting('target_type', 'bs_shop_type')
    ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Owner'))
    ->setDescription(t('The shop owner.'))
    //->setDefaultValueCallback('Drupal\commerce_store\Entity\Store::getCurrentUserId')
    ->setSetting('target_type', 'user')
    ->setDisplayOptions('form', [
      'type' => 'entity_reference_autocomplete',
      'weight' => 50,
    ]);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The shop name.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
    ->setLabel(t('Email'))
    ->setDescription(t('Shop email notifications are sent from this address. If empty site main e-mail address will be used.'))
    //->setRequired(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'email_default',
      'weight' => 1,
    ])
    ->setSetting('display_description', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Description'))
    ->setTranslatable(TRUE)
    ->setDisplayConfigurable('view', FALSE)
    ->setDisplayOptions('form', [
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE);

    $fields['currencies'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Currencies'))
    ->setDescription(t('Currencies available in the shop.'))
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_currency')
    ->setSetting('handler', 'default')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => 2,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['is_default'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Default'))
    ->setDescription(t('Whether the shop is default shop.'))
    ->setDefaultValue(FALSE);

    $fields['def_interface_lang'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Default interface language'))
    ->setDescription(t('Default interface language'))
    ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }
}