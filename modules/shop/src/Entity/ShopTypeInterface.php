<?php

namespace Drupal\bs_shop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for store types.
 */
interface ShopTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
