<?php
namespace Drupal\bs_shop\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class ShopsOverviewForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_shop_overview_shops';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $shops = \Drupal::entityTypeManager()
    ->getStorage('bs_shop')
    ->loadMultiple();

    $form['shops'] = [
      '#type' => 'table',
      '#header' => [$this->t('Name'), $this->t('Is default'), $this->t('Operations')],
      '#empty' => $this->t('No shops available. <a href=":link">Add new shop</a>.', [':link' => $this->url('entity.bs_shop.add_page')]),
      '#attributes' => [
        'id' => 'shops-list',
      ],
    ];

    foreach ($shops as $id => $shop) {
      $form['shops'][$id]['#shop'] = $shop;

      $form['shops'][$id]['name'] = [
        '#type' => 'markup',
        '#markup' => $shop->getName(),
      ];

      $form['shops'][$id]['is_default'] = [
        '#type' => 'radio',
        '#return_value' => $shop->id(),
        '#default_value' => $shop->isDefault() ? $shop->id() : 0,
        '#name' => 'default_shop',
        '#id' => 'default-shop',
        '#parents' => ['default_shop']
      ];

      $operations = [
        '#type' => 'operations',
        '#links' => $shop->getOperations(),
      ];

      $form['shops'][$id]['operations'] = $operations;

    }

    if (count($shops) > 1) {
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Save'),
      ];
    }

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($form['shops'])) {
      foreach (Element::children($form['shops']) as $id) {
        if (!empty($form['shops'][$id]['#shop'])) {
          $shop = $form['shops'][$id]['#shop'];
          $shop->setIsDefault($values['default_shop'] == $shop->id());
          $shop->save();
        }
      }
    }
  }

}

