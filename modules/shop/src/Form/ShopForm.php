<?php

namespace Drupal\bs_shop\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class ShopForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\bs_store\Entity\ShopInterface $shop */
    $shop = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

//     $shop = $this->entity;
//     dpm($shop->isNew());

//     if ($form_state->getValue('default')) {
//       /** @var \Drupal\commerce_store\StoreStorageInterface $store_storage */
// //       $store_storage = $this->entityTypeManager->getStorage('commerce_store');
// //       $store_storage->markAsDefault($this->entity);
//     }
//     drupal_set_message($this->t('The shop %label has been saved.', [
//       '%label' => $this->entity->label(),
//     ]));
    $form_state->setRedirect('entity.bs_shop.collection');
  }

}
