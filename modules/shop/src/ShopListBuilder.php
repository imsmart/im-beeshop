<?php

namespace Drupal\bs_shop;

use Drupal\bs_shop\Entity\ShopType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for stores.
 */
class ShopListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = t('Name');
    $header['type'] = t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\bs_shop\Entity\ShopInterface $entity */
    $shop_type = ShopType::load($entity->bundle());

    $row['name']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
    ] + $entity->toUrl()->toRenderArray();

    $row['type'] = $shop_type->label();

    return $row + parent::buildRow($entity);
  }

}
