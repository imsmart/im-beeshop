<?php

namespace Drupal\bs_shop\EventSubscriber;

use Drupal\bs_shop\Entity\Shop;
use Drupal\bs_shop\Language\ByShopLanguageNegotiator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Drupal\bs_shop\Event\CurrentShopEvent;
use Drupal\bs_shop\Event\ShopEvents;

class ShopEventSubscriber implements EventSubscriberInterface {

  public function checkForShopSwitching(GetResponseEvent $event) {
    $request = $event->getRequest();

    if ($shop_id = $request->query->get('shop')) {
      if ($shop = Shop::load($shop_id)) {
        $session = $request->getSession();

        $prev_active_shop = NULL;

        if ($prev_shop_id = $session->get('default_sid')) {
          $prev_active_shop = Shop::load($prev_shop_id);
        }

        //if (!$prev_shop_id || $prev_shop_id != $shop_id) {

          $session->set('default_sid', $shop_id);
          $session->save();
          user_cookie_save(['shop_id' => $shop_id]);

          $dispatcher = \Drupal::service('event_dispatcher');
          $event = new CurrentShopEvent();
          $event->setCurrentShop($shop);

          if ($prev_active_shop) {
            $event->setPrevShop($prev_active_shop);
          }

          $dispatcher->dispatch(ShopEvents::CURRENT_SHOP_SWITCHED, $event);

        //}
      }
    }
  }

  public function checkAndSwitchLangByCurrentShop(CurrentShopEvent $event) {
    $current_active_shop = $event->getCurrentActiveShop();

    if ($shop_lang = $current_active_shop->getDefaultInterfaceLangcode()) {
      $languageManager = \Drupal::languageManager();
      $current_lang = $languageManager->getCurrentLanguage()->getId();

      if ($current_lang != $shop_lang) {

        $path_matcher = \Drupal::service('path.matcher');
        $route_name = $path_matcher->isFrontPage() ? '<front>' : '<current>';
        $url = Url::fromRoute($route_name);

        $prev_url = $url->toString();

        /**
         * @var ByShopLanguageNegotiator $customLanguageNegotiator
         */
        $customLanguageNegotiator = \Drupal::service('bs_shop.language_negotiator');
        $languageManager->setNegotiator($customLanguageNegotiator);
        $languageManager->reset();
        $languageManager->getNegotiator()->setLanguageCode($shop_lang);

        $url = Url::fromRoute($route_name);

        if ($prev_url != $url->toString()) {
          $redirect_response = new RedirectResponse($url->toString(), 301);
          $redirect_response->send();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForShopSwitching', 30);
    $events[ShopEvents::CURRENT_SHOP_SWITCHED][] = ['checkAndSwitchLangByCurrentShop', 100];
    return $events;
  }

}