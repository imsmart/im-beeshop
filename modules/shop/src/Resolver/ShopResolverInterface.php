<?php

namespace Drupal\bs_shop\Resolver;

/**
 * Defines the interface for store resolvers.
 */
interface ShopResolverInterface {

  /**
   * Resolves the store.
   *
   * @return \Drupal\commerce_store\Entity\StoreInterface|null
   *   The store, if resolved. Otherwise NULL, indicating that the next
   *   resolver in the chain should be called.
   */
  public function resolve();

}
