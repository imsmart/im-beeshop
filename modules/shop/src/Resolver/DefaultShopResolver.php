<?php

namespace Drupal\bs_shop\Resolver;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Returns the default store, if known.
 */
class DefaultShopResolver implements ShopResolverInterface {

  /**
   * The store storage.
   *
   * @var \Drupal\bs_shop\ShopStorageInterface
   */
  protected $storage;

  /**
   * Constructs a new DefaultStoreResolver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('bs_shop');
  }

  /**
   * {@inheritdoc}
   */
  public function resolve() {

    if ($session = \Drupal::request()->getSession()) {
      if ($session_sid = $session->get('default_sid')) {
        if ($shop = $this->storage->load($session_sid)) {
          return $shop;
        }
      }
    }

    return $this->storage->loadDefault();
  }

}
