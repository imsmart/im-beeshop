<?php

namespace Drupal\bs_shop;

use Drupal\bs_shop\Entity\ShopInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * Defines the store storage.
 */
class ShopStorage extends SqlContentEntityStorage implements ShopStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadDefault() {
    $default = NULL;

    if ($entities = $this->loadByProperties(['is_default' => 1])) {
      return reset($entities);
    }

    if ($entities = $this->loadMultiple()) {
      return reset($entities);
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function markAsDefault(ShopInterface $store) {
//     $config = $this->configFactory->getEditable('commerce_store.settings');
//     if ($config->get('default_store') != $store->uuid()) {
//       $config->set('default_store', $store->uuid());
//       $config->save();
//     }
  }

}
