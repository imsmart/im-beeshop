<?php
namespace Drupal\bs_promotion;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class PromotionCouponListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'code' => $this->t('Code'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [
      'code' => $entity->label(),
      //'currencyCode' => $entity->id(),
    ];

    return $row + parent::buildRow($entity);
  }

}

