<?php

namespace Drupal\bs_promotion\Element;

use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 * @FormElement("bs_promotion_coupon_code")
 *
 */
class CouponCode extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    $info = parent::getInfo();

    $info['#validate'] = TRUE;
    $info['#process'][] = [$class, 'processValidation'];

    return $info;

  }


  public static function processValidation(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!empty($element['#validate'])) {
      $element['#element_validate'][] = [get_called_class(), 'validateCoupon'];
    }

    return $element;
  }

  public static function validateCoupon(&$element, FormStateInterface $form_state, &$complete_form) {
    if ($element['#value'] !== '') {

      $coupon_storage = \Drupal::entityTypeManager()->getStorage('bs_promotion_coupon');

      $coupon = $coupon_storage->loadCouponByCode($element['#value']);

      if (!$coupon || !$coupon->isValid()) {
        $message = !empty($coupon->validationErrorMessage) ?
          $coupon->validationErrorMessage :
          t('Coupon code "%code" is invalid.', ['%code' => $element['#value']]);

        $form_state->setError($element, $message);

      }
    }
  }

}

