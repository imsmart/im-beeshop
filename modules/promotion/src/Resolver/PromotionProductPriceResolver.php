<?php
namespace Drupal\bs_promotion\Resolver;

use Drupal\bs_product\Resolver\ProductPriceResolverBase;
use Drupal\bs_product\Plugin\ProductSelection\SelectionPluginManager;

class PromotionProductPriceResolver extends ProductPriceResolverBase {

  protected $activePromotions = NULL;

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::resolve()
   */
  public function resolve(\Drupal\beeshop\PurchasableEntityInterface $entity, $prev_resolved) {
      // TODO Auto-generated method stub

    $this->getActivePromotions($entity);

    if ($entity->hasField('field_exclude_from_promotions') &&
      $entity->get('field_exclude_from_promotions')->value) {
        return $prev_resolved;
      }

    /**
     *
     * @var SelectionPluginManager $product_select_pm
     */
    $product_select_pm = \Drupal::service('plugin.manager.product_selection');

    foreach ($this->activePromotions as $promotion_data) {

      $promotion = $promotion_data['promotion'];

      if (!$promotion->isActive()) {
        continue;
      }

      if (!$promotion->get('excluded_products_list_selector')->isEmpty()) {

        if ($promotion->excluded_products_list === NULL) {
          $plugin_settings = $promotion->get('excluded_products_list_selector')->first()->getValue();

          $selection_plugin = $product_select_pm->createInstance($plugin_settings['plugin_id'], $plugin_settings['plugin_configuration']);
          $promotion->excluded_products_list = $selection_plugin->getReferenceableProductIds();
        }

        if ($promotion->excluded_products_list) {
          $root_parent_id = $entity->getRootParentId();

          if (($root_parent_id && in_array($root_parent_id, $promotion->excluded_products_list)) ||
            in_array($entity->id(), $promotion->excluded_products_list)) {
            continue;
          }
        }

      }

      if (!$promotion->get('applicable_products_list_selector')->isEmpty()) {

        if ($promotion->applicable_products_list === NULL) {
          $plugin_settings = $promotion->get('applicable_products_list_selector')->first()->getValue();

          $selection_plugin = $product_select_pm->createInstance($plugin_settings['plugin_id'], $plugin_settings['plugin_configuration']);
          $promotion->applicable_products_list = $selection_plugin->getReferenceableProductIds();
        }

        if ($promotion->applicable_products_list) {
          if (!in_array($entity->id(), $promotion->applicable_products_list)) {
            if (($root_parent_id = $entity->getRootParentId()) && !in_array($root_parent_id, $promotion->applicable_products_list)) {
              continue;
            }
            else {
              continue;
            }
          }

        }

      }

      $promotion_price_resolver_context = $promotion->get('price_resolver_context')->value;

      if ($promotion_price_resolver_context && $entity->getPriceResolvingContext() != $promotion_price_resolver_context) {
        continue;
      }

      if ($promotion_price_resolver = $promotion_data['promotion']->get('discount_calculation_settings_pluging')->first()->getPluginInstance()) {
        $promotion_price_resolver->setContext($promotion_data['context']);
      }

      $prev_resolved = $promotion_price_resolver->resolve($entity, $prev_resolved);

    }

    return $prev_resolved;
  }

  protected function getActivePromotions($entity) {

//     if ($this->activePromotions !== NULL) {
//       return $this->activePromotions;
//     }

    $this->activePromotions = [];
    $cart = drupal_static('user_cart', NULL) ?: $entity->getPriceResolvingContextEntity('bs_cart');

    if ($cart) {
      if ($cart->hasField('coupons') && !$cart->get('coupons')->isEmpty()) {
        foreach ($cart->get('coupons')->referencedEntities() as $cart_coupon) {
          $promotion = $cart_coupon->getPromotion();
          $this->activePromotions[$promotion->id()] = [
            'promotion' => $promotion,
            'context' => [
                'coupon' => $cart_coupon,
            ],
          ];
        }
      }
    }

    return $this->activePromotions;
  }


}

