<?php

namespace Drupal\bs_promotion\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Ensures coupon code uniqueness.
 *
 * @Constraint(
 *   id = "promotion_coupon",
 *   label = @Translation("Coupon code", context = "Validation")
 * )
 */
class PromotionCouponConstraint extends UniqueFieldConstraint {

  public $message = 'The coupon %value is not valid or expired.';

}
