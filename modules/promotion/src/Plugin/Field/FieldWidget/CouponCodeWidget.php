<?php

namespace Drupal\bs_promotion\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\bs_cart\Event\CartEvents;
use Drupal\bs_cart\CartInterface;
use Drupal\beeshop\Ajax\BeeShopPageReload;
use Drupal\beeshop\Ajax\BeeshopSowAlert;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "bs_promo_coupon_activation",
 *   label = @Translation("Coupon activation"),
 *   description = @Translation("Coupon code activation field."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = true,
 * )
 */
class CouponCodeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => '10',
      'placeholder' => '',
      'validate' => TRUE,
      'coupon_field_description' => 'Clear coupon code field if You want to remove it',
      'widget_description' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return (!empty($field_definition->getItemDefinition()->getSettings()['target_type']) &&
      $field_definition->getItemDefinition()->getSettings()['target_type'] == 'bs_promotion_coupon') ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#min' => 1,
      '#required' => TRUE,
    ];

    $element['validate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate coupon before attach to entity'),
      '#default_value' => $this->getSetting('validate'),
    ];

    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    $element['coupon_field_description'] = [
      '#type' => 'textarea',
      '#title' => t('Coupon field description'),
      '#default_value' => $this->getSetting('coupon_field_description'),
    ];

    $element['widget_description'] = [
      '#type' => 'textarea',
      '#title' => t('Widget description'),
      '#default_value' => $this->getSetting('widget_description'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }
    else {
      $summary[] = t('No placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $result = parent::form($items, $form, $form_state, $get_delta);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    foreach ($items as $item) {
      if ($coupon = $item->entity) {
        $element['coupon_codes'][] = [
          '#type' => 'bs_promotion_coupon_code',
          '#size' => $this->getSetting('size'),
          '#default_value' => $coupon->get('code')->value,
          '#placeholder' => $this->getSetting('placeholder'),
          '#validate' => $this->getSetting('validate'),
          '#attributes' => [
            'class' => [
              'activated-coupon-code',
              'coupon-code'
            ]
          ],
          '#description' => $this->getSetting('coupon_field_description'),
        ];
      }
    }

    if (!count($items)) {
      $element['coupon_codes'][] = [
        '#type' => 'bs_promotion_coupon_code',
        '#size' => $this->getSetting('size'),
        '#placeholder' => $this->getSetting('placeholder'),
        '#validate' => $this->getSetting('validate'),
        '#attributes' => [
          'class' => [
            'new-coupon-code',
            'coupon-code'
          ]
        ],
      ];
    }

    $element['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply coupon(s)'),
      '#submit' => ['::submitForm', '::save'],
    ];


    if (!empty($form['#wrapper_id'])) {
      $element['apply']['#ajax'] = [
        'callback' => get_class($this) . '::applyResetCoupon',
        'wrapper' => $form['#wrapper_id'],
      ];
    }

    if ($this->getSetting('widget_description')) {
      $element['widget_description'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            'coupon-ativation-widget-description'
          ],
        ],
        'content' => [
          '#markup' => new TranslatableMarkup($this->getSetting('widget_description')),
        ]
      ];
    }

//     dpm($element);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element['target_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    $result_values = [];

    $coupon_storage = \Drupal::entityTypeManager()->getStorage('bs_promotion_coupon');

    foreach ($values['coupon_codes'] as $delta => $coupon_code) {
      if (!$coupon_code || !is_numeric($delta)) {
        continue;
      }

      if ($coupon = $coupon_storage->loadCouponByCode($coupon_code)) {
        $result_values[] = $coupon->id();
      }

    }

    return $result_values;
  }

  public static function applyResetCoupon(array $form, FormStateInterface $form_state) {

//     $cart = &drupal_static('user_cart', NULL);

//     $cart = \Drupal::service('current_user_cart');

//     /**
//      * @var CartInterface $cart
//      */

    $form_errors = $form_state->getErrors();

    if ($form_errors && empty($form_errors['coupons][coupon_codes][0'])) {
//       $cart = &drupal_static('user_cart', NULL);

      /**
       *
       * @var CartInterface $cart_original
       */
      $cart_original = $form_state->getFormObject()->getEntity();
      $cart_items = $cart_original->get('cart_items')->getValue();
      $cart = $form_state->getFormObject()->buildEntity($form, $form_state);
      $cart->set('cart_items', $cart_items);
//       $cart->validateAndFix();
      $cart->save();

    }
    else {
      $cart = $form_state->getFormObject()->getEntity();
    }


    $form = \Drupal::service('form_builder')->rebuildForm('bs_cart_edit_form', $form_state, $form);

    $request = \Drupal::request();
    $rm = \Drupal::service('current_route_match');
    $ajax_renderer = \Drupal::service('main_content_renderer.ajax');

    /**
     * @var AjaxResponse $response
     */
    $response = $ajax_renderer->renderResponse($form, $request, $rm);

    if (empty($form_errors['coupons][coupon_codes][0'])) {
//       $response->addCommand(new BeeShopPageReload());
    }
    else {

//       $cart->set('coupons', []);
//       $cart->save();

      $alert_command = new BeeshopSowAlert([
        'title' => 'Ошибка промокода',
        'text' => $form_errors['coupons][coupon_codes][0'],
        'type' => 'warning',
      ]);

      $response->addCommand($alert_command);
    }

//     $dispatcher = \Drupal::service('event_dispatcher');
//     $event = $cart->getCartEvent($response);
//     $dispatcher->dispatch(CartEvents::CART_UPDATED, $event);

    return $response;

  }

}
