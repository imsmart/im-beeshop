<?php
namespace Drupal\bs_promotion\Plugin\views\field;


use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to display entity label optionally linked to entity page.
 *
 * @ViewsField("coupon_discount_info")
 */
class PromotionCouponDiscountInfo extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $entity = $this->getEntity($values);

    if ($promotion = $entity->get('promotion_id')->entity) {
      if (!$promotion->get('discount_calculation_settings_pluging')->isEmpty()) {
        $context = [
          'coupon' => $entity,
        ];
        if ($pcp = $promotion->get('discount_calculation_settings_pluging')->first()->getPluginInstance()) {
          $pcp->setContext($context);
          return $pcp->getDiscountInfoSummary();
        }

      }
      else {
        return '-pcp not set for linked promotion-';
      }

    }

    return '-';

//     $value = $this->getValue($values);
//     return $this->sanitizeValue($value);
  }

}

