<?php
namespace Drupal\bs_promotion\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class PromotionCouponsLocalTasks extends DeriverBase {

  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['entity.entity.bs_promotion_coupon.promotion_coupons_list'] = $base_plugin_definition;
    $this->derivatives['entity.entity.bs_promotion_coupon.promotion_coupons_list']['title'] = "Coupons list";
    $this->derivatives['entity.entity.bs_promotion_coupon.promotion_coupons_list']['route_name'] = 'entity.bs_promotion_coupon.promotion_coupons_collection';
    $this->derivatives['entity.entity.bs_promotion_coupon.promotion_coupons_list']['base_route'] = 'entity.bs_promotion.canonical';

    return $this->derivatives;
  }

}

