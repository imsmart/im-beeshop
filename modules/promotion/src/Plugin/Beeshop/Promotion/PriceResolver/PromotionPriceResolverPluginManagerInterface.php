<?php

namespace Drupal\bs_promotion\Plugin\Beeshop\Promotion\PriceResolver;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines an interface for the entity reference selection plugin manager.
 */
interface PromotionPriceResolverPluginManagerInterface extends PluginManagerInterface {

  /**
   * Gets the plugin ID for a given target entity type and base plugin ID.
   *
   * @param string $target_type
   *   The target entity type.
   * @param string $base_plugin_id
   *   The base plugin ID (e.g. 'default' or 'views').
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId($target_type, $base_plugin_id);

}
