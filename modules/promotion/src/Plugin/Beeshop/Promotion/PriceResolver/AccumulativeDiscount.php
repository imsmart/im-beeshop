<?php
namespace Drupal\bs_promotion\Plugin\Beeshop\Promotion\PriceResolver;

use Drupal\bs_promotion\Annotation\PromotionPriceResolver;
use Drupal\bs_promotion\Entity\PromotionCouponInterface;
use Drupal\bs_price\Calculator;

/**
 *
 * @author andrey
 *
 * @PromotionPriceResolver(
 *   id = "bs_promo_accumulative_discount_pr",
 *   label = @Translation("Accumalative discount"),
 * )
 *
 */
class AccumulativeDiscount extends PromotionPriceResolverBase implements PromotionPriceResolverInterface {

  protected $context;

  public function setContext(array $context) {
    $this->context = $context;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::applies()
   */
  public function applies(\Drupal\beeshop\PurchasableEntityInterface $entity) {
      // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::resolve()
   */
  public function resolve(\Drupal\beeshop\PurchasableEntityInterface $entity, $prev_resolved) {
      // TODO Auto-generated method stub

    if ($product_price = $prev_resolved ?: $entity->getPrice()) {

      $coupon_discount_factors = $this->getCouponDiscountFactors();

      $factor_key = $product_price->isDiscounted() ? 'isDiscounted' : 'notDiscounted';

      if ($coupon_discount_factors && !empty($coupon_discount_factors[$factor_key])) {

        $price = clone $product_price;

        $product_price->set('discount_source', 'bs_promotions');
        $product_price->set('discount_reason', $this->context['coupon']->getPromotion()->id());
        $product_price->set('discount_amount', $coupon_discount_factors[$factor_key] . '%');
        $product_price->set('discount_description', '-' . $coupon_discount_factors[$factor_key] . '% по купону "' . $this->context['coupon']->get('code')->value . '"');
        $price->setOldPrice($product_price);

        $price->value = '' . $price->value * ((100 - $coupon_discount_factors[$factor_key])/100);
        $price->value = Calculator::roundTo($price->value, 10);

        return $price;
      }

      return $product_price;
    }

  }


  protected function getDiscountAmountForNotDiscountedPrice() {

    if (empty($this->context['coupon'])) return 0;

    return $this->context['coupon']->get('total_amount')->value >= 10000 ? 10 : 5;

  }

  protected function getDiscountAmountForDiscountedPrice() {

    if (empty($this->context['coupon'])) return 0;

    return $this->context['coupon']->get('total_amount')->value >= 10000 ? 3 : 2;

  }

  protected function getCouponDiscountFactors() {
    return [
      'isDiscounted' => $this->getDiscountAmountForDiscountedPrice(),
      'notDiscounted' => $this->getDiscountAmountForNotDiscountedPrice(),
    ];
  }

  public function getDiscountInfoSummary() {
    $discounts = $this->getCouponDiscountFactors();

    return $this->t('@for_not_discounted% / @for_discounted%', ['@for_not_discounted' => $discounts['notDiscounted'], '@for_discounted'  => $discounts['isDiscounted']]);

  }
}

