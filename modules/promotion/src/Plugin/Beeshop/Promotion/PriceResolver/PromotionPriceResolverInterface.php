<?php

namespace Drupal\bs_promotion\Plugin\Beeshop\Promotion\PriceResolver;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\bs_price\Resolver\PriceResolverInterface;

/**
 * Interface definition for Entity Reference Selection plugins.
 *
 * @see \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager
 * @see \Drupal\Core\Entity\Annotation\EntityReferenceSelection
 * @see plugin_api
 */
interface PromotionPriceResolverInterface extends PluginFormInterface, PriceResolverInterface {



}
