<?php
namespace Drupal\bs_promotion\Plugin\Beeshop\Promotion\PriceResolver;

use Drupal\bs_promotion\Annotation\PromotionPriceResolver;
use Drupal\bs_promotion\Entity\PromotionCouponInterface;
use Drupal\bs_price\Calculator;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 * @author andrey
 *
 * @PromotionPriceResolver(
 *   id = "bs_promo_simple_discount_pr",
 *   label = @Translation("Simple discount"),
 * )
 *
 */
class SimpleDiscount extends PromotionPriceResolverBase implements PromotionPriceResolverInterface {

  protected $context;

  public function setContext(array $context) {
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['nd_price_discount'] = [
      '#title' => 'Discount amount (%)',
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#default_value' => !empty($this->getConfiguration()['nd_price_discount']) ? $this->getConfiguration()['nd_price_discount'] : 0,
    ];

//     $view_settings = $this->getConfiguration()['view'];
//     $displays = Views::getApplicableViews('product_ids_select');
//     //     // Filter views that list the entity type we want, and group the separate
//     //     // displays by view.
//     $entity_type = $this->entityManager->getDefinition('bs_product');
//     $view_storage = $this->entityManager->getStorage('view');

//     $options = [];
//     foreach ($displays as $data) {
//       list($view_id, $display_id) = $data;
//       $view = $view_storage->load($view_id);
//       if (in_array($view->get('base_table'), [$entity_type->getBaseTable(), $entity_type->getDataTable()])) {
//         $display = $view->get('display');
//         $options[$view_id . ':' . $display_id] = $view_id . ' - ' . $display[$display_id]['display_title'];
//       }
//     }

//     //     // The value of the 'view_and_display' select below will need to be split
//     //     // into 'view_name' and 'view_display' in the final submitted values, so
//     //     // we massage the data at validate time on the wrapping element (not
//     //     // ideal).
//     $form['view']['#element_validate'] = [[get_called_class(), 'settingsFormValidate']];

//     if ($options) {
//       $default = !empty($view_settings['view_name']) ? $view_settings['view_name'] . ':' . $view_settings['display_name'] : NULL;
//       $form['view']['view_and_display'] = [
//         '#type' => 'select',
//         '#title' => $this->t('View used to select the entities'),
//         '#required' => TRUE,
//         '#options' => $options,
//         '#default_value' => $default,
//         '#description' => '<p>' . $this->t('Choose the view and display that select the entities that can be referenced.<br />Only views with a display of type "Entity Reference" are eligible.') . '</p>',
//       ];

//       $default = !empty($view_settings['arguments']) ? implode(', ', $view_settings['arguments']) : '';
//       $form['view']['arguments'] = [
//         '#type' => 'textfield',
//         '#title' => $this->t('View arguments'),
//         '#default_value' => $default,
//         '#required' => FALSE,
//         '#description' => $this->t('Provide a comma separated list of arguments to pass to the view.'),
//       ];
//     }
//     else {
//       if ($this->currentUser->hasPermission('administer views') && $this->moduleHandler->moduleExists('views_ui')) {
//         $form['view']['no_view_help'] = [
//           '#markup' => '<p>' . $this->t('No eligible views were found. <a href=":create">Create a view</a> with an <em>Entity Reference</em> display, or add such a display to an <a href=":existing">existing view</a>.', [
//             ':create' => Url::fromRoute('views_ui.add')->toString(),
//             ':existing' => Url::fromRoute('entity.view.collection')->toString(),
//           ]) . '</p>',
//         ];
//       }
//       else {
//         $form['view']['no_view_help']['#markup'] = '<p>' . $this->t('No eligible views were found.') . '</p>';
//       }
//     }
    return $form;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::applies()
   */
  public function applies(\Drupal\beeshop\PurchasableEntityInterface $entity) {
      // TODO Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\bs_price\Resolver\PriceResolverInterface::resolve()
   */
  public function resolve(\Drupal\beeshop\PurchasableEntityInterface $entity, $prev_resolved) {
      // TODO Auto-generated method stub

    if ($product_price = $prev_resolved ?: $entity->getPrice()->first()) {

      if (!empty($this->getConfiguration()['nd_price_discount']) && !$product_price->isDiscounted()) {

        $price = clone $product_price;

        $product_price->set('discount_source', 'bs_promotions');
        $product_price->set('discount_reason', $this->context['coupon']->getPromotion()->id());
        //$product_price->set('discount_amount', $this->getConfiguration()['nd_price_discount'] . '%');
        //$product_price->set('discount_description', '-' . $coupon_discount_factors[$factor_key] . '% по купону "' . $this->context['coupon']->get('code')->value . '"');
        $price->setOldPrice($product_price);

        $price->value = '' . $price->value * ((100 - $this->getConfiguration()['nd_price_discount'])/100);
        //$price->value = Calculator::roundTo($price->value, 1);

        return $price;

      }

      return $product_price;
    }

  }

}

