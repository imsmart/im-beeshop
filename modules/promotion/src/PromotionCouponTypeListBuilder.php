<?php

namespace Drupal\bs_promotion;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\beeshop\HierarchicalDraggableListBuilder;
use Drupal\Core\Routing\RedirectDestinationTrait;

/**
 * Defines the list builder for currencies.
 */
class PromotionCouponTypeListBuilder extends HierarchicalDraggableListBuilder {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_promotion_coupon_type_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => $this->t('Name'),
      //'currencyCode' => $this->t('Currency code'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['name']['data']['label_markup']['#markup'] = $entity->label();
//    $row['variationsSupport']['data']['label_markup']['#markup'] = $entity->isVariationsSuport() ? '+' : '-';

//     $row = [
//       'name' => $entity->label(),
//       'variationsSupport' => $entity->isVariationsSuport() ? '+' : '-',
//     ];

//     if ($entity->isVariationsSuport()) {
//       $row['operations']['data']['#links']['manage-variation-structure'] = [
//         'title' => $this->t('Manage variations structure'),
//         'url' => $entity->toUrl('variations-structure'),
//         'weight' => 30,
//       ];
//     }

    return $row + parent::buildRow($entity);
  }

}
