<?php
namespace Drupal\bs_promotion\Form;

use Drupal\Core\Form\FormBase;

class PrommotionCouponActivationForm extends FormBase {
  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
      // TODO Auto-generated method stub
      return 'promotion-coupon-actiovation-form';
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

    $active_coupon = bs_promotion_get_activated_coupon();

    $form['coupon_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coupon code'),
      '#required' => TRUE,
      '#default_value' => $active_coupon ? $active_coupon->label() : '',
      '#disabled' => $active_coupon ? TRUE : FALSE,
      '#description' => !$active_coupon ?
                        $this->t('If You have discount coupon code, You can activate it by entering it in this field and press "Activate coupon" button below.') :
                        $this->t('To deactivate active coupon please press "Deactivate coupon" button bellow'),
    ];

    $form['actions'] = [
      '#type' => 'actions',

    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $active_coupon ? $this->t('Deactivate coupon') : $this->t('Activate coupon'),
      '#submit' => $active_coupon ? ['::deactivateCoupon'] : ['::submitForm'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

    $coupon_code = $form_state->getValue('coupon_code');

    if (!$coupon_code) return;

    /**
     * @var \Drupal\bs_promotion\PromotionCouponStorageInterface $promotion_coupon_storage
     */
    $promotion_coupon_storage = \Drupal::entityTypeManager()->getStorage('bs_promotion_coupon');

    $coupon = $promotion_coupon_storage->loadCouponByCode($coupon_code);

    if (!$coupon) {
      drupal_set_message('Coupon with specifed code was not found!', 'warning');
      return;
    }

    if (!$coupon->isValid()) {
      drupal_set_message('Coupon with specifed code is no loger valid or expired!', 'warning');
      return;
    }

    $coupon->activate();
  }


  public function deactivateCoupon() {
    bs_promotion_deactivate_coupons();
  }

}

