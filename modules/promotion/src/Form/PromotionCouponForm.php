<?php
namespace Drupal\bs_promotion\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_promotion\Entity\PromotionInterface;

class PromotionCouponForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $coupon = $this->entity;

//     if ($coupon->isNew()) {
//       $coupon_code = $this->generateCouponCode();
//       $coupon->set('code', $coupon_code);

//       $rm = \Drupal::routeMatch();
//       $promotion = $rm->getParameter('bs_promotion');

//       $coupon->set('promotion_id', $promotion->id());
//     }

    $form = parent::form($form, $form_state);

    return $form;
  }


  private function generateCouponCode() {

    $coupon = '';

    $symbols    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 1, 2, 3, 4, 5, 6, 7, 8, 9];

    for ($i = 0; $i < 10; $i++) {
      $coupon .= $symbols[mt_rand(0, count($symbols) - 1)];
    }

    return $coupon;
  }

}

