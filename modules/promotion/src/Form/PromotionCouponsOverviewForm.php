<?php
namespace Drupal\bs_promotion\Form;

use Drupal\bs_promotion\Entity\PromotionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class PromotionCouponsOverviewForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   * @var \Drupal\bs_promotion\PromotionCouponStorageInterface
   */
  protected $promotionCouponStorage;

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
      // TODO Auto-generated method stub
    return 'promotion-coupons-collection-form';
  }

  /**
   * Constructs an PromotionsOverviewForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->promotionCouponStorage = $entity_type_manager->getStorage('bs_promotion_coupon');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
      // TODO Auto-generated method stub

    $destination = $this->getDestinationArray();

    $rm = \Drupal::routeMatch();
    $bs_promotion = $rm->getParameter('bs_promotion');

    $promotion_coupons = $this->promotionCouponStorage->loadPromotionCoupons($bs_promotion->id());

    $form['add_new_coupon_link'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<a href=":link">Add new coupon to promotion</a>.',  [':link' => $this->url('entity.bs_promotion_coupon.add_form', ['bs_promotion' => $bs_promotion->id()])]),
    ];

    $form['promotion_coupons'] = [
      '#type' => 'table',
      '#header' => [$this->t('Code'), $this->t('Operations')],
      '#empty' => $this->t('No promotion coupons available. <a href=":link">Add new coupon to promotion</a>.', [':link' => $this->url('entity.bs_promotion_coupon.add_form', ['bs_promotion' => $bs_promotion->id()])]),
      '#attributes' => [
        'id' => 'promotions-overview',
      ],
    ];

    foreach ($promotion_coupons as $id => $coupon) {
      $form['promotion_coupons'][$id]['#promotion_coupon'] = $coupon;

      $form['promotion_coupons'][$id]['promotion_coupon'] = [
        '#type' => 'markup',
        '#markup' => $coupon->label(),
      ];
      //       $form['types'][$key]['type']['id'] = [
      //         '#type' => 'hidden',
      //         '#value' => $type->id(),
      //         '#attributes' => [
        //           'class' => ['type-id'],
      //         ],
      //       ];
      //       $form['types'][$key]['type']['parent'] = [
        //         '#type' => 'hidden',
      //         // Yes, default_value on a hidden. It needs to be changeable by the
      //         // javascript.
      //         '#default_value' => $type->parents[0],
      //         '#attributes' => [
        //           'class' => ['type-parent'],
        //         ],
      //       ];
      //       $form['types'][$key]['type']['depth'] = [
      //         '#type' => 'hidden',
      //         // Same as above, the depth is modified by javascript, so it's a
      //         // default_value.
      //         '#default_value' => isset($type->depth) ? $type->depth : 0,
      //         '#attributes' => [
        //           'class' => ['type-depth'],
        //         ],
      //       ];

      //       $form['types'][$key]['weight'] = [
      //         '#type' => 'weight',
      //         '#delta' => $delta,
      //         '#title' => $this->t('Weight for added term'),
      //         '#title_display' => 'invisible',
      //         '#default_value' => $type->getWeight(),
      //         '#attributes' => [
        //           'class' => ['type-weight'],
        //         ],
      //       ];

      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          'query' => $destination,
          'url' => $coupon->toUrl('edit-form'),
        ],
        'delete' => [
          'title' => $this->t('Delete'),
          'query' => $destination,
          'url' => $coupon->toUrl('delete-form'),
        ],
      ];

      $form['promotion_coupons'][$id]['operations'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      //$form['promotions'][$key]['#attributes']['class'][] = 'draggable';
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

  }


}

