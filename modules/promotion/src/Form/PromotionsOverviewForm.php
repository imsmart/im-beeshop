<?php
namespace Drupal\bs_promotion\Form;

use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;

class PromotionsOverviewForm extends FormBase {


  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   * @var \Drupal\bs_promotion\PromotionStorageInterface
   */
  protected $promotionStorage;

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'promotions_overview_form';
  }

  /**
   * Constructs an PromotionsOverviewForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->promotionStorage = $entity_type_manager->getStorage('bs_promotion');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {

    $promotions = $this->promotionStorage->loadForOverviewForm();
    $destination = $this->getDestinationArray();

    $form['promotions'] = [
      '#type' => 'table',
      '#header' => [$this->t('Name'), /*$this->t('Weight'),*/ $this->t('Description'), $this->t('Operations')],
      '#empty' => $this->t('No promotions available. <a href=":link">Add new promotion</a>.', [':link' => $this->url('entity.bs_promotion.add_page')]),
      '#attributes' => [
        'id' => 'promotions-overview',
      ],
    ];


    foreach ($promotions as $id => $promotion) {
      $form['promotions'][$id]['#promotion'] = $promotion;

      $form['promotions'][$id]['promotion'] = [
        '#type' => 'markup',
        '#markup' => $promotion->label(),
      ];
//       $form['types'][$key]['type']['id'] = [
//         '#type' => 'hidden',
//         '#value' => $type->id(),
//         '#attributes' => [
//           'class' => ['type-id'],
//         ],
//       ];
//       $form['types'][$key]['type']['parent'] = [
//         '#type' => 'hidden',
//         // Yes, default_value on a hidden. It needs to be changeable by the
//         // javascript.
//         '#default_value' => $type->parents[0],
//         '#attributes' => [
//           'class' => ['type-parent'],
//         ],
//       ];
//       $form['types'][$key]['type']['depth'] = [
//         '#type' => 'hidden',
//         // Same as above, the depth is modified by javascript, so it's a
//         // default_value.
//         '#default_value' => isset($type->depth) ? $type->depth : 0,
//         '#attributes' => [
//           'class' => ['type-depth'],
//         ],
//       ];

//       $form['types'][$key]['weight'] = [
//         '#type' => 'weight',
//         '#delta' => $delta,
//         '#title' => $this->t('Weight for added term'),
//         '#title_display' => 'invisible',
//         '#default_value' => $type->getWeight(),
//         '#attributes' => [
//           'class' => ['type-weight'],
//         ],
//       ];

      $form['promotions'][$id]['descripton'] = [
        '#type' => 'markup',
        '#markup' => $promotion->getDescription(),
      ];

      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          'query' => $destination,
          'url' => $promotion->toUrl('edit-form'),
        ],
//         'add_coupon' => [
//           'title' => $this->t('Add coupon'),
//           'query' => $destination,
//           'url' => Url::fromRoute('entity.bs_promotion_coupon.promotion_coupons_collection', ['bs_promotion' => $promotion->id()]),
//         ],
        'delete' => [
          'title' => $this->t('Delete'),
          'query' => $destination,
          'url' => $promotion->toUrl('delete-form'),
        ],
      ];

      if ($promotion->getBundleEntity()->isCouponsSupported()) {
//         $operations['coupons_list'] = [
//           'title' => $this->t('Coupons list'),
//           'query' => $destination,
//           'url' => Url::fromRoute('entity.bs_promotion_coupon.promotion_coupons_collection', ['bs_promotion' => $promotion->id()]),
//         ];
      }

      $form['promotions'][$id]['operations'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      //$form['promotions'][$key]['#attributes']['class'][] = 'draggable';
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

  }

}

