<?php

namespace Drupal\bs_promotion\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_promotion\Entity\PromotionTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Form handler for promotion type forms.
 *
 * @internal
 */
class PromotionTypeForm extends BundleEntityFormBase {

  /**
   * @var \Drupal\bs_promotion\Entity\PromotionTypeInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $type = $this->entity;

    $form['name'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => $type->label(),
      '#description' => t('The human-readable name of this content type. This text will be displayed as part of the list on the <em>Add content</em> page. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['type'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => $type->isLocked(),
      '#machine_name' => [
        'exists' => ['Drupal\bs_promotion\Entity\PromotionType', 'load'],
        'source' => ['name'],
      ],
      '#description' => t('A unique machine-readable name for this promotion type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %promotion-add page, in which underscores will be converted into hyphens.', [
        '%promotion-add' => t('Add promotion'),
      ]),
    ];

    $form['description'] = [
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->getDescription(),
      '#description' => t('This text will be displayed on the <em>Add new promotion</em> page.'),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
      '#attached' => [
//         'library' => ['node/drupal.content_types'],
      ],
    ];

    $form['coupons'] = [
      '#type' => 'details',
      '#title' => t('Coupons settings'),
      '#group' => 'additional_settings',
    ];
    $form['coupons']['coupons_supported'] = [
      '#type' => 'checkbox',
      '#title' => t('Support coupons'),
      '#default_value' => $type->isCouponsSupported(),
      '#description' => t('The promotion type support coupons.'),
    ];

    return $this->protectBundleIdElement($form);
  }

}

