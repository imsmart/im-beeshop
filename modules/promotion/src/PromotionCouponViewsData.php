<?php
namespace Drupal\bs_promotion;

use Drupal\views\EntityViewsData;

class PromotionCouponViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bs_promotion_coupon']['coupon_discount_info'] = [
      'title' => $this->t('Coupon discount info'),
      'field' => [
        'id' => 'coupon_discount_info',
        'title' => t('Coupon discount info'),
      ],
    ];


    return $data;
  }

}

