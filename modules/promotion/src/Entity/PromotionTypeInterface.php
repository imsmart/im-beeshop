<?php

namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface PromotionTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this promotion type.
   */
  public function getDescription();

  /**
   * Gets the help information.
   *
   * @return string
   *   The help information of this promotion type.
   */
  public function getHelp();

  /**
   * Determines whether the promotion type is locked.
   *
   * @return string|false
   *   The module name that locks the type or FALSE.
   */
  public function isLocked();

  /**
   * Determines whether the promotion type support coupons.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isCouponsSupported();


}

