<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface PromotionInterface extends ContentEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this promotion.
   */
  public function getDescription();

  /**
   *
   * @return bool
   */
  public function isActive();
}

