<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\beeshop\EntityBundleTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines the Coupon entity.
 *
 * @ContentEntityType(
 *   id = "bs_promotion_coupon",
 *   label = @Translation("Promotion coupon"),
 *   label_singular = @Translation("coupon"),
 *   label_plural = @Translation("coupons"),
 *   label_count = @PluralTranslation(
 *     singular = "@count coupon",
 *     plural = "@count coupons",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_promotion\PromotionCouponStorage",
 *     "list_builder" = "Drupal\bs_promotion\PromotionCouponListBuilder",
 *     "views_data" = "Drupal\bs_promotion\PromotionCouponViewsData",
 *     "route_provider" = {
 *       "default" = "Drupal\bs_promotion\Routing\PromotionCouponRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\bs_promotion\Form\PromotionCouponForm",
 *       "add" = "Drupal\bs_promotion\Form\PromotionCouponForm",
 *       "edit" = "Drupal\bs_promotion\Form\PromotionCouponForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *   },
 *   base_table = "bs_promotion_coupon",
 *   admin_permission = "administer bs_promotion",
 *   fieldable = TRUE,
 *   field_ui_base_route = "entity.bs_promotion_coupon_type.edit_form",
 *   bundle_entity_type = "bs_promotion_coupon_type",
 *   common_reference_target = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "code",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-page" = "/admin/beeshop/promotions/coupons/add",
 *     "add-form" = "/admin/beeshop/promotions/coupons/add/{bs_promotion_coupon_type}",
 *     "edit-form" = "/admin/beeshop/promotions/coupons/{bs_promotion_coupon}/edit",
 *     "delete-form" = "/admin/beeshop/promotions/coupons/{bs_promotion_coupon}/delete",
 *     "collection" = "/admin/beeshop/promotions/coupons",
 *   },
 * )
 */
class PromotionCoupon extends ContentEntityBase implements PromotionCouponInterface {

  use EntityBundleTrait;

  public static function loadByCode($coupon_code) {
    $entity_query = \Drupal::entityQuery('bs_promotion_coupon');
    $entity_query->condition('code', $coupon_code);

    $ids = $entity_query->execute();

    $coupons = self::loadMultiple($ids);

    return $coupons ? reset($coupons) : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The promotion backreference, populated by Promotion::postSave().
    $fields['promotion_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Promotion'))
    ->setDescription(t('The parent promotion.'))
    ->setSetting('target_type', 'bs_promotion')
    ->setDisplayConfigurable('form', TRUE)
    ->setRequired(TRUE)
    ->setReadOnly(TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Coupon code'))
    ->setDescription(t('The unique, machine-readable identifier for a coupon.'))
    ->addConstraint('CouponCode')
    ->setSettings([
      'max_length' => 50,
      'text_processing' => 0,
    ])
    ->setDefaultValue('')
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'string',
      'weight' => -4,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the product was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['activations_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Coupon usage count limit'))
      ->setDescription(t('Coupon usage count limit.'));

    $fields['usage_count_limit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Coupon usage count limit'))
      ->setDescription(t('Coupon usage count limit.'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['total_amount'] = BaseFieldDefinition::create('bs_fin_amount')
      ->setLabel(t('Orders total amount by coupon'))
      ->setDescription(t('Orders total amount by coupon'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['orders'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Orders submited with coupon'))
      ->setDescription(t('Orders submited with coupon.'))
      ->setSetting('target_type', 'bs_order')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setComputed(TRUE);

    $fields['custom_discount_settings'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Custom discount settings'))
      ->setDescription(t('Will this coupon determine the parameters for calculating the discount by its own user settings?.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 100,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the coupon is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Enabled'),
        'off_label' => t('Disabled'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 100,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

  public function increaseActivationsCount($inc_value = 1) {
    $this->set('activations_count', $this->get('activations_count')->value + intval($inc_value));
  }

  /**
   * {@inheritDoc}
   */
  public function getCode() {
    return $this->get('code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {

    $fields = [];

    if (($bundle_entity = PromotionCouponType::load($bundle)) &&
      $bundle_entity->isValidForPeriod()) {

//         $fields['start_date'] = BaseFieldDefinition::create('datetime')
//           ->setLabel(t('Start date'))
//           ->setDescription(t('The date the promotion coupon becomes valid.'))
//           ->setSetting('datetime_type', 'date')
//           ->setDisplayOptions('form', [
//             'type' => 'datetime_default',
//             'weight' => 5,
//           ])
//           ->setDisplayConfigurable('form', TRUE);

//         $fields['end_date'] = BaseFieldDefinition::create('datetime')
//           ->setLabel(t('End date'))
//           ->setDescription(t('The date after which the promotion coupon is invalid.'))
//           ->setRequired(FALSE)
//           ->setSetting('datetime_type', 'date')
//           ->setDisplayOptions('form', [
//             'type' => 'datetime_default',
//             'weight' => 6,
//           ])
//           ->setDisplayConfigurable('form', TRUE);

      }

      return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndDate() {
    return $this->get('end_date')->date;
  }

  /**
   * {@inheritdoc}
   */
  public function getStartDate() {
    return $this->get('start_date')->date;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  public function isExpired() {

    if (!$this->getBundleEntity()->isValidForPeriod()) {
      return FALSE;
    }

    $time = \Drupal::time()->getRequestTime();

    $start_date = $this->getStartDate()->format('U');
    $end_date = $this->getEndDate();

    $result = TRUE;

    if ( $start_date && $start_date > $time) {
      return FALSE;
    }


    if ($end_date && $end_date->format('U') >= $time) {
      return TRUE;
    }

    return FALSE;
  }

  public function isValid() {

    if ($this->get('promotion_id')->isEmpty()) {
      return FALSE;
    }

    if (!$this->get('promotion_id')->entity->isActive()) {
      return FALSE;
    }

    $validation_result = TRUE;

    \Drupal::moduleHandler()->alter('bs_promotion_coupon_validation_result', $validation_result, $this);

    return $validation_result;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($operation == 'view label') {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

  public function activate() {
    if ($session = \Drupal::request()->getSession()) {
      $coupons = [];//$session->get('bs_promotion_coupons', []);
      $coupons[] = $this->id();
      $session->set('bs_promotion_coupons', $coupons);

      $cart_provider = \Drupal::service('bs_cart.cart_provider');
      $cart = $cart_provider->getCart();

      $cart->save();
    }
  }

  public function getPromotion() {
    return $this->get('promotion_id')->entity;
  }

  public function getCouponDiscountAmount() {
    if ($promotions = $this->promotion_id->referencedEntities()) {
      $coupon_promotion = reset($promotions);
      return $coupon_promotion->discount_value->value;
    }
  }

}

