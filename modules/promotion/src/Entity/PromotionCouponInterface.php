<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface PromotionCouponInterface extends ContentEntityInterface {

  /**
   * The coupon code
   *
   * @return string the coupon code
   */
  public function getCode();

}

