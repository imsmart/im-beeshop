<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Promotion type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "bs_promotion_coupon_type",
 *   label = @Translation("Promotion coupon type"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\bs_promotion\Form\PromotionCouponTypeForm",
 *       "add" = "Drupal\bs_promotion\Form\PromotionCouponTypeForm",
 *       "edit" = "Drupal\bs_promotion\Form\PromotionCouponTypeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_promotion\PromotionCouponTypeListBuilder",
 *   },
 *   admin_permission = "administer bs_promotion types",
 *   config_prefix = "coupon_type",
 *   bundle_of = "bs_promotion_coupon",
 *   hierarchy_inheritance_support = TRUE,
 *   default_hierarchy_mode = "strict",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name",
 *     "parent" = "parent",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/promotions/coupon-types/add",
 *     "edit-form" = "/admin/beeshop/promotions/coupon-types/{bs_promotion_coupon_type}/edit",
 *     "delete-form" = "/admin/beeshop/promotions/coupon-types/{bs_promotion_coupon_type}/delete",
 *     "collection" = "/admin/beeshop/promotions/coupon-types",
 *   },
 *   config_export = {
 *     "name",
 *     "type",
 *     "description",
 *     "help",
 *     "with_validity_period",
 *     "parent",
 *     "weight"
 *   }
 * )
 */
class PromotionCouponType extends ConfigEntityBundleBase implements PromotionCouponTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return $this->help;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('bs_promotion.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  public function isValidForPeriod() {
    return $this->with_validity_period;
  }

}

