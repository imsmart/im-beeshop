<?php

namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Promotion type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "bs_promotion_type",
 *   label = @Translation("Promotion type"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\bs_promotion\Form\PromotionTypeForm",
 *       "add" = "Drupal\bs_promotion\Form\PromotionTypeForm",
 *       "edit" = "Drupal\bs_promotion\Form\PromotionTypeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_promotion\Routing\PromotionTypeRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_promotion\PromotionTypeListBuilder",
 *   },
 *   admin_permission = "administer bs_promotion types",
 *   config_prefix = "type",
 *   bundle_of = "bs_promotion",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name"
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/promotions/types/add",
 *     "edit-form" = "/admin/beeshop/promotions/types/{bs_promotion_type}/edit",
 *     "delete-form" = "/admin/beeshop/promotions/types/{bs_promotion_type}/delete",
 *     "collection" = "/admin/beeshop/promotions/types",
 *   },
 *   config_export = {
 *     "name",
 *     "type",
 *     "description",
 *     "help",
 *     "with_validity_period",
 *     "coupons_supported",
 *   }
 * )
 */
class PromotionType extends ConfigEntityBundleBase implements PromotionTypeInterface {

  /**
   * The machine name of this promotion type.
   *
   * @var string
   *
   * @todo Rename to $id.
   */
  protected $type;

  /**
   * The human-readable name of the promotion type.
   *
   * @var string
   *
   * @todo Rename to $label.
   */
  protected $name;

  /**
   * A brief description of this promotion type.
   *
   * @var string
   */
  protected $description;

  /**
   * Help information shown to the user when creating a Node of this type.
   *
   * @var string
   */
  protected $help;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return $this->help;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('bs_promotion.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isCouponsSupported() {
    return $this->coupons_supported;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidForPeriod() {
    return $this->with_validity_period;
  }

}

