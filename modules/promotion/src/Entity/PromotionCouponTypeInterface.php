<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface PromotionCouponTypeInterface extends ConfigEntityInterface {

  /**
   * @return bool
   */
  public function isValidForPeriod();

}

