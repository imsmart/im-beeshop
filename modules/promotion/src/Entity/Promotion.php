<?php
namespace Drupal\bs_promotion\Entity;

use Drupal\bs_promotion\Entity\PromotionInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\beeshop\EntityBundleTrait;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Condition\ConditionPluginBase;

/**
 * Defines the promotion entity class.
 *
 * @ContentEntityType(
 *   id = "bs_promotion",
 *   label = @Translation("Promotion"),
 *   label_collection = @Translation("Promotions"),
 *   label_singular = @Translation("promotion"),
 *   label_plural = @Translation("promotions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count promotion",
 *     plural = "@count promotions",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_promotion\PromotionStorage",
 *     "list_builder" = "Drupal\bs_promotion\Form\PromotionsOverviewForm",
 *     "route_provider" = {
 *       "default" = "Drupal\bs_promotion\Routing\PromotionRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\bs_promotion\Form\PromotionForm",
 *       "add" = "Drupal\bs_promotion\Form\PromotionForm",
 *       "edit" = "Drupal\bs_promotion\Form\PromotionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *   },
 *   base_table = "bs_promotion",
 *   data_table = "bs_promotion_field_data",
 *   admin_permission = "administer bs_promotion",
 *   fieldable = TRUE,
 *   bundle_entity_type = "bs_promotion_type",
 *   permission_granularity = "bundle",
 *   field_ui_base_route = "entity.bs_promotion_type.edit_form",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "promotion_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "vf_start_date" = "start_date",
 *   },
 *   links = {
 *     "add-page" = "/admin/beeshop/promotions/promotions-list/add",
 *     "add-form" = "/admin/beeshop/promotions/promotions-list/add/{bs_promotion_type}",
 *     "canonical" = "/promotions/{bs_promotion}",
 *     "edit-form" = "/admin/beeshop/promotions/promotions-list/{bs_promotion}/edit",
 *     "delete-form" = "/admin/beeshop/promotions/promotions-list/{bs_promotion}/delete",
 *     "collection" = "/admin/beeshop/promotions/promotions-list",
 *     "coupons-collection" = "/admin/beeshop/promotions/promotions-list/{bs_promotion}/coupons",
 *   },
 * )
 */
class Promotion extends ContentEntityBase implements PromotionInterface {

  use EntityBundleTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The promotion name.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
    ->setLabel(t('Description'))
    ->setDescription(t('Additional information about the promotion'))
    ->setTranslatable(TRUE)
    ->setDefaultValue('')
    ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => 10,
      'settings' => [
        'rows' => 3,
      ],
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['shops'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Stores'))
    ->setDescription(t('The shops for which the promotion is valid.'))
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_shop')
    ->setSetting('handler', 'default')
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => 20,
    ]);

    $fields['price_resolver_context'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Promotion price resolver context'))
      ->setDescription(t('Promotion price resolver context. If leave empty default product price recolver context will be used.'))
//       ->setRequired(TRUE)
      ->setSettings([
        'default_value' => 'product',
        'max_length' => 255,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'weight' => 30,
      ]);;

    $fields['discount_calculation_settings_pluging'] = BaseFieldDefinition::create('plugin_field_item')
      ->setSetting('plugin_manager_name', 'plugin.manager.promotion_price_resolver')
      ->setLabel(t('Discount calculation lugin'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['applicable_products_list_selector'] = BaseFieldDefinition::create('plugin_field_item')
      ->setSetting('plugin_manager_name', 'plugin.manager.product_selection')
      ->setLabel(t('Applicable products selector'))
      ->setCardinality(1)
      //->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['excluded_products_list_selector'] = BaseFieldDefinition::create('plugin_field_item')
      ->setSetting('plugin_manager_name', 'plugin.manager.product_selection')
      ->setLabel(t('Excluded products selector'))
      ->setCardinality(1)
      //->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['can_be_aggregated'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Can be agregated with other promotions'))
      ->setDescription(t('Whether the promotion is can be agregated with other promotions.'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Enabled'),
        'off_label' => t('Disabled'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 1000,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the promotion is enabled.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 1000,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Weight'))
    ->setDescription(t('The weight of this promotion in relation to others.'))
    ->setDefaultValue(0);

    return $fields;
  }

  public function setData($key, $value) {

    $key =  (is_string($key)) ? [$key] : $key;

    $data = $this->get('data')->isEmpty() ? [] : $this->get('data')->first()->getValue();

    NestedArray::setValue($data, $key, $value);

    $this->set('data', $data);
  }

  public function getDataByKey($key) {

    $key =  (is_string($key)) ? [$key] : $key;

    $data = $this->get('data')->isEmpty() ? [] : $this->get('data')->first()->getValue();

    return NestedArray::getValue($data, $key);
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  public function isValid() {

    return $this->isActive();
  }

  public function isExpired() {

    if (!$this->getBundleEntity()->isValidForPeriod()) {
      return FALSE;
    }

    $time = \Drupal::time()->getRequestTime();

    $start_date = $this->getStartDate()->format('U');
    $end_date = $this->getEndDate()->format('U');

    if ( $start_date && $start_date > $time) {
      return FALSE;
    }


    if ($end_date && $end_date < $time) {
      return false;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description->value;
  }


  public function isActive() {
    if (!$this->isEnabled()) {
      return FALSE;
    }

    $conditions = $this->getDataByKey(['conditions']);

    if ($conditions) {
      /**
       * @var ConditionManager $conditions_manager
       */
      $conditions_manager = \Drupal::service('plugin.manager.condition');

      foreach ($conditions as $condition_id => $condition_config) {
        /**
         * @var ConditionPluginBase $condition
         */
        $condition = $conditions_manager->createInstance($condition_id, $condition_config ? $condition_config : []);
        if (!$condition->evaluate()) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  public function isAplicableForProduct(ProductInterface $product) {
    /**
     *
     * @var SelectionPluginManager $product_select_pm
     */
    $product_select_pm = \Drupal::service('plugin.manager.product_selection');

    if (!$this->get('excluded_products_list_selector')->isEmpty()) {

      if ($this->excluded_products_list === NULL) {
        $plugin_settings = $this->get('excluded_products_list_selector')->first()->getValue();

        $selection_plugin = $product_select_pm->createInstance($plugin_settings['plugin_id'], $plugin_settings['plugin_configuration']);
        $this->excluded_products_list = $selection_plugin->getReferenceableProductIds();
      }

//       dpm($this->excluded_products_list);

      if ($this->excluded_products_list) {
        $root_parent_id = $product->getRootParentId();

        if (($root_parent_id && in_array($root_parent_id, $this->excluded_products_list)) ||
          in_array($product->id(), $this->excluded_products_list)) {
            return FALSE;
          }
      }

    }

    if (!$this->get('applicable_products_list_selector')->isEmpty()) {

      if ($this->applicable_products_list === NULL) {
        $plugin_settings = $this->get('applicable_products_list_selector')->first()->getValue();

        $selection_plugin = $product_select_pm->createInstance($plugin_settings['plugin_id'], $plugin_settings['plugin_configuration']);
        $this->applicable_products_list = $selection_plugin->getReferenceableProductIds();
      }

      if ($this->applicable_products_list) {
        $root_parent_id = $product->getRootParentId();
        if (($root_parent_id && !in_array($root_parent_id, $promotion->applicable_products_list)) &&
          !in_array($product->id(), $this->applicable_products_list)) {
            return FALSE;;
          }
      }

    }

    return TRUE;
  }

}

