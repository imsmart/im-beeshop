<?php
namespace Drupal\bs_promotion;

use Drupal\Core\Entity\ContentEntityStorageInterface;

interface PromotionCouponStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads all promotion coupons.
   *
   * @return \Drupal\bs_promotion\Entity\PromotionCouponInterface[]
   *   An array of promotions coupons objects indexed by their IDs. Returns an empty array
   *   if no promotions are found.
   */
  public function loadPromotionCoupons($promotion_id);

  /**
   * Loads promotion coupon by code.
   *
   * @return \Drupal\bs_promotion\Entity\PromotionCouponInterface|false
   *   Promotions coupon objects. Returns false if promotion coupon with specifed code are not found.
   */
  public function loadCouponByCode($coupon_code);

}

