<?php
namespace Drupal\bs_promotion;

use Drupal\Core\Entity\ContentEntityStorageInterface;

interface PromotionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads all promotions ordered by their weight for promotions overview form.
   *
   * @return \Drupal\bs_promotion\Entity\PromotionInterface[]
   *   An array of promotions objects indexed by their IDs and ordered by their weigth. Returns an empty array
   *   if no promotions are found.
   */
  public function loadForOverviewForm();

}

