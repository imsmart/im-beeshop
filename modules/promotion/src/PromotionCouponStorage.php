<?php
namespace Drupal\bs_promotion;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

class PromotionCouponStorage extends SqlContentEntityStorage implements PromotionCouponStorageInterface {

  public function checkCouponCodeExists($code, $exclude_coupon_id = NULL) {

    $entity_query = $this->getQuery();
    $entity_query->condition('code', $code);

    if ($exclude_coupon_id) {
      $entity_query->condition('id', $exclude_coupon_id, '<>');
    }

    $entity_query->count();

    return $entity_query->execute();
  }

  /**
   * @inheritdoc
   */
  public function loadPromotionCoupons($promotion_id) {
    $entity_query = $this->getQuery();
    $entity_query->condition('promotion_id', $promotion_id);

    $ids = $entity_query->execute();

    $coupons = $this->loadMultiple($ids);

    return $coupons;
  }

  public function loadCouponByCode($coupon_code) {
    $entity_query = $this->getQuery();
    $entity_query->condition('code', $coupon_code);

    $ids = $entity_query->execute();

    $coupons = $this->loadMultiple($ids);

    return $coupons ? reset($coupons) : FALSE;
  }

}

