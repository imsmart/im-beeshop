<?php
namespace Drupal\bs_promotion;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

class PromotionStorage extends SqlContentEntityStorage implements PromotionStorageInterface {

  protected $loadPromotionsOrderedByWeigth = FALSE;

  public function setSortByWeigthOnLoad() {
    $this->loadPromotionsOrderedByWeigth = TRUE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function loadForOverviewForm() {
    $this->setSortByWeigthOnLoad();

    return $this->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildQuery($ids, $revision_id = FALSE) {
    $query = parent::buildQuery($ids, $revision_id);

    return /*$this->loadPromotionsOrderedByWeigth ? $query->orderBy('weight') :*/ $query;
  }
}

