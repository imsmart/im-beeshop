<?php
namespace Drupal\bs_promotion\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class PromotionCouponRouteProvider extends AdminHtmlRouteProvider {

//   /**
//    * {@inheritdoc}
//    */
//   protected function getAddFormRoute(EntityTypeInterface $entity_type) {
//     if ($route = parent::getAddFormRoute($entity_type)) {
//       $route->setOption('_admin_route', TRUE);

//       $options = [
//         'parameters' => [
//           'bs_promotion' => [
//             'type' => 'entity:bs_promotion',
//           ]
//         ],
//       ];

//       $route->addOptions($options);

// //       $route_defaults = $route->getDefaults();

// //       if (!empty($route_defaults['_entity_form'])) {
// //         $route_defaults['_form'] = $route_defaults['_entity_form'];
// //         unset($route_defaults['_entity_form']);
// //         $route->setDefaults($route_defaults);
// //       }

//       return $route;
//     }
//   }

//   /**
//    * Gets the promotion coupons collection route.
//    *
//    * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
//    *   The entity type.
//    *
//    * @return \Symfony\Component\Routing\Route|null
//    *   The generated route, if available.
//    */
//   protected function getPromotionCouponsCollectionRoute(EntityTypeInterface $entity_type) {
//     // If the entity type does not provide an admin permission, there is no way
//     // to control access, so we cannot provide a route in a sensible way.
//     if ($entity_type->hasLinkTemplate('promotion-coupons-collection') && ($admin_permission = $entity_type->getAdminPermission())) {
//       /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
//       $label = $entity_type->getCollectionLabel();

//       $route = new Route($entity_type->getLinkTemplate('promotion-coupons-collection'));
//       $route
//         ->addDefaults([
//           '_form' => 'Drupal\bs_promotion\Form\PromotionCouponsOverviewForm',
//           '_title' => $label->getUntranslatedString(),
//           '_title_arguments' => $label->getArguments(),
//           '_title_context' => $label->getOption('context'),
//         ])
//         ->setRequirement('_permission', $admin_permission);

//       $options = [
//         'parameters' => [
//           'bs_promotion' => [
//             'type' => 'entity:bs_promotion',
//           ]
//         ],
//       ];

//       $route->addOptions($options)
//       ->setOption('_admin_route', TRUE);

//       return $route;
//     }
//   }

}

