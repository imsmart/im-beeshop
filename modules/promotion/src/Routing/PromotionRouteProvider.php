<?php
namespace Drupal\bs_promotion\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class PromotionRouteProvider extends AdminHtmlRouteProvider {

  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getCollectionRoute($entity_type)) {

      $route_defaults = $route->getDefaults();

      if (!empty($route_defaults['_entity_list'])) {
        unset($route_defaults['_entity_list']);
      }

      $route->setDefaults($route_defaults);

      $route
        ->addDefaults([
          '_form' => $entity_type->getListBuilderClass(),
        ]);

      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    return $collection;
  }

}

