<?php

namespace Drupal\bs_price\Entity;

use Drupal\bs_price\PriceTypeInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the currency entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_price_type",
 *   label = @Translation("Price type"),
 *   label_singular = @Translation("price type"),
 *   label_plural = @Translation("price types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count price type",
 *     plural = "@count price types",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\bs_price\PriceTypeStorage",
 *     "form" = {
 *       "add" = "Drupal\bs_price\Form\PriceTypeForm",
 *       "edit" = "Drupal\bs_price\Form\PriceTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_price\Routing\BsPriceAdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\bs_price\PriceTypeListBuilder",
 *   },
 *   admin_permission = "administer beeshop price types",
 *   config_prefix = "bs_price",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "parent" = "parent",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "description",
 *     "uuid",
 *     "sid",
 *     "weight",
 *     "parent",
 *     "calcType",
 *     "extraSettings",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/shops/{bs_shop}/price_types/add",
 *     "edit-form" = "/admin/beeshop/config/price_types/{bs_price_type}/edit",
 *     "delete-form" = "/admin/beeshop/config/price_types/{bs_price_type}/delete",
 *   }
 * )
 */
class PriceType extends ConfigEntityBase implements PriceTypeInterface {

  /**
   * A brief description of this store type.
   *
   * @var string
   */
  protected $description;

  /**
   * A price type weight.
   *
   * @var int
   */
  protected $weight;


  protected $childs = NULL;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  public function isChildType($price_type_id) {
    if (empty($this->childs)) return FALSE;

    foreach ($this->childs as $child_type) {
      if ($price_type_id == $child_type->id()) return TRUE;
    }

    return FALSE;
  }

  public function hasChild() {
    if ($this->childs !== NULL && $this->childs) return TRUE;

    $this->childs = $this->getChilds();

    return !empty($this->childs);
  }

  public function getChilds() {

    if ($this->childs !== NULL) return $this->childs;

    $child_ids = \Drupal::entityQuery('bs_price_type')
    ->condition('parent', $this->id(), '=')
    ->execute();

    $childs = [];

    if ($child_ids) {
      $childs = self::loadMultiple(array_values($child_ids));
    }

    $this->childs = $childs;

    return $this->childs;
  }

  public function addChild($child_type) {
    if ($this->childs === NULL) $this->childs = [];

    $this->childs[] = $child_type;

    return $this;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public function toUrl($rel = 'edit-form', array $options = []) {
//     // Unless language was already provided, avoid setting an explicit language.
//     $options += ['language' => NULL];
//     return parent::toUrl($rel, $options);
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function url($rel = 'edit-form', $options = []) {
//     return parent::url($rel, $options);
//   }
}