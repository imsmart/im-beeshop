<?php

namespace Drupal\bs_price\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a price form element.
 *
 * Usage example:
 * @code
 * $form['amount'] = [
 *   '#type' => 'bs_extended_product_price',
 *   '#title' => $this->t('Amount'),
 *   '#default_value' => ['value' => '99.99', 'currency_code' => 'USD'],
 *   '#allow_negative' => FALSE,
 *   '#size' => 60,
 *   '#maxlength' => 128,
 *   '#required' => TRUE,
 *   '#available_currencies' => ['USD', 'EUR',],
 * ];
 * @endcode
 *
 * @FormElement("bs_extended_product_price")
 */
class ExtendedProductPrice extends ProductPrice {

  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {

    $element = parent::processElement($element, $form_state, $complete_form);

    $element['#attached']['library'][] = 'bs_price/priceExtended';

    $element['#attributes']['class'][] = 'bs-extended-product-price';

    $element['extra_config'] = [
      '#weight' => -10,
    ];

    $element['extra_config']['type'] = [
      '#type' => 'select',
      //'#title' => 'Type',
      '#options' => [
        'static' => new TranslatableMarkup('Static'),
        'formula' => new TranslatableMarkup('Calculatable')
      ],
      '#default_value' => !empty($element['#default_value']['extra_config']['type']) ?
                           $element['#default_value']['extra_config']['type'] :
                           'absolute',
      '#attributes' => [
        'class' => [
          'child-price-type'
        ]
      ],
    ];


    $element['extra_config']['formula']['f_value'] = [
      '#type' => 'number',
      '#default_value' => !empty($element['#default_value']['extra_config']['formula']['f_value']) ?
                          $element['#default_value']['extra_config']['formula']['f_value']:
                          '',
      '#min' => 0,
    ];

    $element['extra_config']['formula']['type'] = [
      '#type' => 'select',
      //'#title' => 'Calculation type',
      '#options' => [
        'absolute' => new TranslatableMarkup('In absolute value'),
        'percents' => new TranslatableMarkup('%')
      ],
      '#default_value' => !empty($element['#default_value']['extra_config']['formula']['type']) ?
                           $element['#default_value']['extra_config']['formula']['type']:
      'percents',
    ];

    return $element;
  }

}
