<?php

namespace Drupal\bs_price\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Render\Element\FormElement;


/**
 * Provides a price form element.
 *
 * Usage example:
 * @code
 * $form['amount'] = [
 *   '#type' => 'bs_product_price',
 *   '#title' => $this->t('Amount'),
 *   '#default_value' => ['value' => '99.99', 'currency_code' => 'USD'],
 *   '#allow_negative' => FALSE,
 *   '#size' => 60,
 *   '#maxlength' => 128,
 *   '#required' => TRUE,
 *   '#available_currencies' => ['USD', 'EUR',],
 * ];
 * @endcode
 *
 * @FormElement("bs_product_price")
 */
class ProductPrice extends Price {

  use CompositeFormElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      // List of currencies codes. If empty, all currencies will be available.
      '#size' => 10,
      '#maxlength' => 128,
      '#default_value' => NULL,
      '#allow_negative' => FALSE,
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#attached' => [
        'library' => [
          'bs_price/admin',
        ],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
        [$class, 'preRenderCompositeFormElement']
      ],
      '#input' => TRUE,
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Builds the bs_price form element.
   *
   * @param array $element
   *   The initial bs_price form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built bs_price form element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #default_value is not an instance of
   *   \Drupal\bs_price\Price.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = $element['#default_value'];
    if (isset($default_value) && !self::validateDefaultValue($default_value)) {
      throw new \InvalidArgumentException('The #default_value for a bs_price element must be an array with "value" and "currency_code" keys.');
    }

    $shops_em = \Drupal::entityTypeManager()->getStorage('bs_shop');

    $available_currencies = [];

    if (!empty($element['#price_type']) && isset($element['#price_type']->sid)) {
      if ($shop = $shops_em->load($element['#price_type']->sid)) {
        $available_currencies = $shop->getShopCurrenciesSelectOptions();
      }
    }
    elseif (!empty($element['#shop_id'])) {
      if ($shop = $shops_em->load($element['#shop_id'])) {
        $available_currencies = $shop->getShopCurrenciesSelectOptions();
      }
    }
    elseif (!empty($element['#available_currencies'])) {
      $available_currencies = $element['#available_currencies'];
    }

    // Stop rendering if there are no currencies available.
    if (empty($available_currencies)) {
      return $element;
    }

    $element['#tree'] = TRUE;
    $element['#type'] = '';
    $element['#attributes']['class'][] = 'form-type-bs-price';

    $element['value'] = [
      '#type' => 'number',
      '#default_value' => (!empty($default_value) && !empty($default_value['value'])) ?
                          round($default_value['value'], 2) : NULL,
      '#required' => $element['#required'],
      '#size' => $element['#size'],
      '#maxlength' => $element['#maxlength'],
      '#min_fraction_digits' => 2,
      // '6' is the field storage maximum.
      '#max_fraction_digits' => 2,
      '#min' => $element['#allow_negative'] ? NULL : 0.0,
      '#step' => 0.01,
      '#attributes' => [
        'class' => ['price-value']
      ]
    ];

    if (!empty($element['#title'])) {
      $element['#title'] = $element['#title'];
    }

    unset($element['#size']);
    unset($element['#maxlength']);

    if (count($available_currencies) == 1) {
      $last_visible_element = 'number';
      $currency_code = reset($available_currencies);
      $element['value']['#field_suffix'] = $currency_code;
      $element['currency_code'] = [
        '#type' => 'hidden',
        '#value' => $currency_code,
      ];
    }
    else {
      $last_visible_element = 'currency_code';
      $element['currency_code'] = [
        '#type' => 'select',
        '#title' => t('Currency'),
        '#default_value' => $default_value ? $default_value['currency_code'] : NULL,
        '#options' => array_combine($available_currencies, $available_currencies),
        '#title_display' => 'invisible',
        '#field_suffix' => '',
      ];
    }

    if (!empty($element['#price_type'])) {
      $element['price_type'] = [
        '#type' => 'value',
        '#value' => $element['#price_type']->id(),
      ];
      $element['shop_id'] = [
        '#type' => 'value',
        '#value' => $element['#price_type']->sid,
      ];
    }

    // Add the help text if specified.
    if (!empty($element['#description'])) {
      //$element[$last_visible_element]['#field_suffix'] .= '<div class="description">' . $element['#description'] . '</div>';
    }

    return $element;
  }

  /**
   * Validates the default value.
   *
   * @param mixed $default_value
   *   The default value.
   *
   * @return bool
   *   TRUE if the default value is valid, FALSE otherwise.
   */
  public static function validateDefaultValue($default_value) {
    if (!is_array($default_value)) {
      return FALSE;
    }
    if (!array_key_exists('value', $default_value) || !array_key_exists('currency_code', $default_value)) {
      return FALSE;
    }
    return TRUE;
  }

}
