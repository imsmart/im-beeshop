<?php

namespace Drupal\bs_price;

use Drupal\bs_currencies\Entity\Currency;

//use Drupal\commerce_price\Exception\CurrencyMismatchException;

/**
 * Provides a value object for monetary values.
 */
final class Price {

  /**
   * The number.
   *
   * @var string
   */
  protected $value;

  /**
   * The currency code.
   *
   * @var string
   */
  protected $currencyCode;


  protected $oldPrice = NULL;

  protected $_isOldPrice = FALSE;

  /**
   * Constructs a new Price object.
   *
   * @param string $number
   *   The number.
   * @param string $currency_code
   *   The currency code.
   */
  public function __construct($number, $currency_code) {
    $number = is_string($number) ? $number : (string) $number;
    Calculator::assertNumberFormat($number);
    $this->assertCurrencyCodeFormat($currency_code);

    $this->value = (string) $number;
    $this->currencyCode = strtoupper($currency_code);
  }

  /**
   * Gets the number.
   *
   * @return string
   *   The number.
   */
  public function getPriceValue() {
    return $this->value;
  }

  public function getValue() {
    return $this->value;
  }

  public function setPriceValue($val, $skip_validation = FALSE) {
    if (!$skip_validation) {
      Calculator::assertNumberFormat($val);
    }
    $this->value = (string) $val;
  }

  public function getTrimmedValue() {
    return Calculator::trim($this->value);
  }

  public function getFormatedValue() {
    $formated = number_format($this->value, 2, '.', ' ');
    return Calculator::trim($formated);
  }

  /**
   * Gets the currency code.
   *
   * @return string
   *   The currency code.
   */
  public function getCurrencyCode() {
    return $this->currencyCode;
  }

  public function getRendableArray() {
    $rendable_array = [
      '#theme' => 'bs_price_item',
      '#price_item' => $this,
    ];

    return $rendable_array;
  }

  /**
   * Gets the string representation of the price.
   *
   * @return string
   *   The string representation of the price.
   */
  public function __toString() {
    $currency = Currency::load($this->currencyCode);
    return Calculator::trim($this->getFormatedValue()) . ' ' . ($currency->getSymbol() ?: $this->currencyCode);
  }

  /**
   * Gets the array representation of the price.
   *
   * @return array
   *   The array representation of the price.
   */
  public function toArray() {
    return ['value' => $this->value, 'currency_code' => $this->currencyCode];
  }

  /**
   * Converts the current price to the given currency.
   *
   * @param string $currency_code
   *   The currency code.
   * @param string $rate
   *   A currency rate corresponding to the currency code.
   *
   * @return static
   *   The resulting price.
   */
  public function convert($currency_code, $rate = '1') {
    $new_number = Calculator::multiply($this->value, $rate);
    return new static($new_number, $currency_code);
  }

  /**
   * Adds the given price to the current price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return static
   *   The resulting price.
   */
  public function add(Price $price) {
    $this->assertSameCurrency($this, $price);
    $new_number = Calculator::add($this->value, $price->getPriceValue());
    $this->value = $new_number;
    return $this;
  }

  /**
   * Adds the given price to the current price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return static
   *   The resulting price.
   */
  public function addNumericValue($value) {
    $new_number = Calculator::add($this->value, $value);
    $this->value = $new_number;
    return $this;
  }

  /**
   * Subtracts the given price from the current price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return static
   *   The resulting price.
   */
  public function subtract(Price $price) {
    $this->assertSameCurrency($this, $price);
    $new_number = Calculator::subtract($this->value, $price->getPriceValue());
    return new static($new_number, $this->currencyCode);
  }

  /**
   * Multiplies the current price by the given number.
   *
   * @param string $number
   *   The number.
   *
   * @return static
   *   The resulting price.
   */
  public function multiply($number) {
    $new_number = Calculator::multiply($this->value, $number);
    return new static($new_number, $this->currencyCode);
  }

  /**
   * Divides the current price by the given number.
   *
   * @param string $number
   *   The number.
   *
   * @return static
   *   The resulting price.
   */
  public function divide($number) {
    $new_number = Calculator::divide($this->value, $number);
    return new static($new_number, $this->currencyCode);
  }

  public function roundTo($to = 1, $mode = PHP_ROUND_HALF_UP) {
    $new_number = Calculator::roundTo($this->value, $to, $mode);
    return new static($new_number, $this->currencyCode);
  }

  public function ceil() {
    $new_number = Calculator::ceil($this->value);
    return new static($new_number, $this->currencyCode);
  }

  /**
   * Compares the current price with the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return int
   *   0 if both prices are equal, 1 if the first one is greater, -1 otherwise.
   */
  public function compareTo(Price $price) {
    $this->assertSameCurrency($this, $price);
    return Calculator::compare($this->value, $price->getPriceValue());
  }

  /**
   * Gets whether the current price is zero.
   *
   * @return bool
   *   TRUE if the price is zero, FALSE otherwise.
   */
  public function isZero() {
    return Calculator::compare($this->value, '0') == 0;
  }

  public function isOldPrice($value = NULL) {
    if ($value !== NULL) {
      $this->_isOldPrice = (bool) $value;
    }
    return $this->_isOldPrice;
  }

  /**
   * Gets whether the current price is equivalent to the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return bool
   *   TRUE if the prices are equal, FALSE otherwise.
   */
  public function equals(Price $price) {
    return $this->compareTo($price) == 0;
  }

  /**
   * Gets whether the current price is greater than the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return bool
   *   TRUE if the current price is greater than the given price,
   *   FALSE otherwise.
   */
  public function greaterThan(Price $price) {
    return $this->compareTo($price) == 1;
  }

  /**
   * Gets whether the current price is greater than or equal to the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return bool
   *   TRUE if the current price is greater than or equal to the given price,
   *   FALSE otherwise.
   */
  public function greaterThanOrEqual(Price $price) {
    return $this->greaterThan($price) || $this->equals($price);
  }

  /**
   * Gets whether the current price is lesser than the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return bool
   *   TRUE if the current price is lesser than the given price,
   *   FALSE otherwise.
   */
  public function lessThan(Price $price) {
    return $this->compareTo($price) == -1;
  }

  /**
   * Gets whether the current price is lesser than or equal to the given price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return bool
   *   TRUE if the current price is lesser than or equal to the given price,
   *   FALSE otherwise.
   */
  public function lessThanOrEqual(Price $price) {
    return $this->lessThan($price) || $this->equals($price);
  }

  /**
   *
   * @return \Drupal\bs_price\Price
   */
  public function getOldPrice() {
    return $this->oldPrice;
  }

  /**
   *
   * @param Price $old_price
   * @return \Drupal\bs_price\Price
   */
  public function setOldPrice(Price $old_price) {
    $this->oldPrice = $old_price;
    return $this;
  }

  /**
   * Asserts that the currency code is in the right format.
   *
   * Serves only as a basic sanity check.
   *
   * @param string $currency_code
   *   The currency code.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the currency code is not in the right format.
   */
  protected function assertCurrencyCodeFormat($currency_code) {
    if (strlen($currency_code) != '3') {
      throw new \InvalidArgumentException();
    }
  }

  /**
   * Asserts that the two prices have the same currency.
   *
   * @param \Drupal\commerce_price\Price $first_price
   *   The first price.
   * @param \Drupal\commerce_price\Price $second_price
   *   The second price.
   *
   * @throws \Drupal\commerce_price\Exception\CurrencyMismatchException
   *   Thrown when the prices do not have the same currency.
   */
  protected function assertSameCurrency(Price $first_price, Price $second_price) {
    if ($first_price->getCurrencyCode() != $second_price->getCurrencyCode()) {
      //throw new CurrencyMismatchException();
    }
  }

}
