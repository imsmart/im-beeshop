<?php
namespace Drupal\bs_price;

use Drupal\Core\Field\FieldItemList;
use Drupal\bs_price\Plugin\Field\FieldType\PriceItem;

class PriceItemList extends FieldItemList {

  public function appendPriceItem(PriceItem $price_item) {
    $this->list[] = $price_item;
  }

}

