<?php

namespace Drupal\bs_price;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Represents a list of price adjustment item field values.
 */
interface PriceAdjustmentItemListInterface extends FieldItemListInterface {

  public function filterByType($type);

}

