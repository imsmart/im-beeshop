<?php

namespace Drupal\bs_price\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for BeeShop price adjustment type plugins.
 *
 * @see \Drupal\bs_cart\Plugin\Beeshop\Cart\CartViolationFixerPluginBase
 *
 * @Annotation
 */
class PriceAdjustmentType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Default adjusment item label.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $default_item_label;

  /**
   * An integer to determine the weight of this type relative to other types
   * in the UI when selecting a type.
   *
   * @var int optional
   */
  public $weight = NULL;

  /**
   * A boolean stating that this type cannot be used through the UI.
   *
   * @var bool
   */
  public $no_ui = FALSE;

  /**
   * A boolean stating that this type cannot be used through the UI.
   *
   * @var bool
   */
  public $can_be_included = TRUE;

  public $only_one_in_list = FALSE;

}

