<?php
namespace Drupal\bs_price\Form;

use Drupal\bs_shop\Entity\ShopInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

class PriceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $type = $this->entity;

    $shop_id = $type->sid;

    if (!$type->id) {
      $current_shop = $this->getEntityFromRouteMatch(\Drupal::routeMatch(), 'bs_shop');
      if (!$current_shop || !$current_shop instanceof ShopInterface) {
        drupal_set_message('Invalid shop for new price type', 'error');
        return;
      }
      $shop_id = $current_shop->id();
    }

    $form['sid'] = [
      '#type' => 'value',
      '#value' => $shop_id,
    ];

    $form['calcType'] = [
      '#type' => 'value',
      '#value' => 'static',
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\bs_price\Entity\PriceType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $type->enabled,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $type->getDescription(),
    ];

    $form['third_party_settings'] = [
      '#tree' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    dpm($form_state->getValues());

    parent::submitForm($form, $form_state);

    dpm($this->entity);
  }
}

