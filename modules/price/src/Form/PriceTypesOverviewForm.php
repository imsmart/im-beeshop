<?php
namespace Drupal\bs_price\Form;

use Drupal\bs_shop\Entity\ShopInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PriceTypesOverviewForm extends FormBase {

  /**
   * The price types storage handler.
   *
   * @var \Drupal\bs_price\PriceTypeStorageInterface
   */
  protected $priceTypesStorageController;

  /**
   * Constructs an PriceTypesOverviewForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityManagerInterface $entity_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityManager = $entity_manager;
    $this->priceTypesStorageController= $entity_manager->getStorage('bs_price_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity.manager')
      );
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state, ShopInterface $bs_shop = NULL) {

    $price_types_tree = \Drupal::entityTypeManager()
      ->getStorage('bs_price_type')
      ->loadTree($bs_shop->id());

    $delta = count($price_types_tree);
    $destination = $this->getDestinationArray();

    $form['types'] = [
      '#type' => 'table',
      '#header' => [$this->t('Name'), $this->t('Weight'), $this->t('Operations')],
      '#empty' => $this->t('No price types available. <a href=":link">Add new price type</a>.', [':link' => $this->url('entity.bs_price_type.add_form', ['bs_shop' => $bs_shop->id()])]),
      '#attributes' => [
        'id' => 'shop-price-types',
      ],
    ];

    $form['types']['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'type-weight',
    ];
    $form['types']['#tabledrag'][] = [
      'action' => 'match',
      'relationship' => 'parent',
      'group' => 'type-parent',
      'subgroup' => 'type-parent',
      'source' => 'type-id',
      'hidden' => FALSE,
    ];
    $form['types']['#tabledrag'][] = [
      'action' => 'depth',
      'relationship' => 'group',
      'group' => 'type-depth',
      'hidden' => FALSE,
    ];

    foreach ($price_types_tree as $key => $type) {
      /** @var $term \Drupal\Core\Entity\EntityInterface */
      $form['types'][$key]['#price_type'] = $type;
      $indentation = [];
      if (isset($type->depth) && $type->depth > 0) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => $type->depth,
        ];
      }
      $form['types'][$key]['type'] = [
        '#type' => 'markup',
        '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
        '#markup' => $type->label,
      ];
      $form['types'][$key]['type']['id'] = [
        '#type' => 'hidden',
        '#value' => $type->id(),
        '#attributes' => [
          'class' => ['type-id'],
        ],
      ];
      $form['types'][$key]['type']['parent'] = [
        '#type' => 'hidden',
        // Yes, default_value on a hidden. It needs to be changeable by the
        // javascript.
        '#default_value' => $type->parents[0],
        '#attributes' => [
          'class' => ['type-parent'],
        ],
      ];
      $form['types'][$key]['type']['depth'] = [
        '#type' => 'hidden',
        // Same as above, the depth is modified by javascript, so it's a
        // default_value.
        '#default_value' => isset($type->depth) ? $type->depth : 0,
        '#attributes' => [
          'class' => ['type-depth'],
        ],
      ];

      $form['types'][$key]['weight'] = [
        '#type' => 'weight',
        '#delta' => $delta,
        '#title' => $this->t('Weight for added term'),
        '#title_display' => 'invisible',
        '#default_value' => $type->getWeight(),
        '#attributes' => [
          'class' => ['type-weight'],
        ],
      ];

      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          'query' => $destination,
          'url' => $type->toUrl('edit-form', ['bs_shop' => $bs_shop->id()]),
        ],
        'delete' => [
          'title' => $this->t('Delete'),
          'query' => $destination,
          'url' => $type->toUrl('delete-form', ['bs_shop' => $bs_shop]),
        ],
      ];

      $form['types'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      $form['types'][$key]['#attributes']['class'][] = 'draggable';
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bs_shop_price_types_overview_form';
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Sort term order based on weight.
    uasort($form_state->getValue('types'), ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $changed_types = [];
    $level_weights = [];
    $weight = 0;
    foreach ($form_state->getValue('types') as $id => $values) {
      if (isset($form['types'][$id]['#price_type'])) {
        $type = $form['types'][$id]['#price_type'];

        if ($values['type']['parent'] == '' && $type->getWeight() !== $weight) {
          $type->setWeight($weight);
          $changed_types[$type->id()] = $type;
        }
        // Terms not at the root level can safely start from 0 because they're all on this page.
        elseif ($values['type']['parent'] != '') {
          $level_weights[$values['type']['parent']] = isset($level_weights[$values['type']['parent']]) ? $level_weights[$values['type']['parent']] + 1 : 0;
          if ($level_weights[$values['type']['parent']] !== $type->getWeight()) {
            $type->setWeight($level_weights[$values['type']['parent']]);
            $changed_types[$type->id()] = $type;
          }
        }
        // Update any changed parents.
        if ($values['type']['parent'] != $type->parents[0]) {
          $type->parent = $values['type']['parent'];
          $changed_types[$type->id()] = $type;
        }
        $weight++;
      }
    }
    // Save all updated terms.
    foreach ($changed_types as $type) {
      $type->save();
    }
    drupal_set_message($this->t('The configuration options have been saved.'));
  }
}

