<?php
namespace Drupal\bs_price;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

class PriceTypeStorage extends ConfigEntityStorage implements PriceTypeStorageInterface {

  /**
   * Array of price types ancestors keyed by shop ID and parent price type ID.
   *
   * @var array
   */
  protected $treeChildren = [];

  /**
   * Array of price types parents keyed by shop ID and child price type ID.
   *
   * @var array
   */
  protected $treeParents = [];

  /**
   * Array of loaded trees keyed by a cache id matching tree arguments.
   *
   * @var array
   */
  protected $trees = [];

  /**
   * Array of price types in a tree keyed by shop ID and price type ID.
   *
   * @var array
   */
  protected $treeTypes = [];

  /**
   * {@inheritdoc}
   */
  public function getShopsPriceTypes($shop_ids) {
    $query = \Drupal::entityQuery('bs_price_type');
    $query->condition('sid', $shop_ids);

    $query->sort('weight');

    $price_type_ids = $query->execute();

    $price_types = $this->loadMultiple($price_type_ids);

    return $price_types;
  }

  public function getShopActivePriceType($shop_id) {
    $price_type = NULL;

    $roots_types = $this->getShopStructuredPriceTypes($shop_id);

    foreach ($roots_types as $price_type) {
      if ($price_type->enabled) {
        return $price_type;
      }
    }

    return FALSE;

  }

  public function getShopStructuredPriceTypes($shop_id) {
    return $this->loadTree($shop_id, '', 1);
  }

  /**
   * {@inheritdoc}
   */
  public function getShopsPriceTypesKeyedByShopId($shop_ids) {
    $result = [];
    if ($price_types = $this->getShopsPriceTypes($shop_ids)) {
      foreach ($price_types as $price_type_name => $price_type) {
        $result[$price_type->sid][$price_type_name] = $price_type;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTree($sid, $parent = '', $max_depth = NULL) {
    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {

      $shop_price_types = $this->getShopsPriceTypes($sid);

      if (!isset($this->treeTypes[$sid])) {
        $this->treeChildren[$sid] = [];
        $this->treeParents[$sid] = [];
        $this->treeTypes[$sid] = [];

        foreach ($shop_price_types as $price_type) {
          $this->treeChildren[$sid][$price_type->parent][] = $price_type->id();
          $this->treeParents[$sid][$price_type->id()][] = $price_type->parent;
          $this->treeTypes[$sid][$price_type->id()] = $price_type;
        }

        foreach ($this->treeTypes[$sid] as $id => $price_type) {
          if ($price_type->parent && isset($this->treeTypes[$sid][$price_type->parent])) {
            $this->treeTypes[$sid][$price_type->parent]->addChild($this->treeTypes[$sid][$id]);
          }
        }
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$sid]) : $max_depth;
      $tree = [];

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = [];
      $process_parents[] = $parent;

      // Loops over the parent terms and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents determines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$sid][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$sid][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $price_type = $shop_price_types[$child];
            if (isset($this->treeTypes[$sid][$price_type->id()])) {
              $price_type = $this->treeTypes[$sid][$price_type->id()];
            }
            if (isset($this->treeParents[$sid][$price_type->id()])) {
              // Clone the term so that the depth attribute remains correct
              // in the event of multiple parents.
              $price_type = clone $price_type;
            }
            $price_type->depth = $depth;
            $tid = $price_type->id();
            $price_type->parents = $this->treeParents[$sid][$tid];
            $tree[] = $price_type;
            if (!empty($this->treeChildren[$sid][$tid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current term as parent for the next iteration.
              $process_parents[] = $tid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$sid][$tid]);
              // Move pointer so that we get the correct term the next time.
              next($this->treeChildren[$sid][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$sid][$parent]));

          if (!$has_children) {
            // We processed all terms in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$sid][$parent]);
          }
        }
      }

      $this->trees[$cache_key] = $tree;
    }

    return $this->trees[$cache_key];
  }

}

