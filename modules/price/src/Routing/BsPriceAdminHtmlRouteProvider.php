<?php
namespace Drupal\bs_price\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

class BsPriceAdminHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getAddFormRoute($entity_type)) {
      $route->setOption('_admin_route', TRUE);
      $params = $route->getOption('parameters');
      $params['bs_shop']['type'] = 'entity:bs_shop';
      $route->setOption('parameters', $params);
      return $route;
    }
  }

}

