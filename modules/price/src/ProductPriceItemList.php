<?php
namespace Drupal\bs_price;

class ProductPriceItemList extends PriceItemList {

  /**
   * By type id indexed array of items.
   *
   * @var \Drupal\Core\TypedData\TypedDataInterface[]
   */
  protected $indexedList = [];

  /**
   * {@inheritdoc}
   */
  public function filterItemsByShopId($shop_id) {
    $this->filter(function ($item) use ($shop_id) {
      return $item->getShopId() == $shop_id;
    });
      return $this;
  }

  public function getItemByType($type) {
    $result = NULL;

    $this->reindexByTypeId();

    if (isset($this->indexedList[$type->id()])) {
      $base_item = $this->indexedList[$type->id()];
      $result = $base_item;

      if ($child_types = $type->getChilds()) {
        $child_type = reset($child_types);

        if (isset($this->indexedList[$child_type->id()])) {
          $result = $this->indexedList[$child_type->id()];
          if ($base_item->isMoreThan($result)) {
            $base_item->set('discount_source', 'price');
            $base_item->set('discount_reason', 'Override price has been specified');
            $result->setOldPrice($base_item);
          }
          else {
            $result = $base_item;
          }
        }
      }
    }

    return $result;
  }

  public function getItemByTypeId($type_id, $apply_ovveride = TRUE) {
    $this->reindexByTypeId();

    if (isset($this->indexedList[$type_id])) {
      return $this->indexedList[$type_id];
    }

  }

  protected function reindexByTypeId() {
    $this->indexedList = [];

    foreach ($this->list as $item) {
      $this->indexedList[$item->getType()] = $item;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {

    if ($this->getEntity()->hasField('inh_price_from_parent') && $this->getEntity()->get('inh_price_from_parent')->value && ($parent = $this->getEntity()->getParent())) {
      $parent_items = $parent->get($this->getName())->filterEmptyItems();

      $this->list = [];

      foreach ($parent_items as $item) {
        $this->appendItem($item->getValue());
      }
    }

    parent::preSave();
  }

}

