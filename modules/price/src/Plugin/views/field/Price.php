<?php

namespace Drupal\bs_price\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Displays the product prices.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("bs_price")
 */
class Price extends EntityField {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->multiple = FALSE;
    $this->limit_values = FALSE;
    $this->options['separator'] = '';
  }

  /**
   * {@inheritdoc}
   */
  public function query($use_groupby = FALSE) {
    parent::query($use_groupby);
    $current_shop_id = 1;

    /**
     * @var \Drupal\Core\Entity\PriceTypeStorageInterface $price_type_storage
     */
    $price_type_storage = \Drupal::entityTypeManager()->getStorage('bs_price_type');
    $shop_price_type =  $price_type_storage->getShopActivePriceType($current_shop_id);

  }

  /**
   * {@inheritdoc}
   */
  public function renderItems($items) {
    $build = parent::renderItems($items);
    return $build;
  }

}
