<?php
namespace Drupal\bs_price\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views\Views;

/**
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("price_type")
 */
class PiceType extends StringFilter {

  /**
   * {@inheritDoc}
   */
  public function query() {

   $this->realField = 'price_price_type';

//     $this->ensureMyTable();
//     $field = "$this->tableAlias.$this->realField";

//     $info = $this->operators();
//     if (!empty($info[$this->operator]['method'])) {
//       $this->{$info[$this->operator]['method']}($field);
//     }

//     dpm($this->query);

    $join_conf = [
      'type' => 'LEFT',
      'table' => $this->table,
      'field' => 'entity_id',
      'left_field' => 'pid',
      'left_table' => 'bs_product_field_data',
      'operator' => '=',
      'extra' => [
        0 => [
          'field' => 'price_price_type',
          'value' => 'base_retail_price'
        ]
      ],
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $join_conf);

    $this->query->queueTable($this->table, NULL, $join, 'base_price');

    $join_conf = [
      'type' => 'LEFT',
      'table' => $this->table,
      'field' => 'entity_id',
      'left_field' => 'pid',
      'left_table' => 'bs_product_field_data',
      'operator' => '=',
      'extra' => [
        0 => [
          'field' => 'price_price_type',
          'value' => 'discounted_price'
        ]
      ],
    ];
    $join_discounted_price = Views::pluginManager('join')->createInstance('standard', $join_conf);

    $this->query->queueTable($this->table, NULL, $join_discounted_price, 'discounted_price');

//     $info = $this->operators();
//     if (!empty($info[$this->operator]['method'])) {
//       $this->{$info[$this->operator]['method']}();
//     }


//     parent::query();
//     dpm($this);
  }

}

