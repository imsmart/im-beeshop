<?php

namespace Drupal\bs_price\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use function Drupal\Core\Form\drupal_set_message;

/**
 * Plugin implementation of the 'bs_price_default' widget.
 *
 * @FieldWidget(
 *   id = "fin_amount_default",
 *   label = @Translation("Amount (finance)"),
 *   field_types = {
 *     "bs_fin_amount"
 *   }
 * )
 */
class FinAmountDefaultWidget extends WidgetBase {

//   /**
//    * {@inheritdoc}
//    */
//   public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
//     $field_name = $this->fieldDefinition->getName();
//     // Extract the values from $form_state->getValues().
//     $path = array_merge($form['#parents'], [$field_name]);
//     $key_exists = NULL;
//     $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

//     $values_to_save = [];

//     if (!empty($values['prices_set'])) {
//       foreach ($values['prices_set']['by_shops'] as $shop_id => $price_by_types) {
//         foreach ($price_by_types as $price_type_name => $price_values) {
//           $values_to_save[] = $price_values;
//         }
//       }
//     }

//     if ($values_to_save) {
//       $items->setValue($values_to_save);
//       $items->filterEmptyItems();
//     }

//     if (!empty($values['prices_set']['copy_to_childs'])) {
//       $this->fieldDefinition->setSetting('copy_to_variations', TRUE);
//     }

//   }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
//     $element['#type'] = 'commerce_price';
//     if (!$items[$delta]->isEmpty()) {
//       $element['#default_value'] = $items[$delta]->toPrice()->toArray();
//     }
//     $element['#available_currencies'] = $this->getFieldSetting('available_currencies');

    $element['value'] = [
      '#title' => $this->t('Amount'),
      '#type' => 'number',
      '#default_value' => $items[$delta]->value,
      '#min_fraction_digits' => 2,
      '#max_fraction_digits' => 2,
      '#step' => 0.01,
    ];

    $element['currency_code'] = [
      '#type' => 'select',
      '#read_only' => TRUE,
      '#options' => [
        'RUB' => 'RUB'
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return $values;
  }

}
