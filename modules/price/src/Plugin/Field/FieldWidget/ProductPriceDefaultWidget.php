<?php

namespace Drupal\bs_price\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use function Drupal\Core\Form\drupal_set_message;

/**
 * Plugin implementation of the 'bs_price_default' widget.
 *
 * @FieldWidget(
 *   id = "bs_product_price_default",
 *   label = @Translation("Product price"),
 *   field_types = {
 *     "bs_product_price"
 *   }
 * )
 */
class ProductPriceDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $form_state_values = $form_state->getValues();
    $values = NestedArray::getValue($form_state_values, $path, $key_exists);

    $values_to_save = [];

    if (!empty($values['prices_set'])) {
      foreach ($values['prices_set']['by_shops'] as $shop_id => $price_by_types) {
        foreach ($price_by_types as $price_type_name => $price_values) {
          if (!empty($price_values['extra_config'])) {
            //$price_values['extra_config'] = serialize($price_values['extra_config']);
            //unset($price_values['extra_config']);
          }
          $values_to_save[] = $price_values;
        }
      }
    }

    if ($values_to_save) {
      $items->setValue($values_to_save);
      $items->filterEmptyItems();
    }

    if (!empty($values['prices_set']['copy_to_childs'])) {
      $this->fieldDefinition->setSetting('copy_to_variations', TRUE);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
//     $element['#type'] = 'commerce_price';
//     if (!$items[$delta]->isEmpty()) {
//       $element['#default_value'] = $items[$delta]->toPrice()->toArray();
//     }
//     $element['#available_currencies'] = $this->getFieldSetting('available_currencies');

    $element = [
      '#markup' => 'Price widget must be there.'
    ];

    return $this->formMultipleElements($items, $form, $form_state);
  }

  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    $parents = $form['#parents'];
    $field_name = $this->fieldDefinition->getName();
    $widget_state = static::getWidgetState($parents, $field_name, $form_state);
//     dpm($widget_state);
    /**
     * @var \Drupal\bs_product\Entity\ProductInterface $product
     */
    $product = $items->getEntity();
    $parent = $product->getParent();

    $defined_values = [];

    if ($values = $items->getValue()) {
      foreach ($values as $delta => $item_value) {
        $defined_values[$item_value['shop_id']][$item_value['price_type']] = $item_value;
      }
    }

    $elements = [];

    $elements['prices_set'] = [
      '#type' => 'container',
      '#title' => t('Product prices'),
      '#tree' => true,
      '#attributes' => [
        'class' => [
          'product-price-set'
        ]
      ],
    ];

    if ($product->shops->isEmpty()) {
      $elements['prices_set']['new_product_must_be_saved_message'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [t('Product prices can be defined after saving new product!')]
        ],
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ];
      return $elements;
    }

    $product_shop_ids = $product->getShopIds();
    $product_shops = $product->getShopsKeyedByID();

    //loading price types
    /**
     * @var \Drupal\Core\Entity\PriceTypeStorageInterface $price_type_storage
     */
    $price_type_storage = \Drupal::entityTypeManager()->getStorage('bs_price_type');

    $price_types_by_shops = $price_type_storage->getShopsPriceTypesKeyedByShopId($product_shop_ids);

    if (!$price_types_by_shops) {
      //drupal_set_message('Price types not defined for any one shop!', 'warning');
      return $elements;
    }

    $price_field_counter = 0;

    foreach ($product_shops as $shop_id => $shop) {

      $elements['prices_set']['by_shops'][$shop_id] = [
        '#type' => count($product_shops) > 1 ? 'fieldset' : 'container',
        '#title' => t('Shop "@shop_name" prices', ['@shop_name' => $shop->label()]),
        '#tree' => TRUE,
        '#attributes' => [
          'class' => [
            'prices-set-by-shop',
          ]
        ],
      ];

      if ($parent) {
        if (empty($widget_state['by_shops']['states'])) {
          $elements['prices_set']['by_shops'][$shop_id]['#states'] = [
            'invisible' => [
              'input[name="inh_price_from_parent[value]"]' => ['checked' => TRUE],
            ]
          ];
        }
        else {
          foreach ($widget_state['by_shops']['states'] as $state => $state_settings) {
            $elements['prices_set']['by_shops'][$shop_id]['#states'][$state] = $state_settings;
          }
        }
      }

      $shop_root_price_types = $price_type_storage->getShopStructuredPriceTypes($shop_id, '', 1);

      if (empty($shop_root_price_types)) {
        $elements['prices_set']['by_shops'][$shop_id]['no_price_types_message'] = [
          '#theme' => 'status_messages',
          '#message_list' => [
            'warning' => [t('Price types not defined for shop "@shop_name"!', ['@shop_name' => $shop->label()])]
          ],
          '#status_headings' => [
            'status' => t('Status message'),
            'error' => t('Error message'),
            'warning' => t('Warning message'),
          ],
        ];
        continue;
      }


      foreach ($shop_root_price_types as $price_type) {

        $elements['prices_set']['by_shops'][$shop_id][$price_type->id()] = [
          '#type' => 'bs_product_price',
          '#default_value' => !empty($defined_values[$shop_id][$price_type->id()]) ?
                              $defined_values[$shop_id][$price_type->id()] :
                              ['value' => '', 'currency_code' => 'RUB'],
          '#price_type' => $price_type,
          '#title' => $price_type->label,
          '#attributes' => [
            'data-price-type' => $price_type->id(),
          ]
        ];

        if (!empty($price_type->getChilds())) {
          foreach ($price_type->getChilds() as $child_price_type) {
            $elements['prices_set']['by_shops'][$shop_id][$child_price_type->id()] = [
              '#type' => 'bs_extended_product_price',
              '#default_value' => !empty($defined_values[$shop_id][$child_price_type->id()]) ?
                                  $defined_values[$shop_id][$child_price_type->id()] :
                                  ['value' => '', 'currency_code' => 'RUB'],
              '#price_type' => $child_price_type,
              '#parent_price_type' => $price_type,
              '#title' => $child_price_type->label,
              '#attributes' => [
                'data-parent-price-type' => $price_type->id(),
                'class' => [
                  'child-price'
                ]
              ],
              '#description' => $this->t('The price specified in this field
                will be used as the base price, and the price in the
                "@parent_price_label" field will be defined as the old price', [
                  '@parent_price_label' => $price_type->label,
                ])
            ];
          }
        }

        $price_field_counter++;
      }
    }

    if ($product->getBundleEntity()->isVariationsSuport() && ($product->isNew() || $product->getLevel() < $product->getBundleEntity()->getMaxChildLevel())) {
      $elements['prices_set']['copy_to_childs'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Copy prices to variations'),
        '#description' => $this->t('If checked, defined prices will be set for all child variations'),
        //'#default_value' => FALSE,
        '#wrapper_attributes' => [
          'class' => [
            'copy-price-to-childs'
          ]
        ],
        '#attributes' => [
          'data-pid' => $product->id()
        ],
        '#states' => [
          'checked' => [
            'input[name="inh_price_from_parent[value]"]' => ['checked' => TRUE]
          ],
        ]
      ];
    }

    return $elements;
  }



}
