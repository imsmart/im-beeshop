<?php

namespace Drupal\bs_price\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_price\PriceAdjustmentTypePluginManager;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\bs_price\Plugin\Field\FieldType\PriceAdjustmentItem;
use Drupal\Component\Utility\SortArray;

/**
 * Plugin implementation of 'bs_price_adjustment_default'.
 *
 * @FieldWidget(
 *   id = "bs_price_adjustment_default",
 *   label = @Translation("Price adjustment"),
 *   field_types = {
 *     "bs_price_adjustment"
 *   },
 *   multiple_values = true,
 * )
 */
class PriceAdjustmentDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $elements = $element;
    $title = $this->fieldDefinition->getLabel();
    $description = FieldFilteredMarkup::create(\Drupal::token()->replace($this->fieldDefinition->getDescription()));
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $id_prefix = implode('-', array_merge($parents, [$field_name]));
    $wrapper_id = $id_prefix;//Html::getUniqueId($id_prefix);
    $entity = $items->getEntity();

    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    /** @var \Drupal\bs_price\PriceAdjustmentTypePluginManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.price_adjustment_type_plugin');

    $types = $plugin_manager->getApplicablePluginOptions($entity);

    $this->prepareItems($field_state, $items);
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $max = $items->count() + 1;

    foreach ($items as $delta => $adjusment_item) {

      /**
       * @var PriceAdjustmentItem $adjusment_item
       */

      $item_element = [
        'type' => [
          '#type' => 'value',
          '#value' => $adjusment_item->get('type')->getValue(),
          'label' => [
            '#markup' => $types[$adjusment_item->get('type')->getValue()]
          ],
        ],
        'amount' => [
          '#type' => 'number',
          '#default_value' => $adjusment_item->get('amount')->getValue()
        ],
        'label' => [
          '#type' => 'textfield',
          '#default_value' => $adjusment_item->get('label')->getValue()
        ],
        'included' => [
          '#type' => 'checkbox',
          '#default_value' => $adjusment_item->get('included')->getValue(),
          //'#disabled' => TRUE,
        ],
        'original_delta' => [
          '#type' => 'value',
          '#value' => $delta,
        ],
        'operations' => [
          'remove_item' => [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#item_delta' => $delta,
            '#name' => 'remove_item_' . $delta,
            '#submit' => [[get_class($this), 'removeItemSubmitAjax']],
            '#ajax' => [
              'callback' => [get_class($this), 'removeItemAjax'],
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ],
          ],
        ],
      ];

      $item_element['_weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for row @number', ['@number' => $delta + 1]),
        '#title_display' => 'invisible',
        // Note: this 'delta' is the FAPI #type 'weight' element's property.
        '#delta' => $max,
        '#default_value' => $items[$delta]->_weight ?: $delta,
        '#weight' => 100,
      ];

      $elements[$adjusment_item->delta] = $item_element;
    }



    $elements['add_more'] = [
      '#type' => 'fieldset',
      '#title' => 'Add adjustment',

      'new_adjustment_type' => [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => $types,
        '#weight' => 1,
        '#empty_option' => $this->t('- Select type of adjustment to add -'),
        '#weight' => 10,
      ],
      'add_more' => [
        '#type' => 'submit',
        '#name' => strtr($id_prefix, '-', '_') . '_add_more',
        '#value' => t('Add'),
        '#attributes' => ['class' => ['field-add-more-submit']],
        '#limit_validation_errors' => [array_merge($parents, [$field_name])],
        '#submit' => [[get_class($this), 'addMoreSubmit']],
        '#ajax' => [
          'callback' => [get_class($this), 'addMoreAjax'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
        '#weight' => 20,
        '#states' => [
          'invisible' => [
            '[name="adjustments[add_more][new_adjustment_type]"]' => ['value' => '_none']
          ],
        ]
      ],
    ];

    $elements += [
      '#theme' => 'bs_field_multiple_value_and_options_form',
      '#show_operations_column' => TRUE,
      '#empty' => $this->t('No adjustments'),
      '#field_name' => $field_name,
      '#cardinality' => $cardinality,
      '#cardinality_multiple' => $this->fieldDefinition->getFieldStorageDefinition()->isMultiple(),
      '#field_definition' => $this->fieldDefinition,
//       '#required' => $this->fieldDefinition->isRequired(),
      '#title' => $title,
      '#description' => $description,
      //'#max_delta' => $max,
    ];


    $elements['#prefix'] = '<div id="' . $wrapper_id . '">';
    $elements['#suffix'] = '</div>';
    $elements['#type'] = 'fieldset';

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    $items = [];

    unset($values['add_more']);

    // The original delta, before drag-and-drop reordering, is needed to
    // route errors to the correct form element.
    foreach ($values as $delta => &$value) {
      $value['_original_delta'] = $delta;
    }

    usort($values, function ($a, $b) {
      return SortArray::sortByKeyInt($a, $b, '_weight');
    });

    foreach ($values as $delta => $data) {
      if (is_numeric($delta)) {
        $items[$delta] = $data;
      }
    }

    return $items;
  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addMoreAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    return $element;
  }

  public static function removeItemAjax(array $form, FormStateInterface $form_state) {

    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));

    return $element;
  }

  public static function removeItemSubmitAjax(array $form, FormStateInterface $form_state) {

    $button = $form_state->getTriggeringElement();

    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    if (isset($button['#item_delta'])) {
      $field_state['remove_item'] = $button['#item_delta'];
      static::setWidgetState($parents, $field_name, $form_state, $field_state);
    }

    $form_state->setRebuild();
  }

  /**
   * Submission handler for the "Add another item" button.
   */
  public static function addMoreSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    $values = $form_state->getValues();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    if (!empty($values['adjustments']['add_more']['new_adjustment_type'])) {
      $field_state['new_adjustment_type'] = $values['adjustments']['add_more']['new_adjustment_type'];
    }

    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

  protected function prepareItems(array &$field_state, FieldItemListInterface $items) {

    if (isset($field_state['remove_item'])) {
      $items->removeItem($field_state['remove_item']);
      unset($field_state['remove_item']);
    }

    if (!empty($field_state['new_adjustment_type'])) {
      $new_item = $items->appendItem([
        'type' => $field_state['new_adjustment_type'],
        'label' => '*',
        'amount' => 0,
      ]);
      $field_state['items_count']++;
      $new_item->delta = $field_state['items_count'];
      unset($field_state['new_adjustment_type']);
    }


    foreach ($items as $delta => $item) {
      $item->delta = $item->delta ?: $delta;
    }
  }

}
