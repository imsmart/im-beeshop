<?php

namespace Drupal\bs_price\Plugin\Field\FieldType;

use Drupal\bs_price\Price;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\bs_price\Calculator;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 * default_formatter = "commerce_price_default",
 *
 * @FieldType(
 *   id = "bs_price",
 *   label = @Translation("Price"),
 *   description = @Translation("Stores a product price data."),
 *   category = @Translation("BeeShop"),
 *   no_ui = TRUE,
 *   default_widget = "bs_price_default",
 *   default_token_formatter = "price_plain",
 *   list_class = "Drupal\bs_price\PriceItemList",
 * )
 */
class PriceItem extends FieldItemBase {

  protected $presaveInvoked = FALSE;

  protected $oldPrice = NULL;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Currency code'))
      ->setRequired(FALSE);

//     $properties['extra_data'] = MapDataDefinition::create()
//       ->setLabel(t('Price extra config data'))
//       ->setRequired(FALSE);

    $properties['discount_source'] = DataDefinition::create('string')
      ->setLabel(t('Discount source'))
      ->setRequired(FALSE);

    $properties['discount_reason'] = DataDefinition::create('string')
      ->setLabel(t('Discount reason'))
      ->setRequired(FALSE);

    $properties['discount_amount'] = DataDefinition::create('string')
      ->setLabel(t('Discount amount'))
      ->setRequired(FALSE);

   $properties['discount_description'] = DataDefinition::create('string')
      ->setLabel(t('Discount amount'))
      ->setRequired(FALSE);

    $properties['is_old_price'] = DataDefinition::create('boolean')
      ->setLabel(t('Is old price'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The price value.',
          'type' => 'numeric',
          'precision' => 19,
          'scale' => 6,
        ],
        'currency_code' => [
          'description' => 'The currency code.',
          'type' => 'varchar',
          'length' => 3,
        ],
//         'extra_data' => [
//           'description' => 'Price extra data',
//           'type' => 'blob',
//           'serialize' => TRUE,
//         ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'available_currencies' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    $element = [];
    $element['available_currencies'] = [
      '#type' => count($currency_codes) < 10 ? 'checkboxes' : 'select',
      '#title' => $this->t('Available currencies'),
      '#description' => $this->t('If no currencies are selected, all currencies will be available.'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();
    $constraints[] = $manager->create('ComplexData', [
      'value' => [
        'Regex' => [
          'pattern' => '/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/i',
        ],
      ],
    ]);
    $available_currencies = $this->getSetting('available_currencies');
    //$constraints[] = $manager->create('Currency', ['availableCurrencies' => $available_currencies]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '' || empty($this->currency_code);
  }

  public function isOldPrice($value = NULL) {
    if ($value !== NULL) {
      $this->writePropertyValue('is_old_price', (bool) $value);
    }

    return $this->get('is_old_price')->getValue();
  }

  public function setIsOldPrice() {
    $this->writePropertyValue('is_old_price', TRUE);
    return $this;
  }

  public function isDiscounted() {
    return (boolean) $this->oldPrice;
  }

  public function isMoreThan(PriceItem $price_for_compare) {
    if ($this->getCurrencyCode() == $price_for_compare->getCurrencyCode()) {
      return $this->get('value')->getValue() > $price_for_compare->get('value')->getValue();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow callers to pass a Price value object as the field item value.
    if ($values instanceof Price) {
      $price = $values;
      $values = [
        'value' => $price->getPriceValue(),
        'currency_code' => $price->getCurrencyCode(),
      ];
    }
    parent::setValue($values, $notify);
  }

  /**
   * Gets the Price value object for the current field item.
   *
   * @return \Drupal\commerce_price\Price
   *   The Price value object.
   */
  public function toPrice() {
    if ($this->value !== NULL && $this->currency_code) {
      $price = new Price($this->value, $this->currency_code);
      if ($old_price = $this->getOldPrice()) {
        $price->setOldPrice($old_price->toPrice());
      }
      return $price;
    }
    return FALSE;
  }

  public function getCurrencyCode() {
    return $this->currency_code;
  }

  public function getFormatedValue($thousands_sep = ' ', $dec_point = '.') {
    $formated = number_format($this->value, 2, $dec_point, $thousands_sep);
    return Calculator::trim($formated);
  }

  /**
   * @return unknown
   */
  public function getOldPrice() {
    return $this->oldPrice;
  }

  public function getPriceValue() {
    return $this->value;
  }

  public function getPriceDiscountAmount() {
    if (!$this->oldPrice) {
      return new Price(0, $this->currency_code);
    }

    $diff = Calculator::subtract($this->oldPrice->value, $this->value);

    return new Price($diff, $this->currency_code);
  }

  public function getType() {
    return $this->price_type;
  }

  public function setOldPrice($base_item) {
    $this->oldPrice = $base_item;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {

    if ($this->presaveInvoked)
      return;

    if ($presave_callbacks = $this->getSetting('presave_callback')) {
      foreach ($presave_callbacks as $callback) {
        $this->invokeCallback($callback);
      }
    }

    $this->presaveInvoked = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeCallback($callback) {
    if (is_string($callback) && substr($callback, 0, 8) == 'entity::') {
      $callback = substr($callback, 8);
      $entity = $this->getEntity();
      if (method_exists($entity, $callback)) {
        $entity->{$callback}($this);
      }
    }
    return $callback;
  }
}
