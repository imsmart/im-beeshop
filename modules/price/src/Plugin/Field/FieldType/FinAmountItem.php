<?php

namespace Drupal\bs_price\Plugin\Field\FieldType;

use Drupal\bs_price\Price;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Field\Annotation\FieldType;

/**
 * Plugin implementation of the 'bs_fin_amount' field type.
 *
 * default_formatter = "amount_default",
 * list_class = "Drupal\bs_price\PriceItemList",
 *
 * @FieldType(
 *   id = "bs_fin_amount",
 *   label = @Translation("Finance amount (amount with currency)"),
 *   description = @Translation("Stores a data in amount/currency format."),
 *   category = @Translation("BeeShop"),
 *   default_widget = "fin_amount_default",
 * )
 */
class FinAmountItem extends FieldItemBase {

  protected $presaveInvoked = FALSE;

  protected $oldPrice = NULL;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Currency code'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The price value.',
          'type' => 'numeric',
          'precision' => 19,
          'scale' => 6,
        ],
        'currency_code' => [
          'description' => 'The currency code.',
          'type' => 'varchar',
          'length' => 3,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'available_currencies' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    $element = [];
    $element['available_currencies'] = [
      '#type' => count($currency_codes) < 10 ? 'checkboxes' : 'select',
      '#title' => $this->t('Available currencies'),
      '#description' => $this->t('If no currencies are selected, all currencies will be available.'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();
    $constraints[] = $manager->create('ComplexData', [
      'value' => [
        'Regex' => [
          'pattern' => '/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/i',
        ],
      ],
    ]);
    $available_currencies = $this->getSetting('available_currencies');
    //$constraints[] = $manager->create('Currency', ['availableCurrencies' => $available_currencies]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '' || empty($this->currency_code);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow callers to pass a Price value object as the field item value.
    if ($values instanceof Price) {
      $price = $values;
      $values = [
        'value' => $price->getPriceValue(),
        'currency_code' => $price->getCurrencyCode(),
      ];
    }
    parent::setValue($values, $notify);
  }

  public function getCurrencyCode() {
    return $this->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {

    if ($this->presaveInvoked)
      return;

    if ($presave_callbacks = $this->getSetting('presave_callback')) {
      foreach ($presave_callbacks as $callback) {
        $this->invokeCallback($callback);
      }
    }

    $this->presaveInvoked = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeCallback($callback) {
    if (is_string($callback) && substr($callback, 0, 8) == 'entity::') {
      $callback = substr($callback, 8);
      $entity = $this->getEntity();
      if (method_exists($entity, $callback)) {
        $entity->{$callback}($this);
      }
    }
    return $callback;
  }
}
