<?php

namespace Drupal\bs_price\Plugin\Field\FieldType;

use Drupal\bs_price\Price;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 * default_formatter = "commerce_price_default",
 *
 * @FieldType(
 *   id = "bs_product_price",
 *   label = @Translation("Price"),
 *   description = @Translation("Stores a product price data."),
 *   category = @Translation("BeeShop"),
 *   no_ui = TRUE,
 *   default_widget = "bs_product_price_default",
 *   default_token_formatter = "price_plain",
 *   list_class = "Drupal\bs_price\ProductPriceItemList",
 * )
 */
class ProductPriceItem extends PriceItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['price_type'] = DataDefinition::create('string')
      ->setLabel(t('Price type'))
      ->setRequired(FALSE);

    $properties['shop_id'] = DataDefinition::create('integer')
      ->setLabel(t('Shop ID'))
      ->setRequired(FALSE);

    $properties['extra_config'] = MapDataDefinition::create()
      ->setLabel(t('Price extra config data'))
      ->setRequired(FALSE);

    $properties['is_minimal'] = DataDefinition::create('boolean')
      ->setLabel(t('This price is minimal price that was defined by variations'));

//     $properties['oldPrice'] = FieldItemDataDefinition::createFromDataType('field_item:bs_price')
//       ->setComputed(TRUE)
//       ->setLabel(t('The old price'))
//       ->setRequired(FALSE);


    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = parent::schema($field_definition);

    $schema['columns']['price_type'] = [
      'description' => 'Price type.',
      'type' => 'varchar',
      'length' => 255,
    ];

    $schema['columns']['shop_id'] = [
      'description' => 'Shop id.',
      'type' => 'int',
    ];

    $schema['columns']['extra_config'] = [
      'description' => 'Price extra config data',
      'type' => 'blob',
      'serialize' => TRUE,
    ];

    $schema['columns']['is_minimal'] = [
      'description' => 'This price is minimal price that was defined by variations',
      'type' => 'int',
      'size' => 'tiny',
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow callers to pass a Price value object as the field item value.
    if ($values instanceof Price) {
      $price = $values;
      $values = [
        'value' => $price->getPriceValue(),
        'currency_code' => $price->getCurrencyCode(),
      ];
    }
    parent::setValue($values, $notify);
  }

  public function setPriceValue($price_value) {
    $values = $this->getValue();

    $values['value'] = (string) $price_value;

    parent::setValue($values);
  }


  public function setExtraConfig($extra_config = []) {
    $values = $this->getValue();

    $values['extra_config'] = $extra_config;

    parent::setValue($values);
  }

  public function getType() {
    return $this->price_type;
  }

  public function getShopId() {
    return $this->shop_id;
  }

  public function getExtraConfig() {
    $values = $this->getValue();

    return !empty($values['extra_config']) ? $values['extra_config'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {

    if ($this->oldPrice && $this->extra_config && !empty($this->extra_config['formula'])) {

      $base_price_value = $this->oldPrice->getPriceValue();

      switch ($this->extra_config['type']) {
        case 'formula':
          switch ($this->extra_config['formula']['type']) {
            case 'percents':
              $calculateted_value = round($base_price_value * (100 - $this->extra_config['formula']['f_value']) / 100, 4);
              $this->setPriceValue($calculateted_value);
              break;
            case 'absolute':
              if ($this->extra_config['formula']['f_value'] >=  $base_price_value) {
                throw new \Exception('Discount amount is greater or equal base price');
              }
              if (!is_numeric($this->extra_config['formula']['f_value'])) {
                throw new \Exception('Discount amount has not numeric value');
              }
              break;
          }
          break;
        case 'static':
        case 'absolute':
          $this->extra_config = [
          'type' => 'static'
            ];
          break;
      }
    }

    parent::preSave();
  }
}
