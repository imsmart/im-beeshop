<?php

namespace Drupal\bs_price\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'bs_price_adjustment' field type.
 *
 * @FieldType(
 *   id = "bs_price_adjustment",
 *   label = @Translation("Price djustment"),
 *   description = @Translation("Stores adjustments to the price."),
 *   category = @Translation("BeeShop"),
 *   list_class = "\Drupal\bs_price\PriceAdjustmentItemList",
 *   no_ui = TRUE,
 *   default_widget = "bs_price_adjustment_default",
 * )
 */
class PriceAdjustmentItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'type';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['type'] = DataDefinition::create('string')
    ->setLabel(t('Type'))
    ->setRequired(TRUE)
    ->setSetting('has_ui_element', TRUE);

    $properties['label'] = DataDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('has_ui_element', TRUE);

    $properties['amount'] = DataDefinition::create('string')
      ->setLabel(t('Amount'))
      ->setSetting('has_ui_element', TRUE);

    $properties['source_id'] = DataDefinition::create('string')
      ->setLabel(t('Adjustment source'));

    $properties['included'] = DataDefinition::create('boolean')
      ->setLabel(t('Included in base price'))
      ->setSetting('has_ui_element', TRUE);

    $properties['locked'] = DataDefinition::create('boolean')
      ->setLabel(t('Locked'));

    $properties['data'] = DataDefinition::create('any')
      ->setLabel(t('Extra data'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'type' => [
          'description' => 'The adjustment type.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'amount' => [
          'description' => 'The adjustment amount.',
          'type' => 'numeric',
          'precision' => 19,
          'scale' => 6,
        ],
        'label' => [
          'description' => 'The adjustment label.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'source_id' => [
          'description' => 'The adjustment source.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'source_id' => [
          'description' => 'The adjustment source.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'included' => [
          'description' => 'The adjustment is included to base price.',
          'type' => 'int',
          'size' => 'tiny',
        ],
        'locked' => [
          'description' => 'The adjustment is locked.',
          'type' => 'int',
          'size' => 'tiny',
        ],
        'data' => [
          'description' => 'The adjustment extra data.',
          'type' => 'blob',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if ($this->amount === '') {
      $this->amount = NULL;
    }
  }


  public function getAmount() {
    return $this->get('amount')->getValue();
  }

  public function isIncluded() {
    return $this->get('included')->getValue();
  }
}

