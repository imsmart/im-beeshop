<?php

namespace Drupal\bs_price\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'commerce_price_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "price_plain",
 *   label = @Translation("Plain price"),
 *   field_types = {
 *     "bs_price"
 *   }
 * )
 */
class PricePlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $item->toPrice()->__toString(),
      ];
    }

    return $elements;
  }

}
