<?php
namespace Drupal\bs_price\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\bs_product\Entity\Product;
use Drupal\bs_price\Plugin\Field\FieldType\PriceItem;

/**
 * Plugin implementation of the 'commerce_price_default' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_product_price_min_max",
 *   label = @Translation("Product Min/Max price"),
 *   field_types = {
 *     "bs_product_price"
 *   }
 * )
 */
class MinMaxProductPriceFormatter extends PriceDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    //dpm($field_definition);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['old_price_fist'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show discounted price (if exist) as first price item'),
      '#default_value' => $this->getSetting('old_price_fist'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    /**
     * @var \Drupal\Core\Entity\EntityInterface $product
     */
    $product = $items->getEntity();
    $prices = [];

    if ($variations = $product->getVariations()) {


      foreach ($variations as $variation) {

        /**
         * @var Product $variation
         */

        if ($variation_price = $variation->getActualPrice()) {
          $prices[] = $variation_price;
        }
      }

      if ($prices) {
        uasort($prices, [$this, 'sortByPriceValue']);
      }


    }

    if ($prices) {

      if (count($prices) > 1) {

        $elements[] = [
          '#theme' => 'bs_price_min_max',
          '#min' => reset($prices)->setOldPrice(null),
          '#max' => end($prices)->setOldPrice(null),
        ];
      }
      else {

        $elements[] = [
          '#theme' => 'bs_price_item',
          '#price_item' => reset($prices),
        ];
      }

    }
    else {
      $elements[]['#markup'] = '<!-- no price -->';
    }

    return $elements;
  }

  public static function sortByPriceValue(PriceItem $a, PriceItem $b) {
      if ($a->value == $b->value) {
        return 0;
      }

      return ($a->value < $b->value) ? -1 : 1;
  }

}

