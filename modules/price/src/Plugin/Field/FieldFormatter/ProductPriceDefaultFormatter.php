<?php
namespace Drupal\bs_price\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'commerce_price_default' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_product_price_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "bs_product_price"
 *   }
 * )
 */
class ProductPriceDefaultFormatter extends PriceDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'old_price_fist' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['old_price_fist'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show discounted price (if exist) as first price item'),
      '#default_value' => $this->getSetting('old_price_fist'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);

//     $entity = $items->getEntity();

//     if ($entity instanceof Product) {
//       //       dpm($elements);
//     }

//     dpm($elements);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    /**
     * @var \Drupal\Core\Entity\EntityInterface $product
     */
    $product = $items->getEntity();

    if ($price = $product->getActualPrice()) {
      $elements[] = [
        '#theme' => 'bs_price_item',
        '#price_item' => $price,
      ];
    }
    else {
      $elements[]['#markup'] = '<!-- no price -->';
    }

    return $elements;
  }

}

