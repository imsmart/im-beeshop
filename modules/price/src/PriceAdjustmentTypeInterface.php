<?php
namespace Drupal\bs_price;

use Drupal\Core\Entity\EntityInterface;

interface PriceAdjustmentTypeInterface {

  /**
   * Returns if the price adjustment type can be used for the provided entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition that should be checked.
   *
   * @return bool
   *   TRUE if the widget can be used, FALSE otherwise.
   */
  public static function isApplicable(EntityInterface $entity);

}

