<?php
namespace Drupal\bs_price;

use Drupal\views\Plugin\views\PluginBase;
use Drupal\Core\Entity\EntityInterface;

abstract class PriceAdjustmentTypeBase extends PluginBase implements PriceAdjustmentTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityInterface $entity) {
    return TRUE;
  }
}

