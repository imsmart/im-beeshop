<?php
namespace Drupal\bs_price;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityInterface;

class PriceAdjustmentTypePluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Beeshop/Price/PriceAdjustment/Type',
      $namespaces,
      $module_handler,
      'Drupal\bs_price\PriceAdjustmentTypeInterface',
      'Drupal\bs_price\Annotation\PriceAdjustmentType'
      );
    $this->setCacheBackend($cache_backend, 'price_adjustment_type_plugins');
    $this->alterInfo('price_adjustment_type_plugin_info');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

  public function getApplicablePluginOptions(EntityInterface $entity) {
    $options = $this->getOptions();
    $applicable_options = [];
    foreach ($options as $option => $label) {
      $plugin_class = DefaultFactory::getPluginClass($option, $this->getDefinition($option));
      if ($plugin_class::isApplicable($entity)) {
        $applicable_options[$option] = $label;
      }
    }
    return $applicable_options;
  }

  /**
   * Returns an array of widget type options.
   *
   * @return array
   *   Returns a nested array of all widget types
   */
  public function getOptions() {
    if (!isset($this->widgetOptions)) {
      $options = [];
      $widget_types = $this->getDefinitions();
      uasort($widget_types, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
      foreach ($widget_types as $name => $widget_type) {
        $options[$name] = $widget_type['label'];
      }
      $this->widgetOptions = $options;
    }

    return $this->widgetOptions;
  }

}

