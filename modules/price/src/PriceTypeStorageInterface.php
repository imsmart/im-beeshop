<?php
namespace Drupal\bs_price;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

interface PriceTypeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Load price types registered for shops
   *
   * @param array $shop_ids
   *
   * @return \Drupal\bs_price\PriceTypeInterface[]
   */
  public function getShopsPriceTypes($shop_ids);

  /**
   * Loads price types registered for shops and groups them by shop IDs
   *
   * @param array $shop_ids
   *
   * @return []\Drupal\bs_price\PriceTypeInterface[]
   */
  public function getShopsPriceTypesKeyedByShopId($shop_ids);

  /**
   * Finds all price types in a given shop ID.
   *
   * @param string $sid
   *   Shop ID to retrieve price types for.
   * @param int $parent
   *   The price type ID under which to generate the tree. If 0, generate the tree
   *   for the entire shop.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   *
   * @return object[]|\Drupal\bs_price\PriceTypeInterface[]
   *   An array of price types objects that are the children of the shop $sid.
   */
  public function loadTree($sid, $parent = 0, $max_depth = NULL);

}

