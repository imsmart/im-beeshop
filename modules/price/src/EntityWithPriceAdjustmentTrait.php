<?php
namespace Drupal\bs_price;

use Drupal\Core\Field\FieldDefinitionInterface;

trait EntityWithPriceAdjustmentTrait {

  protected $adjustmentInProcess = FALSE;

  public function hasAdjustment($adjustment_type) {

    if ($adjusment_field_name = $this->getEntityType()->getKey('adjustment')) {
      $adjusments = $this->get($adjusment_field_name);

      if ($adjusments instanceof PriceAdjustmentItemListInterface) {
        return $adjusments->filterByType($adjustment_type) ? TRUE : FALSE;
      }
    }

    return FALSE;
  }

  public function getAdjustmentByType($adjustment_type) {
    if ($adjusment_field_name = $this->getEntityType()->getKey('adjustment')) {
      $adjusments = $this->get($adjusment_field_name);

      if ($adjusments instanceof PriceAdjustmentItemListInterface) {
        return $adjusments->filterByType($adjustment_type);
      }
    }
  }

  public function &__get($name) {

    $result = parent::__get($name);

    $adjusment_field_name = $this->getEntityType()->getKey('adjustment');

    if (!$adjusment_field_name || !isset($this->fieldDefinitions[$adjusment_field_name])) return $result;

    /**
     * @var FieldDefinitionInterface $adjusment_field_definition
     */
    $adjusment_field_definition = $this->fieldDefinitions[$adjusment_field_name];

    if (($adj_target_field_name = $adjusment_field_definition->getSetting('target_field')) &&
      $adj_target_field_name == $name) {

        $this->processAdjustmentField();
        return $result;
    }
    else {
      return $result;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function get($field_name) {
    $result = parent::get($field_name);

    if ($this->isAdjustmentTarget($field_name) && !$this->adjustmentInProcess) {
      $this->adjustmentInProcess = TRUE;
      $this->processAdjustmentField();
      $this->adjustmentInProcess = FALSE;
    }

    return $result;
  }


  public function getAdjusments() {
    if ($adjusment_field_name = $this->getEntityType()->getKey('adjustment')) {
      return $this->get($adjusment_field_name);
    }
  }


  protected function isAdjustmentTarget($field_name) {
    $adjusment_field_name = $this->getEntityType()->getKey('adjustment');
    if (!$adjusment_field_name || !isset($this->fieldDefinitions[$adjusment_field_name])) return FALSE;

    /**
     * @var FieldDefinitionInterface $adjusment_field_definition
     */
    $adjusment_field_definition = $this->fieldDefinitions[$adjusment_field_name];

    if (($adj_target_field_name = $adjusment_field_definition->getSetting('target_field')) &&
      $adj_target_field_name == $field_name) {
        return TRUE;
      }

    return FALSE;
  }

  protected function processAdjustmentField() {
    $adjusment_field_name = $this->getEntityType()->getKey('adjustment');

    if (!$adjusment_field_name) return;

    $this->get($adjusment_field_name)->preSave();
  }

}

