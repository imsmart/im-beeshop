<?php

namespace Drupal\bs_price;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for price type.
 */
interface PriceTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the weight of this price type.
   *
   * @return int
   *   The weight of the price type.
   */
  public function getWeight();

  /**
   * Sets the weight of this price type.
   *
   * @param int $weight
   *   The type's weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}