<?php
namespace Drupal\bs_price\Resolver;

use Drupal\beeshop\PurchasableEntityInterface;

class ChainPriceResolver implements ChainPriceResolverInterface {

  /**
   * Holds arrays of price resolvers, keyed by priority.
   *
   * @var array
   */
  protected $resolvers = [];

  /**
   * Holds the array of price resolvers sorted by priority.
   *
   * Set to NULL if the array needs to be re-calculated.
   *
   * @var \Drupal\bs_price\Resolver\PriceResolverInterface[]|null
   */
  protected $sortedResolvers;

  /**
   * {@inheritdoc}
   */
  public function addResolver(PriceResolverInterface $resolver, $priority) {
    $this->resolvers[$priority][] = $resolver;
    // Force the builders to be re-sorted.
    $this->sortedResolvers = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(PurchasableEntityInterface $entity) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(PurchasableEntityInterface $entity, $prev_resolved = NULL) {

    $result = $prev_resolved;

    foreach ($this->getSortedBuilders() as $resolver) {

      if (!$resolver->applies($entity)) {
        // The resolverer does not apply, so we continue with the other resolversers.
        continue;
      }

      if ($current_result = $resolver->resolve($entity, $result)) {
        $result = $current_result;
      }

    }

    return $result;
  }

  /**
   * Returns the sorted array of price resolvers.
   *
   * @return \Drupal\bs_price\Resolver\PriceResolverInterface[]
   *   An array of price resolvers objects.
   */
  protected function getSortedBuilders() {
    if (!isset($this->sortedResolvers)) {
      // Sort the resolvers according to priority.
      krsort($this->resolvers);
      // Merge nested resolvers from $this->resolvers into $this->sortedResolvers.
      $this->sortedResolvers = [];
      foreach ($this->resolvers as $resolvers) {
        $this->sortedResolvers = array_merge($this->sortedResolvers, $resolvers);
      }
    }
    return $this->sortedResolvers;
  }
}

