<?php

namespace Drupal\bs_price\Resolver;

use Drupal\beeshop\PurchasableEntityInterface;

/**
 * Defines the interface for base price resolvers.
 */
interface PriceResolverInterface {

  /**
   * Whether this price resolver should be used to resolve entity price.
   *
   * @param \Drupal\beeshop\PurchasableEntityInterface $entity
   *   The current route match.
   *
   * @return bool
   *   TRUE if this builder should be used or FALSE to let other builders
   *   decide.
   */
  public function applies(PurchasableEntityInterface $entity);

  /**
   * Resolves the base price of a given purchasable entity.
   *
   * @param \Drupal\beeshop\PurchasableEntityInterface $entity
   *   The purchasable entity.
   *
   * @return \Drupal\bs_price\Price|null
   *   A price value object, if resolved. Otherwise NULL, indicating that the
   *   next resolver in the chain should be called.
   */
  public function resolve(PurchasableEntityInterface $entity, $prev_resolved);

}
