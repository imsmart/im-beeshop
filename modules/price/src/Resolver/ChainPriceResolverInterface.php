<?php

namespace Drupal\bs_price\Resolver;

interface ChainPriceResolverInterface extends PriceResolverInterface {

  /**
   * Adds another price resolver.
   *
   * @param Drupal\bs_price\Resolver\PriceResolverInterface $resolver
   *   The price resolver to add.
   * @param int $priority
   *   Priority of the price resolver.
   */
  public function addResolver(PriceResolverInterface $resolver, $priority);

}

