<?php

namespace Drupal\bs_price;

use Drupal\Core\Field\FieldItemList;

/**
 * Represents a list of price adjustment item field values.
 */
class PriceAdjustmentItemList extends FieldItemList implements PriceAdjustmentItemListInterface {

  /**
   * {@inheritDoc}
   */
  public function appendOrReplaceItem($value = NULL) {

    $new_item = $this->createItem(0, $value);

    foreach ($this as $offset => $item) {
      if ($item->type == $new_item->type) {
        $this->list[$offset] = $new_item;
        return $new_item;
      }
    }

    return $this->appendItem($value);
  }

  /**
   * {@inheritdoc}
   */
  public function filterByType($type) {

    $result = [];

    if (isset($this->list)) {
      $result = array_filter($this->list, function ($item) use ($type) {
        if ($item->type == $type) {
          return TRUE;
        }
      });

    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
    $this->updateTargetField();
  }

  public function updateTargetField() {
    if ($sorce_field_name = $this->getSetting('source_field')) {
      if ($this->getEntity()->hasField($sorce_field_name)) {

        if (!$this->getEntity()->get($sorce_field_name)->isEmpty()) {
          $sorce_field_item = $this->getEntity()->get($sorce_field_name)->first();
          $sorce_field_item->preSave();

          $source_price = $sorce_field_item->toPrice();

          foreach ($this as $adjusment_item) {
            if (!$adjusment_item->isIncluded() && ($adjustment_amount = $adjusment_item->getAmount())) {
              $source_price = $source_price->addNumericValue($adjustment_amount);
            }
          }

          if ($target_field_name = $this->getSetting('target_field')) {
            $target_field_item = $this->getEntity()->get($target_field_name)->first();
            $target_field_item->setValue($source_price);
          }
        }
      }
    }
  }

}

