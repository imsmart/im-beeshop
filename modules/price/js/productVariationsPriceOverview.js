/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {

  'use strict';
  
  $(document).ready(function() {
  	$('input[name$="[copy_to_childs]"]').change(function() {
  		var pid = $(this).data('pid'),
  		    copyCheckbox = $(this),
  		    checked = copyCheckbox.prop('checked');
  		
  		$(`tr[data-parent-id="${pid}"] input[name$="[inh_price_from_parent][value]"]`).each(function (index, element) {
  			$(element).prop('checked', checked);
  			$(element).change();
  		});
  		
  	});
  });
  
})(jQuery, window, Drupal, drupalSettings);