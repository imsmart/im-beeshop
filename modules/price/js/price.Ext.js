/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {

  'use strict';
  
   $(document).ready(function() {
  	 $('.bs-extended-product-price').each(function (index, container) {
  		 var $container = $(container),
  		     parentPriceType = $container.data('parent-price-type'),
  		     $shopPricesContainer = $container.parents('.prices-set-by-shop'),
  		     $parentPriceInput = parentPriceType ?
  		    		                 $('[data-price-type="' + parentPriceType + '"] input[name$="[value]"]', $shopPricesContainer) :
  		    		                 false,
  		     $typeSelect = $('[name$="[extra_config][type]"]', $container),
  		     $priceInput = $('[name$="[value]"]', $container),
  		     $formulaInputs = $('[name*="[formula]"]', $container),
  		     $formulaValue = $('[name$="[formula][f_value]"]', $container),
  		     $formulaType = $('[name$="[formula][type]"]', $container);
  		 
  		 $typeSelect.change(function() {
  			 var val = $(this).val();
  		 
  			 if (val == 'absolute' || val == 'static') {
  				 $priceInput.closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-readonly', false).find('select, input, textarea').prop('readonly', false);
  				 $formulaInputs.hide();
  			 }
  			 
  			 if (val == 'formula') {
  				 $priceInput.closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-readonly', true).find('select, input, textarea').prop('readonly', true);
  				 $formulaInputs.show();
  			 }
  		 });
  		 
  		 $typeSelect.change();
  		 
  		 var caclulateChildPrice = function () {
  			 var f_value = $formulaValue.val();
  			 
  			 if ($typeSelect.val() == 'absolute' || $typeSelect.val() == 'static') return;
  			 
  			 if ($parentPriceInput) {
  				 var childPrice = 0;
  				 
  				 switch ($formulaType.val()) {
    				 case 'absolute':
    					 childPrice = Math.round($parentPriceInput.val() - $formulaValue.val());
    					 break;
    				 case 'percents':
    					 childPrice = Math.round($parentPriceInput.val()*((100 - $formulaValue.val()) / 100));
    					 break;
    			 }
  				 
  				 $priceInput.val(childPrice > 0 ? childPrice : '');
  			 }
  			 else {
  				 alert('Parent (base) price input cant be detected!');
  			 }
  		 }
  		 
  		 $formulaInputs.change(function () {
  			 caclulateChildPrice();
  		 }).on('keyup', function () {
  			 caclulateChildPrice();
  		 });
  		 $parentPriceInput.change(function () {
  			 caclulateChildPrice();
  		 }).on('keyup', function () {
  			 caclulateChildPrice();
  		 });
  	 });
   });
  
  
})(jQuery, window, Drupal, drupalSettings);