<?php
namespace Drupal\bs_commerceml\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\ProductsStorageInterface;
use Drupal\im_commerceml\importFilesCrawler;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Database\Query\Select;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;

/**
 * A CommerceML import files processor.
 *
 * @QueueWorker(
 *   id = "bs_commerceml_import_processor",
 *   title = @Translation("BeeShop CommerceML import files processor"),
 * )
 */
class ImportFilesProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * @var ProductsStorageInterface
   */
  protected $productStorage;

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   *
   * @var RefLinksList
   */
  protected $existingRefLinksList;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(ProductsStorageInterface $product_storage, QueryFactory $entity_query, QueueFactory $queue_factory) {

      $this->productStorage = $product_storage;
      $this->entityQuery = $entity_query;
      $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('bs_product'),
      $container->get('entity.query'),
      $container->get('queue')
      );
  }

  public function getIdsForAdd($external_ids = []) {
    return $this->existingLinks()->rightDiffExternalIds($external_ids);
  }

  public function existingLinks() {
    if ($this->existingRefLinksList) {
      return $this->existingRefLinksList;
    }

    $this->existingRefLinksList = $this->getDataExchangeNode()->getAllReferencedIds('bs_product');

    return $this->existingRefLinksList;
  }

  /**
   * @return \Drupal\bs_data_exchange\BsDataExchangeNodeInterface | NULL
   */
  protected function getDataExchangeNode() {
    return BsDataExchangeNode::load('1c');;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!empty($data->files)) {

      $crawler = new importFilesCrawler($data->files);
      $offers_crawler = $crawler->getFileCrawler('offers');
      $import_crawler = $crawler->getFileCrawler('import');

      // Need to find product with barcode not referenced to

      if ($groups_node = $import_crawler->getGroupsNode()) {

        /**
         * @var Serializer $serializer
         */
        $serializer = \Drupal::service('serializer');


        $groups = $serializer->decode($groups_node->ownerDocument->saveXML($groups_node), 'xml');

        if (!empty($groups['Группа'])) {
          foreach ($groups['Группа'] as $key => $group) {
            if (is_numeric($key)) {
              _hm_process_1c_group_node($group);
            }
          }
        }


      }

      if ($offers_ids = $offers_crawler->getOffersIds()) {

        /**
         * @var  $query
         */
        $query = $this->entityQuery->get('bs_product')
          ->condition('data_exchange_node_ref.target_id', $data->dataExchangeNodeId)
          ->condition('data_exchange_node_ref.external_id', $offers_ids, 'IN')
          ->condition('data_exchange_node_ref.status', 1);

        if ($ids_to_update = $query->execute()) {

          /** @var QueueInterface $queue */
          $update_products_queue = $this->queueFactory->get('bs_commerceml_update_products');


          foreach ($ids_to_update as $id) {
            $item = new \stdClass();
            $item->dataExchangeNodeId = '1c';
            $item->files = $data->files;
            $item->productId = $id;
            $update_products_queue->createItem($item);
          }
        }
        else {

          // TODO: Need notice about "Nothing to update"

        }

        // Get linked products that does not exist in importfile
//         $query = $this->entityQuery->get('bs_product')
//         ->condition('data_exchange_node_ref.data_exchange_node', $data->dataExchangeNodeId)
//         ->condition('data_exchange_node_ref.external_id', $offers_ids, 'NOT IN');

//         // Get new products ids list
//         $query = db_select('bs_product__data_exchange_node_ref', 'ids');
//         $query->condition('data_exchange_node_ref_data_exchange_node', 'my_store_data_exchange', '=')
//         ->addField('ids', 'data_exchange_node_ref_external_id', 'ext_id');

//         $result = $query->execute()->fetchCol();

//         $new_products_queue = $this->queueFactory->get('bs_commerceml_create_products');

//         foreach ($offers_ids as $offer_id) {

//           if (!in_array($offer_id, $result)) {
//             $item = new \stdClass();
//             $item->dataExchangeNodeId = 'my_store_data_exchange';
//             $item->files = $data->files;
//             $item->offerId = $offer_id;
//             $new_products_queue->createItem($item);
//           }

//         }
      }

      $offers_to_add = $this->getIdsForAdd($offers_ids);

      $new_products_queue = $this->queueFactory->get('bs_commerceml_create_products');

      foreach ($offers_to_add as $offer_id) {
        $item = new \stdClass();
        $item->dataExchangeNodeId = '1c';
        $item->files = $data->files;
        $item->offerId = $offer_id;
        $new_products_queue->createItem($item);
      }

    }
  }
}

