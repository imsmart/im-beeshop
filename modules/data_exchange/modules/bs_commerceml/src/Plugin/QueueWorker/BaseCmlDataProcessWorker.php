<?php
namespace Drupal\bs_commerceml\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;
use Drupal\bs_product\Entity\ProductInterface;
use Drupal\im_commerceml\Entity\Offer;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Psr\Log\LoggerInterface;

abstract class BaseCmlDataProcessWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * @var Serializer
   */
  protected $serialiser;

  /**
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(Serializer $serializer, LoggerInterface $logger, ModuleHandlerInterface $module_handler) {
    $this->serialiser = $serializer;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('serializer'),
      $container->get('logger.channel.cron'),
      $container->get('module_handler')
      );
  }

  protected function setProductPrice(ProductInterface $product, Offer $product_offer, BsDataExchangeNodeInterface $data_exchange_node) {
    if (isset($product_offer->Цены)) {

      $linked_price_types = $data_exchange_node->getLinkedPriceTypes();

      foreach ($product_offer->Цены as $price) {

        if (isset($linked_price_types[$price['ИдТипаЦены']])) {
          $price_type_name = $linked_price_types[$price['ИдТипаЦены']];

          if ($ex_price = $product->getPriceByPriceType($price_type_name)) {
            $old_price_values = $ex_price->getValue();
            $old_price_values['value'] = floatval($price['ЦенаЗаЕдиницу']);
            $ex_price->setValue($old_price_values);
          }
          else {
            $values = [
              'value' => $price['ЦенаЗаЕдиницу'],
              'currency_code' => 'RUB',
              'price_type' => $price_type_name,
              'shop_id' => 1,
            ];
            $product->price->appendItem($values);
          }

          $product->set('inh_price_from_parent', 0);
        }

      }

    }
  }

  protected function setProductQuantity(ProductInterface $product, Offer $product_offer, BsDataExchangeNodeInterface $data_exchange_node) {
    if ($product->hasField('quantity_in_wh')) {
      $linked_whs = $data_exchange_node->getReferencedEntities('bs_shop_warehouse');

      foreach ($linked_whs as $wh_ex_id => $wh) {
        $product->get('quantity_in_wh')->setInWarehoseQuantity($wh->id(), 0);
      }

      if (isset($product_offer->КоличествоНаСкладах)) {
        foreach ($product_offer->КоличествоНаСкладах as $on_wh_quantity) {
          if (isset($linked_whs[$on_wh_quantity['ИдСклада']])) {
            $product->get('quantity_in_wh')->setInWarehoseQuantity($linked_whs[$on_wh_quantity['ИдСклада']]->id(), $on_wh_quantity['Количество']);
          }
        }
      }
      elseif (!empty($product_offer->Склад)) {
        if (!empty($product_offer->Склад[0])) {
          foreach ($product_offer->Склад as $on_wh_quantity) {
            if (isset($linked_whs[$on_wh_quantity['@ИдСклада']])) {
              $product->get('quantity_in_wh')->setInWarehoseQuantity($linked_whs[$on_wh_quantity['@ИдСклада']]->id(), $on_wh_quantity['@КоличествоНаСкладе']);
            }
          }
        }
        elseif (($wh_id = $product_offer->Склад['@ИдСклада']) && isset($linked_whs[$wh_id])) {
          $product->get('quantity_in_wh')->setInWarehoseQuantity($linked_whs[$wh_id]->id(), $product_offer->Склад['@КоличествоНаСкладе']);
        }
      }
    }
  }

}

