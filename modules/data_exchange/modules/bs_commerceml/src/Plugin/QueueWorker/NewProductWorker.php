<?php
namespace Drupal\bs_commerceml\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\bs_data_exchange\Exception\InvalidQueueItemDataExeption;
use Drupal\im_commerceml\importFilesCrawler;
use Drupal\bs_product\Entity\Product;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Drupal\taxonomy\Entity\Term;
use Drupal\bs_price\Entity\PriceType;
use Drupal\Core\Logger\LoggerChannelInterface;
use Psr\Log\LoggerInterface;

/**
 * A CommerceML import files processor for update linked products.
 *
 * @QueueWorker(
 *   id = "bs_commerceml_products_creator",
 *   title = @Translation("BeeShop CommerceML products creator"),
 * )
 */
class NewProductWorker extends BaseCmlDataProcessWorker {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (empty($data->dataExchangeNodeId)) {
      throw new InvalidQueueItemDataExeption('Queue item does not contain "dataExchangeNodeId"');
    }

    $logger_context = [
      '%dataexchange_node_id' => $data->dataExchangeNodeId,
      '%offer_id' => $data->offerId,
    ];

    if ($data_exchange_node = BsDataExchangeNode::load($data->dataExchangeNodeId)) {

      $crawler = new importFilesCrawler($data->files);
      $offers_crawler = $crawler->getFileCrawler('offers');
      $import_crawler = $crawler->getFileCrawler('import');

      if ($cml_offer = $offers_crawler->getOffer($data->offerId)) {
        /**
         * @var Offer $cml_offer
         */
        $offer_id_parts = explode('#', $data->offerId);
        $parent_product = NULL;

        if (count($offer_id_parts) == 2) {
          if ($cml_product = $import_crawler->getProduct($offer_id_parts[0])) {
            if ($parent_product = $data_exchange_node->loadLinkedEntityByExternalID('bs_product', $offer_id_parts[0])) {
              $parent_pid = $parent_product->id();
            }
            else {
              if ($parent_product = $this->createNewProduct($cml_product, $cml_offer, $data_exchange_node, $cml_product->Ид)) {

                if (!$parent_product->save()) {
                  throw new \Exception('Error while saving new parent product for offer #' . $data->offerId);
                }
                $product_link = $parent_product->toLink(t('View'));
                $context = ['%title' => $parent_product->getTitle(), 'link' => $product_link->toString()];
                $this->logger->notice('New parent product "%title"', $context);
              }
            }

          }
        }
        else {
          $cml_product = $import_crawler->getProduct($data->offerId);
        }


        $new_product = $this->createNewProduct($cml_product, $cml_offer, $data_exchange_node, $cml_offer->Ид);
        if ($parent_product) {
          $new_product->set('parent', $parent_product->id());
        }

        $this->setProductPrice($new_product, $cml_offer, $data_exchange_node);
        $this->setProductQuantity($new_product, $cml_offer, $data_exchange_node);
        $this->moduleHandler->invokeAll('cml_item_process_product_presave', [$new_product, $cml_product, $cml_offer]);

        if ($new_product->save()) {
          if ($parent_product) {
            $parent_product->variations->appendItem($new_product->id());

            $parent_product->set('price', $new_product->get('price')->getValue());

            $parent_product->save();

            $context = [
              '%id' => $new_product->id(),
              'link' => $parent_product->toLink(t('View'))->toString(),
              '%parent_title' => $parent_product->getTitle(),
            ];
            $this->logger->notice('New product "#%id" was created and added to parent "%parent_title"', $context);
          }
          else {
            $context = [
              'link' => $new_product->toLink(t('View'))->toString(),
              '%title' => $new_product->getTitle(),
            ];
            $this->logger->notice('New product "#%title" was created', $context);
          }


        }

      }
      else {
        $this->logger->notice('Offer node "%offer_id" not found in source doc.', $logger_context);
      }

    }
    else {
      throw new \Exception('Data exchange node #' . $data->dataExchangeNodeId . ' not loaded!');
    }

  }

  protected function createNewProduct($cml_product, $cml_offer, $data_exchange_node, $uid) {

    $bundle = 'spare_part';

    $values = [
      'type' => $bundle,
      'uid' => [
        'target_id' => \Drupal::currentUser()->id(),
      ]
    ];

    $title = $cml_product->Наименование;
    $sku = $cml_product->Артикул;

    $categories = [];

    $new_product = Product::create($values);
    $new_product->setTitle($title);
    $new_product->setSku($sku);
    $new_product->setShopIds([1, 2, 3]);
    $new_product->set('status', TRUE);

    $data_exch_node_ref_values = [
      'target_id' => $data_exchange_node->id(),
      'external_id' => $uid,
      'status' => 1,
    ];
    $new_product->set('data_exchange_node_ref', $data_exch_node_ref_values);

    if ($cml_product && !empty($cml_product->Группы)) {
      foreach ($cml_product->Группы as $group_id) {
        if ($category = $data_exchange_node->loadLinkedEntityByExternalID('bs_products_category', $group_id)) {
          $categories[] = $category->id();
        }
      }
    }

    $categories = $categories ? $categories : [27];
    $new_product->setCategoriesIds($categories);

    if (!empty($cml_offer) && !empty($cml_offer->ХарактеристикиТовара['ХарактеристикаТовара'])) {
      $properties = [];

      foreach ($cml_offer->ХарактеристикиТовара['ХарактеристикаТовара'] as $property) {
        if (!empty($property['Наименование']) && !empty($property['Значение'])) {
          $properties[$property['Наименование']] = $property['Значение'];
        }
      }

      if (!empty($properties['Размер'])) {
        $value = preg_replace('#[^a-zA-Z0-9]#uim', '', $properties['Размер']);

        $term = NULL;

        if ($terms = taxonomy_term_load_multiple_by_name($value, 'sizes')) {
          $term = reset($terms);
        }
        else {
          $term = Term::create([
            'vid' => 'sizes',
            'name' => $value,
          ]);
          $term->setName($value);
          if (!$term->save()) {
            $term = NULL;
          }
        }

        if ($term) {
          $new_product->set('field_product_size', $term->id());
        }
        else {

        }

      }

      if (!empty($properties['Цвет'])) {

        $term = NULL;

        if ($terms = taxonomy_term_load_multiple_by_name($properties['Цвет'], 'colors')) {
          $term = reset($terms);
        }
        else {
          $term = Term::create([
            'vid' => 'colors',
            'name' => $properties['Цвет'],
          ]);
          $term->setName($properties['Цвет']);
          if (!$term->save()) {
            $term = NULL;
          }
        }

        if ($term) {
          $new_product->set('field_product_color', $term->id());
        }

      }
    }

    return $new_product;
  }
}

