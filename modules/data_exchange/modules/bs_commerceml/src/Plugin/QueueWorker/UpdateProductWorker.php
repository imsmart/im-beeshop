<?php
namespace Drupal\bs_commerceml\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\ProductsStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\im_commerceml\importFilesCrawler;
use Drupal\bs_product\Entity\Product;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\bs_data_exchange\Exception\InvalidQueueItemDataExeption;
use Symfony\Component\Serializer\Serializer;
use Drupal\im_commerceml\Entity\Offer;
use Drupal\bs_price\Price;
use Drupal\bs_price\Entity\PriceType;
use Drupal\bs_product\Entity\ProductInterface;

/**
 * A CommerceML import files processor for update linked products.
 *
 * @QueueWorker(
 *   id = "bs_commerceml_products_update",
 *   title = @Translation("BeeShop CommerceML products updater"),
 * )
 */
class UpdateProductWorker extends BaseCmlDataProcessWorker {

  /**
   * @var QueryInterface
   */
  protected $entityQuery;

  /**
   * @var ProductsStorageInterface
   */
  protected $productStorage;


  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (empty($data->dataExchangeNodeId)) {
      throw new InvalidQueueItemDataExeption('Queue item does not contain "dataExchangeNodeId"');
    }

    if ($data_exchange_node = BsDataExchangeNode::load($data->dataExchangeNodeId)) {

      if (!empty($data->productId)) {
        if ($product = Product::load($data->productId)) {

          if ($external_id = $data_exchange_node->getEntityExtarnalID($product)) {
            if (!empty($data->files)) {
              $crawler = new importFilesCrawler($data->files);
              /**
               * @var Ambigous <NULL, \Drupal\im_commerceml\offersDocCrawler> $offers_crawler
               */
              $offers_crawler = $crawler->getFileCrawler('offers');
              $import_crawler = $crawler->getFileCrawler('import');

              $offer_id_parts = explode('#', $external_id);
              $cml_product_id = count($offer_id_parts) == 2 ? $offer_id_parts[0] : $external_id;
              $cml_product = $import_crawler->getProduct($cml_product_id);

              if ($product_offer = $offers_crawler->getOffer($external_id)) {

                // Some times barcodes may be attached to onother product :)
                if ($product->hasField('field_barcode') && !$product->get('field_barcode')->isEmpty() && empty($product_offer->Штрихкод)) {
                  // We need to check for is exist other offer with this barcode
                  foreach($product->get('field_barcode') as $barcode_item) {
                    $barcode = $barcode_item->value;

                    $ns_prefix = $offers_crawler->getXPathQueryNSPrefix();
                    $xpath = '//' . $ns_prefix . 'Предложение[./Штрихкод/text()=\'' . $barcode . '\']';

                    if ($other_offer = $offers_crawler->getOfferByXPat($xpath)) {

                      $ref_values = [
                        'status' => 1,
                        'external_id' => $other_offer->Ид,
                      ];

                      $product_link = $product->toLink(t('Edit form'), 'edit-form');

                      $context = [
                        '%title' => $product->label(),
                        'link' => $product_link->toString(),
                        '@prev_uuid' => $external_id,
                        '@new_uuid' => $other_offer->Ид
                      ];

                      \Drupal::logger('gs_ext:ref_to_1c')->warning('Product will be reasign to another offer beacause his barcode was set to another offer: "@prev_uuid" -> "@new_uuid"', $context);

                      if ($data_exch_node = BsDataExchangeNode::load($data->dataExchangeNodeId)) {
                        $ref_values['entity'] = $data_exch_node;

                        if ($data_exch_node->setEntityReference($product, $ref_values) && $product->save()) {

                          \Drupal::logger('gs_ext:ref_to_1c')->notice('Product "%title" has been referenced to "1c" data exchange node.', $context);

                          $update_products_queue = \Drupal::queue('bs_commerceml_update_products');

                          $item = new \stdClass();
                          $item->dataExchangeNodeId = '1c';
                          $item->files = $data->files;
                          $item->productId = $product->id();

                          $update_products_queue->createItem($item);
                        }
                        else {
                          \Drupal::logger('gs_ext:ref_to_1c')->error('Can\'t reasign product "%title" to "1c" data exchange node.', $context);
                        }
                      }

                    }

                    return;
                  }
                }
                //

                $this->setProductPrice($product, $product_offer, $data_exchange_node);
                $this->setProductQuantity($product, $product_offer, $data_exchange_node);

                $this->moduleHandler->invokeAll('cml_item_process_product_presave', [$product, $cml_product, $product_offer]);

                $product->save();

                if ($parent_product = $product->getParent()) {
                  $parent_product->save();
                }

              }

            }
            else {
              throw new InvalidQueueItemDataExeption('Queue item does not contain "files" data for reading');
            }
          }
          else {

          }
        }
      }
    }
    else {

    }

  }

}

