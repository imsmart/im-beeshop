<?php
namespace Drupal\bs_commerceml\Plugin\Beeshop\DataExchange\processor;

use Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Drupal\im_commerceml\CommercemlProtocolHandler;
use Drupal\im_commerceml\importDocCrawler;
use Drupal\im_commerceml\offersDocCrawler;
use Drupal\bs_data_exchange\Event\BsDataExchangeNodeEvent;
use Drupal\bs_data_exchange\Event\BsDataExchangeEvents;
use Symfony\Component\Config\Util\XmlUtils;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Drupal\Core\Queue\QueueInterface;

/**
 * CML
 *
 * @ingroup bs_data_exchange_processor_plugins
 *
 * @BsDataExchangeProcessor(
 *   id="commerceml",
 *   title = @Translation("CommerceML"),
 *   support_price_types = TRUE,
 *   operations = {
 *     "exchange" = {
 *       "callback" = "::do1cExchange",
 *       "route_config" = {
 *         "options" = {
 *           "_auth" = {"basic_auth"}
 *         },
 *         "requirements" = {
 *           "_user_is_logged_in" = "TRUE",
 *         }
 *       }
 *     },
 *   }
 * )
 */
class CommerceML extends BsDataExchangeProcessorPluginBase {

  public function do1cExchange(BsDataExchangeNodeInterface $dx_node, $bs_dxnode_exch_point, Response &$response) {

    $request = \Drupal::request();

    $request_handler = new CommercemlProtocolHandler();

    $request_handler->setCallback('catalog', 'import', [$this, 'catalogImportCallback']);

    $response = $request_handler->handle($request);

  }

  public function catalogImportCallback(CommercemlProtocolHandler $request_handler) {

    $response = $request_handler->getResponse();
    $session = $request_handler->getSession();
    $response_content_lines = [];

    if ($filename = $request_handler->getFullFileName()) {

      $filename_short = basename($filename);
      $file_dir = dirname($filename);

      try {
        $dom = XmlUtils::loadFile($filename);


        switch ($filename_short) {
          case 'import.xml':
            $import_doc = new importDocCrawler($dom);
            if ($products_count = $import_doc->getProductsCount()) {
              $response_content_lines[] = 'success';
              $response_content_lines[] = "File \"$filename_short\" saved succesfuly";
              $response_content_lines[] = 'Products count in file: ' . $products_count;

              $session->set('import.xml', $filename);
            }
            else {
              $response_content_lines[] = 'failure';
              $response_content_lines[] = "Cant detect products count in file \"$filename_short\"";
            }
            break;
          case 'offers.xml':
            $offers_doc = new offersDocCrawler($dom);
            if ($offers_count = $offers_doc->getOffersCount()) {
              $response_content_lines[] = 'success';
              $response_content_lines[] = "File \"$filename_short\" saved succesfuly";
              $response_content_lines[] = 'Offers count in file: ' . $offers_count;

              $session->set('offers.xml', $filename);
            }
            else {
              $response_content_lines[] = 'failure';
              $response_content_lines[] = "Cant detect products count in file \"$filename_short\"";
            }

            break;
        }


      } catch (\Exception $e) {
        $response_content_lines[] = 'failure';
        $response_content_lines[] = "Error while loading file \"$filename_short\"";
      }
    }

    if ($response_content_lines) {
      $response->setContent(implode("\n", $response_content_lines));
    }

    if ((!empty($session->get('import.xml')) && !empty($session->get('offers.xml'))) ||
      ($file_dir && file_exists($file_dir . '/import.xml') && file_exists($file_dir . '/offers.xml') &&
        \Drupal::request()->get('filename') == 'offers.xml')) {
      // All information successfuly saved and ready to be bprocessed

        $import_file = $session->get('import.xml') ?: $file_dir . '/import.xml';
        $offers_file = $session->get('offers.xml') ?: $file_dir . '/offers.xml';

      if ($_1c_data_exchange_node = BsDataExchangeNode::load('1c')) {
        $dispatcher = \Drupal::service('event_dispatcher');
        $event = new BsDataExchangeNodeEvent($_1c_data_exchange_node);

        $data = [
          'files' => [
            'import' => $import_file,
            'offers' => $offers_file,
          ]
        ];

        $event->setData($data);

        $dispatcher->dispatch(BsDataExchangeEvents::BS_DATAEXCHANGE_NEW_IMPORT_DATA_RECIVED, $event);
      }

      /** @var QueueFactory $queue_factory */
      $queue_factory = \Drupal::service('queue');
      /** @var QueueInterface $queue */
      $queue = $queue_factory->get('bs_commerceml_import');
      $item = new \stdClass();
      $item->sessionId = $session->getId();
      $item->dataExchangeNodeId = '1c';
      $item->files = [
        'import' => $import_file,
        'offers' => $offers_file,
      ];
      $queue->createItem($item);
    }
  }

}

