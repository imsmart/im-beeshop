<?php
namespace Drupal\bs_commerceml;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Queue\QueueInterface;
use Drupal\bs_data_exchange\Exception\InvalidQueueItemDataExeption;

class Cron implements CronInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The queue plugin manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * Constructs a cron object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switching service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue plugin manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LockBackendInterface $lock, QueueFactory $queue_factory, StateInterface $state, AccountSwitcherInterface $account_switcher, LoggerInterface $logger, QueueWorkerManagerInterface $queue_manager) {
    $this->moduleHandler = $module_handler;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->accountSwitcher = $account_switcher;
    $this->logger = $logger;
    $this->queueManager = $queue_manager;

  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Allow execution to continue even if the request gets cancelled.
    @ignore_user_abort(TRUE);

    $this->logger->notice('CommerceML cron start');

    $this->processQueues();

  }

  /**
   * Processes cron queues.
   */
  protected function processQueues() {

    set_time_limit(240);

    /** @var QueueInterface $queue */
    $queue = $this->queueFactory->get('bs_commerceml_import');

    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('bs_commerceml_import_processor');

    while($item = $queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $e) {
        watchdog_exception('bs_commerceml', $e);
      }
    }

    /** @var QueueInterface $update_products_queue */
    $update_products_queue = $this->queueFactory->get('bs_commerceml_update_products');
    $update_products_queue->garbageCollection();
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('bs_commerceml_products_update');

    while($item = $update_products_queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (InvalidQueueItemDataExeption $e) {
        watchdog_exception('bs_commerceml', $e);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $e) {
        watchdog_exception('bs_commerceml', $e);
      }
    }


    /** @var QueueInterface $update_products_queue */
    $update_products_queue = $this->queueFactory->get('bs_product_resave');
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('bs_product_resave');

    while($item = $update_products_queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (InvalidQueueItemDataExeption $e) {
        watchdog_exception('bs_commerceml', $e);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $e) {
        watchdog_exception('bs_commerceml', $e);
      }
    }

    // TODO: Это надо убрать отсюда в крон движка, когда он будет реализован
    // bs_dexn_action
    /** @var QueueInterface $update_products_queue */
    $update_products_queue = $this->queueFactory->get('bs_dexn_action');
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('bs_dexn_action');

    while($item = $update_products_queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (\Exception $e) {
        watchdog_exception('bs_dexn_action', $e);
      }
    }

    /** @var QueueInterface $update_products_queue */
    $update_products_queue = $this->queueFactory->get('bs_commerceml_create_products');
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('bs_commerceml_products_creator');

    while($item = $update_products_queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (InvalidQueueItemDataExeption $e) {
        watchdog_exception('bs_commerceml', $e);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $e) {
        watchdog_exception('bs_commerceml', $e);
      }
    }
  }
}

