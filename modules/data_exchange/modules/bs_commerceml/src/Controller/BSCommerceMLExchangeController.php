<?php
namespace Drupal\bs_commerceml\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bs_shop\Entity\ShopInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\File\FileSystem;
use Drupal\bs_commerceml\RequestHandler;
use Symfony\Component\Config\Util\XmlUtils;
use Drupal\im_commerceml\CommercemlProtocolHandler;
use Drupal\im_commerceml\importDocCrawler;
use Drupal\im_commerceml\offersDocCrawler;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\bs_data_exchange\Event\BsDataExchangeNodeEvent;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Drupal\bs_data_exchange\Event\BsDataExchangeEvents;

class BSCommerceMLExchangeController extends ControllerBase {

  public function shopExcange(ShopInterface $bs_shop) {

    $request = \Drupal::request();

    $request_handler = new CommercemlProtocolHandler();

    $request_handler->setCallback('catalog', 'import', [$this, 'catalogImportCallback']);

    return $request_handler->handle($request);

  }

  public function catalogImportCallback(CommercemlProtocolHandler $request_handler) {

    $response = $request_handler->getResponse();
    $session = $request_handler->getSession();
    $response_content_lines = [];

    if ($filename = $request_handler->getFullFileName()) {

      $filename_short = basename($filename);
      $file_dir = dirname($filename);

      try {
        $dom = XmlUtils::loadFile($filename);


        switch ($filename_short) {
          case 'import.xml':
            $import_doc = new importDocCrawler($dom);
            if ($products_count = $import_doc->getProductsCount()) {
              $response_content_lines[] = 'success';
              $response_content_lines[] = "File \"$filename_short\" saved succesfuly";
              $response_content_lines[] = 'Products count in file: ' . $products_count;

              $session->set('import.xml', $filename);
            }
            else {
              $response_content_lines[] = 'failure';
              $response_content_lines[] = "Cant detect products count in file \"$filename_short\"";
            }
            break;
          case 'offers.xml':
            $offers_doc = new offersDocCrawler($dom);
            if ($offers_count = $offers_doc->getOffersCount()) {
              $response_content_lines[] = 'success';
              $response_content_lines[] = "File \"$filename_short\" saved succesfuly";
              $response_content_lines[] = 'Offers count in file: ' . $offers_count;

              $session->set('offers.xml', $filename);
            }
            else {
              $response_content_lines[] = 'failure';
              $response_content_lines[] = "Cant detect products count in file \"$filename_short\"";
            }

            break;
        }


      } catch (\Exception $e) {
        $response_content_lines[] = 'failure';
        $response_content_lines[] = "Error while loading file \"$filename_short\"";
      }
    }

    if ($response_content_lines) {
      $response->setContent(implode("\n", $response_content_lines));
    }


    if ((!empty($session->get('import.xml')) && !empty($session->get('offers.xml'))) ||
      ($file_dir && file_exists($file_dir . '/import.xml') && file_exists($file_dir . '/offers.xml'))) {
      // All information successfuly saved and ready to be bprocessed

      if ($_1c_data_exchange_node = BsDataExchangeNode::load('1c')) {
        $dispatcher = \Drupal::service('event_dispatcher');
        $event = new BsDataExchangeNodeEvent($_1c_data_exchange_node);

        $data = [
          'files' => [
            'import' => $session->get('import.xml'),
            'offers' => $session->get('offers.xml'),
          ]
        ];

        $event->setData($data);

        $dispatcher->dispatch(BsDataExchangeEvents::BS_DATAEXCHANGE_NEW_IMPORT_DATA_RECIVED, $event);
      }

      /** @var QueueFactory $queue_factory */
      $queue_factory = \Drupal::service('queue');
      /** @var QueueInterface $queue */
      $queue = $queue_factory->get('bs_commerceml_import');
      $item = new \stdClass();
      $item->sessionId = $session->getId();
      $item->dataExchangeNodeId = '1c';
      $item->files = [
        'import' => $session->get('import.xml'),
        'offers' => $session->get('offers.xml'),
      ];
      $queue->createItem($item);

    }
  }

}

