<?php
namespace Drupal\bs_data_exchange\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_product\Entity\Product;

class BsDataExchangeNodeEntityReferenceForm extends FormBase {

  public function buildForm(array $form, FormStateInterface $form_state, Product $bs_product = NULL) {
    $plugin_manager = \Drupal::service('plugin.manager.field.widget');
    $widget = $plugin_manager->getInstance([
      'field_definition' => $bs_product->get('data_exchange_node_ref')->getFieldDefinition(),
      'form_mode' => 'edit',
      // No need to prepare, defaults have been merged in setComponent().
      'prepare' => FALSE,
      'configuration' => [
        'settings' => [],
        'third_party_settings' => [],
      ]
    ]);
    dpm($bs_product->get('data_exchange_node_ref'));
//     dpm($bs_product->get('data_exchange_node_ref')->getFieldDefinition());
    dpm($widget);
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
      // TODO Auto-generated method stub
    return 'bs-data-exchange-nodes-product-references';
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
      // TODO Auto-generated method stub

  }

//   /**
//    * {@inheritDoc}
//    * @see \Drupal\Core\DependencyInjection\ContainerInjectionInterface::create()
//    */
//   public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
//       // TODO Auto-generated method stub

//   }


}

