<?php
namespace Drupal\bs_data_exchange\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_data_exchange\Plugin\BsDataExchangeProcessorPluginManager;

class BsDataExchangeNodeForm extends EntityForm {

  /**
   * @var \Drupal\bs_data_exchange\BsDataExchangeNodeInterface
   */
  protected $entity;

  /**
   *
   * @var BsDataExchangeProcessorPluginManager
   */
  protected $processorMluginManager;

  public function __construct(BsDataExchangeProcessorPluginManager $processor_plugin_manager) {
    $this->processorMluginManager = $processor_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.bs_data_exchane.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $processor_plugins_manager = \Drupal::service('plugin.manager.bs_data_exchane.processor');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('View name'),
      '#required' => TRUE,
      '#size' => 32,
      '#default_value' => $this->entity->label(),
      '#maxlength' => 255,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => 128,
      '#machine_name' => [
        'exists' => '\Drupal\bs_data_exchange\Entity\BsDataExchangeNode::load',
      ],
      '#required' => TRUE,
      '#description' => $this->t('A unique machine-readable name for this View. It must only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $this->entity->id(),
    ];

//     $processor_plugin_id = $this->entity->getProcessor();

//     $form['processor']['#tree'] = TRUE;

//     $form['processor']['plugin'] = [
//       '#type' => 'select',
//       '#title' => $this->t('Processor'),
//       '#options' => $this->processorMluginManager->getPluginList(),
//       '#default_value' => $processor_plugin_id,
//       '#required' => TRUE,
//       '#empty_option' => $this->t('Select processor'),
//     ];

    $form['exhange_point'] = [
      '#tree' => TRUE,
      '#type' => 'details',
      '#title' => $this->t('Exchange points'),
      '#open' => TRUE,
    ];

    $exchange_points = $this->entity->getExchangePoints();

    foreach ($exchange_points as $exch_point_config) {
      $form['exhange_point'][$exch_point_config['id']] = $this->getExchangePointFormElements($exch_point_config);
    }

    $form['exhange_point'][] = $this->getExchangePointFormElements([]);

    $form['status'] = [
      '#title' => $this->t('Status'),
      '#type' => 'checkbox',
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = $this->entity->save();
    return $result;
  }

  protected function getExchangePointFormElements($exch_point_config) {

    $class = get_class($this);

    $element = [
      '#tree' => TRUE,
      'id' => [
        '#type' => 'machine_name',
        '#maxlength' => 128,
        '#machine_name' => [
          'exists' => [$class, 'exchPointExist'],
        ],
        '#required' => FALSE,
        '#description' => $this->t('A unique machine-readable name for this View. It must only contain lowercase letters, numbers, and underscores.'),
        '#default_value' => !empty($exch_point_config['id']) ? $exch_point_config['id'] : '',
      ],
      'processor_plugin' => [
        '#type' => 'select',
        '#title' => $this->t('Processor'),
        '#options' => $this->processorMluginManager->getPluginList(),
        '#default_value' => !empty($exch_point_config['processor_plugin']) ?
                            $exch_point_config['processor_plugin'] : NULL,
        //'#required' => TRUE,
        '#empty_option' => $this->t('Select processor'),
      ],
    ];

    return $element;
  }

  public function exchPointExist($value, $element, FormStateInterface $form_state) {

    $count = 0;

    foreach ($form_state->getValue('exhange_point', []) as $exch_point) {
      if ($exch_point['id'] !== '' && $exch_point['id'] == $value)
        $count++;
    }

    return $count <= 1 ? FALSE : TRUE;
  }
}

