<?php

namespace Drupal\bs_data_exchange\Event;

final class BsDataExchangeEvents {

  const BS_DATAEXCHANGE_NEW_IMPORT_DATA_RECIVED = 'bs_data_exchange.import.new_data_recived';

}

