<?php
namespace Drupal\bs_data_exchange\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;

class BsDataExchangeNodeEvent extends Event {

  /**
   * @var BsDataExchangeNodeInterface
   */
  protected $dataEchangeNode;

  protected $data;

  /**
   * Constructs a new BsDataExchangeNodeEvent.
   *
   * @param \Drupal\bs_data_exchange\BsDataExchangeNodeInterface  $data_echange_node
   *   The Data exchange node.
   */
  public function __construct(BsDataExchangeNodeInterface $data_echange_node) {
    $this->dataEchangeNode = $data_echange_node;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\bs_data_exchange\BsDataExchangeNodeInterface
   *   Gets the order.
   */
  public function getDataExchangeNode() {
    return $this->dataEchangeNode;
  }

  public function getData() {
    return $this->data;
  }

  public function setData($data) {
    $this->data = $data;
  }
}

