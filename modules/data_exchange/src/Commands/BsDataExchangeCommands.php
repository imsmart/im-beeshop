<?php
namespace Drupal\bs_data_exchange\Commands;

use Drush\Commands\DrushCommands;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Symfony\Component\HttpFoundation\Response;

class BsDataExchangeCommands extends DrushCommands {

  /**
   * Execute DataExchangeNode exchange point operation.
   *
   * @param string $dex_node_id
   *   Data excange node ID.
   *
   * @param string $exch_point
   *   Exchange point ID.
   *
   * @param string $operation
   *   Operation name
   *
   * @command bs-dex:execute-exch-point-operation
   *
   * @usage drush bs-dex:execute-exch-point-operation
   *   Deletes all Solr Field Type and re-installs them from their yml files.
   *
   */
  public function executeDexPointOperation($dex_node_id, $exch_point, $operation) {

    $response = new Response();

    if ($dex_node = BsDataExchangeNode::load($dex_node_id)) {
      if ($processor_plugin = $dex_node->getProcessorPlugin([$exch_point, 'processor_plugin'])) {
        $processor_plugin->doOperation($operation, $dex_node, $exch_point, $response);
      }
      else {
        \Drupal::logger('bs-dex:execute-exch-point-operation')->warning('Processor plugin not found or could not be loaded');
      }
    }
    else {
      \Drupal::logger('bs-dex:execute-exch-point-operation')->warning('Defined Data exchange node not found');
    }
  }

}

