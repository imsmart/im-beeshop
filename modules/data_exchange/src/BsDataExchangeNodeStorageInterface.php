<?php
namespace Drupal\bs_data_exchange;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

interface BsDataExchangeNodeStorageInterface extends ConfigEntityStorageInterface {
}

