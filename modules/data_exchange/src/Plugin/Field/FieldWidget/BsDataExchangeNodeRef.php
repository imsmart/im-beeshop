<?php
namespace Drupal\bs_data_exchange\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_data_exchange\Entity\BsDataExchangeNode;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldFilteredMarkup;

/**
 * Plugin implementation of the 'bs_price_default' widget.
 *
 * @FieldWidget(
 *   id = "bs_data_exchange_node_ref",
 *   label = @Translation("Reference with data exchange nodes"),
 *   field_types = {
 *     "bs_data_exchange_node_ref",
 *     "bs_data_exchange_node_ref_wurl"
 *   }
 * )
 */
class BsDataExchangeNodeRef extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Abstract over the actual field columns, to allow different field types to
   * reuse those widgets.
   *
   * @var string
   */
  protected $column;

  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;

    $property_names = $this->fieldDefinition->getFieldStorageDefinition()->getPropertyNames();
    $this->column = $property_names[0];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'));
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Field\WidgetInterface::formElement()
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $item = $items[$delta];
    $this->required = $element['#required'];
    $this->multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple();

//     $data_exchange_node_storage = $this->entityTypeManager->getStorage('bs_data_exchange_node');

//     $data_exchange_nodes = $data_exchange_node_storage->loadMultiple();

// //     dpm($data_exchange_nodes);

// //     dpm($items);
// //     dpm($item);

    $element['target_id'] = [
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => $item->target_id,
      '#title' => $this->t('Data exchange node'),
    ];

    $element['external_id'] = [
      '#type' => 'textfield',
      '#default_value' => $item->external_id,
    ];

    $element['status'] = [
      '#type' => 'checkbox',
      '#default_value' => $item->status,
      '#title' => $this->t('Enabled'),
    ];

    return $element;
  }


  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      // Limit the settable options for the current user account.
      $options = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getOptionsProvider($this->column, $entity)
      ->getSettableOptions(\Drupal::currentUser());

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      $module_handler = \Drupal::moduleHandler();
      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
      ];
      $module_handler->alter('options_list', $options, $context);

      array_walk_recursive($options, [$this, 'sanitizeLabel']);

      // Options might be nested ("optgroups"). If the widget does not support
      // nested options, flatten the list.
      if (!$this->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }

      $this->options = $options;
    }
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if ($this->multiple) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return t('- None -');
      }
    }
    else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return t('- None -');
      }
      if (!$this->has_value) {
        return t('- Select a value -');
      }
    }
  }

  /**
   * Sanitizes a string label to display as an option.
   *
   * @param string $label
   *   The label to sanitize.
   */
  protected function sanitizeLabel(&$label) {
    // Allow a limited set of HTML tags.
    $label = FieldFilteredMarkup::create($label);
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }



//   /**
//    * {@inheritDoc}
//    * @see \Drupal\Core\Field\WidgetInterface::formMultipleElements()
//    */
//   protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

//     $data_exchange_nodes = BsDataExchangeNode::loadMultiple();
//     $delta = 0;
//     $elements = [];

//     $items_data = [];

//     foreach ($items as $item) {
//       $item_values = $item->getValue();
//       $items_data[$item_values['data_exchange_node']] = $item_values;
//     }

//     foreach ($data_exchange_nodes as $data_exchange_node) {
//       $element['data_exchange_node'] = [
//         '#type' => 'value',
//         '#value' => $data_exchange_node->id(),
//       ];

//       $element['external_id'] = [
//         '#title' => $this->t('External ID for ":data_exchange_node_label"', [':data_exchange_node_label' => $data_exchange_node->label()]),
//         '#type' => 'textfield',
//         '#default_value' => !empty($items_data[$data_exchange_node->id()]) ?
//                             $items_data[$data_exchange_node->id()]['external_id'] :
//                             '',
//       ];

//       $elements[$delta] = $element;

//       $delta++;
//     }

//     return $elements;
//   }

}

