<?php
namespace Drupal\bs_data_exchange\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 * @FieldType(
 *   id = "bs_data_exchange_node_ref",
 *   label = @Translation("Data exchange node references"),
 *   description = @Translation("Reference info for the data exchange node"),
 *   category = @Translation("BeeShop"),
 *   no_ui = TRUE,
 *   default_widget = "bs_data_exchange_node_ref",
 *   list_class = "Drupal\bs_data_exchange\BsDataExchNodeRefFieldItemList",
 * )
 */
class BsDataExchangeNodeRef extends BsDataExchangeNodeRefBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return parent::schema($field_definition);
  }

}

