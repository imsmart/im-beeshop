<?php
namespace Drupal\bs_data_exchange\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 */
abstract class BsDataExchangeNodeRefBase extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'bs_data_exchange_node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['external_id'] = DataDefinition::create('string')
    ->setLabel(t('External ID'))
    ->setRequired(FALSE);

    $properties['status'] = DataDefinition::create('boolean')
    ->setLabel(t('Ref status'))
    ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = parent::schema($field_definition);

    $schema['columns']['external_id'] = [
      'description' => 'ID on external source.',
      'type' => 'varchar',
      'length' => 255,
    ];

    $schema['columns']['status'] = [
      'description' => 'ID on external source.',
      'type' => 'varchar',
      'length' => 255,
    ];

    $schema['columns']['status'] = [
      'description' => 'Flag to control whether this link width data exchange node is enabled for target entity',
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'default' => 1,
    ];

    return $schema;
  }

//   /**
//    * {@inheritdoc}
//    */
//   public static function mainPropertyName() {
//     return 'external_id';
//   }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->external_id === NULL || $this->external_id === '';
  }

}

