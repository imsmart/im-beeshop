<?php
namespace Drupal\bs_data_exchange\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'bs_price' field type.
 *
 * default_formatter = "commerce_price_default",
 *
 * @FieldType(
 *   id = "bs_data_exchange_node_ref_wurl",
 *   label = @Translation("Data exchange node references (with url)"),
 *   description = @Translation("Reference info for the data exchange node (width url)"),
 *   category = @Translation("BeeShop"),
 *   no_ui = TRUE,
 *   default_widget = "bs_data_exchange_node_ref",
 *   list_class = "Drupal\bs_data_exchange\BsDataExchNodeRefFieldItemList",
 * )
 */
class BsDataExchangeNodeWithExternalUrl extends BsDataExchangeNodeRef {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['external_url'] = DataDefinition::create('string')
    ->setLabel(t('External url'))
    ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = parent::schema($field_definition);

    $schema['columns']['external_url'] = [
      'description' => 'Url on external source',
      'type' => 'varchar',
      'length' => 255,
    ];

    return $schema;
  }

}

