<?php
namespace Drupal\bs_data_exchange\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bs_product\ProductsStorageInterface;
use Drupal\im_commerceml\importFilesCrawler;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Database\Query\Select;
use Drupal\bs_data_exchange\BsDataExchangeNodeStorageInterface;

/**
 * A CommerceML import files processor.
 *
 * @QueueWorker(
 *   id = "bs_dexn_action",
 *   title = @Translation("BeeShop Data exchange node action"),
 * )
 */
class BsDexnAction extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * @var BsDataExchangeNodeStorageInterface
   */
  protected $dataExchangeNodeStorage;

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(BsDataExchangeNodeStorageInterface $dx_node_storage) {

    $this->dataExchangeNodeStorage = $dx_node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('bs_data_exchange_node')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data->dxNode) || empty($data->dxEP) || empty($data->actions)) {
      // TODO: Need to log this situation
      return;
    }

    $dex_node = $this->dataExchangeNodeStorage->load($data->dxNode);

    if (!$dex_node) {
      // TODO: Need to log this situation
      return;
    }

    $end_point_plugin = $dex_node->getProcessorPlugin([$data->dxEP, 'processor_plugin']);

    try {
      foreach ($data->actions as $action => $action_args) {
        $end_point_plugin->$action($dex_node, $data->dxEP, $action_args);
      }
    }
    catch (\Exception $e) {

    }


  }

}

