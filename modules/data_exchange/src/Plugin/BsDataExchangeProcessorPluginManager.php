<?php
namespace Drupal\bs_data_exchange\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Utility\NestedArray;

class BsDataExchangeProcessorPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      "Plugin/Beeshop/DataExchange/processor",
      $namespaces,
      $module_handler,
      'Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginInterface',
      'Drupal\bs_data_exchange\Annotation\BsDataExchangeProcessor');

//     $this->defaults += [
//       'parent' => 'parent',
//       'plugin_type' => $type,
//       'register_theme' => TRUE,
//     ];

    $this->alterInfo('bs_data_exchange_processor_plugin');
    $this->setCacheBackend($cache_backend, "beesdhop:data_exchange:processor");
    $this->factory = new DefaultFactory($this->getDiscovery());

  }


  public function getPluginList() {
    $definitions = $this->getDefinitions();

    $plugin_list = [];

    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $plugin['title']->render();
    }
    return $plugin_list;
  }

  public function getPluginsIdsWithDefinition($needed_definition) {

    $definitions = $this->getDefinitions();

    $plugin_id_list = [];

    foreach ($definitions as $plugin_id => $plugin) {

      if (is_scalar($needed_definition) && isset($plugin[$needed_definition])) {
        $plugin_id_list[$plugin_id] = $plugin;
      }
      elseif (is_array($needed_definition) && NestedArray::keyExists($plugin, $needed_definition)) {
        $plugin_id_list[$plugin_id] = $plugin;
      }

    }

    return $plugin_id_list;
  }
}

