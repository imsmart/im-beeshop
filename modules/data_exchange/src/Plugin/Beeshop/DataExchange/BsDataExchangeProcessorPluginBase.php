<?php
namespace Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange;


use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginInterface;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystem;
use Drupal\bs_data_exchange\RefLinksList;
use Drupal\Core\Queue\QueueInterface;

abstract class BsDataExchangeProcessorPluginBase extends PluginBase implements BsDataExchangeProcessorPluginInterface {

  use PluginWithFormsTrait;
  use StringTranslationTrait;

  /**
   *
   * @var RefLinksList
   */
  protected $existingRefLinksList;

  /**
   * @var string
   */
  protected $loggerChanelPrefix = 'bsdex_processor';

  /**
   * @var QueueInterface
   */
  protected $_actionQueue;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct([], $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'file_directory' => 'temporary://beeshop/dxnodes/[bs_data_exchange_node:id]/[bs_data_exchange_point:id]',
      'referenced_entity_type' => 'bs_product',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  public function getConfigurationSetting($setting_name) {
    if (is_string($setting_name)) {
      $setting_name = [$setting_name];
    }
    $configuration = $this->getConfiguration();

    return NestedArray::getValue($configuration, $setting_name);
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    // Merge in defaults.
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
      );
  }

  public function isOperationSupported($operation) {
    return !empty($this->pluginDefinition['operations'][$operation]['callback']);
  }

  public function doOperation($operation, BsDataExchangeNodeInterface $dx_node, $bs_dxnode_exch_point, Response &$response) {

    if ($operation_callback = $this->getOperationCallback($operation)) {
      if (is_array($operation_callback) && $operation_callback[0] == $this) {
        return $this->{$operation_callback[1]}($dx_node, $bs_dxnode_exch_point, $response);
      }

      return call_user_func($operation_callback, $dx_node, $bs_dxnode_exch_point, $response);
    }
    else {
      $response->setStatusCode(405);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function doExchange(BsDataExchangeNodeInterface $bs_data_exchange_node, $bs_dxnode_exch_point, Response $response) {
    $response = new Response();
    $response->setStatusCode(403);
    return $response;
  }

  public function getRefLinks() {
    if ($this->refLinksList) {
      return $this->refLinksList;
    }

    $data_exchange_node = $this->getDataExchangeNode();

    if (!$data_exchange_node) {
      throw new \Exception('Data exchange node can\'t be fetched');
    }

    $refenced_entity_type_id = $this->referencedEntityTypeId();

    if (!$refenced_entity_type_id) {
      throw new \Exception('Referenced entity type id not defined');
    }

    $exting_offer_ids = $data_exchange_node->getAllReferencedIds($refenced_entity_type_id);
  }

  protected function getOperationCallback($operation) {
    if (!$this->isOperationSupported($operation)) {
      return FALSE;
    }

    $callback = $this->pluginDefinition['operations'][$operation]['callback'];

    if (is_string($callback) && substr($callback, 0, 2) == '::') {
      $callback = [$this, substr($callback, 2)];
    }

    return is_callable($callback) ? $callback : FALSE;
  }

  protected function getLoggerChanel() {

    $chanel_parts = [
      $this->loggerChanelPrefix,
    ];

    if ($dex_node = $this->getDataExchangeNode()) {
      $chanel_parts[] = $dex_node->id();
    }

    if ($point_id = $this->getConfigurationSetting(['bs_data_exchange_point', 'id'])) {
      $chanel_parts[] = $point_id;
    }

    return implode(':', $chanel_parts);
  }

  /**
   *
   * @return FileSystem
   */
  protected function fileSystem() {
    return \Drupal::service('file_system');
  }

  /**
   * @return \Psr\Log\LoggerInterface
   */
  protected function logger() {
    return \Drupal::logger($this->getLoggerChanel());
  }

  /**
   * @return \Drupal\bs_data_exchange\BsDataExchangeNodeInterface | NULL
   */
  protected function getDataExchangeNode() {
    return $this->getConfigurationSetting('bs_data_exchange_node');
  }

  protected function referencedEntityTypeId() {
    return $this->getConfigurationSetting('referenced_entity_type');
  }

  /**
   *
   * @param string $entity_type_id
   */
  public function setReferencedEntityTypeId($entity_type_id) {
    $this->configuration['referenced_entity_type'] = $entity_type_id;
  }

  protected function getStateKeySuffixParts() {
    return [
      $this->getDataExchangeNode() ? $this->getDataExchangeNode()->id() : '-',
      $this->getConfigurationSetting(['bs_data_exchange_point', 'id']) ? $this->getConfigurationSetting(['bs_data_exchange_point', 'id']) : '-',
    ];
  }

  protected function getStateKey($prefix_parts) {
    if (is_string($prefix_parts)) {
      $prefix_parts = [$prefix_parts];
    }

    $key_parts = array_merge($prefix_parts, $this->getStateKeySuffixParts());
    return implode(':', $key_parts);
  }

  public function entityResave(BsDataExchangeNodeInterface $dx_node, $dx_node_end_point, array $action_args = []) {

    if (empty($action_args['entity_type']) || empty($action_args['id'])) {
      $this->logger()->warning('Not anouth action args for "entityResave" action');
      return;
    }

    if ($entity = \Drupal::entityTypeManager()->getStorage($action_args['entity_type'])->load($action_args['id'])) {
      $entity->save();
    }
  }

  public function getIdsForAdd($external_ids = []) {
    return $this->existingLinks()->rightDiffExternalIds($external_ids);
  }

  public function existingLinks() {
    if ($this->existingRefLinksList) {
      return $this->existingRefLinksList;
    }

    $this->existingRefLinksList = $this->getDataExchangeNode()->getAllReferencedIds($this->getConfigurationSetting('referenced_entity_type'));

    return $this->existingRefLinksList;
  }


  /**
   * @return \Drupal\Core\Queue\QueueInterface
   */
  protected function actionQueue() {

    if ($this->_actionQueue) {
      return $this->_actionQueue;
    }

    $queue_factory = \Drupal::service('queue');
    $this->_actionQueue = $queue_factory->get('bs_dexn_action');

    return $this->_actionQueue;
  }

}

