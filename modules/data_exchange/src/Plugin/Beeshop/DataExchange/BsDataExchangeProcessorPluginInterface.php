<?php
namespace Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Plugin\ConfigurablePluginInterface;

interface BsDataExchangeProcessorPluginInterface extends PluginInspectionInterface, ConfigurablePluginInterface {

  /**
   *
   * @param BsDataExchangeNodeInterface $bs_data_exchange_node
   * @param string $bs_dxnode_exch_point
   */
  public function doExchange(BsDataExchangeNodeInterface $bs_data_exchange_node, $bs_dxnode_exch_point, Response $response);

}

