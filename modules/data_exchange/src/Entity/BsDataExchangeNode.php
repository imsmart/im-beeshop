<?php
namespace Drupal\bs_data_exchange\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\bs_product\Entity\Product;
use Drupal\Core\Config\Entity\Query\Query;
use Drupal\bs_price\Entity\PriceType;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\bs_data_exchange\RefLinksList;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines a BeeShop Data exchange node configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_data_exchange_node",
 *   label = @Translation("Data exchange node", context = "Data exchange node entity type"),
 *   admin_permission = "administer beeshop dataexchange nodes",
 *   handlers = {
 *     "list_builder" = "Drupal\bs_data_exchange\BsDataExchangeNodeListBuilder",
 *     "storage" = "Drupal\bs_data_exchange\BsDataExchangeNodeStorage",
 *     "exchange" = "Drupal\bs_data_exchange\Controller\BsDataExchangeNodeController",
 *     "form" = {
 *       "default" = "Drupal\bs_data_exchange\Form\BsDataExchangeNodeForm",
 *       "add" = "Drupal\bs_data_exchange\Form\BsDataExchangeNodeForm",
 *       "edit" = "Drupal\bs_data_exchange\Form\BsDataExchangeNodeForm",
 *       "delete" = "Drupal\beeshop\Form\BeeShopBundleEntityDeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_data_exchange\Routing\BsDataExchangeNodeRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "module",
 *     "description",
 *     "processor",
 *     "exhange_point",
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/data-exchange/data-exchange-nodes/add",
 *     "edit-form" = "/admin/beeshop/data-exchange/data-exchange-nodes/{bs_data_exchange_node}/edit",
 *     "delete-form" = "/admin/beeshop/data-exchange/data-exchange-nodes/{bs_data_exchange_node}/delete",
 *     "collection" = "/admin/beeshop/data-exchange/data-exchange-nodes",
 *   }
 * )
 */
class BsDataExchangeNode extends ConfigEntityBase implements BsDataExchangeNodeInterface {

  protected $linkedPriceTypes = NULL;

  protected $referencedEntities = [];

  public function getExchangePoints() {
    return !empty($this->exhange_point) ? $this->exhange_point : [];
  }

  public function getUsedProcessorPlugins() {
    $result = [];

    if (!empty($this->exhange_point)) {
      foreach ($this->exhange_point as $point_id => $exch_point) {
        if ($point_processor_plugin = $this->getProcessorPlugin([$point_id, 'processor_plugin'])) {
          $result[$point_id] = $point_processor_plugin;
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorPlugin(array $path_to_exh_point_plugin) {

    $exist = FALSE;

    $exch_points = $this->getExchangePoints();

    if ($plugin_id = NestedArray::getValue($exch_points, $path_to_exh_point_plugin, $exist)) {
      $point_id = array_shift($path_to_exh_point_plugin);
      if ($processor_instance = $this->processorPluginManager()->createInstance($plugin_id, [
        'bs_data_exchange_node' => $this,
        'bs_data_exchange_point' => [
          'id' => $point_id,
        ],
      ])) {

        return $processor_instance;
      }
    }

    return FALSE;
  }

  public function calculateDependencies() {
    parent::calculateDependencies();

    foreach ($this->getUsedProcessorPlugins() as $processor_plugin) {
      $this->calculatePluginDependencies($processor_plugin);
    }

    return $this;
  }

  public function getEntityExtarnalID(EntityInterface $entity) {

    if ($entity instanceof ConfigEntityInterface) {
      $ref_settings = $entity->getThirdPartySetting('bs_data_exchange', $this->id(), ['external_id' => NULL]);
      return $ref_settings['external_id'];
    }


    if ($entity instanceof ContentEntityInterface) {

      if ($data_exchange_node_ref_field_name = $entity->getEntityType()->getKey('data_exchange_node_ref')) {
        if ($values = $entity->get($data_exchange_node_ref_field_name)->getValue()) {
          foreach ($values as $item_value) {
            if ($item_value['target_id'] == $this->id()) {
              return $item_value['external_id'];
            }
          }
        }
      }

    }

    return FALSE;
  }

  public function loadLinkedEntityByExternalID($entity_type_id, $external_id) {

    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($entity_type_id);

//     $query = \Drupal::service('entity.query')->get($entity_type_id);

//     $query->condition('data_exchange_node_ref.data_exchange_node', $this->id())
//     ->condition('data_exchange_node_ref.external_id', $external_id, '=');

//     $ids = $query->execute();

//     if ($ids) {
//       $entity = $entity_storage->load(reset($ids));
//       return $entity;
//     }

    $prop_values = [
      'data_exchange_node_ref.target_id' => $this->id(),
      'data_exchange_node_ref.external_id' => $external_id,
    ];

    if ($entities = $entity_storage->loadByProperties($prop_values)) {
      return reset($entities);
    }

    return FALSE;
  }

  public function getReferencedEntities($entity_type_id) {

    $entities = [];

    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);

    if ($data_exchange_node_ref_field_name = $entity_type->getKey('data_exchange_node_ref')) {
      $entity_prop_values = [
        "${data_exchange_node_ref_field_name}.target_id" => $this->id(),
      ];

      $entity_storage = $this->entityTypeManager()->getStorage($entity_type_id);

      $entities = $entity_storage->loadByProperties($entity_prop_values);
      $entities_by_ext_id = [];

      foreach ($entities as $entity) {
        $refs = $entity->get($data_exchange_node_ref_field_name);

        foreach ($refs as $ref_item) {
          if ($ref_item->target_id == $this->id()) {
            $entities_by_ext_id[$ref_item->external_id] = $entity;
            break;
          }
        }
      }

      return $entities_by_ext_id;
    }

    return [];
  }

  public function getReferencedEntityByExternalID($entity_type_id, $external_id) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);

    if (!$data_exchange_node_ref_field_name = $entity_type->getKey('data_exchange_node_ref')) {
      return NULL;
    }

    $entity_prop_values = [
      "${data_exchange_node_ref_field_name}.target_id" => $this->id(),
      "${data_exchange_node_ref_field_name}.external_id" => $external_id,
      ];

    $entity_storage = $this->entityTypeManager()->getStorage($entity_type_id);

    $entities = $entity_storage->loadByProperties($entity_prop_values);

    return reset($entities);
  }

  public function getAllReferencedIds($entity_type_id, array $conditions = []) {
    return self::getAllReferencedIdsByNode($this->id(), $entity_type_id, $conditions);
  }

  public static function getAllReferencedIdsByNode($dx_node_id, $entity_type_id, array $conditions = []) {
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $data_exchange_node_ref_field_name = $entity_type->getKey('data_exchange_node_ref');

    if (!$data_exchange_node_ref_field_name) {
      return NULL;
    }

    /**
     * @var \Drupal\Core\Field\BaseFieldDefinition[] $definitions
     */
    $definitions = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);

    if (!isset($definitions[$data_exchange_node_ref_field_name])) {
      throw new \Exception('Data exchange node reference field storage not found');
    }

    $table_mapping = new DefaultTableMapping($entity_type, $definitions);

    $list = new RefLinksList($definitions[$data_exchange_node_ref_field_name]->getType(), $table_mapping->getColumnNames($data_exchange_node_ref_field_name));

    if ($table_mapping->requiresDedicatedTableStorage($definitions[$data_exchange_node_ref_field_name])) {
      $table = $table_mapping->getDedicatedDataTableName($definitions[$data_exchange_node_ref_field_name]);

      if (!$table) {
        return NULL;
      }

      $select = \Drupal::database()->select($table, 't');
      $select->fields('t')
        ->condition('data_exchange_node_ref_target_id', $dx_node_id, '=')
        ->condition('deleted', 0);

      if ($conditions) {
        foreach ($conditions as $field => $condition_args) {
          $condition_args += [
            'value' => NULL,
            'operator' => '=',
          ];

          if ($condition_args['value']) {
            $select->condition($field, $condition_args['value'], $condition_args['operator']);
          }
        }
      }

      $results = $select->execute();

      while ($row = $results->fetchAssoc()) {
        $list->appendItem($row);
      }

      return $list;

    }

    // TODO: Need add get results from shared table
    return NULL;

  }

//   public function getLinkedEntitiesIds($entity_type_id, $external_ids = []) {
//     $query = \Drupal::service('entity.query')->get($entity_type_id);
//     $query->condition('data_exchange_node_ref.data_exchange_node', $this->id());

//   }

  public function getLinkedPriceTypes() {

    if (!empty($this->linkedPriceTypes) || is_array($this->linkedPriceTypes))
      return $this->linkedPriceTypes;

    /**
     * @var Query $price_types_query
     */
    $price_types_query = \Drupal::service('entity.query')->get('bs_price_type');
    $price_types_query->exists('third_party_settings.bs_data_exchange.' . $this->id() . '.external_id');

    $result = $price_types_query->execute();

    $linked_price_types = [];

    if ($price_types = PriceType::loadMultiple(array_keys($result))) {
      foreach ($price_types as $price_type_id => $price_type) {

        $linked_price_types[$price_type->getThirdPartySetting('bs_data_exchange', $this->id())['external_id']] = $price_type->id();
      }
    }

    $this->linkedPriceTypes = $linked_price_types;
    return $this->linkedPriceTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!empty($this->exhange_point)) {
      foreach ($this->exhange_point as $key => $exch_point_data) {
        if (empty($exch_point_data['id'])) {
          unset($this->exhange_point[$key]);
          continue;
        }

        if (is_numeric($key)) {
          $this->exhange_point[$exch_point_data['id']] = $exch_point_data;
          unset($this->exhange_point[$key]);
        }
      }
    }
  }

  public function setEntityReference(EntityInterface $entity, $values) {

    if ($entity instanceof ContentEntityBase) {
      $entity_type = $entity->getEntityType();

      if (($data_exchange_node_ref_field_name = $entity_type->getKey('data_exchange_node_ref')) &&
        $entity->hasField($data_exchange_node_ref_field_name)) {
        $entity_ds_refs = $entity->get($data_exchange_node_ref_field_name);

        return $entity_ds_refs->setRef($this->id(), $values);

      }

    }

  }

  /**
   * @return \Drupal\bs_data_exchange\Plugin\BsDataExchangeProcessorPluginManager
   */
  protected function processorPluginManager() {
    return \Drupal::service('plugin.manager.bs_data_exchane.processor');
  }

}

