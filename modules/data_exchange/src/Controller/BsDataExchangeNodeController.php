<?php
namespace Drupal\bs_data_exchange\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;
use Symfony\Component\HttpFoundation\Response;

class BsDataExchangeNodeController extends ControllerBase {

  public function processExchangeRequest(BsDataExchangeNodeInterface $bs_data_exchange_node, $bs_dxnode_exch_point) {
    $response = new Response();

    if ($processor_plugin = $bs_data_exchange_node->getProcessorPlugin([$bs_dxnode_exch_point, 'processor_plugin'])) {
      if ($plugin_response = $processor_plugin->doExchange($bs_data_exchange_node, $bs_dxnode_exch_point, $response)) {
        return $plugin_response;
      }
    }


    return $response;
  }


  public function pluginOperation(BsDataExchangeNodeInterface $bs_data_exchange_node, $bs_dxnode_exch_point, $operation) {

    $response = new Response();

    if ($processor_plugin = $bs_data_exchange_node->getProcessorPlugin([$bs_dxnode_exch_point, 'processor_plugin'])) {
      $processor_plugin->doOperation($operation, $bs_data_exchange_node, $bs_dxnode_exch_point, $response);
    }

//     dpm($response);

    return $response;
  }

}

