<?php

namespace Drupal\bs_data_exchange\Annotation;

use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for BeeShop data excange processor plugins.
 *
 * @see \Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginBase
 *
 * @Annotation
 */
class BsDataExchangeProcessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title used in the views UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title = '';

  /**
   * (optional) The short title used in the views UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $short_title = '';

  /**
   * The administrative name of the display.
   *
   * The name is displayed on the Views overview and also used as default name
   * for new displays.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admin = '';

  /**
   * A short help string; this is displayed in the views UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $help = '';

  /**
   * Does the display plugin registers routes to the route.
   *
   * @var bool
   */
  public $uses_route;

  /**
   * Does the data exchange processor plugin support price types.
   *
   * @var bool
   */
  public $support_price_types;

  /**
   * List of supported configuration entity types.
   *
   * @var array (optional)
   */
  public $supported_config_entities;

  /**
   * The category under which the field type should be listed in the UI.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $category = '';

}
