<?php

namespace Drupal\bs_data_exchange\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;
use Drupal\bs_data_exchange\Plugin\BsDataExchangeProcessorPluginManager;
use Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginInterface;
use Symfony\Component\Routing\Route;

// TODO: Need to refactor this
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    //dpm('alterRoutes');

//     foreach ($this->getViewsDisplayIDsWithRoute() as $pair) {
//       list($view_id, $display_id) = explode('.', $pair);
//       $view = $this->viewStorage->load($view_id);
//       // @todo This should have an executable factory injected.
//       if (($view = $view->getExecutable()) && $view instanceof ViewExecutable) {
//         if ($view->setDisplay($display_id) && $display = $view->displayHandlers->get($display_id)) {
//           if ($display instanceof DisplayRouterInterface) {
//             // If the display returns TRUE a route item was found, so it does not
//             // have to be added.
//             $view_route_names = $display->alterRoutes($collection);
//             $this->viewRouteNames = $view_route_names + $this->viewRouteNames;
//             foreach ($view_route_names as $id_display => $route_name) {
//               $view_route_name = $this->viewsDisplayPairs[$id_display];
//               unset($this->viewsDisplayPairs[$id_display]);
//               $collection->remove("views.$view_route_name");
//             }
//           }
//         }
//         $view->destroy();
//       }
//     }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::FINISHED] = ['routeRebuildFinished'];
    // Ensure to run after the entity resolver subscriber
    // @see \Drupal\Core\EventSubscriber\EntityRouteAlterSubscriber
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -175];

    return $events;
  }

  /**
   * Stores the new route names after they have been rebuilt.
   *
   * Callback for the RoutingEvents::FINISHED event.
   *
   * @see \Drupal\views\EventSubscriber::getSubscribedEvents()
   */
  public function routeRebuildFinished() {
//     $this->reset();
//     $this->state->set('views.view_route_names', $this->viewRouteNames);

    //dpm('routeRebuildFinished');
  }

  /**
   * Returns a set of route objects.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   A route collection.
   */
  public function routes() {

    $collection = new RouteCollection();

    $nodes = $this->getApplicableDXNodes();

    if (!$nodes) {
      return $collection;
    }

    foreach ($this->getApplicableDXNodes() as $dx_node) {
      $dx_node_id = $dx_node->id();

      foreach ($dx_node->getExchangePoints() as $dxn_exch_point) {

        /**
         * @var BsDataExchangeProcessorPluginInterface $exch_point_plugin
         */
        $exch_point_plugin = $this->processorPluginManager()->createInstance($dxn_exch_point['processor_plugin'], []);

        $processor_definition = $exch_point_plugin->getPluginDefinition();
        $plugin_id = $exch_point_plugin->getPluginId();

        $route_name = "entity.bs_data_exchange_node.$dx_node_id.$plugin_id.do_exchange";
        $route = new Route('/beeshop/data-exchange/{bs_data_exchange_node}/{bs_dxnode_exch_point}');

        $route
        ->addDefaults([
          '_controller' => "Drupal\bs_data_exchange\Controller\BsDataExchangeNodeController::processExchangeRequest",
        ])
        // TODO: Need acces to exch node plugin operation
        //->setRequirement('_entity_access', "bs_data_exchange_node.exchange")
        ->setRequirement('_permission', 'access content')
        ->setOption('parameters', [
          'bs_data_exchange_node' => ['type' => 'entity:bs_data_exchange_node'],
          'bs_dxnode_exch_point' => ['type' => 'string'],
        ]);

        $collection->add($route_name, $route);

        if (!empty($processor_definition['operations'])) {

          $route_name = "entity.bs_data_exchange_node.$dx_node_id.$plugin_id.operation";
          $route = new Route('/beeshop/data-exchange/{bs_data_exchange_node}/{bs_dxnode_exch_point}/{operation}');

          $route
          ->addDefaults([
            '_controller' => "Drupal\bs_data_exchange\Controller\BsDataExchangeNodeController::pluginOperation",
          ])
          // TODO: Need acces to exch node plugin operation
//           ->setRequirement('_entity_access', "bs_data_exchange_node.exchange")
          ->setRequirement('_permission', 'access content')
          ->setOption('parameters', [
            'bs_data_exchange_node' => ['type' => 'entity:bs_data_exchange_node'],
            'bs_dxnode_exch_point' => ['type' => 'string'],
            'operation' => ['type' => 'string'],
          ]);

          $collection->add($route_name, $route);


          foreach ($processor_definition['operations'] as $operation_name => $operation) {
            if (!empty($operation['route_config'])) {

              $route_name = "entity.bs_data_exchange_node.$dx_node_id.$plugin_id.operation.{$operation_name}";
              $route = new Route("/beeshop/data-exchange/{bs_data_exchange_node}/{bs_dxnode_exch_point}/{$operation_name}");

              $route
              ->addDefaults([
                '_controller' => "Drupal\bs_data_exchange\Controller\BsDataExchangeNodeController::pluginOperation",
                'operation' => $operation_name,
              ])
              // TODO: Need acces to exch node plugin operation
              //           ->setRequirement('_entity_access', "bs_data_exchange_node.exchange")
              ->setRequirement('_permission', 'access content')
              ->setOption('parameters', [
                'bs_data_exchange_node' => ['type' => 'entity:bs_data_exchange_node'],
                'bs_dxnode_exch_point' => ['type' => 'string'],
              ]);


              foreach ($operation['route_config'] as $conf_key => $conf_data) {
                switch ($conf_key) {
                  case 'options':
                    $options = $route->getOptions();
                    $options = array_merge_recursive($conf_data, $options);
                    $route->setOptions($options);
                    break;
                  case 'requirements':
                    $requirements = $route->getRequirements();
                    $requirements = array_merge_recursive($conf_data, $requirements);
                    $route->setRequirements($requirements);
                    break;
                }
              }

              $collection->add($route_name, $route);

            }
          }

        }

      }

    }

//     dpm($collection);

    return $collection;
  }

  /**
   * Returns all data exchange nodes with extra operations.
   *
   * @see \Drupal\views\Views::getApplicableViews()
   */
  protected function getApplicableDXNodes() {

    if ($processors_with_operations = $this->processorPluginManager()->getPluginsIdsWithDefinition('operations')) {
      $processors_with_operations_ids = array_keys($processors_with_operations);

      $entity_ids = \Drupal::entityQuery('bs_data_exchange_node')
      ->condition('status', TRUE)
      ->condition("exhange_point.*.processor_plugin", $processors_with_operations_ids, 'IN')
      ->execute();

      $result = [];
      foreach (\Drupal::entityTypeManager()->getStorage('bs_data_exchange_node')->loadMultiple($entity_ids) as $dx_node) {
        $result[$dx_node->id()] = $dx_node;
      }

      return $result;
    }

//     foreach ($manager->getDefinitions() as $definition) {

//     }

    //return Views::getApplicableViews('uses_route');
  }


  /**
   *
   * @return BsDataExchangeProcessorPluginManager
   */
  protected function processorPluginManager() {
    return \Drupal::service('plugin.manager.bs_data_exchane.processor');
  }
}

