<?php
namespace Drupal\bs_data_exchange;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class BsDataExchangeNodeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'name' => [
        'data' => $this->t('Name'),
      ],
      'machine_name' => [
        'data' => $this->t('Machine name'),
      ],
      'description' => [
        'data' => $this->t('Description'),
      ],
      'processor' => [
        'data' => $this->t('Processor'),
      ],
      'operations' => [
        'data' => $this->t('Operations'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row = parent::buildRow($entity);

    $row = [
      'name' => $entity->label(),
      'machine_name' => $entity->id(),
      'description' => $entity->description,
      'processor' => '-',//$entity->getProcessor(),
    ];

    $row = $row + parent::buildRow($entity);

    return $row;
  }

}

