<?php
namespace Drupal\bs_data_exchange;

use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\Core\TypedData\ListDataDefinition;

class RefLinksList extends ItemList implements RefLinksListInterface {

  protected $columnNames = [];

  /**
   *
   */
  public function __construct($field_type, $column_names = []) {
    $definition = ListDataDefinition::create('field_item:' . $field_type);
    $this->columnNames = $column_names;
    parent::__construct($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function appendItem($value = NULL) {
    if (is_array($value) && $this->columnNames) {
      foreach ($this->columnNames as $property_name => $column_name) {
        if (isset($value[$column_name])) {
          $value[$property_name] = $value[$column_name];
          unset($value[$column_name]);
        }
      }
    }

    return parent::appendItem($value);
  }

  public function rightDiffExternalIds(array $ids_list) {
    $ids = [];
    foreach ($this->list as $item) {
      $ids[] = $item->external_id;
    }

    return array_diff($ids_list, $ids);
  }

  public function leftDiffExternalIds(array $ids_list) {
    $ids = [];
    foreach ($this->list as $item) {
      $ids[] = $item->external_id;
    }

    return array_diff($ids, $ids_list);
  }
}

