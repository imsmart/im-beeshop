<?php
namespace Drupal\bs_data_exchange;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface BsDataExchangeNodeInterface extends ConfigEntityInterface {

  public function getExchangePoints();

  /**
   * @param array $path_to_exh_point_plugin
   * @return \Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginInterface |boolean
   */
  public function getProcessorPlugin(array $path_to_exh_point_plugin);

  /**
   *
   * @return \Drupal\bs_data_exchange\Plugin\Beeshop\DataExchange\BsDataExchangeProcessorPluginInterface[]
   */
  public function getUsedProcessorPlugins();

  /**
   * @param string $entity_type_id
   * @param array $conditions
   *
   * @return RefLinksList
   */
  public function getAllReferencedIds($entity_type_id, array $conditions);
}

