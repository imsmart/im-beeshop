<?php
namespace Drupal\bs_data_exchange\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class BsDataExchangeNodeRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    return $collection;
  }

  /**
   * Gets the canonical route for data exchenge node. This route will be used for
   * get or output data for exchange
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('canonical') && $entity_type->hasHandlerClass('exchange')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('canonical'));
      $route
      ->addDefaults([
        '_controller' => "Drupal\bs_data_exchange\Controller\BsDataExchangeNodeController::processExchangeRequest",
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.exchange")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

        return $route;
    }
  }
}

