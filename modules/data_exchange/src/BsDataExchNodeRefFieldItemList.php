<?php
namespace Drupal\bs_data_exchange;

use Drupal\Core\Field\EntityReferenceFieldItemList;

class BsDataExchNodeRefFieldItemList extends EntityReferenceFieldItemList {

  /**
   * @param unknown $target_id
   * @param unknown $value
   *
   * @return @return \Drupal\Core\TypedData\TypedDataInterface
   *   The item that was appended or updated.
   */
  public function setRef($target_id, $value) {
    foreach ($this->list as $delta => $item) {
      if ($item->target_id == $target_id) {
        $item->setValue($value);
        return $item;
      }
    }

    $value['target_id'] = $target_id;

    return $this->appendItem($value);
  }


  public function hasRefToDxNode($dx_node_id_to_check) {
    foreach ($this->list as $item) {
      if ($item->target_id == $dx_node_id_to_check) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function getItemByDxNodeId($dx_node_id) {
    foreach ($this->list as $item) {
      if ($item->target_id == $dx_node_id) {
        return $item;
      }
    }

    return FALSE;
  }

}

