<?php

/**
 * @file
 * Builds placeholder replacement tokens system-wide data.
 *
 * This file handles tokens for the global 'site' and 'date' tokens.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\bs_data_exchange\BsDataExchangeNodeInterface;

/**
 * Implements hook_token_info().
 */
function bs_data_exchange_token_info() {
  $types['bs_data_exchange_node'] = [
    'name' => t("BeeShop Data exchange node"),
    'description' => t("Tokens for BeeShop dataexchange node"),
  ];

  $types['bs_data_exchange_point'] = [
    'name' => t("BeeShop Data exchange node exchange point"),
    'description' => t("Tokens for BeeShop Data exchange node exchange point"),
  ];

  $dx_node['id'] = [
    'name' => t("Id"),
    'description' => t("Data exchange node ID."),
  ];

  $dx_node_exch_point['id'] = [
    'name' => t("Id"),
    'description' => t("Data exchange node exchange point ID."),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'bs_data_exchange_node' => $dx_node,
      'bs_data_exchange_point' => $dx_node_exch_point,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function bs_data_exchange_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'bs_data_exchange_node' && !empty($data['bs_data_exchange_node'])) {
    /**
     * @var BsDataExchangeNodeInterface $bsdex_node
     */
    $bsdex_node = $data['bs_data_exchange_node'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $bsdex_node->id();
          break;

      }
    }
  }

  elseif ($type == 'bs_data_exchange_point' && !empty($data['bs_data_exchange_point'])) {
    $data_exch_point = $data['bs_data_exchange_point'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $data_exch_point['id'];
          break;
      }
    }

  }

  return $replacements;
}
