<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_tokens().
 */
function bs_payment_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'bs_order' && !empty($data['bs_order'])) {

    /** @var OrderInterface $order */
    $order = $data['bs_order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'total_payded_amount':
          if ($order->hasField('total_payded_amount') &&
          ($total_payded_amount = $order->get('total_payded_amount')->first())) {
            $replacements[$original] = $total_payded_amount->toPrice()->__toString();
          }
          break;
      }
    }
  }

  return $replacements;
}
