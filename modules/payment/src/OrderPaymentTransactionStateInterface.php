<?php

namespace Drupal\bs_payment;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for price type.
 */
interface OrderPaymentTransactionStateInterface extends ConfigEntityInterface {


}