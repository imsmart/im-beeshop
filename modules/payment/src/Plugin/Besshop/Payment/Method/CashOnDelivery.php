<?php

namespace Drupal\bs_payment\Plugin\Besshop\Payment\Method;

use Drupal\bs_payment\PaymentMethodPluginBase;

/**
 *
 * @PaymentMethod(
 *   id = "cash_on_delivery",
 *   label = @Translation("Cash on delivery"),
 *   category = "Cash",
 *   onlinePayment = false,
 * )
 */
class CashOnDelivery extends PaymentMethodPluginBase {
}

