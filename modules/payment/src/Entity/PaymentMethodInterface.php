<?php
namespace Drupal\bs_payment\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface PaymentMethodInterface extends ContentEntityInterface {

  /**
   * Returns an array of applicable condition configurations.
   *
   * @return array
   *   An array of applicable condition configuration keyed by the condition ID.
   */
  public function getApplicabe();

}

