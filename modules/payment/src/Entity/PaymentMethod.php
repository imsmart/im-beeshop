<?php

namespace Drupal\bs_payment\Entity;

use Drupal\bs_payment\Entity\PaymentMethodInterface;
use Drupal\bs_shipping\ShippingMethodPluginCollection;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines a Shipping method entity class.
 *
 * @ContentEntityType(
 *   id = "bs_payment_method",
 *   label = @Translation("Payment method"),
 *   label_singular = @Translation("payment method"),
 *   label_plural = @Translation("payment methods"),
 *   label_count = @PluralTranslation(
 *     singular = "@count payment method",
 *     plural = "@count payment methods",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bs_payment\PaymentMethodListBulder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\bs_payment\Form\PaymentMethodForm",
 *       "add" = "Drupal\bs_payment\Form\PaymentMethodForm",
 *       "edit" = "Drupal\bs_payment\Form\PaymentMethodForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\bs_payment\Routing\PaymentMethodRouteProvider",
 *     },
 *   },
 *   base_table = "bs_payment_method",
 *   data_table = "bs_payment_method_field_data",
 *   admin_permission = "administer bs_payment_method",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "pm_id",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/beeshop/config/payment/payment_methods/add",
 *     "edit-form" = "/admin/beeshop/config/payment/payment_methods/{bs_payment_method}",
 *     "delete-form" = "/admin/beeshop/config/payment/payment_methods/{bs_payment_method}/delete",
 *     "enable" = "/admin/beeshop/config/payment/payment_methods/{bs_payment_method}/enable",
 *     "disable" = "/admin/beeshop/config/payment/payment_methods/{bs_payment_method}/disable",
 *     "collection" =  "/admin/beeshop/config/payment/payment_methods",
 *     "collection-by-shop" = "/p",
 *   },
 *   field_ui_base_route = "entity.bs_payment_method.collection",
 * )
 */
class PaymentMethod extends ContentEntityBase implements PaymentMethodInterface {

  /**
   * The ID of the shipping method.
   *
   * @var string
   */
  protected $id;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\bs_shipping\ShippingMethodPluginCollection
   */
  protected $pluginCollection;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = [];

  public function getApplicabe() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {

    if ($this->get('plugin')->isEmpty()) {
      return NULL;
    }

    return $this->get('plugin')->first()->getPluginInstance();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['shops'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Shops'))
    ->setDescription(t('The stores for which the shipping method is valid.'))
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setRequired(TRUE)
    ->setSetting('target_type', 'bs_shop')
    ->setSetting('handler', 'default')
    ->setDisplayConfigurable('form', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The shipping method name.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['plugin'] = BaseFieldDefinition::create('plugin_field_item')
    ->setSetting('plugin_manager_name', 'plugin.manager.bs_payment_method')
    ->setLabel(t('Plugin'))
    ->setCardinality(1)
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE);
//     ->setDisplayOptions('form', [
//       'type' => 'commerce_plugin_radios',
//       'weight' => 1,
//     ]);

    $fields['weight'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Weight'))
    ->setDescription(t('The weight of this shipping method in relation to others.'))
    ->setDefaultValue(0)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'integer',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'hidden',
    ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Enabled'))
    ->setDescription(t('Whether the shipping method is enabled.'))
    ->setDefaultValue(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => TRUE,
      ],
      'weight' => 20,
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * Helper callback for uasort() to sort configuration entities by weight and label.
   */
  public static function sort(EntityInterface $a, EntityInterface $b) {
    $a_weight = isset($a->weight->value) ? $a->weight->value : 0;
    $b_weight = isset($b->weight->value) ? $b->weight->value : 0;
    if ($a_weight == $b_weight) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($operation == 'view label') {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

}

