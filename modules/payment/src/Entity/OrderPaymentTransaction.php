<?php
namespace Drupal\bs_payment\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\bs_price\Price;
use Drupal\bs_payment\PaymentMethodManagerInterface;
use Drupal\bs_payment\PaymentMethodPluginInterface;


/**
 * Defines the order entity class.
 *
 * @ContentEntityType(
 *   id = "bs_order_payment_transaction",
 *   label = @Translation("Order payment transaction"),
 *   label_collection = @Translation("Orders payment transactions"),
 *   label_singular = @Translation("order payment transaction"),
 *   label_plural = @Translation("payment transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order payment transaction",
 *     plural = "@count orders payment transactions",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\bs_payment\PaymentTransactionListBuilder",
 *     "route_provider" = {
 *       "default" = "Drupal\bs_payment\Routing\PaymentTransactionRouteProvider",
 *     },
 *   },
 *   base_table = "bs_orders_payment_tranactions",
 *   admin_permission = "administer bs_order",
 *   entity_keys = {
 *     "id" = "tid",
 *   },
 *   common_reference_target = TRUE,
 *   links = {
 *     "by-order-collection" = "/admin/beeshop/order/orders/list/{bs_order}/payment-transactions",
 *   }
 * )
 *
 * @property Price amount
 */
class OrderPaymentTransaction extends ContentEntityBase implements OrderPaymentTransactionInterface {


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['oid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Referenced order'))
    ->setDescription(t('The order id.'))
    ->setSetting('target_type', 'bs_order')
    ->setReadOnly(TRUE);

    $fields['provider_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Provider id'))
      ->setDescription(t('Transaction id in payment provider system.'))
      ->setReadOnly(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_method'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment method'))
      ->setDescription(t('Payment method.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bs_payment_method')
      ->setReadOnly(TRUE);

    $fields['plugin_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment plugin'))
      ->setDescription(t('Payment plugin.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['amount'] = BaseFieldDefinition::create('bs_price')
    ->setLabel(t('Transaction amount'))
    ->setDescription(t('Transaction amount.'));


    $fields['timestamp'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The time when the transaction was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the transaction was last edited.'));

    $fields['state'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Transaction state'))
      ->setDescription(t('The transaction state.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bs_order_payment_trns_state')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'sort' => [
          'field' => 'weight',
          'order' => 'ASC',
        ],
      ])
      ->setTranslatable(FALSE)
//       ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Data'))
    ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return $this->get('provider_id')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getAmount() {
    return $this->amount->first();
  }

  public function getPaydedAmount() {
    $plugin = $this->getPlugin();

    if (!$plugin) {
      return $this->getAmount();
    }

//     dpm($plugin);
  }

  public function getOperations() {
    return $this->get('payment_method')->entity->getPlugin()->getPaymentTransactionOperations($this);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrder() {
    return $this->get('oid')->entity;
  }

  /**
   *
   * @param OrderPaymentTransactionInterface $transaction
   * @return boolean
   */
  public function isTransactionFinished(OrderPaymentTransactionInterface $transaction) {
    return FALSE;
  }

  /**
   * @return NULL|PaymentMethodPluginInterface
   */
  public function getPlugin() {

    if ($this->get('plugin_id')->isEmpty()) {
      return NULL;
    }

    /**
     * @var PaymentMethodManagerInterface $plugin_manager
     */
    $plugin_manager = \Drupal::service('plugin.manager.bs_payment_method');
    return $plugin_manager->createInstance($this->get('plugin_id')->value);
  }

}

