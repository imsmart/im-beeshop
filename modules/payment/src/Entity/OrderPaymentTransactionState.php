<?php

namespace Drupal\bs_payment\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\bs_payment\OrderPaymentTransactionStateInterface;

/**
 * Defines the currency entity class.
 *
 * @ConfigEntityType(
 *   id = "bs_order_payment_trns_state",
 *   label = @Translation("Order payment transaction state"),
 *   label_singular = @Translation("order payment transaction state"),
 *   label_plural = @Translation("order payment transaction states"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order payment transaction state",
 *     plural = "@count order payment transaction states",
 *   ),
 *   admin_permission = "administer beeshop order payment transaction states",
 *   config_prefix = "payment_state",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   inheritable_options = {
 *
 *   },
 *   config_export = {
 *     "id",
 *     "provider_state_id",
 *     "label",
 *     "uuid",
 *     "description",
 *   }
 * )
 *
 */
class OrderPaymentTransactionState extends ConfigEntityBase implements OrderPaymentTransactionStateInterface {

  /**
   * A brief description of this store type.
   *
   * @var string
   */
  protected $description;

  /**
   * A price type weight.
   *
   * @var int
   */
  protected $weight;

  protected $parentStateEntity;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($operation == 'view label') {
      return AccessResult::allowed();
    }

    return parent::access($operation, $account, $return_as_object);
  }

}