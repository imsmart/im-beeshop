<?php
namespace Drupal\bs_payment\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\bs_price\PriceItemList;
use Drupal\bs_price\Plugin\Field\FieldType\PriceItem;
use Drupal\bs_order\Entity\OrderInterface;

/**
 *
 * @author andrey
 *
 *
 */
interface OrderPaymentTransactionInterface extends ContentEntityInterface {

  /**
   * @return PriceItem
   */
  public function getAmount();

  /**
   * @return string|null
   */
  public function getProviderId();

  /**
   * @return OrderInterface
   */
  public function getOrder();
}

