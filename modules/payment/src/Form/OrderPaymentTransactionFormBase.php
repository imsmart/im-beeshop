<?php
namespace Drupal\bs_payment\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bs_payment\Entity\OrderPaymentTransactionInterface;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\ConfirmFormInterface;

abstract class OrderPaymentTransactionFormBase extends FormBase implements ConfirmFormInterface {

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    // TODO: Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    // TODO: Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getDescription()
   */
  public function getDescription() {
    // TODO: Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelText()
   */
  public function getCancelText() {
    // TODO: Auto-generated method stub
    return $this->t('Cancel');
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getFormName()
   */
  public function getFormName() {
    // TODO: Auto-generated method stub

  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, OrderPaymentTransactionInterface $bs_order_payment_transaction = NULL) {

    $actions = $this->actionsElement($form, $form_state);
    if (!empty($actions)) {
      $form['actions'] = $actions;
    }

    return $form;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function getConfirmText() {
    return $this->t('Submit');
  }

  /**
   * Returns the action form element for the current entity form.
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = $this->actions($form, $form_state);

    if (isset($element['delete'])) {
      // Move the delete action as last one, unless weights are explicitly
      // provided.
      $delete = $element['delete'];
      unset($element['delete']);
      $element['delete'] = $delete;
      $element['delete']['#button_type'] = 'danger';
    }

    if (isset($element['submit'])) {
      // Give the primary submit button a #button_type of primary.
      $element['submit']['#button_type'] = 'primary';
    }

    $count = 0;
    foreach (Element::children($element) as $action) {
      $element[$action] += [
        '#weight' => ++$count * 5,
      ];
    }

    if (!empty($element)) {
      $element['#type'] = 'actions';
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->getConfirmText(),
        '#submit' => [
          [$this, 'submitForm'],
        ],
      ],
      'cancel' => ConfirmFormHelper::buildCancelLink($this, $this->getRequest()),
    ];
  }
}

