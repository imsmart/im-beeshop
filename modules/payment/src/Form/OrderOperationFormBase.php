<?php
namespace Drupal\bs_payment\Form;

use Drupal\Core\Form\FormBase;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\Routing\RouteMatchInterface;

abstract class OrderOperationFormBase extends FormBase implements OrderOperationFormInterface {

  /**
   * @var \Drupal\bs_order\Entity\OrderInterface
   */
  protected $order;


  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(OrderInterface $order) {
    $this->order = $order;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderFromRouteMatch(RouteMatchInterface $route_match) {
    if ($route_match->getRawParameter('bs_order') !== NULL) {
      return $route_match->getParameter('bs_order');
    }
  }


  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    // TODO Auto-generated method stub

  }

}

