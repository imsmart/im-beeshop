<?php

namespace Drupal\bs_payment\Form;

use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\Routing\RouteMatchInterface;

interface OrderOperationFormInterface {

  public function getOrder();

  public function setOrder(OrderInterface $order);

  public function getOrderFromRouteMatch(RouteMatchInterface $route_match);

}

