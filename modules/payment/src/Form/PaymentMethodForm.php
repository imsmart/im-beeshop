<?php
namespace Drupal\bs_payment\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class PaymentMethodForm extends ContentEntityForm {

  /**
   * The block entity.
   *
   * @var \Drupal\bs_payment\Entity\PaymentMethodInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['is_applicable'] = $this->buildApplicableInterface([], $form_state);

    return $form;
  }

  /**
   * Helper function for building the "Is Applicable" UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the visibility UI added in.
   */
  protected function buildApplicableInterface(array $form, FormStateInterface $form_state) {

    $applicabe = $this->entity->getApplicabe();

    $cpm = \Drupal::service('plugin.manager.condition');
    $context_repository = \Drupal::service('context.repository');
    $avalable_contexts = $context_repository->getAvailableContexts();

//     dpm($avalable_contexts);

    $definitions = $cpm->getDefinitionsForContexts($context_repository->getAvailableContexts());

//     dpm($definitions);

    return $form;
  }

}

