<?php
namespace Drupal\bs_payment\Form;

use Drupal\Core\Form\FormBase;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\Response;

class PayOrder extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    /**
     * @var OrderInterface $order
     */
    $order = !empty($form_state->getBuildInfo()['args'][0]) ? $form_state->getBuildInfo()['args'][0] : NULL;

    if (!$order) return;

    $build = [];

    $order_payment_method = $order->get('payment_method')->entity;

    if ($order_payment_method->getPlugin()->isOnlinePaymentSupported()) {
      if ($pay_order_plugin_response = $order_payment_method->getPlugin()->payOrder($order)) {

        if (is_array($pay_order_plugin_response)) {
          $build['form'] = $pay_order_plugin_response;
        }
        elseif ($pay_order_plugin_response instanceof Response) {
          return $pay_order_plugin_response;
        }

      }
    }
    else {
      $message = new TranslatableMarkup('Order "@order_number" can\'t be paid online.', ['@order_number' => $order->getFullNumber()]);
      \Drupal::messenger()->addWarning($message);
    }

    $noindex = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex',
      ],
    ];

    $build['#attached']['html_head'][] = [$noindex, 'description'];

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
      return 'pay_order_form';
  }

  public function getOrder() {
//     return $th
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {

    $form_state->setCached(FALSE);

    /**
     * @var OrderInterface $order
     */
    $order = !empty($form_state->getBuildInfo()['args'][0]) ? $form_state->getBuildInfo()['args'][0] : NULL;

    $order_state = $order->getState();

    if (!$order) return [];

    \Drupal::moduleHandler()->invokeAll('pay_order_form_prebuild', [$order]);

    $pay_access = $order->access('pay', NULL, TRUE);

    if ($pay_access->isForbidden()) {
      if ($order_state->getThirdPartySetting('bs_payment', 'can_be_paid', FALSE)) {
        $message = $this->t('Order "@order_number" can\'t be paid at this time.', ['@order_number' => $order->getFullNumber()]);
        \Drupal::messenger()->addWarning($message);
      }
      return [];
    }

    // TODO Auto-generated method stub
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['pay_order'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay order'),
    ];

    return $form;
  }


}

