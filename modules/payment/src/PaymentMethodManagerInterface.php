<?php
namespace Drupal\bs_payment;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;

interface PaymentMethodManagerInterface extends CategorizingPluginManagerInterface {
}

