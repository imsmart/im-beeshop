<?php
namespace Drupal\bs_payment;

use Drupal\bs_payment\Entity\OrderPaymentTransactionInterface;

interface PaymentMethodPluginInterface {

  public function getPaymentTransactionOperations(OrderPaymentTransactionInterface $transaction);

}

