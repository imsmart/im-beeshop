<?php
namespace Drupal\bs_payment;

use Drupal\Core\Entity\EntityListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

class PaymentTransactionListBuilder extends EntityListBuilder {

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match')
      );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RouteMatchInterface $route_match) {
    $this->entityTypeId = $entity_type->id();
    $this->storage = $storage;
    $this->entityType = $entity_type;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function load() {

    if (($order = $this->routeMatch->getParameter('bs_order')) &&
      !$order->get('payment_transactions')->isEmpty()) {
        $order->get('payment_transactions')->referencedEntities();
    }

    $entity_ids = $this->getEntityIds();

    return $this->storage->loadMultiple($entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['amount'] = $this->t('Amount');
    $header['state'] = $this->t('Payment state');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['amount']['data'] = $entity->get('amount')->first()->toPrice()->getRendableArray();
    $row['state']['data'] = $entity->get('state')->entity ? $entity->get('state')->entity->get('label') : 'not defined';

    $row += parent::buildRow($entity);

    return $row;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
    ->sort($this->entityType->getKey('id'));

    if ($order = $this->routeMatch->getParameter('bs_order')) {
      $query->condition('oid', $order->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}

