<?php
namespace Drupal\bs_payment;

use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormState;
use Drupal\bs_payment\Entity\OrderPaymentTransactionInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\RedirectDestinationTrait;

abstract class PaymentMethodPluginBase extends PluginBase implements PaymentMethodPluginInterface, ContainerFactoryPluginInterface {

  use RedirectDestinationTrait;

  protected $handlers = [];

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClassResolverInterface $class_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->classResolver = $class_resolver;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('class_resolver')
      );
  }

  public function getFormObject($operation) {

    if (empty($this->pluginDefinition['handlers']['form'][$operation])) {
      throw new InvalidPluginDefinitionException($this->pluginId, sprintf('The "%s" plugin did not specify a "%s" form class.', $this->pluginId, $operation));
    }

    $class = $this->pluginDefinition['handlers']['form'][$operation];
    $form_object = $this->classResolver->getInstanceFromDefinition($class);

    return $form_object;
  }

  /**
   * {@inheritDoc}
   */
  public function getPaymentTransactionOperations(OrderPaymentTransactionInterface $transaction) {
    return [];
  }

  public function payOrder(OrderInterface $order) {
    if (!empty($this->pluginDefinition['handlers']['form']['pay_order'])) {
      $form_object = $this->getFormObject('pay_order');

      $form_object->setOrder($order);
      $form_state = (new FormState())->setFormState([]);

      $form = \Drupal::formBuilder()->buildForm($form_object, $form_state);

      return $form;
    }
  }

  public function isOnlinePaymentSupported() {
    return !empty($this->pluginDefinition['onlinePayment']);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property, $value) {
    if (property_exists($this, $property)) {
      $this->{$property} = $value;
    }
    else {
      $this->additional[$property] = $value;
    }
    return $this;
  }


  protected function getOrderOperationForm($operation) {

  }

  /**
   * Ensures that a destination is present on the given URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL object to which the destination should be added.
   *
   * @return \Drupal\Core\Url
   *   The updated URL object.
   */
  protected function ensureDestination(Url $url) {
    return $url->mergeOptions(['query' => $this->getRedirectDestination()->getAsArray()]);
  }

  /**
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  protected static function getPaymentTransactionsStorage() {
    return \Drupal::entityTypeManager()->getStorage('bs_order_payment_transaction');
  }

}

