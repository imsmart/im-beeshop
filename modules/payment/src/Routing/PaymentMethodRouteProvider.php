<?php

namespace Drupal\bs_payment\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class PaymentMethodRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $routes = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($enable_route = $this->getEnableRoute($entity_type)) {
      $routes->add("entity.{$entity_type_id}.enable", $enable_route);
    }

    if ($disable_route = $this->getDisableRoute($entity_type)) {
      $routes->add("entity.{$entity_type_id}.disable", $disable_route);
    }

    return $routes;
  }

  /**
   * Gets the enable method route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getEnableRoute(EntityTypeInterface $entity_type) {
//     if ($entity_type->hasLinkTemplate('enable')) {
//       $route = new Route($entity_type->getLinkTemplate('enable'));
//       $route->setDefault('_controller', 'Drupal\bs_shipping\Controller\ShippingMethodController::performOperation');
//       $route->setDefault('op', 'enable')
//         ->setOption('parameters', [
//           'bs_shipping_method' => ['type' => 'entity:bs_shipping_method'],
//         ]);
// //       $route->setRequirement('_entity_access', 'bs_shipping_method.enable');
//       $route->setRequirement('_permission', 'administer bs_shipping methods');

//       return $route;
//     }
  }

  /**
   * Gets the disable method route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getDisableRoute(EntityTypeInterface $entity_type) {
//     if ($entity_type->hasLinkTemplate('disable')) {
//       $route = new Route($entity_type->getLinkTemplate('disable'));
//       $route->setDefault('_controller', 'Drupal\bs_shipping\Controller\ShippingMethodController::performOperation');
//       $route->setDefault('op', 'disable')
//         ->setOption('parameters', [
//           'bs_shipping_method' => ['type' => 'entity:bs_shipping_method'],
//         ]);
// //       $route->setRequirement('_entity_access', 'bs_shipping_method.disable');
//       $route->setRequirement('_permission', 'administer bs_shipping methods');

//       return $route;
//     }
  }

}

