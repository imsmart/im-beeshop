<?php
namespace Drupal\bs_payment\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class PaymentTransactionRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($order_payment_transactions_route = $this->getOrderPaymentTransactionsCollectionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.by_order_collection", $order_payment_transactions_route);
    }

    return $collection;
  }

  /**
   * Gets the order shipments collection route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getOrderPaymentTransactionsCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('by-order-collection') && $entity_type->hasListBuilderClass() && ($admin_permission = $entity_type->getAdminPermission())) {
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
      $label = $entity_type->getCollectionLabel();

      $route = new Route($entity_type->getLinkTemplate('by-order-collection'));
      $route
      ->addDefaults([
        '_entity_list' => $entity_type->id(),
        '_title' => $label->getUntranslatedString(),
        '_title_arguments' => $label->getArguments(),
        '_title_context' => $label->getOption('context'),
      ])
      ->setRequirement('_permission', $admin_permission);

      $route->setOption('parameters', ['bs_order' => ['type' => 'entity:bs_order']]);

      return $route;
    }
  }

  protected function addBsOrderParametrToRoute(Route $route) {
    $parameters = $route->getOption('parameters') ?: [];

    $upd_parametrs = [
      'bs_order' => [
        'type' => 'entity:bs_order'
      ]
    ];

    $upd_parametrs += $parameters;

    $route->setOption('parameters', $upd_parametrs);
  }
}

