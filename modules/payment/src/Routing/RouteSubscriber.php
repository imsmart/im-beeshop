<?php
namespace Drupal\bs_payment\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Symfony\Component\Routing\Route;

class RouteSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityRouteProviderSubscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::DYNAMIC] = 'onDynamicRouteEvent';
    return $events;
  }

  /**
   * Provides routes on route rebuild time.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onDynamicRouteEvent(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    $order_entity_type = $this->entityTypeManager->getDefinition('bs_order');

    if ($route = $this->getOrderPayRoute($order_entity_type)) {
      $collection->add('entity.bs_order.pay', $route);
    }

  }

  protected function getOrderPayRoute(ContentEntityTypeInterface $entity_type) {
    if ($devel_load = $entity_type->getLinkTemplate('pay')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($devel_load);
      $route
      ->addDefaults([
        '_controller' => '\Drupal\bs_payment\Controller\PayOrderController::payOrder',
        '_title_callback' => '\Drupal\bs_payment\Controller\PayOrderController::payOrderPageTitle',
      ])
      ->addRequirements([
        '_permission' => 'access content',
      ])
      ->setOption('no_cache', TRUE)
      ->setOption('parameters', [
        'bs_order' => ['type' => 'bs_order_by_full_number'],
      ]);

      return $route;
    }
  }

}

