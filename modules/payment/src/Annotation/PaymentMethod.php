<?php
namespace Drupal\bs_payment\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Shipping method annotation object.
 *
 * @ingroup beeshop_payment_api
 *
 * @Annotation
 */
class PaymentMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label of the shipping method.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

  /**
   * The category in the admin UI where the shipping method will be listed.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category = '';

  public $onlinePayment = TRUE;

  /**
   * An array of context definitions describing the context used by the plugin.
   *
   * The array is keyed by context names.
   *
   * @var \Drupal\Core\Annotation\ContextDefinition[]
   */
  public $context = [];
}

