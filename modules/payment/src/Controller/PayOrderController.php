<?php
namespace Drupal\bs_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bs_order\Entity\OrderInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\Response;

class PayOrderController extends ControllerBase {

  public function payOrder(OrderInterface $bs_order) {

    $build = [
      '#theme' => 'bs_payment_pay_order_page',
      '#order' => $bs_order,
    ];

    $order_payment_method = $bs_order->get('payment_method')->entity;

    $build['#payment_method'] = $order_payment_method;

    if (!$bs_order->getState()->getThirdPartySetting('bs_payment', 'can_be_paid') || !$order_payment_method) {
      $message = new TranslatableMarkup('Order "@order_number" can\'t be paid at this time.', ['@order_number' => $bs_order->getFullNumber()]);
      \Drupal::messenger()->addWarning($message);
    }
    elseif ($order_payment_method) {

      if ($order_payment_method->getPlugin()->isOnlinePaymentSupported()) {
        if ($pay_order_plugin_response = $order_payment_method->getPlugin()->payOrder($bs_order)) {

          if (is_array($pay_order_plugin_response)) {
            $build['form'] = $pay_order_plugin_response;
          }
          elseif ($pay_order_plugin_response instanceof Response) {
            return $pay_order_plugin_response;
          }

        }
      }
      else {
        $message = new TranslatableMarkup('Order "@order_number" can\'t be paid online.', ['@order_number' => $bs_order->getFullNumber()]);
        \Drupal::messenger()->addWarning($message);
      }

    }

    $noindex = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex',
      ],
    ];

    $build['#attached']['html_head'][] = [$noindex, 'description'];

    return $build;
  }

  public function payOrderPageTitle(OrderInterface $bs_order) {
    return new TranslatableMarkup('Pay order "@order_number"', ['@order_number' => $bs_order->getFullNumber()]);
  }

}

