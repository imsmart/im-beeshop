/**
 * 
 */
(function ($, window, Drupal, drupalSettings) {

  'use strict';
  
  window['swalQueue'] = [];
  
  function processSwalQueue() {
    delete(window['activeSwal']);
    
    var item = window['swalQueue'].shift()

    if (item) {
      item.fire().then((result) => {processSwalQueue()});
    }
    
  }
  
  Drupal.Ajax.prototype._beforeSerialize = Drupal.Ajax.prototype.beforeSerialize;
  Drupal.Ajax.prototype.beforeSerialize = function(element, options) {

  	var pageState = drupalSettings.ajaxPageState;
    options.data['ajax_page_state[beeshop]'] = pageState.beeshop || {};
    
    return this._beforeSerialize(element, options);

  }
  
  
  Drupal.Ajax.prototype._beforeSend = Drupal.Ajax.prototype.beforeSend;
  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
  	
  	if (this.element.dataset && this.element.dataset.ajaxRequestType) {
  		//options.type = this.element.dataset.ajaxRequestType;
  	}
  	
  	return this._beforeSend(xmlhttprequest, options);
  }
  
  function BeeShop() {
    
  }
  
  Drupal.BeeShop = new BeeShop();
  
  BeeShop.prototype.invokeEvent = function(eventName, args) {
  	var event = new CustomEvent(eventName, {
  		detail: { args: args }
  	});
  	window.dispatchEvent(event);
  }
  
  Drupal.AjaxCommands.prototype.BeeShopEvent = function (ajax, response, status) {
    if (typeof response.eventName != 'undefined') {
      response.args = response.args || [];
      Drupal.BeeShop.invokeEvent(response.eventName, response.args);
    }
    
    if (response.reattachBehavors) {
      Drupal.attachBehaviors('', drupalSettings);
    }
  }
  
  //BeeShopUpdateSettings
  Drupal.AjaxCommands.prototype.BeeShopUpdateSettings = function (ajax, response, status) {
    if (typeof response.name != 'undefined' && typeof response.value != 'undefined') {
      drupalSettings.BeeShop = drupalSettings.BeeShop || {};
      drupalSettings.BeeShop[response.name] = response.value;
    }
    
    if (response.reattachBehavors) {
      Drupal.attachBehaviors('', drupalSettings);
    }
    
  }
  
  Drupal.AjaxCommands.prototype.showAlert = function (ajax, response, status) {
    if (Swal != undefined) {
      
      if (response.mixin_name != undefined) {
        if (window[response.mixin_name] != undefined) {
          if (window['activeSwal'] != undefined) {
            window['swalQueue'].push(window[response.mixin_name]);
          }
          else {
            window[response.mixin_name].fire(response.alertSettings).then(processSwalQueue());
          }
          
        }
      }
      else {
        if (window['activeSwal'] != undefined) {
          window['swalQueue'].push(Swal.mixin(response.alertSettings));
        }
        else {
          window['activeSwal'] = Swal.fire(response.alertSettings).then((result) => {processSwalQueue()});
        }
      }
      
    }    
  }
  
  Drupal.AjaxCommands.prototype.BeeshoPageReload = function (ajax, response, status) {
    $('body').append($(Drupal.theme('ajaxProgressIndicatorFullscreen')));
    location.reload();
  }
  
})(jQuery, window, Drupal, drupalSettings);