/**
 * 
 */
(function (window, Drupal, drupalSettings, domready) {

  'use strict';
  
  domready(() => {
    document.querySelectorAll('.beeshop-sweetalert2').forEach((element) => {
      element.addEventListener('click', (e) => {
        e.preventDefault();
        
        let options = {};
        
//        element.dataset.forEach((key, value) => {
//          console.log(key, value);
//        });
        
        for (const [key, value] of Object.entries(element.dataset)) {
          try {
            options[key] = JSON.parse(value);
          }
          catch (e) {
            options[key] = value;
          }
        }
        
        if (element.dataset['mixin'] && window[element.dataset['mixin']]) {
          window['fired_swal'] = window[element.dataset['mixin']].fire(options);
        }
        
      });
    });
  })
  
  
})(window, Drupal, drupalSettings, domready);