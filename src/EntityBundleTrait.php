<?php
namespace Drupal\beeshop;

trait EntityBundleTrait {

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getBundleEntity() {
    $entity_type = $this->getEntityType();

    if ($entity_type->hasKey('bundle')) {
      if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        return $this->get($entity_type->getKey('bundle'))->entity;
      }

    }
  }

}

