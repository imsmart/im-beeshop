<?php

namespace Drupal\beeshop\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for invoking an arbitrary jQuery method.
 *
 * The 'invoke' command will instruct the client to invoke the given jQuery
 * method with the supplied arguments on the elements matched by the given
 * selector. Intended for simple jQuery commands, such as attr(), addClass(),
 * removeClass(), toggleClass(), etc.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.invoke()
 * defined in misc/ajax.js.
 *
 * @ingroup ajax
 */
class BeeShopEventCommand implements CommandInterface {

  /**
   * A jQuery method to invoke.
   *
   * @var string
   */
  protected $eventName;

  /**
   * An optional list of arguments to pass to the method.
   *
   * @var array
   */
  protected $arguments;

  protected $reattachBehavors;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param string $method
   *   The name of a jQuery method to invoke.
   * @param array $arguments
   *   An optional array of arguments to pass to the method.
   */
  public function __construct($event_name, array $arguments = [], $reattach_behavors = FALSE) {
    $this->eventName = $event_name;
    $this->arguments = $arguments;
    $this->reattachBehavors = $reattach_behavors;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {

    return [
      'command' => 'BeeShopEvent',
      'eventName' => $this->eventName,
      'args' => $this->arguments,
      'reattachBehavors' => $this->reattachBehavors,
    ];
  }

}
