<?php
namespace Drupal\beeshop\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class BeeShopUpdateSettings implements CommandInterface {

  protected $settingsName;

  protected $value;

  protected $reattachBehavors;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param string $method
   *   The name of a jQuery method to invoke.
   * @param array $arguments
   *   An optional array of arguments to pass to the method.
   */
  public function __construct($name, $new_value, $reattach_behavors = FALSE) {
    $this->settingsName = $name;
    $this->value = $new_value;
    $this->reattachBehavors = $reattach_behavors;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {

    return [
      'command' => 'BeeShopUpdateSettings',
      'name' => $this->settingsName,
      'value' => $this->value,
      'reattachBehavors' => $this->reattachBehavors,
    ];
  }

}

