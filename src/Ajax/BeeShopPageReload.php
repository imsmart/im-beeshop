<?php
namespace Drupal\beeshop\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class BeeShopPageReload implements CommandInterface {

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {

    return [
      'command' => 'BeeshoPageReload',
    ];
  }

}

