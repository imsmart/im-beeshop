<?php
namespace Drupal\beeshop\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class BeeshopSowAlert implements CommandInterface {

  protected $alertSettings = [];

  protected $mixin = NULL;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param string $method
   *   The name of a jQuery method to invoke.
   * @param array $arguments
   *   An optional array of arguments to pass to the method.
   */
  public function __construct($alert_settings = [], $mixin = NULL) {
    $this->alertSettings = $alert_settings;
    $this->mixin = $mixin;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {

    $return = [
      'command' => 'showAlert',
      'alertSettings' => $this->alertSettings,
    ];

    if ($this->mixin) {
      $return['mixin_name'] = $this->mixin;
    }

    return $return;
  }
}

