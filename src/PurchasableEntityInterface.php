<?php
namespace Drupal\beeshop;

interface PurchasableEntityInterface {

  /**
   * Gets the purchasable entity's price.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The price, or NULL.
   */
  public function getPrice();


  public function getActualPrice();

  /**
   *
   */
  public function getPriceResolvingContext();

  /**
   *
   */
  public function setPriceResolvingContext();
}

