<?php

namespace Drupal\beeshop\Plugin\Mail;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;


/**
 * Defines the default Drupal mail backend, using PHP's native mail() function.
 *
 * @Mail(
 *   id = "beeshop_mail",
 *   label = @Translation("BeeShop mailer"),
 *   description = @Translation("Sends the message using PHP's native mail() function.")
 * )
 */
class BesshopMail extends PhpMail {

  /**
   * Concatenates and wraps the email body for plain-text mails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    $message['body'] = implode("\n\n", $message['body']);

    // Convert any HTML to plain-text.
    $body = $message['body'];
    $pattern = '/(<a[^>]+href=")([^"]*)/mi';
    $body = preg_replace_callback($pattern, ['\Drupal\beeshop\Plugin\Mail\BesshopMail', 'expandLinks'], $body);
    $pattern = '/(<img[^>]+src=")([^"]*)/mi';
    $body = preg_replace_callback($pattern, ['\Drupal\beeshop\Plugin\Mail\BesshopMail', 'expandLinks'], $body);

    $message['body'] = $body;
    // Wrap the mail body for sending.

    return $message;
  }

  /**
   * Helper function to format URLs.
   *
   * @param string $url
   *   The file path.
   * @param boolean $to_embed
   *   (optional) Wheter the URL is used to embed the file. Defaults to NULL.
   *
   * @return string
   *   A processed URL.
   */
  public static function mailUrl($url, $to_embed = NULL) {
    $url = urldecode($url);

    $to_link = \Drupal::config('mimemail.settings')->get('linkonly');
    $is_image = preg_match('!\.(png|gif|jpg|jpeg)!i', $url);
    $is_absolute = \Drupal::service('file_system')->uriScheme($url) != FALSE || preg_match('!(mailto|callto|tel)\:!', $url);

    // Strip the base path as Uri adds it again at the end.
    $base_path = rtrim(base_path(), '/');
    $url = preg_replace('!^' . $base_path . '!', '', $url, 1);

    if ($is_image) {
      if ($to_link) {
        // Exclude images from embedding if needed.
        $url = file_create_url($url);
        $url = str_replace(' ', '%20', $url);
      }
      else {
        // Remove security token from URL, this allows for styled image embedding.
        // @see https://drupal.org/drupal-7.20-release-notes
        $url = preg_replace('/\\?itok=.*$/', '', $url);
      }

    }

    if (!$to_embed) {
      if ($is_absolute) {
        return str_replace(' ', '%20', $url);
      }
    }

    $url = str_replace('?q=', '', $url);
    @list($url, $fragment) = explode('#', $url, 2);
    @list($path, $query) = explode('?', $url, 2);

    // If we're dealing with an intra-document reference, return it.
    if (empty($path)) {
      return '#' . $fragment;
    }

    // Get a list of enabled languages.
    $languages = \Drupal::languageManager()->getLanguages(LanguageInterface::STATE_ALL);

    // Default language settings.
    $prefix = '';
    $language =  \Drupal::languageManager()->getDefaultLanguage();

    // Check for language prefix.
    $args = explode('/', $path);
//     foreach ($languages as $lang) {
//       if ($args[1] == $lang->getId()) {
//         $prefix = array_shift($args);
//         $language = $lang;
//         $path = implode('/', $args);
//         break;
//       }
//     }

    parse_str($query, $arr);
    $options = array(
      'fragment' => $fragment,
      'absolute' => TRUE,
//       'language' => $language,
      'prefix' => $prefix,
    );

    if (!empty($arr)) {
      $options['query'] = $arr;
    }

    $url_obj = Url::fromUserInput($path, $options);
    $url = $url_obj->toString();
    $url = preg_replace('#\?file=#', '/', $url);
    // If url() added a ?q= where there should not be one, remove it.
    if (preg_match('!^\?q=*!', $url)) {
      $url = preg_replace('!\?q=!', '', $url);
    }

    $url = str_replace('+', '%2B', $url);
    return $url;
  }

  /**
   * @param $matches
   * @return string
   */
  public static function expandLinks($matches) {
    return $matches[1] . self::mailUrl($matches[2]);
  }

//   /**
//    * Sends an email message.
//    *
//    * @param array $message
//    *   A message array, as described in hook_mail_alter().
//    *
//    * @return bool
//    *   TRUE if the mail was successfully accepted, otherwise FALSE.
//    *
//    * @see http://php.net/manual/function.mail.php
//    * @see \Drupal\Core\Mail\MailManagerInterface::mail()
//    */
//   public function mail(array $message) {
//     // If 'Return-Path' isn't already set in php.ini, we pass it separately
//     // as an additional parameter instead of in the header.
//     if (isset($message['headers']['Return-Path'])) {
//       $return_path_set = strpos(ini_get('sendmail_path'), ' -f');
//       if (!$return_path_set) {
//         $message['Return-Path'] = $message['headers']['Return-Path'];
//         unset($message['headers']['Return-Path']);
//       }
//     }
//     $mimeheaders = [];
//     foreach ($message['headers'] as $name => $value) {
//       $mimeheaders[] = $name . ': ' . Unicode::mimeHeaderEncode($value);
//     }
//     $line_endings = Settings::get('mail_line_endings', PHP_EOL);
//     // Prepare mail commands.
//     $mail_subject = Unicode::mimeHeaderEncode($message['subject']);
//     // Note: email uses CRLF for line-endings. PHP's API requires LF
//     // on Unix and CRLF on Windows. Drupal automatically guesses the
//     // line-ending format appropriate for your system. If you need to
//     // override this, adjust $settings['mail_line_endings'] in settings.php.
//     $mail_body = preg_replace('@\r?\n@', $line_endings, $message['body']);
//     // For headers, PHP's API suggests that we use CRLF normally,
//     // but some MTAs incorrectly replace LF with CRLF. See #234403.
//     $mail_headers = join("\n", $mimeheaders);

//     $request = \Drupal::request();

//     // We suppress warnings and notices from mail() because of issues on some
//     // hosts. The return value of this method will still indicate whether mail
//     // was sent successfully.
//     if (!$request->server->has('WINDIR') && strpos($request->server->get('SERVER_SOFTWARE'), 'Win32') === FALSE) {
//       // On most non-Windows systems, the "-f" option to the sendmail command
//       // is used to set the Return-Path. There is no space between -f and
//       // the value of the return path.
//       $additional_headers = isset($message['Return-Path']) ? '-f' . $message['Return-Path'] : '';
//       $mail_result = @mail(
//         $message['to'],
//         $mail_subject,
//         $mail_body,
//         $mail_headers,
//         $additional_headers
//       );
//     }
//     else {
//       // On Windows, PHP will use the value of sendmail_from for the
//       // Return-Path header.
//       $old_from = ini_get('sendmail_from');
//       ini_set('sendmail_from', $message['Return-Path']);
//       $mail_result = @mail(
//         $message['to'],
//         $mail_subject,
//         $mail_body,
//         $mail_headers
//       );
//       ini_set('sendmail_from', $old_from);
//     }

//     return $mail_result;
//   }

}
