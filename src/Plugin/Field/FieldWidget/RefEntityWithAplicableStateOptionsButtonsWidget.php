<?php

namespace Drupal\beeshop\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Plugin implementation of the 'options_buttons' widget.
 *
 * @FieldWidget(
 *   id = "bs_ref_entity_with_applicable_state_options_buttons",
 *   label = @Translation("Check boxes/radio buttons (Entity with applicable state)"),
 *   field_types = {
 *     "bs_applicable_entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class RefEntityWithAplicableStateOptionsButtonsWidget extends OptionsButtonsWidget {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

//     dpm(\Drupal::service('plugin.manager.entity_reference_selection')->getSelectionGroups('bs_payment_method'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (empty($element['#default_value']) && !empty($element['#options'])) {
      $keys = array_keys($element['#options']);
      $element['#default_value'] = reset($keys);
    }

    return $element;
  }

}
