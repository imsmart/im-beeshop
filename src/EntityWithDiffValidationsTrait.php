<?php
namespace Drupal\beeshop;

trait EntityWithDiffValidationsTrait {

  protected $validationConditionsSetName;

  public function validateFor($conditions_set_name) {
    $this->validationConditionsSetName = $conditions_set_name;
    return $this->validate();
  }

}

