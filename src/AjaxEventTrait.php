<?php
namespace Drupal\beeshop;

use Drupal\Core\Ajax\AjaxResponse;

trait AjaxEventTrait {

  /**
   * The ajax response.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  protected $response;

  /**
   * @param AjaxResponse $response
   */
  public function setResponse(AjaxResponse $response) {
    $this->response = $response;
    return $this;
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Request
   */
  public function getRequest() {
    return \Drupal::request();
  }

}

