<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides the interface for BeeShop bundle entities.
 */
interface BeeShopBundleEntityBaseInterface extends ConfigEntityInterface {

}

