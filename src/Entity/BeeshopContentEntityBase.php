<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\beeshop\Entity\BeeshopContentEntityInterface;

abstract class BeeshopContentEntityBase extends ContentEntityBase implements BeeshopContentEntityInterface {


  /**
   * {@inheritDoc}
   */
  public function getReferencedEntities($field_name) {
    $referenced_entities = $this->get($field_name)->referencedEntities();
    return $referenced_entities;
  }

  /**
   * {@inheritDoc}
   */
  public function getReferencedEntity($field_name) {
    $referenced_entities = $this->getReferencedEntity($field_name);
    return reset($referenced_entities);
  }

}

