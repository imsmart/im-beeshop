<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface BeeshopContentEntityInterface extends ContentEntityInterface {

  /**
   * Gets referenced entities by field name.
   *
   * @param string $field_name
   *   The entity reference field name.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Referenced endtites
   */
  public function getReferencedEntities($field_name);

  /**
   * Get referenced entity by field name
   *
   * @param string $field_name
   *   The entity reference field name.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Referenced endtity
   */
  public function getReferencedEntity($field_name);

//   /**
//    * Gets the translations of an entity reference field.
//    *
//    * @param string $field_name
//    *   The entity reference field name.
//    *
//    * @return \Drupal\Core\Entity\ContentEntityInterface[]
//    *   The entities.
//    */
//   public function getTranslatedReferencedEntities($field_name);

//   /**
//    * Gets the translation of an referenced entity.
//    *
//    * @param string $field_name
//    *   The entity reference field name.
//    *
//    * @return \Drupal\Core\Entity\ContentEntityInterface
//    *   The entity.
//    */
//   public function getTranslatedReferencedEntity($field_name);

}

