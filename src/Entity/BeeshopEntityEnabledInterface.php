<?php
namespace Drupal\beeshop\Entity;

/**
 * Provides an interface for access to an entity's enabled state.
 */
interface BeeshopEntityEnabledInterface {

  /**
   * Denotes that the entity is disabled.
   */
  const DISABLED = 0;

  /**
   * Denotes that the entity is enabled.
   */
  const ENABLED = 1;

  /**
   * Returns whether or not the entity is enabled.
   *
   * @return bool
   *   TRUE if the entity is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the entity as published.
   *
   * @return $this
   *
   * @see \Drupal\Core\Entity\EntityPublishedInterface::setUnpublished()
   */
  public function setEnabled();

  /**
   * Sets the entity as disabled.
   *
   * @return $this
   */
  public function setDisabled();

}

