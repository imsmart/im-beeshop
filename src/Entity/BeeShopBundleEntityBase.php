<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

class BeeShopBundleEntityBase extends ConfigEntityBundleBase implements BeeShopBundleEntityBaseInterface {

  /**
   * The bundle ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The bundle label.
   *
   * @var string
   */
  protected $label;




}

