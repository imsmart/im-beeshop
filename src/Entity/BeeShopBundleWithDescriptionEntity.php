<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

class BeeShopBundleWithDescriptionEntity extends BeeShopBundleEntityBase implements BeeShopBundleWithDescriptionEntityInterface {

  /**
   * A brief description of this store type.
   *
   * @var string
   */
  protected $description;


  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }
}

