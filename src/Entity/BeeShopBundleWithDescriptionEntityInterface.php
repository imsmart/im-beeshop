<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides the interface for BeeShop bundle entities.
 */
interface BeeShopBundleWithDescriptionEntityInterface extends ConfigEntityInterface, EntityDescriptionInterface {


}

