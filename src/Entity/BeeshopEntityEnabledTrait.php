<?php
namespace Drupal\beeshop\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

trait BeeshopEntityEnabledTrait {


  /**
   * Returns an array of base field definitions for enabled status.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to add the publishing status field to.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   An array of base field definitions.
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   *   Thrown when the entity type does not implement EntityPublishedInterface
   *   or if it does not have a "published" entity key.
   */
  public static function enabledBaseFieldDefinitions(EntityTypeInterface $entity_type) {
    if (!is_subclass_of($entity_type->getClass(), BeeshopEntityEnabledInterface::class)) {
      throw new UnsupportedEntityTypeDefinitionException('The entity type ' . $entity_type->id() . ' does not implement \Drupal\beeshop\Entity\BeeshopEntityEnabledInterface.');
    }
    if (!$entity_type->hasKey('enabled')) {
      throw new UnsupportedEntityTypeDefinitionException('The entity type ' . $entity_type->id() . ' does not have a "enabled" entity key.');
    }

    return [
      $entity_type->getKey('enabled') => BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Enabled'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    $key = $this->getEntityType()->getKey('enabled');
    return (bool) $this->get($key)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled() {
    $key = $this->getEntityType()->getKey('enabled');
    $this->set($key, TRUE);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisabled() {
    $key = $this->getEntityType()->getKey('enabled');
    $this->set($key, FALSE);

    return $this;
  }

}

