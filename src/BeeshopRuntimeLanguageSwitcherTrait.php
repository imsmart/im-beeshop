<?php
namespace Drupal\beeshop;

trait BeeshopRuntimeLanguageSwitcherTrait {

  protected $prev_langcode;


  protected function switchLanguageTo($langcode_switch_to) {
    $current_langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    if ($current_langcode && $current_langcode != $langcode_switch_to) {

      $this->prev_langcode = $current_langcode;
      $language_manager = $this->languageManager();

      if ($language = $language_manager->getLanguage($langcode_switch_to)) {
        $customLanguageNegotiator = \Drupal::service('beeshop.language_negotiator');
        $language_manager->setNegotiator($customLanguageNegotiator);
        $language_manager->reset();
        $language_manager->getNegotiator()->setLanguageCode($langcode_switch_to);
        $language_manager->setConfigOverrideLanguage($language);
      }
    }

    return $langcode_switch_to;
  }

  protected function restoreSwitchedLanguage() {
    if ($this->prev_langcode) {
      $language_manager = $this->languageManager();
      $prev_language = $language_manager->getLanguage($this->prev_langcode);
      $language_manager->reset();
      $language_manager->getNegotiator()->setLanguageCode($this->prev_langcode);
      $language_manager->setConfigOverrideLanguage($prev_language);
    }
  }

  /**
   * Gets the language manager service.
   *
   * @return \Drupal\Core\Language\LanguageManagerInterface
   */
  protected function languageManager() {
    return \Drupal::languageManager();
  }
}

