<?php
namespace Drupal\beeshop;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormInterface;


abstract class HierarchicalDraggableListBuilder extends DraggableListBuilder implements FormInterface {

  use EntityTreeProccessorTrait;

  /**
   * Name of the entity's parent field or FALSE if no field is provided.
   *
   * @var string|bool
   */
  protected $parentKey = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);

    // Check if the entity type supports weighting.
    if ($this->entityType->hasKey('parent')) {
      $this->parentKey = $this->entityType->getKey('parent');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entities = parent::load();

    if ($processed_entities = $this->getEntitiesTree($entities)) {
      $entities = [];

      foreach ($processed_entities as $entitty) {
        $entities[$entitty->id()] = $entitty;
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);

    if (!empty($row['weight'])) {
      $weight_element = $row['weight'];
      $row['weight'] = [];
      $row['weight']['weight'] = $weight_element;

      $row['weight']['id'] = [
        '#type' => 'hidden',
        '#value' => $entity->id(),
        '#attributes' => [
          'class' => ['id'],
        ],
      ];
      $row['weight']['parent'] = [
        '#type' => 'hidden',
        // Yes, default_value on a hidden. It needs to be changeable by the
        // javascript.
        '#default_value' => $entity->parents[0],
        '#attributes' => [
          'class' => ['parent'],
        ],
      ];
      $row['weight']['depth'] = [
        '#type' => 'hidden',
        // Same as above, the depth is modified by javascript, so it's a
        // default_value.
        '#default_value' => isset($entity->depth) ? $entity->depth : 0,
        '#attributes' => [
          'class' => ['depth'],
        ],
      ];
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form[$this->entitiesKey]['#tabledrag'][] = [
      'action' => 'match',
      'relationship' => 'parent',
      'group' => 'parent',
      'subgroup' => 'parent',
      'source' => 'id',
      'hidden' => FALSE,
    ];
    $form[$this->entitiesKey]['#tabledrag'][] = [
      'action' => 'depth',
      'relationship' => 'group',
      'group' => 'depth',
      'hidden' => FALSE,
    ];

    foreach (Element::children($form[$this->entitiesKey]) as $index => $key) {
      $row = $form[$this->entitiesKey][$key];
      reset($row);
      $first_key = key($row);

      $depth = !empty($row['weight']['depth']['#default_value'])
               ? $row['weight']['depth']['#default_value'] :
               0;
      $indentation = $indentation = [
        '#theme' => 'indentation',
        '#size' => $depth,
      ];

      $form[$this->entitiesKey][$key][$first_key]['data']['prefix']['#prefix'] = !empty($indentation) ? drupal_render($indentation) : '';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && (
            $this->entities[$id]->get($this->weightKey) != $value['weight']['weight'] ||
            $this->entities[$id]->get($this->parentKey) != $value['weight']['parent']
          )) {

        // Save entity only when its weight was changed.
        $this->entities[$id]->set($this->weightKey, $value['weight']['weight']);
        $this->entities[$id]->set($this->parentKey, $value['weight']['parent']);
        $this->entities[$id]->save();
      }
    }
  }
}

