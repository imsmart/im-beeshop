<?php

namespace Drupal\beeshop;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheableResponseTrait;

class CacheableAjaxResponce extends AjaxResponse implements CacheableResponseInterface {

  use CacheableResponseTrait;

}
