<?php
namespace Drupal\beeshop\Session;

use Symfony\Component\HttpFoundation\Session\SessionBagInterface;

class BeeshopDataLayerSessionBag implements SessionBagInterface {

  const BAG_NAME = 'BeeshopDataLayer';

  private $rows = [];
  private $storageKey = '_beeshop_datalayer';


  public function add(array $data) {
    $this->rows[] = $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getName() {
    return self::BAG_NAME;
  }

  /**
   * {@inheritDoc}
   * @see \Symfony\Component\HttpFoundation\Session\SessionBagInterface::initialize()
   */
  public function initialize(array &$array) {
    $this->rows = &$array;
  }

  /**
   * {@inheritDoc}
   */
  public function getStorageKey() {
    return $this->storageKey;
  }

  /**
   * {@inheritDoc}
   */
  public function clear() {
    $this->rows = [];
  }

  public function getRows() {
    return $this->rows;
  }

}

