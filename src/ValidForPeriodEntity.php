<?php

namespace Drupal\beeshop;

trait ValidForPeriodEntity {

  public function isExpired() {

    if (!$this->getBundleEntity()->isValidForPeriod()) {
      return TRUE;
    }

    $time = \Drupal::time()->getRequestTime();

    $start_date = $this->getStartDate()->format('U');
    $end_date = $this->getEndDate();

    $result = TRUE;

    if ( $start_date && $start_date > $time) {
      return FALSE;
    }


    if ($end_date && $end_date->format('U') >= $time) {
      return TRUE;
    }

    return FALSE;
  }

}

