<?php

namespace Drupal\beeshop\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Drupal\beeshop\Session\BeeshopDataLayerSessionBag;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class OrderEventsSubscriber.
 *
 * @package Drupal\bs_order
 */
class EventsSubscriber implements EventSubscriberInterface {

  protected $session;

  /**
   * Constructs a new OrderReceiptSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(SessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
//       KernelEvents::RESPONSE => ['onResponse'],
      KernelEvents::REQUEST => ['onRequest'],
    ];

    return $events;
  }

  public function onRequest(GetResponseEvent $event) {
  }

}

