<?php
namespace Drupal\beeshop;

trait EntityTreeProccessorTrait {

  /**
   * Array of price types ancestors keyed by shop ID and parent price type ID.
   *
   * @var array
   */
  protected $treeChildren = [];

  /**
   * Array of price types parents keyed by shop ID and child price type ID.
   *
   * @var array
   */
  protected $treeParents = [];

  /**
   * Array of loaded trees keyed by a cache id matching tree arguments.
   *
   * @var array
   */
  protected $trees = [];

  /**
   * Array of price types in a tree keyed by shop ID and price type ID.
   *
   * @var array
   */
  protected $treeEntities = [];

  protected $parentPropertyName = 'parent';


  public function getEntitiesTree($entities, $group_id = 0, $parent = '', $max_depth = NULL) {

    if (!isset($this->treeTypes[$group_id])) {
      $this->treeChildren[$group_id] = [];
      $this->treeParents[$group_id] = [];
      $this->treeEntities[$group_id] = [];

      foreach ($entities as $entity) {
        $this->treeChildren[$group_id][$entity->parent][] = $entity->id();
        $this->treeParents[$group_id][$entity->id()][] = $entity->parent;
        $this->treeEntities[$group_id][$entity->id()] = $entity;
      }

      foreach ($this->treeEntities[$group_id] as $id => $entity) {
        if ($entity->parent && isset($this->treeEntities[$group_id][$entity->parent])) {
          $this->treeEntities[$group_id][$entity->parent]->childs[] = &$this->treeEntities[$group_id][$id];
        }
      }
    }

    $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$group_id]) : $max_depth;
    $tree = [];

    // Keeps track of the parents we have to process, the last entry is used
    // for the next processing step.
    $process_parents = [];
    $process_parents[] = $parent;

    // Loops over the parent terms and adds its children to the tree array.
    // Uses a loop instead of a recursion, because it's more efficient.
    while (count($process_parents)) {
      $parent = array_pop($process_parents);
      // The number of parents determines the current depth.
      $depth = count($process_parents);
      if ($max_depth > $depth && !empty($this->treeChildren[$group_id][$parent])) {
        $has_children = FALSE;
        $child = current($this->treeChildren[$group_id][$parent]);
        do {
          if (empty($child)) {
            break;
          }
          $entity = $entities[$child];
          if (isset($this->treeEntities[$group_id][$entity->id()])) {
            $entity = $this->treeEntities[$group_id][$entity->id()];
          }
          if (isset($this->treeParents[$group_id][$entity->id()])) {
            // Clone the term so that the depth attribute remains correct
            // in the event of multiple parents.
            $entity = clone $entity;
          }
          $entity->depth = $depth;
          $tid = $entity->id();
          $entity->parents = $this->treeParents[$group_id][$tid];
          $tree[] = $entity;
          if (!empty($this->treeChildren[$group_id][$tid])) {
            $has_children = TRUE;

            // We have to continue with this parent later.
            $process_parents[] = $parent;
            // Use the current term as parent for the next iteration.
            $process_parents[] = $tid;

            // Reset pointers for child lists because we step in there more
            // often with multi parents.
            reset($this->treeChildren[$group_id][$tid]);
            // Move pointer so that we get the correct term the next time.
            next($this->treeChildren[$group_id][$parent]);
            break;
          }
        } while ($child = next($this->treeChildren[$group_id][$parent]));

        if (!$has_children) {
          // We processed all terms in this hierarchy-level, reset pointer
          // so that this function works the next time it gets called.
          reset($this->treeChildren[$group_id][$parent]);
        }
      }
    }

    return $tree;

  }

}

