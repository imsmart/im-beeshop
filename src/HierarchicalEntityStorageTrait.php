<?php
namespace Drupal\beeshop;

trait HierarchicalEntityStorageTrait {

  use EntityTreeProccessorTrait;

  /**
   * {@inheritdoc}
   */
  public function loadTree($group_id = 0, $parent = '', $max_depth = NULL, $ids = NULL) {

    if (!method_exists($this, 'loadMultiple')) {
      throw new \Exception('No "loadMultiple" method for load entities!');
    }

    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {

      $entities = $this->loadMultiple($ids);

      $tree = $this->getEntitiesTree($entities);

      $this->trees[$cache_key] = $tree;
    }

    return $this->trees[$cache_key];
  }


}

