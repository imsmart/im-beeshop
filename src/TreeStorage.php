<?php

namespace Drupal\beeshop;

use Drupal\Core\Database\Query\SelectInterface;

abstract class TreeStorage {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  protected $treeDataTable;

  protected $treeDataTableIdFieldName;

  protected $treeHierarchDataTable;

  protected $treeHierarchDataTableIdFieldName;

  protected $treeWeightKey = FALSE;

  /**
   * @throws \Exception
   * @return \Drupal\Core\Database\StatementInterface|NULL
   */
  public function loadTreeDataFromTables() {

    if (!$this->treeDataTable) {
      throw new \Exception('Data table nod defined for loading tree data');
    }

    $query = $this->connection->select($this->treeDataTable, 'data')
    ->fields('data');

    if ($this->treeHierarchDataTable) {
      $query->join($this->treeHierarchDataTable, 'h', 'h.' . $this->treeHierarchDataTableIdFieldName . ' = data.' . $this->treeDataTableIdFieldName);
      $query->fields('h', ['parent']);
    }

    if ($this->treeWeightKey) {
      $query->orderBy($this->treeWeightKey);
    }

    $this->loadTreeDataQueryAlter($query);

    return $this->safeExecuteSelect($query);

  }

  /**
   * Executes a select query while making sure the database table exists.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The select object to be executed.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   A prepared statement, or NULL if the query is not valid.
   *
   * @throws \Exception
   *   Thrown if the table could not be created or the database connection
   *   failed.
   */
  protected function safeExecuteSelect(SelectInterface $query) {
    try {
      return $query->execute();
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the table.
      if ($this->ensureTableExists()) {
        return $query->execute();
      }
      // Some other failure that we can not recover from.
      throw $e;
    }
  }

  /**
   * Performs specific loading tree data query alters.
   *
   * Override this method to add custom functionality
   *
   * @param Drupal\Core\Database\Query\SelectInterface $query
   */
  abstract protected function loadTreeDataQueryAlter(SelectInterface $query);
}

