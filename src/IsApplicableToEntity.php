<?php
namespace Drupal\beeshop;

use Drupal\Core\Entity\EntityInterface;

interface IsApplicableToEntity {

  /**
   * Returns if the object can be used for entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that should be checked.
   *
   * @return bool
   *   TRUE if the object can be used, FALSE otherwise.
   */
  public static function isApplicable(EntityInterface $entity);

}

